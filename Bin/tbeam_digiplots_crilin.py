# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i plot di analisi di tutti i canali del digitizer, ovvero:
#   - Spettri di PH (1D)
#   - Spettri di tempo di picco (1D)
#   - Spettri di PH vs tempo di picco (2D)
#   - Spettri di PH vs numero di evento (2D)
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_digiplots.py nrun PHstep ifPHlog timestep iftimelog ieventstep ifprofile
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [PHstep]        int, numero di bin per gli spettri in PH (default: 100)
# 3 [ifPHlog]       str, se fare gli spettri di PH in scala log ("True", "False") 
#                     (default: "False")
# 4 [timestep]      int, numero di bin per gli spettri in tempo di picco (default: 50)
# 5 [iftimelog]     str, se fare gli spettri di tempo di picco in scala log ("True", "False")
#                     (default: "False")
# 6 [ieventstep]    int, binning step per gli spettri in numero di eventi (default: 250)
# 7 [ifprofile]     str, se fare il profile plot in PH vs numero di evento ("True", "False") 
#                     (default: "False")
#
# Esempi:
# $     python3 tbeam_digiplots.py 530090 20 True 10 False 100 True
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'], data_namesdict['digiTime'], data_namesdict['ievent'] ]
digiPH, digitime, eventNumber = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                                  COSE COMUNI
# ==========================================================================================
# Prima figura: quella con gli scintillatori
nplot_perside_scinti = 2
inds_scinti = [0,1,2,3]
names_scinti = ['APC','Lead Glass','Cindy 1','Cindy 2']

# Seconda figura: quella con gli scintillatori
nplot_perside_crilin = 3
inds_crilin = [ [17,19,21,11,13,15,5,7,9],       # Plane 1, top
                [16,18,20,10,12,14,4,6,8],       # Plane 1, bottom
                [45,31,49,25,27,29,33,23,37],    # Plane 2, top
                [44,30,48,24,26,28,32,22,36] ]     # Plane 2, bottom
fignames_crilin = ['CRILIN Plane 1, top','CRILIN Plane 1, bottom','CRILIN Plane 2, top','CRILIN Plane 2, bottom']
names_crilin = ['TL','TC','TR','CL','CC','CR','BL','BC','BR']
numfigures_crilin = len(inds_crilin)

# Una variabile per i limiti di tempo
maxtime = 1024

# ==========================================================================================
#                                  PLOT DI CONTROLLO: PH
# ==========================================================================================
# Definisco alcune variabili preliminari: numero di figure da creare, binning step per gli spettri, 
# se usare una scala log...
try:
    nbins_PH = int(sys.argv[2])            # Binning per gli spettri di PH
except:
    nbins_PH = 100                         # Default value

try:
    iflogPH = (sys.argv[3] == 'True')      # Se usare la scala log
except:
    iflogPH = False

# ======================================================================
# Genero la prima figura: 2x2, LG + Cindy + APC + Cindy
fig = plt.figure(figsize = dimfigbig)

for j in range(nplot_perside_scinti**2):
    # Creo l'istogramma di frequenze e lo rappresento
    histo_PH, bins_PH = truehisto1D(digiPH[:,inds_scinti[j]],nbins_PH,ifstep=False,edgeL=0)
    
    ax = fig.add_subplot(nplot_perside_scinti,nplot_perside_scinti,j+1)
    histoplotter1D(ax,bins_PH,histo_PH,'PH [a.u.]','Counts',f"Ch. {inds_scinti[j]}, {names_scinti[j]}",
                    'best',textfont*1.25)
    ax.legend(loc='upper right',framealpha=1,fontsize=textfont*1.25)
    #ax.set_xlim([xlims_low[plotind],xlims_up[plotind]])
    if iflogPH:
        ax.set_yscale('log')

# Rifinisco la grafica
fig.tight_layout(rect=[0, 0.03, 1, 0.925])
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f"PH scintillators" + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

# Salvataggio in due location
savefigname = '_digi_histo1D_PH_scinti'
#fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

# ======================================================================
# Genero le altre figure
for i in range(numfigures_crilin):
    # Creo la figura i-esima
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplot_perside_crilin**2):
        # Creo l'istogramma di frequenze e lo rappresento
        histo_PH, bins_PH = truehisto1D(digiPH[:,inds_crilin[i][j]],nbins_PH,ifstep=False,edgeL=0)
        
        ax = fig.add_subplot(nplot_perside_crilin,nplot_perside_crilin,j+1)
        histoplotter1D(ax,bins_PH,histo_PH,'PH [a.u.]','Counts',
                        f"Ch. {inds_crilin[i][j]}, {names_crilin[j]}",
                        'best',textfont*1.25)
        ax.legend(loc='upper right',framealpha=1,fontsize=textfont*1.25)
        #ax.set_xlim([xlims_low[plotind],xlims_up[plotind]])
        if iflogPH:
            ax.set_yscale('log')
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"PH " + f"{fignames_crilin[i]}" + f' ({datetime.date.today()}, {time_readable})',
                         fontsize = 2.5*textfont)

    # Salvataggio in due location
    savefigname = '_digi_histo1D_PH_crilin' + str(i+1)
    #fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
print(f"")
print(f"PH spectra have been created and saved.")
    
# ==========================================================================================
#                                  PLOT DI CONTROLLO: TEMPO
# ==========================================================================================
# Definisco alcune variabili preliminari: numero di figure da creare, binning step per gli spettri, 
# se usare una scala log...
try:
    nbins_time = int(sys.argv[4])           # Binning per gli spettri di tempo di picco
except:
    nbins_time = 50                         # Default value

try:
    iflogtime = (sys.argv[5] == 'True')     # Se usare la scala log
except:
    iflogtime = False    

# ======================================================================
# Genero la prima figura: 2x2, LG + Cindy + APC + Cindy
fig = plt.figure(figsize = dimfigbig)

for j in range(nplot_perside_scinti**2):
    # Creo l'istogramma di frequenze e lo rappresento
    histo_time, bins_time = truehisto1D(digitime[:,inds_scinti[j]],nbins_time,ifstep=False,edgeL=0,edgeU=maxtime)
    
    ax = fig.add_subplot(nplot_perside_scinti,nplot_perside_scinti,j+1)
    histoplotter1D(ax,bins_time,histo_time,'Peak Time [a.u.]','Counts',f"Ch. {inds_scinti[j]}, {names_scinti[j]}",
                    'best',textfont*1.25)
    ax.legend(loc='upper right',framealpha=1,fontsize=textfont*1.25)
    if iflogtime:
        ax.set_yscale('log')

# Rifinisco la grafica
fig.tight_layout(rect=[0, 0.03, 1, 0.925])
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f"Peak Time scintillators" + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

# Salvataggio in due location
savefigname = '_digi_histo1D_time_scinti'
#fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

# ======================================================================
# Genero le altre figure
for i in range(numfigures_crilin):
    # Creo la figura i-esima
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplot_perside_crilin**2):
        # Creo l'istogramma di frequenze e lo rappresento
        histo_time, bins_time = truehisto1D(digitime[:,inds_crilin[i][j]],nbins_time,ifstep=False,edgeL=0,edgeU=maxtime)
        
        ax = fig.add_subplot(nplot_perside_crilin,nplot_perside_crilin,j+1)
        histoplotter1D(ax,bins_time,histo_time,'Peak Time [a.u.]','Counts',
                        f"Ch. {inds_crilin[i][j]}, {names_crilin[j]}",
                        'best',textfont*1.25)
        ax.legend(loc='upper right',framealpha=1,fontsize=textfont*1.25)
        #ax.set_xlim([xlims_low[plotind],xlims_up[plotind]])
        if iflogPH:
            ax.set_yscale('log')
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"Peak Time " + f"{fignames_crilin[i]}" + f' ({datetime.date.today()}, {time_readable})',
                         fontsize = 2.5*textfont)

    # Salvataggio in due location
    savefigname = '_digi_histo1D_time_crilin' + str(i+1)
    #fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
print(f"")
print(f"PT spectra have been created and saved.")

# ==========================================================================================
#                            PLOT DI CONTROLLO: PH/TEMPO DI PICCO
# ==========================================================================================  
# Definisco alcune variabili preliminari: numero di figure da creare, binning step per gli spettri, 
# se usare una scala log...
colmap = 'jet'   # Parametri per gli istogrammi 2D
slope = 1

# ======================================================================
# Genero la prima figura: 2x2, LG + Cindy + APC + Cindy
fig = plt.figure(figsize = dimfigbig)

for j in range(nplot_perside_scinti**2):
    # Creo l'istogramma di frequenze e lo rappresento
    histo2D_PHtime, binsX_PH, binsY_time = truehisto2D(digiPH[:,inds_scinti[j]],digitime[:,inds_scinti[j]],
                                            nbins_PH,nbins_time,
                                            ifstepX = False, ifstepY = False,edgeLX=0,edgeLY=0,edgeUY=maxtime)
    
    ax = fig.add_subplot(nplot_perside_scinti,nplot_perside_scinti,j+1)
    histoplotter2D(ax,binsX_PH,binsY_time,histo2D_PHtime,colmap,slope,'Pulse Height [a.u.]','Peak Time [ticks]',
                    1.25*textfont,iflog = True,ifcbarfont=True)
    ax.set_title(label = f"Ch. {inds_scinti[j]}, {names_scinti[j]}", fontsize = 2*textfont)


# Rifinisco la grafica
fig.tight_layout(rect=[0, 0.03, 1, 0.925])
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f"PH/PT scintillators" + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

# Salvataggio in due location
savefigname = '_digi_histo2D_PHtime_scinti'
#fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

# ======================================================================
# Genero le altre figure
for i in range(numfigures_crilin):
    # Creo la figura i-esima
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplot_perside_crilin**2):
        # Creo l'istogramma di frequenze e lo rappresento
        histo2D_PHtime, binsX_PH, binsY_time = truehisto2D(digiPH[:,inds_crilin[i][j]],digitime[:,inds_crilin[i][j]],
                                            nbins_PH,nbins_time,
                                            ifstepX = False, ifstepY = False,edgeLX=0,edgeLY=0,edgeUY=maxtime)
    
        ax = fig.add_subplot(nplot_perside_crilin,nplot_perside_crilin,j+1)
        histoplotter2D(ax,binsX_PH,binsY_time,histo2D_PHtime,colmap,slope,'Pulse Height [a.u.]','Peak Time [ticks]',
                        1.25*textfont,iflog = True,ifcbarfont=True)
        ax.set_title(label = f"Ch. {inds_crilin[i][j]}, {names_crilin[j]}", fontsize = 2*textfont)
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"PH/PT " + f"{fignames_crilin[i]}" + f' ({datetime.date.today()}, {time_readable})',
                         fontsize = 2.5*textfont)

    # Salvataggio in due location
    savefigname = '_digi_histo2D_PHtime_crilin' + str(i+1)
    #fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
print(f"")
print(f"PH vs Peak Time 2D-spectra have been created and saved.")
    
# ==========================================================================================
#                                PLOT DI CONTROLLO: PH/EVENTO
# ==========================================================================================  
# Definisco il binning per l'asse del numero di evento e i limiti dei plot
try:
    nbins_event = int(sys.argv[6])
except:
    nbins_event = 250

try:
    if sys.argv[7] == 'True':
        ifproj = True
        pr = 'mean'
    else:
        ifproj = False
except:
    ifproj = False

# ======================================================================
# Genero la prima figura: 2x2, LG + Cindy + APC + Cindy
fig = plt.figure(figsize = dimfigbig)

for j in range(nplot_perside_scinti**2):
    # Creo l'istogramma di frequenze e lo rappresento
    histo2D_PHnevent, binsX_nevent, binsY_PH = truehisto2D(eventNumber,digiPH[:,inds_scinti[j]],nbins_event,2*nbins_PH,
                                                           ifstepX = False, ifstepY = False,edgeLX=0,edgeLY=0)
    
    ax = fig.add_subplot(nplot_perside_scinti,nplot_perside_scinti,j+1)
    histoplotter2D(ax,binsX_PH,binsY_time,histo2D_PHnevent,colmap,slope,'Event number','Pulse Height',
                    1.25*textfont,iflog = True,ifcbarfont=True)
    ax.set_title(label = f"Ch. {inds_scinti[j]}, {names_scinti[j]}", fontsize = 2*textfont)

# Rifinisco la grafica
fig.tight_layout(rect=[0, 0.03, 1, 0.925])
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f"PH/ievent scintillators" + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

# Salvataggio in due location
savefigname = '_digi_histo2D_PHevent_scinti'
#fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

# ======================================================================
# Genero le altre figure
for i in range(numfigures_crilin):
    # Creo la figura i-esima
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplot_perside_crilin**2):
        # Creo l'istogramma di frequenze e lo rappresento
        histo2D_PHnevent, binsX_nevent, binsY_PH = truehisto2D(eventNumber,digiPH[:,inds_crilin[i][j]],
                                            nbins_PH,nbins_time,
                                            ifstepX = False, ifstepY = False,edgeLX=0,edgeLY=0)
    
        ax = fig.add_subplot(nplot_perside_crilin,nplot_perside_crilin,j+1)
        histoplotter2D(ax,binsX_nevent,binsY_PH,histo2D_PHnevent,colmap,slope,'Event number','Pulse Height',
                        1.25*textfont,iflog = True,ifcbarfont=True)
        ax.set_title(label = f"Ch. {inds_crilin[i][j]}, {names_crilin[j]}", fontsize = 2*textfont)
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"PH/ievent " + f"{fignames_crilin[i]}" + f' ({datetime.date.today()}, {time_readable})',
                         fontsize = 2.5*textfont)

    # Salvataggio in due location
    savefigname = '_digi_histo2D_PHevent_crilin' + str(i+1)
    #fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
print(f"")
print(f"PH vs Event 2D-spectra have been created and saved.\n")