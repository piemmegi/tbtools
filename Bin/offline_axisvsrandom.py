# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è confrontare più spettri di PH.
# Argomnenti: numero di canale da mostrare, run1, run2
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 offline_histocomparison.py 5 091 092
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                             DEFINIZIONE DEI TAGLI
# ==========================================================================================
# Definisco soglie e indici per taglio in PH dei Cherenkov
indPHcut_1 = 2
indPHcut_2 = 3
thr_PHcut = 100

# Definisco soglie e indici per taglio in differenza di tempo rispetto al Cherenkov
indTimeDiffcut = 2
thr_timediffcut = [-100,100]

# Definisco soglie e valori per taglio in divergenza
divlims_x = [-3,3]
divlims_y = [-3,3]

# Definisco soglie e valori per taglio in posizioni
poslims = [[[1,4], [2,5]],    # x,y ch 4
           [[3,6], [2,5]],    # x,y ch 5
           [[0,7], [4,8]],    # x,y ch 6
           [[3,6], [4,8]],    # x,y ch 7
           [[2,6], [2,6]]]    # x,y ch 0 LG

# ==========================================================================================
#                             DEFINIZIONE DEI COEFFICIENTI DI EQUALIZZAZIONE
# ==========================================================================================
# LG
LG_m = 1293.163
LG_q = -73.76

# OREO
#eq_m = [2.811, 2.483, 4.008, 3.714]
eq_m = [2.69460784, 2.4877451 , 3.70392157, 3.67205882]
#eq_q = [16.378, 18.264, 9.932, 16.034]
eq_q = [23.1775, 17.5125, 22.92, 19.9825]

digicolors = ['mediumblue','red','cyan','darkorange']
diginames = ['George','John','Paul','Ringo']

# ==========================================================================================
#                             UPLOAD DEI DATI RANDOM
# ==========================================================================================
print(f"Loading random data...")

# Inizializzo le variabili da riempire
digiPH_random = np.zeros((0,16))
ch_pos_random = np.zeros((0,4))
eventNumber_random = np.zeros((0,))
goniosteps_random = np.zeros((0,))
xinfo_random = np.zeros((0,5))
digitime_random = np.zeros((0,16))
ch_numhit_random = np.zeros((0,4))

# Definisco le run da guardare
all_runs = ['720120','720122','720124','720125']

# Ciclo sulle run e le apro
veclist = [ data_namesdict['digiPH'],data_namesdict['xpos'],data_namesdict['ievent'],
            data_namesdict['info_plus'], data_namesdict['xinfo'], data_namesdict['digiTime'],
            data_namesdict['nclu'] ]

for run in all_runs:
    # Apro il file
    run_name = pathnamedef(prename,run,datafiletype)

    # Estraggo i vettori necessari per l'analisi dal file dati 
    digiPH,ch_pos,eventNumber,info_plus,xinfo,digitime,ch_numhit = dataupload(data,run_name,veclist,
                                                                    datafiletype,globaltree,ifmatrix)
    goniosteps = info_plus[:,1]

    # Stacking
    digiPH_random = np.vstack((digiPH_random,digiPH))
    ch_pos_random = np.vstack((ch_pos_random,ch_pos))
    eventNumber_random = np.hstack((eventNumber_random,eventNumber))
    goniosteps_random = np.hstack((goniosteps_random,goniosteps))
    xinfo_random = np.vstack((xinfo_random,xinfo))
    digitime_random = np.vstack((digitime_random,digitime))
    ch_numhit_random = np.vstack((ch_numhit_random,ch_numhit))

# Calcolo punti di hit sul piano del cristallo e angoli di incidenza
all_thetas_random = np.zeros((np.shape(digiPH_random)[0],2))
all_pos_random = np.zeros_like(all_thetas_random)
scalefac = 1e3        # Angoli in mrad

for j in range(2):
    # Seleziono gli eventi di interesse. 
    refind = j % 2                        # Vale 0 quando lavoro su x e 1 su y
    cleanpos_a = ch_pos_random[:,refind]
    cleanpos_b = ch_pos_random[:,refind+2]

    # Calcolo gli angoli di propagazione e li appendo alla lista
    all_thetas_random[:,j] = scalefac * (np.arctan((cleanpos_b-cleanpos_a)/distances['zs1s2']))
    
    # Ricostruisco m e q delle traccie lineari delle traiettorie
    m = (cleanpos_b - cleanpos_a)/distances['zs1s2']
    q = cleanpos_b - m*distances['zs1s2']
    
    # Calcolo il punto di impatto sul piano dell'oggetto e lo appendo alla lista
    all_pos_random[:,j] = m*(distances['zs1s2'] + distances['zcry']) + q

# ==========================================================================================
#                           CONDIZIONI DI TAGLIO COMUNI RANDOM
# ==========================================================================================
# Condizione di taglio in cluster
condmap = (ch_numhit_random[:,:nsili_cut] == 1)
condcomp = condmap.prod(axis = 1)
clustercond_random = (condcomp == 1) & (eventNumber_random >= 0) & (np.isnan(eventNumber_random) == False)

# Condizione di taglio in PH del Cherenkov
PHcutcond_random = (digiPH_random[:,indPHcut_1] >= thr_PHcut) & (digiPH_random[:,indPHcut_2] >= thr_PHcut)

# Condizione di taglio in divergenza
divcutcond_x_random = (all_thetas_random[:,0] >= divlims_x[0]) & (all_thetas_random[:,0] <= divlims_x[1])
divcutcond_y_random = (all_thetas_random[:,1] >= divlims_y[0]) & (all_thetas_random[:,1] <= divlims_y[1])

# Condizione comune
commoncond_random = clustercond_random & PHcutcond_random & divcutcond_x_random & divcutcond_y_random

# ==========================================================================================
#                           ITERAZIONE RANDOM
# ==========================================================================================
# Itero su ogni canale
digichs = [4,5,6,7,0]
allcuttedPHs_random = []

for i,el in enumerate(digichs):
    # Calcolo le differenze di tempo rispetto al Cherenkov e il corrispondente taglio
    timediff = digitime_random[:,el] - digitime_random[:,indTimeDiffcut]
    difftimecond_random = (timediff >= thr_timediffcut[0]) & (timediff <= thr_timediffcut[1])

    # Calcolo la condizione di taglio in posizione
    poscutcond_x_random = (all_pos_random[:,0] >= poslims[i][0][0]) & (all_pos_random[:,0] <= poslims[i][0][1])
    poscutcond_y_random = (all_pos_random[:,1] >= poslims[i][1][0]) & (all_pos_random[:,1] <= poslims[i][1][1])

    # Creo la condizione di taglio globale
    globalcond_random = commoncond_random & difftimecond_random & poscutcond_x_random & poscutcond_y_random

    # Taglio le PH
    allcuttedPHs_random.append(digiPH_random[globalcond_random,el])

# Equalizzo e calibro
allcuttedPHs_random[-1] = (allcuttedPHs_random[-1] - LG_q) / LG_m

for i in range(len(eq_m)):
    allcuttedPHs_random[i] = (allcuttedPHs_random[i] - eq_q[i]) / (1e3 * eq_m[i]) # in GeV

print(f"Done!\n")




























# ==========================================================================================
#                             UPLOAD DEI DATI AXIAL
# ==========================================================================================
# Definisco la run da guardare
print(f"Loading axial data...")
veclist = [ data_namesdict['digiPH'],data_namesdict['xpos'],data_namesdict['ievent'],
            data_namesdict['info_plus'], data_namesdict['xinfo'], data_namesdict['digiTime'],
            data_namesdict['nclu'] ]

run = '720127'
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati 
digiPH_axial,ch_pos_axial,eventNumber_axial,info_plus_axial,xinfo_axial,digitime_axial,ch_numhit_axial = dataupload(data,
                                            run_name,veclist,datafiletype,globaltree,ifmatrix)
goniosteps_axial = info_plus_axial[:,1]

# Calcolo punti di hit sul piano del cristallo e angoli di incidenza
all_thetas_axial = np.zeros((np.shape(digiPH_axial)[0],2))
all_pos_axial = np.zeros_like(all_thetas_axial)

for j in range(2):
    # Seleziono gli eventi di interesse. 
    refind = j % 2                        # Vale 0 quando lavoro su x e 1 su y
    cleanpos_a = ch_pos_axial[:,refind]
    cleanpos_b = ch_pos_axial[:,refind+2]

    # Calcolo gli angoli di propagazione e li appendo alla lista
    all_thetas_axial[:,j] = scalefac * (np.arctan((cleanpos_b-cleanpos_a)/distances['zs1s2']))
    
    # Ricostruisco m e q delle traccie lineari delle traiettorie
    m = (cleanpos_b - cleanpos_a)/distances['zs1s2']
    q = cleanpos_b - m*distances['zs1s2']
    
    # Calcolo il punto di impatto sul piano dell'oggetto e lo appendo alla lista
    all_pos_axial[:,j] = m*(distances['zs1s2'] + distances['zcry']) + q

# ==========================================================================================
#                           CONDIZIONI DI TAGLIO COMUNI AXIAL
# ==========================================================================================
# Condizione di taglio in cluster
condmap = (ch_numhit_axial[:,:nsili_cut] == 1)
condcomp = condmap.prod(axis = 1)
clustercond_axial = (condcomp == 1) & (eventNumber_axial >= 0) & (np.isnan(eventNumber_axial) == False)

# Condizione di taglio in PH del Cherenkov
PHcutcond_axial = (digiPH_axial[:,indPHcut_1] >= thr_PHcut) & (digiPH_axial[:,indPHcut_2] >= thr_PHcut)

# Condizione di taglio in divergenza
divcutcond_x_axial = (all_thetas_axial[:,0] >= divlims_x[0]) & (all_thetas_axial[:,0] <= divlims_x[1])
divcutcond_y_axial = (all_thetas_axial[:,1] >= divlims_y[0]) & (all_thetas_axial[:,1] <= divlims_y[1])

# Condizione comune
commoncond_axial = clustercond_axial & PHcutcond_axial & divcutcond_x_axial & divcutcond_y_axial

# ==========================================================================================
#                           ITERAZIONE AXIAL
# ==========================================================================================
# Itero su ogni canale
digichs = [4,5,6,7,0]
allcuttedPHs_axial = []

for i,el in enumerate(digichs):
    # Calcolo le differenze di tempo rispetto al Cherenkov e il corrispondente taglio
    timediff = digitime_axial[:,el] - digitime_axial[:,indTimeDiffcut]
    difftimecond_axial = (timediff >= thr_timediffcut[0]) & (timediff <= thr_timediffcut[1])

    # Calcolo la condizione di taglio in posizione
    poscutcond_x_axial = (all_pos_axial[:,0] >= poslims[i][0][0]) & (all_pos_axial[:,0] <= poslims[i][0][1])
    poscutcond_y_axial = (all_pos_axial[:,1] >= poslims[i][1][0]) & (all_pos_axial[:,1] <= poslims[i][1][1])

    # Creo la condizione di taglio globale
    globalcond_axial = commoncond_axial & difftimecond_axial & poscutcond_x_axial & poscutcond_y_axial

    # Taglio le PH
    allcuttedPHs_axial.append(digiPH_axial[globalcond_axial,el])

# Equalizzo e calibro
allcuttedPHs_axial[-1] = (allcuttedPHs_axial[-1] - LG_q) / LG_m

for i in range(len(eq_m)):
    allcuttedPHs_axial[i] = (allcuttedPHs_axial[i] - eq_q[i]) / (1e3 * eq_m[i]) # in GeV

print(f"Done!\n")


























# ==========================================================================================
#                           LG CALORIMETER COMPARISON
# ==========================================================================================
# Creo lo spettro del calorimetro
nbins_EDep = 0.025 # GeV
histo_Calo_random, bins_Calo_random = truehisto1D(allcuttedPHs_random[-1],nbins_EDep,ifstep=True)
histo_Calo_axial, bins_Calo_axial = truehisto1D(allcuttedPHs_axial[-1],nbins_EDep,ifstep=True)

# Normalizzo
histo_Calo_random = histo_Calo_random / np.sum(histo_Calo_random)
histo_Calo_axial = histo_Calo_axial / np.sum(histo_Calo_axial)

# Calcolo i valori medi degli spettro di PH
mean_Calo_random, err_Calo_random = wmean(bins_Calo_random,histo_Calo_random)
mean_Calo_axial, err_Calo_axial = wmean(bins_Calo_axial,histo_Calo_axial)

# Printout 
print(f"Average EDep, LG:")
print(f"\t Random: {mean_Calo_random:.3f} +/- {err_Calo_random:.3f} GeV")
print(f"\t Axial: {mean_Calo_axial:.3f} +/- {err_Calo_axial:.3f} GeV")
print(f"")

# Mostro gli spettri
fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,bins_Calo_random,histo_Calo_random,'LG energy [GeV]','Normalized counts','Random',
                    'best',textfont,'mediumblue')
histoplotter1D(ax,bins_Calo_axial,histo_Calo_axial,'LG energy [GeV]','Normalized counts','Axial',
                    'best',textfont,'red')
ax.set_yscale('log')

# Rifinisco la grafica
fig.set_tight_layout('tight')
fig.suptitle(f'LG spectra with cuts', fontsize = 2*textfont)
fig.savefig(imgloc + f'axisvsrandom_calo' + filetype,dpi = filedpi)
plt.show()
plt.close(fig)
print(f"LG plot saved!")

# ==========================================================================================
#                           OREO COMPARISON
# ==========================================================================================
# Binning e altre variabili utili
nbins_EDep = 0.015 # GeV
xlims = [0,3]

# Itero sui cristalli
fig = plt.figure(figsize = dimfigbig)
fig_random, ax_random = plt.subplots(figsize = dimfig)
fig_axial, ax_axial = plt.subplots(figsize = dimfig)

for i in range(len(eq_m)):
    # Creo lo spettro del canale
    histo_Crys_random, bins_Crys_random = truehisto1D(allcuttedPHs_random[i],nbins_EDep,ifstep=True)
    histo_Crys_axial, bins_Crys_axial = truehisto1D(allcuttedPHs_axial[i],nbins_EDep,ifstep=True)

    # Normalizzo
    histo_Crys_random = histo_Crys_random / np.sum(histo_Crys_random)
    histo_Crys_axial = histo_Crys_axial / np.sum(histo_Crys_axial)

    # Calcolo i valori medi degli spettro di PH
    mean_Crys_random, err_Crys_random = wmean(bins_Crys_random,histo_Crys_random)
    mean_Crys_axial, err_Crys_axial = wmean(bins_Crys_axial,histo_Crys_axial)

    # Mostro gli spettri
    ax = fig.add_subplot(2,2,i+1)
    histoplotter1D(ax,bins_Crys_random,histo_Crys_random,'Deposited energy [GeV]','Normalized counts',
                        f'Random, channel {i} ({diginames[i]})','best',textfont,'mediumblue')
    histoplotter1D(ax,bins_Crys_axial,histo_Crys_axial,'Deposited energy [GeV]','Normalized counts',
                        f'Axial, channel {i} ({diginames[i]})','best',textfont,'red')
    ax.set_xlim(xlims)
    ax.set_yscale('log')

    # Aggiungo gli spettri al plot comune
    histoplotter1D(ax_random,bins_Crys_random,histo_Crys_random,'Deposited energy [GeV]','Normalized counts',
                        f'Channel {i} ({diginames[i]})','best',textfont,digicolors[i])
    histoplotter1D(ax_axial,bins_Crys_axial,histo_Crys_axial,'Deposited energy [GeV]','Normalized counts',
                        f'Channel {i} ({diginames[i]})','best',textfont,digicolors[i])

    # Printout 
    print(f"Average EDep, OREO channel {i} ({diginames[i]}):")
    print(f"\t Random: {mean_Crys_random:.3f} +/- {err_Crys_random:.3f} GeV")
    print(f"\t Axial: {mean_Crys_axial:.3f} +/- {err_Crys_axial:.3f} GeV")
    print(f"\t e.f.: {mean_Crys_axial/mean_Crys_random:.3f}\n")

# Rifinisco la grafica dei plot separati
fig.set_tight_layout('tight')
fig.suptitle(f'OREO spectra with cuts', fontsize = 2*textfont)
fig.savefig(imgloc + f'axisvsrandom_crys' + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

# Rifinisco la grafica dei plot comuni
ax_random.set_xlim(xlims)
ax_random.set_yscale('log')
fig_random.set_tight_layout('tight')
fig_random.suptitle(f'OREO Random spectra with cuts', fontsize = 2*textfont)
fig_random.savefig(imgloc + f'axisvsrandom_allrandom_crys' + filetype,dpi = filedpi)
plt.show()
plt.close(fig_random)

ax_axial.set_xlim(xlims)
ax_axial.set_yscale('log')
fig_axial.set_tight_layout('tight')
fig_axial.suptitle(f'OREO Axial spectra with cuts', fontsize = 2*textfont)
fig_axial.savefig(imgloc + f'axisvsrandom_allaxial_crys' + filetype,dpi = filedpi)
plt.show()
plt.close(fig_axial)

# Ultimi printout
print(f"OREO plot saved!")
print(f"Job done!\n")