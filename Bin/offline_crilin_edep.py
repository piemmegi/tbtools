# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre alcuni plot di interesse per l'analisi dei dati 
# prodotti dal CRILIN.

# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 offline_crilin_edep.py nrun PHstep ifPHlog
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [ifstep]        str, se usare il parametro di binning come binstep ("True") o come numero
#                     di bin da inserire ("False") (default: "False")
# 3 [PHstep]        int, numero di bin per gli spettri in PH (default: 100)
# 4 [ifPHlog]       str, se fare gli spettri di PH in scala log ("True", "False") 
#                     (default: "False")
#
# Esempi:
# $     python3 tbeam_digiplots.py 530090 False 20 True
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'] ]
digiPH = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                              COSTRUZIONE DELLE VARIABILI
# ==========================================================================================
# Calcolo le energie depositate nel layer 0 (dx e sx) (somme in serie)
# NOTA: LEFT == TOP, RIGHT == BOTTOM
inds_L0_series_left = [17,19,21,11,13,15,5,7,9]
Edep_L0_series_left = digiPH[:,inds_L0_series_left].sum(axis=1)

inds_L0_series_right = [16,18,20,10,12,14,4,6,8]
Edep_L0_series_right = digiPH[:,inds_L0_series_right].sum(axis=1)

# Calcolo le energie depositate nel layer 1 (dx e sx) (somme in parallelo)
# NOTA: LEFT == TOP, RIGHT == BOTTOM
inds_L1_parallel_left = [45,31,49,25,27,29,33,23,37]
Edep_L1_parallel_left = digiPH[:,inds_L1_parallel_left].sum(axis=1)

inds_L1_parallel_right = [44,30,48,24,26,28,32,22,36]
Edep_L1_parallel_right = digiPH[:,inds_L1_parallel_right].sum(axis=1)

# Calcolo le energie medie per layer
Emean_L0 = (Edep_L0_series_left + Edep_L0_series_right) / 2
Emean_L1 = (Edep_L1_parallel_left + Edep_L1_parallel_right) / 2

# ==========================================================================================
#                              PRODUZIONE DEGLI ISTOGRAMMI
# ==========================================================================================
# Definisco i parametri utili per costruire gli istogrammi
try:
    nbins_PH = int(sys.argv[2])            # Binning per gli spettri di PH
except:
    nbins_PH = 100                         # Default value

try:
    ifstep = (sys.argv[3] == 'True')       # Se nbins_PH è uno step (True) o no
except:
    ifstep = False

try:
    iflogPH = (sys.argv[4] == 'True')      # Se usare la scala log
except:
    iflogPH = False

# Costruisco gli istogrammi richiesti
histo_L0_series_left, bins_L0_series_left = truehisto1D(Edep_L0_series_left,nbins_PH,ifstep=ifstep,edgeL=0)
histo_L0_series_right, bins_L0_series_right = truehisto1D(Edep_L0_series_right,nbins_PH,ifstep=ifstep,edgeL=0)

histo_L1_parallel_left, bins_L1_parallel_left = truehisto1D(Edep_L1_parallel_left,nbins_PH,ifstep=ifstep,edgeL=0)
histo_L1_parallel_right, bins_L1_parallel_right = truehisto1D(Edep_L1_parallel_right,nbins_PH,ifstep=ifstep,edgeL=0)

histo_L0_mean, bins_L0_mean = truehisto1D(Emean_L0,nbins_PH,ifstep=ifstep,edgeL=0)
histo_L1_mean, bins_L1_mean = truehisto1D(Emean_L1,nbins_PH,ifstep=ifstep,edgeL=0)

# ==========================================================================================
#                              PLOT DEGLI ISTOGRAMMI
# ==========================================================================================
# Mostro gli istogrammi del layer 0
fig = plt.figure(figsize = dimfig)
ax1 = fig.add_subplot(1,2,1)
histoplotter1D(ax1,bins_L0_series_left,histo_L0_series_left,'Total PH [a.u.]','Counts',
               r"E$_{dep}$, layer 1, top",'upper right',textfont*1.25)
if iflogPH:
    ax1.set_yscale('log')

ax2 = fig.add_subplot(1,2,2)
histoplotter1D(ax2,bins_L0_series_right,histo_L0_series_right,'Total PH [a.u.]','Counts',
               r"E$_{dep}$, layer 1, bottom",'upper right',textfont*1.25)
if iflogPH:
    ax2.set_yscale('log')

# Rifinisco la grafica e salvo in due punti
fig.set_tight_layout('tight')
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(r"E$_{dep}$" + f" layer 1 ({datetime.date.today()}, {time_readable})", fontsize = 2.5*textfont)

savefigname = '_Edep_L0_series'
fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi_www)
plt.show()
plt.close(fig)

# ==========================================================================================
# Mostro gli istogrammi del layer 1
fig = plt.figure(figsize = dimfig)
ax1 = fig.add_subplot(1,2,1)
histoplotter1D(ax1,bins_L1_parallel_left,histo_L1_parallel_left,'Total PH [a.u.]','Counts',
               r"E$_{dep}$, layer 2 top",'upper right',textfont*1.25)
if iflogPH:
    ax1.set_yscale('log')

ax2 = fig.add_subplot(1,2,2)
histoplotter1D(ax2,bins_L1_parallel_right,histo_L1_parallel_right,'Total PH [a.u.]','Counts',
               r"E$_{dep}$, layer 2 bottom",'upper right',textfont*1.25)
if iflogPH:
    ax2.set_yscale('log')

# Rifinisco la grafica e salvo in due punti
fig.set_tight_layout('tight')
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(r"E$_{dep}$ " + f" layer 2 ({datetime.date.today()}, {time_readable})", fontsize = 2.5*textfont)

savefigname = '_Edep_L1_parallel'
fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi_www)
plt.show()
plt.close(fig)

# ==========================================================================================
# Mostro gli istogrammi medi
fig = plt.figure(figsize = dimfig)
ax1 = fig.add_subplot(1,2,1)
histoplotter1D(ax1,bins_L0_mean,histo_L0_mean,'Total PH [a.u.]','Counts',
               r"Layer 1 mean",'upper right',textfont*1.25)
if iflogPH:
    ax1.set_yscale('log')

ax2 = fig.add_subplot(1,2,2)
histoplotter1D(ax2,bins_L1_mean,histo_L1_mean,'Total PH [a.u.]','Counts',
               r"Layer 2, mean",'upper right',textfont*1.25)
if iflogPH:
    ax2.set_yscale('log')

# Rifinisco la grafica e salvo in due punti
fig.set_tight_layout('tight')
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(r"E$_{dep}$" + f", average ({datetime.date.today()}, {time_readable})", fontsize = 2.5*textfont)

savefigname = '_Edep_average'
fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi_www)
plt.show()
plt.close(fig)

print(f"")
print("All histograms have been plotted and saved.\n")