# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è verificare che i numeri di evento salvati su ogni digitizer
# siano corretti e corrispondenti a quelli del sistema di acquisizione.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_eventchecker.py nrun ...
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [nbins_delta]   int, numero di bin per gli istogrammi di differenze di indice di evento,
#                     oppure "None" se non si vogliono usare limiti custom e si vuole usare
#                     il default value (default: 10)
# 3 [iflog_delta]   str, se gli istogrammi 1D vanno in scala log o no ("True" oppure "False")
#                     (default: "True")
# 4 [edgeL]         float, lower limit per gli istogrammi di differenze di indice di evento,
#                     oppure "None" se non si vogliono usare limiti custom e si vuole usare
#                     il default value (default: -5)
# 5 [edgeU]         float, upper limit per gli istogrammi di differenze di indice di evento,
#                     oppure "None" se non si vogliono usare limiti custom e si vuole usare
#                     il default value (default: +5)
# 6 [nbins_event]   int, numero di bin per gli istogrammi numero di evento vs differenza
#                     di indice di evento, oppure "None" se si vuole usare il default value
#                     (default: 250)
#
# Esempi:
# $     python3 tbeam_eventchecker.py 700140 10 True -5 +5 250
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati
veclist = [ data_namesdict['info_plus'] ]
info_plus = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                                  VARIABILI DI CONTROLLO
# ==========================================================================================
# Creo su base evento per evento la variabile "differenza tra il numero di evento di un digi
# e il numero di evento della daq"
deltadigi_6vsDAQ = info_plus[:,6] - info_plus[:,5]
deltadigi_7vsDAQ = info_plus[:,7] - info_plus[:,5]

# ==========================================================================================
#                                  OUTPUT SCRITTO
# ==========================================================================================
# Calcolo quante volte non si hanno match identici 
numunique_6vsDAQ = np.shape(np.unique(deltadigi_6vsDAQ))[0]
numunique_7vsDAQ = np.shape(np.unique(deltadigi_7vsDAQ))[0]

# Metto l'output in terminale
print(f"Numero di possibili valori dello scarto (evento digi 1 - evento DAQ): {numunique_6vsDAQ}")
print(f"Numero di possibili valori dello scarto (evento digi 2 - evento DAQ): {numunique_7vsDAQ}")

# Metto l'output in un file di testo
outfilename = os.path.join(imgloc_www,'eventchecker_out.txt')

# Scrivo l'output
print(f"Writing the output file... \n")
with open(outfilename,'w', encoding="utf-8") as file:
    # Timestamp
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    file.write(f"Timestamp: {datetime.date.today()}, {time_readable} \n")

    # Numero di run
    file.write(f"Run number: {run} \n\n")

    # Eventi disponibili
    file.write(f"Number of possible differences between the event of digi1 and the event of the DAQ: {numunique_6vsDAQ} \n")
    file.write(f"Number of possible differences between the event of digi2 and the event of the DAQ: {numunique_7vsDAQ} \n")
    
print(f"Outputs written also at:")
print(outfilename)
print(f"")

# ==========================================================================================
#                                  ISTOGRAMMI
# ==========================================================================================
# Definisco alcune variabili preliminari
try:  # Numero di bin per gli spettri
    if (sys.argv[2] == 'None'):
        nbins_delta = 10
    else:
        nbins_delta = int(sys.argv[2])
except:
    nbins_delta = 10

try:  # Se gli spettri 1D vanno in scala log
    iflog_delta = (sys.argv[3] == 'True')
except:
    iflog_delta = True

try:  # Lower limit per gli spettri
    if (sys.argv[4] == 'None'):
        edgeL = -5
    else:
        edgeL = int(sys.argv[4])
except:
    edgeL = -5

try:  # Upper limit per gli spettri
    if (sys.argv[5] == 'None'):
        edgeU = +5
    else:
        edgeU = int(sys.argv[5])
except:
    edgeU = +5

# Produco gli istogrammi di frequenze delle differenze tra eventi del digitizer e della DAQ
histo_6vsDAQ, bins_6vsDAQ = truehisto1D(deltadigi_6vsDAQ,nbins_delta,ifstep=False,edgeL=edgeL,edgeU=edgeU)
histo_7vsDAQ, bins_7vsDAQ = truehisto1D(deltadigi_7vsDAQ,nbins_delta,ifstep=False,edgeL=edgeL,edgeU=edgeU)

# Li mostro
fig = plt.figure(figsize = dimfigbig)
ax = fig.add_subplot(1,2,1)
histoplotter1D(ax,bins_6vsDAQ,histo_6vsDAQ,r'$\Delta$ event number','Counts',
                r"$\Delta$" + f" event number (Digi 1 - DAQ)",'best',textfont*1.25)
if iflog_delta:
    ax.set_yscale('log')

ax = fig.add_subplot(1,2,2)
histoplotter1D(ax,bins_7vsDAQ,histo_7vsDAQ,r'$\Delta$ event number','Counts',
                r"$\Delta$" + f" event number (Digi 2 - DAQ)",'best',textfont*1.25)
if iflog_delta:
    ax.set_yscale('log')
    
# Rifinisco la grafica
fig.set_tight_layout('tight')
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f"Event number monitor 1D ({datetime.date.today()}, {time_readable})", fontsize = 2.5*textfont)

# Salvataggio in due location
savefigname = '_eventchecker1D'
fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

# ==========================================================================================
#                                  TREND TEMPORALI
# ==========================================================================================
# Definisco il binning per il numero di eventi
try:
    if (sys.argv[6] == 'None'):
        nbins_event = 250
    else:
        nbins_event = int(sys.argv[6])
except:
    nbins_event = 250

# Produco gli istogrammi 2D di frequenze delle differenze tra eventi del digitizer e della DAQ
histo2D_6vsDAQ, binsX_6vsDAQ, binsY_6vsDAQ = truehisto2D(info_plus[:,5],deltadigi_6vsDAQ,
                                nbins_event,nbins_delta,ifstepX = False, ifstepY = False,edgeLX=0,
                                edgeLY=edgeL,edgeUY=edgeU)
histo2D_7vsDAQ, binsX_7vsDAQ, binsY_7vsDAQ = truehisto2D(info_plus[:,5],deltadigi_7vsDAQ,
                                nbins_event,nbins_delta,ifstepX = False, ifstepY = False,edgeLX=0,
                                edgeLY=edgeL,edgeUY=edgeU)

# Li mostro
colmap = 'jet'
slope = 0

fig = plt.figure(figsize = dimfigbig)
ax = fig.add_subplot(1,2,1)
histoplotter2D(ax,binsX_6vsDAQ,binsY_6vsDAQ,histo2D_6vsDAQ,colmap,slope,'Event number (ievent)',
                        r"$\Delta$" + f" event number (Digi 1 - DAQ)",1.25*textfont,iflog = True,ifcbarfont=True)

ax = fig.add_subplot(1,2,2)
histoplotter2D(ax,binsX_7vsDAQ,binsY_7vsDAQ,histo2D_7vsDAQ,colmap,slope,'Event number (ievent)',
                        r"$\Delta$" + f" event number (Digi 2 - DAQ)",1.25*textfont,iflog = True,ifcbarfont=True)
    
# Rifinisco la grafica
fig.set_tight_layout('tight')
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f"Event number monitor 2D ({datetime.date.today()}, {time_readable})", fontsize = 2.5*textfont)

# Salvataggio in due location
savefigname = '_eventchecker2D'
fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

print(f"1D and 2D histograms have been produced and saved.")