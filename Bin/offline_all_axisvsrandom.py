# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è confrontare più spettri di PH.
# Argomnenti: numero di canale da mostrare, run1, run2
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 offline_histocomparison.py 5 091 092
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                             DEFINIZIONE DEI TAGLI
# ==========================================================================================
# Definisco soglie e indici per taglio in PH dei Cherenkov
indPHcut_1 = 2
indPHcut_2 = 3
thr_PHcut = 100

# Definisco soglie e indici per taglio in differenza di tempo rispetto al Cherenkov
indTimeDiffcut = 2
thr_timediffcut = [-100,100]

# Definisco soglie e valori per taglio in divergenza
divlims_x = [-3,3]
divlims_y = [-3,3]

# Definisco soglie e valori per taglio in posizioni
poslims = [[1,7], [2,8]]

# ==========================================================================================
#                             DEFINIZIONE DEI COEFFICIENTI DI EQUALIZZAZIONE
# ==========================================================================================
# LG
LG_m = 1293.163
LG_q = -73.76

# OREO
#eq_m = [2.811, 2.483, 4.008, 3.714]
eq_m = [2.951, 2.518, 4.059, 3.904]
#eq_q = [16.378, 18.264, 9.932, 16.034]
eq_q = [11.387, 16.14, 6.63, 9.34]

digicolors = ['mediumblue','red','cyan','darkorange']
diginames = ['George','John','Paul','Ringo']

# ==========================================================================================
#                             UPLOAD DEI DATI RANDOM
# ==========================================================================================
print(f"Loading random data...")

# Inizializzo le variabili da riempire
digiPH_random = np.zeros((0,16))
ch_pos_random = np.zeros((0,4))
eventNumber_random = np.zeros((0,))
goniosteps_random = np.zeros((0,))
xinfo_random = np.zeros((0,5))
digitime_random = np.zeros((0,16))
ch_numhit_random = np.zeros((0,4))

# Definisco le run da guardare
all_runs = ['720120','720122','720124','720125']

# Ciclo sulle run e le apro
veclist = [ data_namesdict['digiPH'],data_namesdict['xpos'],data_namesdict['ievent'],
            data_namesdict['info_plus'], data_namesdict['xinfo'], data_namesdict['digiTime'],
            data_namesdict['nclu'] ]

for run in all_runs:
    # Apro il file
    run_name = pathnamedef(prename,run,datafiletype)

    # Estraggo i vettori necessari per l'analisi dal file dati 
    digiPH,ch_pos,eventNumber,info_plus,xinfo,digitime,ch_numhit = dataupload(data,run_name,veclist,
                                                                    datafiletype,globaltree,ifmatrix)
    goniosteps = info_plus[:,1]

    # Stacking
    digiPH_random = np.vstack((digiPH_random,digiPH))
    ch_pos_random = np.vstack((ch_pos_random,ch_pos))
    eventNumber_random = np.hstack((eventNumber_random,eventNumber))
    goniosteps_random = np.hstack((goniosteps_random,goniosteps))
    xinfo_random = np.vstack((xinfo_random,xinfo))
    digitime_random = np.vstack((digitime_random,digitime))
    ch_numhit_random = np.vstack((ch_numhit_random,ch_numhit))

# Calcolo punti di hit sul piano del cristallo e angoli di incidenza
all_thetas_random = np.zeros((np.shape(digiPH_random)[0],2))
all_pos_random = np.zeros_like(all_thetas_random)
scalefac = 1e3        # Angoli in mrad

for j in range(2):
    # Seleziono gli eventi di interesse. 
    refind = j % 2                        # Vale 0 quando lavoro su x e 1 su y
    cleanpos_a = ch_pos_random[:,refind]
    cleanpos_b = ch_pos_random[:,refind+2]

    # Calcolo gli angoli di propagazione e li appendo alla lista
    all_thetas_random[:,j] = scalefac * (np.arctan((cleanpos_b-cleanpos_a)/distances['zs1s2']))
    
    # Ricostruisco m e q delle traccie lineari delle traiettorie
    m = (cleanpos_b - cleanpos_a)/distances['zs1s2']
    q = cleanpos_b - m*distances['zs1s2']
    
    # Calcolo il punto di impatto sul piano dell'oggetto e lo appendo alla lista
    all_pos_random[:,j] = m*(distances['zs1s2'] + distances['zcry']) + q

# ==========================================================================================
#                           CONDIZIONI DI TAGLIO COMUNI RANDOM
# ==========================================================================================
# Condizione di taglio in cluster
condmap = (ch_numhit_random[:,:nsili_cut] == 1)
condcomp = condmap.prod(axis = 1)
clustercond_random = (condcomp == 1) & (eventNumber_random >= 0) & (np.isnan(eventNumber_random) == False)

# Condizione di taglio in PH del Cherenkov
PHcutcond_random = (digiPH_random[:,indPHcut_1] >= thr_PHcut) & (digiPH_random[:,indPHcut_2] >= thr_PHcut)

# Condizione di taglio in divergenza
divcutcond_x_random = (all_thetas_random[:,0] >= divlims_x[0]) & (all_thetas_random[:,0] <= divlims_x[1])
divcutcond_y_random = (all_thetas_random[:,1] >= divlims_y[0]) & (all_thetas_random[:,1] <= divlims_y[1])

# Condizione di taglio in posizione
poscutcond_x_random = (all_pos_random[:,0] >= poslims[0][0]) & (all_pos_random[:,0] <= poslims[0][1])
poscutcond_y_random = (all_pos_random[:,1] >= poslims[1][0]) & (all_pos_random[:,1] <= poslims[1][1])

# Condizione comune
globalcond_random = clustercond_random & PHcutcond_random & divcutcond_x_random & poscutcond_x_random & poscutcond_y_random


# ==========================================================================================
#                           ENERGIE RANDOM
# ==========================================================================================
# Applico i tagli
cuttedPH_random = digiPH_random[globalcond_random,:]

# Calcolo l'energia depositata nel LG e in OREO e in totale evento per evento
digichs = [4,5,6,7,0]
EDep_LG_random = (cuttedPH_random[:,digichs[-1]] - LG_q) / LG_m
EDep_OREO_random = np.zeros_like(EDep_LG_random)
for i in range(len(eq_m)):
    EDep_OREO_random = EDep_OREO_random + (cuttedPH_random[:,digichs[i]] - eq_q[i]) / (1e3 * eq_m[i]) # in GeV

EDep_TOT_random = EDep_LG_random + EDep_OREO_random
print(f"Done!\n")




























# ==========================================================================================
#                             UPLOAD DEI DATI AXIAL
# ==========================================================================================
# Definisco la run da guardare
print(f"Loading axial data...")
veclist = [ data_namesdict['digiPH'],data_namesdict['xpos'],data_namesdict['ievent'],
            data_namesdict['info_plus'], data_namesdict['xinfo'], data_namesdict['digiTime'],
            data_namesdict['nclu'] ]

run = '720127'
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati 
digiPH_axial,ch_pos_axial,eventNumber_axial,info_plus_axial,xinfo_axial,digitime_axial,ch_numhit_axial = dataupload(data,
                                            run_name,veclist,datafiletype,globaltree,ifmatrix)
goniosteps_axial = info_plus_axial[:,1]

# Calcolo punti di hit sul piano del cristallo e angoli di incidenza
all_thetas_axial = np.zeros((np.shape(digiPH_axial)[0],2))
all_pos_axial = np.zeros_like(all_thetas_axial)

for j in range(2):
    # Seleziono gli eventi di interesse. 
    refind = j % 2                        # Vale 0 quando lavoro su x e 1 su y
    cleanpos_a = ch_pos_axial[:,refind]
    cleanpos_b = ch_pos_axial[:,refind+2]

    # Calcolo gli angoli di propagazione e li appendo alla lista
    all_thetas_axial[:,j] = scalefac * (np.arctan((cleanpos_b-cleanpos_a)/distances['zs1s2']))
    
    # Ricostruisco m e q delle traccie lineari delle traiettorie
    m = (cleanpos_b - cleanpos_a)/distances['zs1s2']
    q = cleanpos_b - m*distances['zs1s2']
    
    # Calcolo il punto di impatto sul piano dell'oggetto e lo appendo alla lista
    all_pos_axial[:,j] = m*(distances['zs1s2'] + distances['zcry']) + q

# ==========================================================================================
#                           CONDIZIONI DI TAGLIO COMUNI AXIAL
# ==========================================================================================
# Condizione di taglio in cluster
condmap = (ch_numhit_axial[:,:nsili_cut] == 1)
condcomp = condmap.prod(axis = 1)
clustercond_axial = (condcomp == 1) & (eventNumber_axial >= 0) & (np.isnan(eventNumber_axial) == False)

# Condizione di taglio in PH del Cherenkov
PHcutcond_axial = (digiPH_axial[:,indPHcut_1] >= thr_PHcut) & (digiPH_axial[:,indPHcut_2] >= thr_PHcut)

# Condizione di taglio in divergenza
divcutcond_x_axial = (all_thetas_axial[:,0] >= divlims_x[0]) & (all_thetas_axial[:,0] <= divlims_x[1])
divcutcond_y_axial = (all_thetas_axial[:,1] >= divlims_y[0]) & (all_thetas_axial[:,1] <= divlims_y[1])

# Condizione di taglio in posizione
poscutcond_x_axial = (all_pos_axial[:,0] >= poslims[0][0]) & (all_pos_axial[:,0] <= poslims[0][1])
poscutcond_y_axial = (all_pos_axial[:,1] >= poslims[1][0]) & (all_pos_axial[:,1] <= poslims[1][1])

# Condizione comune
globalcond_axial = clustercond_axial & PHcutcond_axial & divcutcond_x_axial & poscutcond_x_axial & poscutcond_y_axial

# ==========================================================================================
#                           ENERGIE axial
# ==========================================================================================
# Applico i tagli
cuttedPH_axial = digiPH_axial[globalcond_axial,:]

# Calcolo l'energia depositata nel LG e in OREO e in totale evento per evento
digichs = [4,5,6,7,0]
EDep_LG_axial = (cuttedPH_axial[:,digichs[-1]] - LG_q) / LG_m
EDep_OREO_axial = np.zeros_like(EDep_LG_axial)
for i in range(len(eq_m)):
    EDep_OREO_axial = EDep_OREO_axial + (cuttedPH_axial[:,digichs[i]] - eq_q[i]) / (1e3 * eq_m[i]) # in GeV

EDep_TOT_axial = EDep_LG_axial + EDep_OREO_axial
print(f"Done!\n")

























# ==========================================================================================
#                           OREO COMPARISON
# ==========================================================================================
# Creo lo spettro del calorimetro
nbins_EDep = 0.015 # GeV
histo_OREO_random, bins_OREO_random = truehisto1D(EDep_OREO_random,nbins_EDep,ifstep=True)
histo_OREO_axial, bins_OREO_axial = truehisto1D(EDep_OREO_axial,nbins_EDep,ifstep=True)

# Normalizzo
histo_OREO_random = histo_OREO_random / np.sum(histo_OREO_random)
histo_OREO_axial = histo_OREO_axial / np.sum(histo_OREO_axial)

# Fitto
result_random, xth_random, yth_random = fitter_expgaussexp(bins_OREO_random,histo_OREO_random,fitcut_low = 0.5)
result_axial, xth_axial, yth_axial = fitter_expgaussexp(bins_OREO_axial,histo_OREO_axial,fitcut_low = 0.5)

# Calcolo i valori medi degli spettro di PH
mean_OREO_random, err_OREO_random = wmean(bins_OREO_random,histo_OREO_random)
mean_OREO_axial, err_OREO_axial = wmean(bins_OREO_axial,histo_OREO_axial)

# Printout 
print(f"Average EDep, OREO (all crystals):")
print(f"\t Random: {mean_OREO_random:.3f} +/- {err_OREO_random:.3f} GeV")
print(f"\t Axial: {mean_OREO_axial:.3f} +/- {err_OREO_axial:.3f} GeV")
print(f"\t e.f.: {mean_OREO_axial/mean_OREO_random:.3f}\n")

try:
    print(f"\t Random fit, MPV = {result_random.params['mu'].value:.3f} +/- {result_random.params['mu'].stderr:.3f}")
    print(f"\t Axial fit, MPV = {result_axial.params['mu'].value:.3f} +/- {result_axial.params['mu'].stderr:.3f}")
except:
    print(f"\t Random fit, MPV = {result_random.params['mu'].value:.3f}")
    print(f"\t Axial fit, MPV = {result_axial.params['mu'].value:.3f}")

print(f"\t e.f.: {result_axial.params['mu'].value/result_random.params['mu'].value:.3f}\n")

# Mostro gli spettri
fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,bins_OREO_random,histo_OREO_random,'OREO energy [GeV]','Normalized counts','Random',
                    'best',textfont,'mediumblue')
histoplotter1D(ax,bins_OREO_axial,histo_OREO_axial,'OREO energy [GeV]','Normalized counts','Axial',
                    'best',textfont,'red')
ax.plot(xth_random,yth_random,color='cyan',label=f"Crystal Ball fit, MPV = {result_random.params['mu'].value:.3f} GeV",
            linestyle = '--', linewidth = 2.5)
ax.plot(xth_axial,yth_axial,color='darkorange',label=f"Crystal Ball fit, MPV = {result_axial.params['mu'].value:.3f} GeV",
            linestyle = '--', linewidth = 2.5)
ax.legend(loc='upper right',framealpha=1,fontsize=textfont)
ax.set_ylim([0,0.025])
#ax.set_yscale('log')

# Rifinisco la grafica
fig.set_tight_layout('tight')
fig.suptitle(f'Sum of all the crystals (with cuts) @ 6 GeV', fontsize = 2*textfont)
fig.savefig(imgloc + f'axisvsrandom_OREO' + filetype,dpi = filedpi)
plt.show()
plt.close(fig)
print(f"OREO plot saved!")

# ==========================================================================================
#                           OREO COMPARISON
# ==========================================================================================
# Creo lo spettro del calorimetro
nbins_EDep = 0.025 # GeV
histo_TOT_random, bins_TOT_random = truehisto1D(EDep_TOT_random,nbins_EDep,ifstep=True)
histo_TOT_axial, bins_TOT_axial = truehisto1D(EDep_TOT_axial,nbins_EDep,ifstep=True)

# Normalizzo
histo_TOT_random = histo_TOT_random / np.sum(histo_TOT_random)
histo_TOT_axial = histo_TOT_axial / np.sum(histo_TOT_axial)

# Calcolo i valori medi degli spettro di PH
mean_TOT_random, err_TOT_random = wmean(bins_TOT_random,histo_TOT_random)
mean_TOT_axial, err_TOT_axial = wmean(bins_TOT_axial,histo_TOT_axial)

# Printout 
print(f"Average EDep, TOT (LG + OREO):")
print(f"\t Random: {mean_TOT_random:.3f} +/- {err_TOT_random:.3f} GeV")
print(f"\t Axial: {mean_TOT_axial:.3f} +/- {err_TOT_axial:.3f} GeV")
print(f"")

# Mostro gli spettri
fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,bins_TOT_random,histo_TOT_random,'Total (OREO + LG) energy [GeV]','Normalized counts','Random',
                    'best',textfont,'mediumblue')
histoplotter1D(ax,bins_TOT_axial,histo_TOT_axial,'Total (OREO + LG) energy [GeV]','Normalized counts','Axial',
                    'best',textfont,'red')
ax.set_xlim([3,7])
#ax.set_yscale('log')

# Rifinisco la grafica
fig.set_tight_layout('tight')
fig.suptitle(f'(OREO + LG) spectra with cuts', fontsize = 2*textfont)
fig.savefig(imgloc + f'axisvsrandom_TOT' + filetype,dpi = filedpi)
plt.show()
plt.close(fig)
print(f"TOTAL plot saved!")
print(f"Job done!\n")