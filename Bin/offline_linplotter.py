# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è interpolare due serie di dati con due rette e poi calcolare
# la posizione di intersezione delle stesse. Questo passaggio è necessario, per esempio,
# per determinare l'asse stereografico di un cristallo a partire dalle posizioni dei suoi 
# picchi.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 offline_linplotter.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                                  DEFINIZIONE DEI PUNTI
# ==========================================================================================
# Definisco manualmente i due set di dati (x1,y1) e (x2,y2) su cui eseguirò un'interpolazione
# lineare.
x1 = np.array([1,2,3,4])            # Theta ang del primo picco
y1 = np.array([2,4,6,8])            # Theta cradle del primo picco
x2 = np.array([0.5,1.5,2.5])        # As above ma per il secondo picco
y2 = np.array([-1,2,5])

# ==========================================================================================
#                                  INTERPOLAZIONE E PLOT
# ==========================================================================================
# Fit lineare sui punti
result1,_,_ = fitter_linear(x1,y1)
result2,_,_ = fitter_linear(x2,y2)

# Estraggo i coefficienti di fit
m1 = result1.params['m'].value
q1 = result1.params['q'].value
m2 = result2.params['m'].value
q2 = result2.params['q'].value

# Calcolo l'intersezione delle due rette
x = -(q1-q2)/(m1-m2)
y = -( (q1-q2)*m1/(m1-m2) ) + q1
print(f"")
print(f"Straight line intersections:")
print(f"\t{x:=.2f} urad")
print(f"\t{y:=.2f} urad")

# Creo le rette teoriche in modo da includere tutti i punti del grafico
xmin = np.min([np.min(x1),np.min(x2),x])
xmax = np.max([np.max(x1),np.max(x2),x])

xth = np.linspace(xmin,xmax,nptsfit)
yth1 = m1*xth+q1
yth2 = m2*xth+q2

# Plot
fig, ax = plt.subplots(figsize = dimfig)
ax.plot(xth,yth1,color = 'cyan', label = 'Fit curva 1', marker = '', linestyle = '--', linewidth = 3)
ax.plot(xth,yth2,color = 'darkorange', label = 'Fit curva 2', marker = '', linestyle = '--', linewidth = 3)
ax.plot(x1,y1,color = 'mediumblue', label = 'Dati curva 1', marker = '.', markersize = 17, linestyle = '')
ax.plot(x2,y2,color = 'red', label = 'Dati curva 2', marker = '.', markersize = 17, linestyle = '')
ax.plot(x,y,color = 'black', label = 'Intersezione', marker = '*', linestyle = '', markersize = 22)
ax.legend(loc = 'upper left',fontsize = textfont)
ax.set_xlabel(r'$x_1$',fontsize = textfont)
ax.set_ylabel(r'$y_1$',fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(imgloc + 'offline_linplotted' + filetype, dpi = filedpi)
plt.show()

# Chiudo lo script
print(f"")
print(f"The data have been plotted and intersecated.")
print(f"")