# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è mostrare le forme d'onda di un insieme di rivelatori a scelta.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_wvfplotter.py nrun listchs listpol listevs ifcut nptsbase ifhisto2D xlims
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [listchs]       list of str (int), lista dei canali di cui si vuole guardare le waveform 
#                     (default: ['0'])
# 3 [listpol]       list of str, lista delle polarità dei canali ("pos", "neg") (default: "neg")
# 4 [listevs]       list of int, lista a due componenti, con la prima e l'ultima waveform da mostrare
#                     (ok anche [-100,-10]) (default: [0,100])
# 5 [ifcut]         int, usato per decidere che c.l. cut applicare prima di mostrare le waveform
#                     (cioè si mostrano solo quelle dove la PH supera ifcut voglie la std del rumore)
#                     Ok anche "False" per non applicare tagli e mostrare tutto. (default: "False")
# 6 [nptsbase]      int, numero di punti su cui calcolare la baseline, oppure "None", se cadere sul
#                     default value (default: 50)
# 7 [ifhisto2D]     str, se fare un istogramma 2D delle waveform sovrapposte oppure se si vuole vedere
#                     il plot di ogni forma separatamente (default: "True")
# 8 [xlims]         list of float, lista dei limiti per ciascun plot, oppure "None", se non si vuole
#                     applicare un limite custom
#
# Esempi:
# $     python3 tbeam_wvfplotter.py 500834 [1,2,3,4] [neg,neg,pos,pos] [0,100] 5 50 True
# 
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Definisco i rivelatori da analizzare come lista
try:
    wvfind = str2list(sys.argv[2],str)
except:
    wvfind = ['0']

wvfkeys = ['wf' + el for el in wvfind]

# Definisco le polarità delle waveform
try:
    polarity = str2list(sys.argv[3],str) # ogni elemento è "pos" o "neg"
except:
    polarity = ['neg' for el in wvfind]

# Definisco gli eventi da guardare in plot separati
try:
    list_wvfs = str2list(sys.argv[4],int)
except:
    list_wvfs = [0,100]

range_wvfs = range(list_wvfs[0],list_wvfs[-1])

# Definisco se mostrare solo i plot delle waveform con una PH sensata
try:
    if (sys.argv[5] != 'False'):
        conf = int(sys.argv[5])
    else:
        conf = None
except:
    conf = None       

# Definisco su quanti punti calcolare la baseline
try:
    if (sys.argv[6] != 'None'):
        npts_base = int(sys.argv[6])
    else:
        npts_base = 50
except:
    npts_base = 50

# Definisco se mostrare un istogramma 2D delle waveform oppure farne molte sovrapposte
try:
    ifhisto = (sys.argv[7] == 'True')
except:
    ifhisto = True
    
# Definisco eventuali xlims per gli assi
try:
    if sys.argv[8] != 'None':
        xlims = str2list(sys.argv[8],int)
    else:
        xlims = None
except:
    xlims = None

# ==========================================================================================
#                                       PRODUZIONE DEI PLOT
# ==========================================================================================
# Definisco numero di punti di header e trailer di ogni waveform
head_edge = 5
lead_edge = 10

# Definisco ogni quante wvf avvisare l'utente del mio lavoro
printstep = 50

# Estraggo le waveform richieste e le plotto, una per una, per ogni rivelatore
nwave = len(range_wvfs)
for j,elj in enumerate(wvfkeys):
    # Print di check
    print(f"")
    print(f"Analyzing channel {wvfind[j]}.")
    
    # ==========================================================================================
    #                                     UPLOAD DEI DATI 
    # ==========================================================================================
    # Estraggo i vettori necessari per l'analisi dal file dati (posizioni di hit dei silici, waveform di interesse)
    veclist = [ data_namesdict['nclu'], data_namesdict['ievent'],
                data_namesdict['info_plus'], data_namesdict[elj] ]
    list_wvfs_forextraction = [list_wvfs[0],5*list_wvfs[-1]] # Non carico tutte le wvf ma solo il numero da mostrare con un margine
    ch_numhit,eventNumber,info_plus,wvfs = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix,
                                                      inds=list_wvfs_forextraction)
    nspill = info_plus[:,0]

    # Se richiesto, taglio gli eventi in numero di cluster
    if ifcuthit:
        condmap = (ch_numhit[:,:nsili_cut] == 1)
        condcomp = condmap.prod(axis = 1)
        cond = (condcomp == 1) & (nspill >= 0) & (eventNumber >= 0)
        clean_wvfs = wvfs[cond,:]
    else:
        clean_wvfs = wvfs
    
    print(f"Waveforms loaded")
    
    # Creo una matrice che riempirò solo con i segnali estratti
    saved_wfs = np.zeros((nwave,np.shape(clean_wvfs)[1]))
    saved_wfs = saved_wfs[:,head_edge+1:-lead_edge]

    # Itero per ogni waveform richiesta
    for i in range_wvfs:
        # ==========================================================================================
        #                                       PLOT SEPARATI
        # ==========================================================================================
        # Printout di check
        if i % printstep == 0:
            print(f"Waveform n. {i} is being processed")

        # Protezione dall'errore
        if (i >= np.shape(clean_wvfs)[0]): # Sto guardando più righe di quelle che ci sono!
            print('Waveform finite!')
            break
        
        # Definisco la waveform i-esima e sottraggo la baseline. Se richiesto, inverto la polarità
        ith_wave = clean_wvfs[i,head_edge+1:-lead_edge]
        sub_wave = ith_wave - np.mean(ith_wave[:npts_base])
        if polarity[j] == 'neg':
            inv_wave = -sub_wave
        else:
            inv_wave = sub_wave
            
        saved_wfs[i,:] = inv_wave

        # Creo il vettore dei tempi
        times = np.arange(np.shape(inv_wave)[0])
        
        # Decido se plottare la waveform o no
        if conf is not None:
            # Controllo se il massimo è sopra (conf) volte la std della baseline. Se sì, vado all'iterazione dopo
            stdthr = conf*np.std(sub_wave[:npts_base])
            if np.max(np.abs(sub_wave)) < stdthr:
                continue
        
        # Creo una figura e plotto la waveform. Se richiesto, aggiungo la wvf smoothed
        fig, ax = plt.subplots(figsize = dimfig)
        ax.plot(times,inv_wave,label = f'Waveform n. {i}, ch. ' + wvfind[j], color = 'mediumblue', marker = '.', markersize = 5,
                linestyle = '-', linewidth = 1)
        ax.legend(loc = 'upper right',fontsize = textfont)
        ax.set_xlabel('Time [ticks]',fontsize = textfont)
        ax.set_ylabel('Charge [a.u.]', fontsize = textfont)
        if xlims is not None:
            ax.set_xlim(xlims)
        ax.grid(linewidth=0.5)
        ax.tick_params(labelsize = textfont)
        fig.set_tight_layout('tight')

        savefile = imgloc + 'run' + run + elj + f'_sepwvfs_{i:03.0f}' + filetype
        fig.savefig(savefile, dpi = filedpi)
        plt.show()
        plt.close(fig)

    # ==========================================================================================
    #                                       MERGE
    # ==========================================================================================
    # Unisco tutte le waveform plottate
    print(f"Merging the plots...")
    try:
        # Creo il file merged
        mergedfile = imgloc + 'run' + run + '_allwvfs_ch' + wvfind[j] + filetype
        if os.path.exists(mergedfile):
            os.remove(mergedfile)    

        # Unisco i singoli file e chiudo il merger
        allfiles = glob.glob(imgloc + 'run' + run + elj + '_sepwvfs_*')
        merger = PyPDF2.PdfMerger()
        for pdf in allfiles:
            merger.append(pdf)
            os.remove(pdf)

        merger.write(mergedfile)
        merger.close()

        # Copio l'output nella www folder
        copiedfile = imgloc_www + 'lastrun_allwvfs_ch' + wvfind[j] + filetype #! non filetype_www
        shutil.copy(mergedfile,copiedfile)

        print(f"Merging completed")
    except PyPDF2.errors.PdfReadError:
        print(f"Only noise was detected, no waveform was plotted.")
        
    # ==========================================================================================
    #                                       PLOT OVERLAP
    # ==========================================================================================
    # Plotto molte wvf insieme. Decido se usare un istogramma o fare plot sovrapposti
    fig, ax = plt.subplots(figsize = dimfig)
    if ifhisto:
        # Variabili hard-coded per gli istogrammi
        timestep = 1
        sigstep = 25        # Passo per istogrammi di segnale

        # Plot sovrapposto
        wvfplotter2D(ax,-saved_wfs,np.zeros(nwave),nwave,timestep,sigstep,textfont = textfont,iflog = True)
    else:
        for i,el in enumerate(range_wvfs):
            ax.plot(times,saved_wfs[el,:],color = 'mediumblue',linestyle = '-', linewidth = 0.5)
            ax.set_xlabel('Time [ticks]',fontsize = textfont)
            ax.set_ylabel('Charge [a.u.]', fontsize = textfont)
            ax.grid(linewidth=0.5)
            ax.tick_params(labelsize = textfont)
    
    if xlims is not None:
            ax.set_xlim(xlims)
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"Waveforms ch. {elj[-1]}" + f' ({time_readable})', fontsize = textfont*3)
    fig.set_tight_layout('tight')

    # Salvo tutto
    savefile = imgloc + 'run' + run + '_' + elj + f'_overlap' + filetype
    fig.savefig(savefile, dpi = filedpi)
    savefile_www = imgloc_www + 'lastrun_' + elj + f'_overlap' + filetype_www
    fig.savefig(savefile_www, dpi = filedpi_www)

    plt.show()
    plt.close(fig)
    
# Chiudo lo script
print(f"")
print(f"The waveforms have been plotted, fitted, merged and saved.")
print(f"")