# TBTools

Scripts and tools to setup and operate an **online monitor system** during a beam test at CERN. 

Toolkit developed by [piemmegi](mailto:monti.guarnieri.p@gmail.com) for the INSULAB research group.

Online tool to access the toolkit: [here](https://piemmegi.web.cern.ch/)

Last update: 2023.10.03

## Aim of the project

This set of `Python` scripts and functions is essentially aimed at being a versatile and complete **online monitor**, capable of analyzing the data acquired during a beamtest and thus verifying that all is going well. The project is intended to work in synergy with the [data conversion toolkit](https://gitlab.com/stex6299/insulabtestbeam) developed by S. Carsi, which deals with the conversion of the data file produced by the DAQ of the setup into a more readable format. However, it is not strictly dependent on the latter.

## Prerequisites

If you're reading these lines, you probably have to deal with a beamtest in the incoming times and you want to set up the online monitor system. If that is the case, we can assume that you have already:

- Decided *where* the analysis will take place. This means that you have already decided whether to work on your local PC, on an online machine (such as CERN's own [Swan Service Machines](https://swan.cern.ch/)) or on an online virtual machine (such as CERN's own [VMs](https://clouddocs.web.cern.ch/tutorial_using_a_browser/create_a_virtual_machine.html)).
- Organized a way to access to the data files that are produced during the beamtest. This usually happens by using `ssh` or `sshfs` and mounting locally the folder where the files are saved by the DAQ system, or equivalently the folder where the output files are converted by another set of scripts. Note that this happens automatically if your data are streamed to an EOS project and you access it from the CERN VM.
- Installed `Python` and `git`, possily with the most recent versions. 

In this sense, we can assume that you already have a machine that can run `Python` scripts by command line and can see the files that you want to analyze on a system path. We heavily recommend to use `conda` as package manager.

## Setup

If all the requisites are already satisfied, you can **setup** your own online monitor. To do so, first copy the entire content of this repository to a local folder in your pc, for example by using the command:

`git clone https://gitlab.com/piemmegi/TBTools`

You will have downloaded, in this way, a few folders:
- **Bin**. As the name suggests, in this folder there are a few scripts that should not be considered in a first phase (since they are "almost trash").
- **Functions**. As the name suggests, in this folder there are a few files that include the word `function` inside them (i.e. `MyFunctions.py`, `beamfunctions.py`, etc.). You should **absolutely not modify** these files, since the online monitor relies on them and on their exact syntax to work.
- **Images**. As the name suggests, in this folder all the images that will be produced during the beam test online analysis will be saved and stored. Initially, this folder will be empty.
- **Scripts**. In this folder there are a few `Python` scripts of interest:
    - The ones called `tbeam_*.py` are the **scripts that you will launch during the analysis**. You **should not need to modify** them, unless you want to implement new eatures. For more details about how each script should be called, please go to the **Online Monitor Functions** section.
    - A file called `statuscreator.py`. This file is the **only one** that you are **required to modify**, since you must specificy here all the details that concern your specific beam test. For more details about it, please go to the **Online Monitor Functions** section.
- **Tempfiles**. As the name suggests, in this folder all the temporary files produced during the beam test online analysis will be stored. Initially, this folder will be empty.
- **Web**. Copy the content of this folder in the `www` folder of your CERNBOX dataspace to have access to a real online monitor, capable of displaying the content created with these scripts.

Moreover, you will find in the root folder a script called `setup.py`. Upon launch, this script simply prepares the local folder where you copied the repository for all the subsequent analysis: it installs any missing subfolder and it verifies that every `Python` module required for the analysis is correctly installed in the system. **You should launch this file immediately after copying the folder on your station** (assuming you have already `cd`-ed yourself in the folder where you have put all your files). Thus, just call:

`python3 /home/.../setup.py`

## Online Monitor Functions

If the setup went well, you should now be able to launch all the scripts you want. The one that must be **constantly monitored** and that can be modified at will is `statuscreator.py`. The way to launch it is simply:

`$ python3 /home/../Scripts/statuscreator.py`

Upon launch, you should see that a file called `statusfile.json` is created. In this file, **all the relevant informations about your beam test are stored**, with the `JSON` format. Thus, you should adjust all the content of the `statuscreator` script depending on how your beam line is configurated. Please note that every point that should be affected is internally marked by `#!`. You should specify:

- The extension (format) of your data files. Currently, the following formats are supported: `.npz`, `.root`, `.h5py`. Sooner or later, other extensions (ASCII files) will be implemented, but this is not a current priority.
- How your files are configured. In other words, you should clarify whether the files contain a global tree where the data columns are stored or not and also whether these columns should be accessed by index or by key-name pair. Example: you should specify if your `.npz` files are simply huge matrix where each line is an event and each column is a variable, or if you have unpacked your columns in single vectors that may be accessed as keys of your dictionary. In the second case, you should clarify which indexes correspond to what variable.
- How many silicon detectors (trackers) are in your setup and what are their inter-distances.
- If you want to choose only the events with single clusters on a fixed set of detectors (you should also say *which* detectors).
- Other relevant informations

Notice that you don't have to reinvent the wheel from scratch: simply **modify the pre-existing code keeping its structure and format**. After you have completed your job (i.e., edited the status creator and run it), you can launch your analysis scripts. The currently available scripts are:

- `tbeam_acounter.py`: this script simply **counts the good events** in a single run.
- `tbeam_corrplot.py`: this script plots the 2D histogram/correlation map between two arbitrary types of variables. Currently, the following have been implemented: PH, number of clusters in the silicon detectors, hit positions in the silicon detectors. The first variable does not need to be of the same type of the second variable.
- `tbeam_crystalscan.py`: this script analyzes a data file produced during an **angular scan** performed with a goniometer and produces as output the 2D histograms containing the distribution of PHs of a chosen digitizer channel as a function of the angular positions of the goniometer. Profile plots are also included. It is **necessary** to execute this script before the production of a stereogram.
- `tbeam_crystalstereogram.py`: this script analyzes all the data produced during the **angular scans** and saved with `tbeam_crystalscan.py`, in order to produce the **stereogram** of a given crystal.
- `tbeam_digiplots.py`: this scripts shows you the **Pulse Height** (PH) and **Peak Time** (PT) spectrum of each digitizer channel that is available. It also shows the **PH vs PT** correlation histograms and the **temporal trend of the PH value** of each channel. This script should be used to check that all the detectors are working correctly and to evaluate the presence of temporal fluctuations of the signals (i.e., the so-called "banana plots"). This is not the right script to study the spectrum of one single channel: use `tbeam_singledigi.py` instead, since the spectra are magnified, or other scripts, where fitting options are also available.
- `tbeam_digiplots_timediff.py`: this scripts works as the previous one, but the Peak Times are replaced with the differencete between the PT of a reference channel and the PT of all the other channels.
- `tbeam_divergence.py`: this script uses the data produced by the first two silicon trackers to **measure the beam divergence at the initial stage** of your beam line, while also observing the frequency distribution on the incoming particles incidence angles.
- `tbeam_efficiency.py`: this script produces the efficiency map of an object of your choice on a plane of your choice. For this script to work, you must specify which silicon trackers should be used to reconstruct the charged particles tracks and how to select the "good" events that go in the numerator of your efficiency map (you can either choose the number of clusters measured by one ore more silicon trackers of your choice, or the PH of one or more channels of your choice, with possibilty of combination with "and"/"or" on all the channels simultaneously).
- `tbeam_foldercleaner.py`: this script simply removes all the image files contained in the image folder defined in the status file, and/or the files contained in the temporary file location.
- `tbeam_siliplots.py`: this script produces the beam profile plots, measured by each silicon strip tracker.
- `tbeam_singlecalibration.py`: this script produces the **calibration, linearity and resolution plots** for a given digitizer channel. In order to operate correctly, this script should be launched after the creation of several temporary files, which may be produced with `tbeam_singledigi`.
- `tbeam_singledigi.py`: this script produces the **PH, PT, PH vs PT and the temporal trend of the PH value** for one or more specified channels. 
- `tbeam_singledigifit.py`: this script produces the **PH distribution** for a single specified channel and allows to perform a **fit on the PH spectrum**, with a Gaussian, Crystal Ball-like or Landau function. This is necessary to perform the calibration of the channel.
- `tbeam_singledigiwithcuts.py`: as `tbeam_singledigi`, but it can apply several cuts before computing the histograms (e.g., PT cut, position cut, etc.).

There may be from time to time also smaller scripts, not as general-purpouse and as optimized as the ones listed before. It is also interesting to notice the presence of the `vm_scriptcaller.py` script. This script simply runs endlessly and calls repeatedly some of the most important scripts of the list given above. This allows to have a "live monitor" continuously updated with the data of the last run.

The complete syntax for each script is specified in the header of the script itself. *A specific readme file will be added to the `Scripts` folder with more details about the syntax.*


# Work in progress

Here is a list of various things that will be done in the future and are currently under work:

- Modify `tbeam_efficiency.py` adding a way to define with a keyword argument the distance at which the efficiency should be calculated
- Add in `tbeam_crystalscan.py` (and consequently in `tbeam_crystalstereogram.py`) a differentiation between different crystal samples (i.e., a reference name for the crystal under test).