# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i plot di divergenza di ingresso di un fascio
# di particelle cariche ad alta energia. 
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_divergence.py nrun binstep ifstep
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [binstep]       float, numero di bin per la costruzione dell'istogramma di divergenza
#                     (default: 150)
# 3 [ifstep]        str, se l'argomento 2 è da interpretare come uno step ("True") o un
#                     numero di bin ("False") (default: "False")
#
# Esempi:
# $     python3 tbeam_divergence.py 500834 150 False
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (posizioni di hit dei silici, numero di cluster)
veclist = [ data_namesdict['xpos'], data_namesdict['ievent'] ]
ch_pos,eventNumber = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# Calcolo la distanza tra i vari silici
zs1s2 = distances['zs1s2']

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    ch_pos = ch_pos[cond,:]
    eventNumber = eventNumber[cond]
    
# ==========================================================================================
#                                  ANALISI DI DIVERGENZA
# ==========================================================================================
# Definisco le variabili utili per l'analisi
try:
    nbinsdiv = float(sys.argv[2])      # Numero di bin nell'istogramma della divergenza
except:
    nbinsdiv = 150                     # Default value

try:
    ifstep = (sys.argv[3] == 'True')   # Se sys.argv[2] è un binning step
except:
    ifstep = False

if ifstep:
    nbinsdiv = float(nbinsdiv)
else:
    nbinsdiv = int(nbinsdiv)

conf = 5                               # Confidence level di taglio degli angoli
conf2 = 3                              # C.L. per il plot
div = np.zeros((2,))                   # Divergenze (x,y)
diverr = np.zeros_like(div)            # Incertezze sulle divergenze
locs = ['x','y']

# Definisco se stampare gli angoli in mrad o urad
unitscale = 1e3
if (unitscale == 1e3):
    unittype = 'mrad'
elif (unitscale == 1e6):
    unittype = 'urad'

# Definisco se usare un limite particolare per l'asse x dei plot
#xlims = None
xlims = [ [-3,3], [-3,3] ]

# Procedo su ogni direzione con la ricostruzione: x,y.
fig = plt.figure(figsize = dimfigbig)

for j in range(2):
    # Seleziono gli eventi di interesse. 
    refind = j % 2                        # Vale 0 quando lavoro su x e 1 su y
    cleanpos_a = ch_pos[:,refind]
    cleanpos_b = ch_pos[:,refind+2]

    # Calcolo gli angoli e filtro gli outliar        
    theta = np.arctan((cleanpos_b-cleanpos_a)/zs1s2)
    thetacond = (theta <= np.nanmean(theta) + conf*np.nanstd(theta)) & (theta >= np.nanmean(theta) - conf*np.nanstd(theta))
    cleantheta = theta[thetacond]
    
    # Calcolo l'istogramma di frequenze degli angoli di impatto (divergenza)
    histo_angle, bins_angle = truehisto1D(cleantheta,nbinsdiv,ifstep=ifstep)

    # Eseguo un fit gaussiano sulla distribuzione degli angoli e ne estraggo la divergenza
    result,xth,yth = fitter_gauss1fun(bins_angle,histo_angle,'P')
    a,b,c,d = result.params['a'], result.params['b'], result.params['c'], result.params['d']
    div[j] = c.value*unitscale     # c nasce in rad, lo porto a mrad o urad
    try:
        diverr[j] = result.params['c'].stderr*unitscale
        printlabel = f'Divergence along ' + locs[j] + f' = ({c.value*unitscale:.4f} +/- {c.stderr*unitscale:.4f}) {unittype}'
        leglabel = r'$\sigma$ ' + f'= ({c.value*unitscale:.4f} +/- {c.stderr*unitscale:.4f}) {unittype}'
    except:
        diverr[j] = np.nan
        printlabel = f'Divergence along ' + locs[j] + f' = {c.value*unitscale:.4f} {unittype} (error is n/a)'
        leglabel = r'$\sigma$ ' + f'= {c.value*unitscale:.4f} {unittype}'
    
    # Aggiungo il plot della divergenza
    ax = fig.add_subplot(2,1,j+1)
    histoplotter1D(ax,bins_angle*unitscale, histo_angle,f'Incidence angle [{unittype}]','Counts',
                   f"Run " + str(run),'best',textfont*2)
    ax.plot(xth*unitscale,yth,color = 'red', linestyle = '-', label = leglabel)
    ax.legend(loc = 'best', fontsize = textfont*1.5,framealpha=1)
    if xlims is not None:
        ax.set_xlim(xlims[j])

    print(f"")
    print(printlabel)

time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f'Incident beam divergence ({datetime.date.today()}, {time_readable})', fontsize = 2*textfont)
fig.tight_layout(rect=[0, 0.03, 1, 0.95])
fig.set_tight_layout('tight')

# Salvo la figura in due location
savefigname = '_beamdivergence'
fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
plt.show()
plt.close(fig)

print(f"The divergence plot has been created and saved.")
print(f"")

# ==========================================================================================
#                                  WRITEOUT
# ==========================================================================================
# Definisco dove printare l'output
outfilename = os.path.join(imgloc_www,'divergence_out.txt')

# Scrivo l'output
print(f"Writing the output file... \n")
with open(outfilename,'w', encoding="utf-8") as file:
    # Timestamp
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    file.write(f"Timestamp: {datetime.date.today()}, {time_readable} \n")

    # Numero di run
    file.write(f"Run number: {run} \n\n")

    # Eventi disponibili
    try:
        file.write(f"Divergence along the x axis: ({div[0]:.3f} +/- {diverr[0]:.3f}) {unittype} \n")
        file.write(f"Divergence along the y axis: ({div[1]:.3f} +/- {diverr[1]:.3f}) {unittype} \n")
    except:
        file.write(f"Divergence along the x axis: {div[0]:.3f} {unittype} \n")
        file.write(f"Divergence along the y axis: {div[1]:.3f} {unittype} \n")
        file.write(f"(note: the errors on the divergences couldn't be estimated.")

print(f"Outputs written also at:")
print(outfilename)
print(f"")