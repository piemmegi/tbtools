# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre un file di stato JSON che contiene la codifica dei 
# file prodotti durante un beamtest del gruppo Insulab.
#
# Modalità d'uso: lo script va chiamato via python3 da linea di comando, senza argomenti. 
#
# Esempio:
# $ python3 statuscreator.py
#
# Nota bene. Questo file è l'unico che dovrebbe essere modificato ad un beamtest, 
# a meno di voler implementare nuove funzioni nei vari script. I punti da modificare 
# sono demarcati da "#!"
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import json, os

# ==========================================================================================
#                                  CODIFICA DEI FILE DATI
# ==========================================================================================
# Definisco il formato dei file dati da analizzare: "npz", "root", "h5py", "ascii"...
#datafiletype = 'root'                                                                     #!
#datafiletype = 'npz'                                                                      #!
datafiletype = 'h5'                                                                        #!

# Definisco se c'è un global tree da pre-aprire prima di trovare le colonne di dati
#globaltree = 't'                                                                          #!
globaltree = None                                                                          #!

# Definisco il path dove i file dati si trovano (può essere un sshfs, un /eos/, etc...
prename = '/eos/project/i/insulab-como/testBeam/TB_2023_10_T9_OREO/HDF5/run'               #!

# Definisco le distanze tra i rivelatori all'interno del setup. N.B. se il numero di silici
# cambiasse (rispetto a 4), le distanze da indicare devono sempre essere incrementali: 
# 1-2, 2-3, 3-4, 4-5 e così via. Se si usano altri oggetti al posto del cristallo, 
# ma comunque non si tratta di rivelatori al silicio, si deve lasciare zchry come variabile di riferimento
distances = {"zs1s2": 500,           # Sili 1 - Sili 2                                      #!
             "zs2calo": 72,          # Sili 2 - Calorimetri                                 #!
             "zcry": 49,             # Sili 2 - Cristallo                                   #!
             "relevantsili": 2}      # Qual è l'ultimo sili prima del cristallo? (1,2,...)  #!

# Definisco il numero di silici presenti nel setup
ndet = 2

# Definisco la vera e propria codifica dei file. In altre parole, metto IN ORDINE GIUSTO le variabili 
# che vengono salvate come colonne dei file ASCII. Per ogni tipo di variabile devo indicare il nome e 
# gli indici di colonna corrispondenti. N.B. al momento gli indici vanno inseriti "a mano".
data = {"xpos":         list(range(0,4)),         # Punto di hit nei silici in cm (x1, y1, x2...)      #!
        "nstrip":       list(range(4,8)),         # Numero di strip accese nel clustermax dei silici   #!
        "nclu":         list(range(8,12)),        # Numero di cluster accesi nei silici                #!
        "digiBase":     list(range(12,20)),       # Digi / Baseline dei rivelatori in ADC              #!
        "digiPH":       list(range(20,28)),       # Digi / PH dei rivelatori in ADC                    #!
        "digiTime":     list(range(28,36)),       # Digi / Tempo di picco dei rivelatori in ticks      #!
        "xinfo":        list(range(36,41)),       # Coordinate del goniometro                          #!
        "info_plus":    list(range(41,44)),       # Numero di spill e di passo angolare gonio          #!
        "ievent":       [44]}                     # Numero di evento                                   #!

# Definisco come le variabili sono salvate nei file. Ovvero: se nei codici uso sempre la variabile
# "ievent", qui indico se nei file dati la troverò salvata come tale, oppure come "Ievent", o come
# "eventNumber", etc.
#                 # Nome nei miei script                # Nome nei file dati
data_namesdict = {"xpos":                               "xpos",                         #!
                  "nstrip":                             "nstrip",                      #!
                  "nclu":                               "nclu",                         #!
                  "digiBase":                           "digiBase",                     #!
                  "digiPH":                             "digiPH",                       #!
                  "digiTime":                           "digiTime",                     #!
                  #"digiTime50":                         "time501742",                  #!
                  "xinfo":                              "xinfo",                        #!
                  "info_plus":                          "info_plus",                    #!
                  "ievent":                             "ievent"}                       #!


# Definisco il contenuto dei digitizer. Uso "None" per ogni canale vuoto.
digi_content = { # FIRST DIGI FROM HERE
                  0:        "Lead Glass",                    #!
                  1:        "APC",                           #!
                  2:        "Cherenkov 0",                   #!
                  3:        "Cherenkov 1",                   #!
                  4:        "George",                        #!
                  5:        "Ringo",                         #!
                  6:        "Paul",                          #!
                  7:        "John",                          #!
                  8:        None,                            #!
                  9:        None,                            #!
                 10:        None,                            #!
                 11:        None,                            #!
                 12:        None,                            #!
                 13:        None,                            #!
                 14:        None,                            #!
                 15:        None}                            #!
 

# Definisco la codifica della variabile "xinfo", cioè quella che contiene le coordinate del goniometro
gonio_content = {"rot":          0,             # Rotational angle (also known as "angular")   [urad]
                 "cradle":       1,             # Cradle angle                                 [urad]
                 "lateral":      2,             # Lateral movement                             [mm]
                 "lateral_big":  3,             # Lateral movement big                         [mm]
                 "vertical":     4}             # Vertical movement                            [mm]

# Definisco se le variabili sono salvate nei file come colonne nominali unpacked oppure se i file contengono solo grosse matrici da spacchettare
ifmatrix = False

# Definisco se applicare i tagli in numero di cluster nella selezione degli eventi (si dovrebbe fare SEMPRE, ammesso che i cluster siano salvati)
ifcuthit = False                                                                           #!

# Definisco quali silici considerare nel taglio in numero di cluster (dato che il taglio sarà del tipo su ch_nhit[:,:nsili_cut], si dovrebbe mettere un valore 2 se si vuole includere S1 (indici 0, 1), 4 per S2 (2, 3) e così via.
nsili_cut = -1                                                                             #!

# Definisco dove salvare le immagini prodotte in analisi online. N.B. questo non andrebbe 
# modificato, perché in teoria il setup.py crea già questa cartella di default! Però se si 
# vogliono salvare le immagini altrove, è virtualmente possibile farlo.
filefolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(filefolder)
imgloc = parentfolder + '/Images/'

# Definisco la cartella dove tutti i plot saranno salvati, per mostrarli sul sito (online monitor)
imgloc_www = '/eos/user/p/pmontigu/www/Images/Betatest/'                                  #!

# Definisco dove salvare i file prodotti temporaneamente. Vale lo stesso discorso di cui sopra.
temploc = parentfolder + '/Tempfiles/'

# Definisco dove sono salvati i file contenenti i tagli da applicare nell'analisi dati
subwww = '/eos/user/p/pmontigu/www/Subpages/Betatest/'
cutfilepaths = {"cutstatus_whichones_tbtools":    "cutstatus_whichones_tbtools.txt",         #!
                "cutstatus_whichvalues_tbtools":  "cutstatus_whichvalues_tbtools.txt",       #!
                "cutstatus_whichones_www":        subwww + "cutstatus_whichones_www.txt",    #!
                "cutstatus_whichvalues_www":      subwww + "cutstatus_whichvalues_www.txt"}  #!

# ==========================================================================================
#                                  CREAZIONE DEL FILE JSON
# ==========================================================================================
# Creo la stringa da convertire in JSON
jsonstring = {"datafiletype": datafiletype,      # Formato dei file
              "globaltree": globaltree,          # Se devo pre-aprire un tree
              "prename": prename,                # Path dove i file si trovano
              "distances": distances,            # Distanze tra i rivelatori nel setup
              "ndet": ndet,                      # Numero di silici presenti nel setup
              "data": data,                      # Codifica dei file dati
              "data_namesdict": data_namesdict,  # Codifica dei nomi delle variabili nei file dati
              "digi_content": digi_content,      # Codifica delle variabili del digitizer
              "gonio_content": gonio_content,    # Codifica delle variabili del goniometro
              "ifmatrix": ifmatrix,              # Se i dati sono matricioni oppure sono già spacchettati in colonne nominali
              "ifcuthit": ifcuthit,              # Se ho a disposizione il numero di cluster per ogni evento
              "nsili_cut": nsili_cut,            # Quanti rivelatori al silicio usare nel taglio in cluster
              "imgloc": imgloc,                  # Dove salvare le immagini
              "imgloc_www": imgloc_www,          # Dove salvare le immagini per plot sul sito
              "temploc": temploc,                # Dove salvare i file temporanei
              "cutfilepaths": cutfilepaths}      # Dove sono salvati i file per la selezione degli eventi

# Converto la stringa in JSON
json_string = json.dumps(jsonstring,indent=4)

# Scrivo il file
filename = os.path.join(filefolder,'statusfile.json')
with open(filename, 'w') as outfile:
    outfile.write(json_string)
    print(f"Status file written: ")
    print(filename)
    print(f"")
