# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i plot di correlazione tra due variabili, che possono
# essere:
#   - PH di canali del digitizer
#   - Numero di cluster nei silici
#   - Posizione di hit nei silici
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_corrplot.py nrun var1type ind1 var2type ind2 binstep axlims slope
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [var1type]      str, tipo della variabile 1 da correlare ("PH", "cluster" o "pos") 
#                     (default: "PH")
# 3 [ind1]          int, colonna della variabile 1 da correlare (default: 0)
# 4 [var2type]      str, tipo della variabile 2 da correlare ("PH", "cluster" o "pos") 
#                     (default: "PH")
# 5 [ind2]          int, colonna della variabile 2 da correlare (default: 1)
# 6 [binstep]       list of float, lista dei binning step per la correlazione di 1 e 2 
#                     (default: [25,25])
# 7 [axlims]        list of float, lista dei limiti degli assi del tipo [xmin, xmax,
#                     ymin, ymax], oppure "None" per nessun limite custom (default: "None")
# 8 [slope]         float, punto di half-way della scala colore, altrimenti "log",
#                     se si vuole una scala log, altirmenti "None" per nessuna scala custom
#                     (default: "None")
#
# Esempi:
# $     python3 tbeam_corrplot.py 680556 PH 1 PH 2 [25,25] None log
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'],data_namesdict['xpos'],data_namesdict['ievent'] ]
digiPH,ch_pos,eventNumber = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    ch_pos = ch_pos[cond,:]
    eventNumber = eventNumber[cond]

# ==========================================================================================
#                                  ESTRAZIONE DELLE VARIABILI
# ==========================================================================================
# Estraggo le variabili date in input allo script che definiscono quali variabili correlare
try:       # Primo tipo di variabile da correlare
    if (sys.argv[2] != 'PH') & (sys.argv[2] != 'cluster') & (sys.argv[2] != 'xpos'):
        # Variabile non riconosciuta
        print(f"Correlation variable n. 1 unrecognized. PH was chosen.")
        first_type = 'PH'
    else:
        # Variabile riconosciuta
        first_type = sys.argv[2]    
except:
    first_type = 'PH'

try:       # Primo indice di variabile da correlare
    first_ind = int(sys.argv[3])
except:
    first_ind = 0
    
try:       # Primo tipo di variabile da correlare
    if (sys.argv[4] != 'PH') & (sys.argv[4] != 'cluster') & (sys.argv[4] != 'xpos'):
        # Variabile non riconosciuta
        print(f"Correlation variable n. 1 unrecognized. PH was chosen.")
        second_type = 'PH'
    else:
        # Variabile riconosciuta
        second_type = sys.argv[4]    
except:
    second_type = 'PH'

try:       # Secondo indice di variabile da correlare
    second_ind = int(sys.argv[5])
except:
    second_ind = -1

# ==========================================================================================
# Estraggo le variabili che regolano il plot
try:       # Binning step per i due canali
    listbin = str2list(sys.argv[6],float)
except:
    listbin = [25,25]
    
try:       # Estremi per gli assi: xmin, xmax, ymin, ymax
    if sys.argv[7] == 'None':
        # Caso 1: non implemento limiti di asse
        listcut = None
    else:
        # Caso 2: implemento limiti specifici
        listcut = str2list(sys.argv[7],float)
except:
    listcut = None
    
try:       # Slope e scala (lineare o log) per l'istogramma 2D
    if sys.argv[8] == 'log':
        slope = 1
        iflog = True
    else:
        slope = float(sys.argv[8])
        iflog = False
except:
    slope = 1
    iflog = True
    
# ==========================================================================================
#                                  VARIABILI DI CORRELAZIONE
# ==========================================================================================
# Avendo definito quali variabili vanno correlate, le estraggo
if first_type == 'PH':
    var1 = digiPH[:,first_ind]
elif first_type == 'pos':
    var1 = ch_pos[:,first_ind]
else:
    var1 = ch_numhit[:,first_ind]
    
if second_type == 'PH':
    var2 = digiPH[:,second_ind]
elif second_type == 'pos':
    var2 = ch_pos[:,second_ind]
else:
    var2 = ch_numhit[:,second_ind]

# Seleziono quelle che non escono dai limiti prefissati (se esistono)
if listcut is not None:
    # Definisco il taglio
    cond_cut = (var1 >= listcut[0]) & (var1 <= listcut[1]) & (var2 >= listcut[2]) & (var2 <= listcut[3])

    # Li applico
    var1 = var1[cond_cut]
    var2 = var2[cond_cut]
    
# ==========================================================================================
#                                  HISTO 2D DI CORRELAZIONE
# ==========================================================================================
# Creo L'istogramma 2D di correlazione
colmap = 'jet'      # Mappa di colore
histo2D, binsX, binsY = truehisto2D(var1,var2,nbinsX = listbin[0], nbinsY = listbin[1], 
                                    ifstepX = True, ifstepY = True)

# Lo plotto
fig, ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binsX,binsY,histo2D,colmap,slope,first_type + f' ch. {first_ind}',
               second_type + f' ch. {second_ind}',textfont,iflog = iflog)
if listcut is not None:
    ax.set_xlim(listcut[0:2])
    ax.set_ylim(listcut[2:])
fig.suptitle('Correlation map: ' + first_type + ' vs ' + second_type, fontsize = 3*textfont)
fig.set_tight_layout('tight')
outfilename = os.path.join(imgloc, 'run' + str(run) + f'_corrmap_' + first_type + f'_{first_ind}_' + second_type + f'_{second_ind}' + filetype)
fig.savefig(outfilename, dpi = filedpi)
plt.show()
plt.close(fig)

print(f"")
print(f"The correlation plot has been produced and saved.")
print(f"")