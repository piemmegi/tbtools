# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i beam profiles di ogni rivelatore al silicio.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_siliplots.py nrun binstep edgeL edgeU
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [binstep]       float, binning step per i profile plot, in cm (default: 0.1)
# 3 [edgeL]         float, limite inferiore per gli istogrammi, oppure "None" se non si
#                     vogliono usare limiti custom (default: -2)
# 4 [edgeU]         float, limite superiore per gli istogrammi, oppure "None" se non si
#                     vogliono usare limiti custom (default: +12.5)
#
# Esempio di chiamata:
# $     python3 tbeam_siliplots.py 720110 0.1 -3 3
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (posizioni di hit dei silici)
veclist = [ data_namesdict['xpos']]
ch_pos = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    ch_pos = ch_pos[cond,:]

# ==========================================================================================
#                           PLOT DEI PROFILI DI FASCIO
# ==========================================================================================
# Definisco le stringhe che saranno usate per i plot
dnames = []                        # Qui inserisco una cosa tipo "x, S1", "y, S1", "x, S2" etc
for i in range(ndet*2):
    ind = int(np.floor(i/2))+1
    if i % 2 == 0:
        dnames.append("x (C" + str(ind) + ")")
    else:
        dnames.append("y (C" + str(ind) + ")")

# Definisco le variabili utili per la produzione degli istogrammi
try:
    nbinspos = float(sys.argv[2])        # Binning nel profile plot posizionale in cm
except:
    nbinspos = 0.01                      # Default value in cm
    
try:
    if sys.argv[3] == 'None':
        edgeL = -2
    else:
        edgeL = float(sys.argv[3])
except:
    edgeL = -2

try:
    if sys.argv[4] == 'None':
        edgeU = 12.5
    else:
        edgeU = float(sys.argv[4])
except:
    edgeU = 12.5

# Procedo su ogni direzione con la ricostruzione: x,y.
fig = plt.figure(figsize = dimfigbig)

for i in range(2*ndet):
    # Produco il profile plot canonico
    histo_pos, bins_pos = truehisto1D(ch_pos[:,i],nbinspos,edgeL,edgeU,ifstep = True)

    # Aggiungo il plot dei profili del fascio
    ax = fig.add_subplot(ndet,2,i+1)
    histoplotter1D(ax,bins_pos,histo_pos,'Hit position [cm]','Counts',
                       leglabel = dnames[i],legloc = 'upper right',textfont = 1.5*textfont)
    ax.legend(loc='upper right',fontsize = 1.5*textfont, framealpha=1)

time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f'Beam profile ({datetime.date.today()}, {time_readable})', fontsize = 2*textfont)
#fig.tight_layout(rect=[0, 0.03, 1, 0.95])
fig.set_tight_layout('tight')

# Salvo i plot
savefile = os.path.join(imgloc,'run' + str(run) + '_siliplots_profiles' + filetype)
savefile_www = os.path.join(imgloc_www,'lastrun_siliplots_profiles' + filetype)
savefile_www2 = os.path.join(imgloc_www,'lastrun_siliplots_profiles' + filetype_www)
fig.savefig(savefile,dpi = filedpi)
fig.savefig(savefile_www,dpi = filedpi_www)
fig.savefig(savefile_www2,dpi = filedpi_www)
plt.show()
plt.close(fig)

print(f"")
print(f"Beam profile plots have been created, merged and saved.\n")