# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i plot necessari per ricostruire i piani e assi di un
# cristallo orientato esposto a un fascio di particelle di alta energia.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_crystalscan.py nrun scantype PHind PHmin PHstep goniostep goniobin 
#                                      slope crystalid cutloc
#
# Argomenti:
#  1 [nrun]         int, numero della run da analizzare
#  2 [scantype]     str, tipo di scan eseguito ("rot", "cradle" o "lateral") (default: "rot")
#  3 [PHind]        int, indice della variabile del digitizer di cui si usa la PH come 
#                     riferimento durante l'allineamento (default: 0)
#  4 [PHmin]        int, minima PH richiesta a un evento per essere analizzato,
#                     oppure "None", se non si vogliono applicare tagli (default: "None")
#  5 [PHstep]       int, passo per gli istogrammi in PH (default: 25)
#  6 [goniostep]    int, passo dello scan angolare settato nel goniometro in urad (default: 250)
#  7 [goniobin]     int, passo per gli istogrammi in angolo espressi in urad (default: 250)
#  8 [slope]        float, punto di half-way della scala colore, altrimenti "log",
#                     se si vuole una scala log, altirmenti "None" per nessuna scala custom
#                     (default: "None")
#  9 [crystalid]    str, un identificativo che riconosce il cristallo sotto test e serve a
#                     distinguerlo, in caso si facciano scan su più cristalli (default: "0")
# 10 [cutloc]       str, tipo di tagli da applicare ("tbtools" per il file in locale,
#                     "www" per il file prodotto dal monitor online, "None" per nessun 
#                     taglio) (default: "None")
#
# Esempi:
# $     python3 tbeam_crystalscan.py 520111 rot 1 100 100 250 250 None pbf2
# $     python3 tbeam_crystalscan.py 710166 rot 1 100 25 1000 250 None pbwo4
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'], data_namesdict['digiTime'], data_namesdict['xpos'],
            data_namesdict['ievent'], data_namesdict['info_plus'], data_namesdict['xinfo'] ]
digiPH,digitime,ch_pos,eventNumber,info_plus,xinfo = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)
goniosteps = info_plus[:,1]

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    digitime = digitime[cond,:]
    ch_pos = ch_pos[cond,:]
    eventNumber = eventNumber[cond]
    goniosteps = goniosteps[cond]
    xinfo = xinfo[cond,:]


# ==========================================================================================
#                              ESTRAZIONE DELLE VARIABILI IN INPUT
# ==========================================================================================
# Estraggo tutte le variabili date in input a questo script.

# Tipo di scan angolare
try:
    if (sys.argv[2] != 'rot') & (sys.argv[2] != 'cradle') & (sys.argv[2] != 'lateral'):
        # Variabile non riconosciuta
        print(f"Scan type (argv[2]) not recognized and set to rot.")
        scantype = 'rot'
    else:
        # Variabile riconosciuta
        scantype = sys.argv[2]
except:
    scantype = 'rot'

scanind = gonio_content[scantype]

# Calcolo l'indice del grado di libertà che NON sto usando; non ne ho bisogno in lateral
if scantype == 'rot':
    # Se ho uno scan rotazionale, salvo il grado cradle
    not_scanind = gonio_content['cradle']
elif scantype == 'cradle':
    # Se lo scan è cradle, salvo il grado angolare
    not_scanind = gonio_content['rot']
else:
    # Se lo scan è lat, setto a non-valore
    not_scanind = None
    
# Indice della variabile di cui la PH verrà esaminata
try:
    PHrefind = int(sys.argv[3])
except:
    PHrefind = 0

# Minima PH per la selezione degli eventi
try:
    PHthr = int(sys.argv[4])
except:
    PHthr = None
    
# Binning step in PH
try:
    PHstep = int(sys.argv[5])
except:
    PHstep = 25
    
# Passo dello scan angolare
try:
    gonioscanstep = int(sys.argv[6])
except:
    gonioscanstep = 250
    
# Binning step in angoli di incidenza
try:
    thetastep = int(sys.argv[7])
except:
    thetastep = 250
    
# Slope per gli istogrammi 2D
try:
    if sys.argv[8] == 'None':
        iflog = True
        slope = 1
    else:
        iflog = False
        slope = int(sys.argv[8])
except:
    iflog = True
    slope = 1

# Identificativo del cristallo
try:
    crystalid = sys.argv[9]
except:
    crystalid = '0' 

# Tipo di tagli da applicare sul cristallo
try:
    if (sys.argv[10] == "tbtools") | (sys.argv[10] == "www"):
        # Caso 1: keyword riconosciuta
        cutloc = sys.argv[10]
    else:
        # Caso 2: keyword non riconosciuta, ricadi nel default
        cutloc = None
except:
    # Caso 3: keyword non definita, ricadi nel default
    cutloc = None

# ==========================================================================================
#                          DEFINIZIONE E APPLICAZIONE DEI TAGLI
# ==========================================================================================
# Definisco i tagli da applicare, se richiesto.
if cutloc is not None:
    # Ricavo dallo statusfile il path del file con i tagli
    whichcuts_path = cutfilepaths["cutstatus_whichones_" + cutloc]
    whichvalues_path = cutfilepaths["cutstatus_whichvalues_" + cutloc]

    # Uso la funzione custom per costruire la condizione di taglio globale
    globalcut, cutoutputdict = cutcalculator(PHrefind,digiPH,digitime,ch_pos,distances['zs1s2'],whichcuts_path,whichvalues_path)
else:
    globalcut = np.ones((np.shape(digiPH)[0],),dtype=bool)

# Aggiungo ai tagli quello in PH del canale da analizzare, che è definito esternamente
if PHthr is not None:
    PHcond = (digiPH[:,PHrefind] >= PHthr)
else:
    PHcond = np.ones((np.shape(digiPH)[0],),dtype=bool)

totcond = globalcut & PHcond

# Applico il taglio
digiPH = digiPH[totcond,:]
ch_pos = ch_pos[totcond,:]
goniosteps = goniosteps[totcond]
xinfo = xinfo[totcond,:]

# ==========================================================================================
#                              PRIMO PLOT: HISTO 2D (n. step vs PH)
# ==========================================================================================
# Definisco alcune variabili utili per l'analisi successiva: mappa di colore, tipo di metodo
# per la proiezione degli istogrammi 2D.
colmap = 'jet'
pr = 'mean'

# Definisco i vettori da guardare e i binning steps
goniosvalues = np.unique(goniosteps)
goniobin = 1
refPHs = digiPH[:,PHrefind]

# Produco l'istogramma    
histo2D, binsX, binsY = truehisto2D(goniosteps,refPHs,nbinsX = goniobin, nbinsY = PHstep, 
                                    ifstepX = True, ifstepY = True)

# Produco anche la proiezione del plot sull'asse orizzontale
truex, histosum, _ = histoproj(histo2D, binsX, binsY, axis = 'x', projtype = pr)

# Procedo con il plot, se richiesto
ifplot1 = False
if ifplot1:
    # Creo la figura e la salvo
    fig, ax = plt.subplots(figsize = dimfig)
    histoplotter2D(ax,binsX,binsY,histo2D,colmap,slope,f'Number of goniometer step',f' PH ch. {PHrefind}',textfont,iflog)
    ax.plot(truex,histosum,color = 'black', marker = '.', linestyle = '--', markersize = 3, linewidth = 2, 
            label = 'Average PH (profile plot)')
    ax.legend(loc = 'upper right',fontsize = textfont)
    fig.suptitle(scantype + f' scan', fontsize = 3*textfont)
    fig.set_tight_layout('tight')
    fig.savefig(os.path.join(imgloc,f'run{run}_crystal_{crystalid}_gonioscan1' + filetype),dpi = filedpi)
    plt.show()
    plt.close(fig)

# ==========================================================================================
#                       SECONDO PLOT: HISTO 2D (coordinata angolare vs PH)
# ==========================================================================================
# Produco l'istogramma 2D goniometro vs PH
goniopositions = xinfo[:,scanind]
histo2D, binsX, binsY = truehisto2D(goniopositions,refPHs,nbinsX = gonioscanstep, nbinsY = PHstep, ifstepX = True, ifstepY = True)

# Produco anche la proiezione del plot sull'asse orizzontale
truex, histosum, _ = histoproj(histo2D, binsX, binsY, axis = 'x', projtype = pr)

# Stampo il punto di massimo assoluto della proiezione
truex_max = truex[np.argmax(histosum)]
truex_min = truex[np.argmin(histosum)]

print(f"")
print(f"Minimum average PH of the detector occurred at loc {truex_min:.2f} (urad/mm) during the scan")
print(f"Maximum average PH of the detector occurred at loc {truex_max:.2f} (urad/mm) during the scan")

# Preparo la stringa per l'asse x
if (scantype == 'rot') | (scantype == 'cradle'):
    xstring = r'Goniometer angle $[\mu rad]$'
elif (scantype == 'lateral'):
    xstring = f"Goniometer center [mm]"
    
# Plotto istogramma e proiezione
fig, ax = plt.subplots(figsize = dimfig)    
histoplotter2D(ax,binsX,binsY,histo2D,colmap,slope,xstring,f'PH ch. {PHrefind}',textfont,iflog)
ax.plot(truex,histosum,color = 'black', marker = '.', linestyle = '--', markersize = 3, linewidth = 2, 
        label = 'Average PH (profile plot)')
ax.legend(loc = 'upper right',fontsize = textfont)
fig.suptitle(scantype + f' scan', fontsize = 3*textfont)
fig.set_tight_layout('tight')
fig.savefig(os.path.join(imgloc,f'run{run}_crystal_{crystalid}_gonioscan2' + filetype),dpi = filedpi)
plt.show()
plt.close(fig)

# Estraggo i limiti per il prossimo plot
xlims = [np.min(binsX),np.max(binsX)]

# Salvo in variabili separate le coordinate di questo scan angolare. Servono sia la coordinata 
# di variazione primaria sia quella che rimane ferma durante lo scan, per poter ricostruire poi
# lo stereogramma del cristallo.
if scantype == 'rot':
    # Opzione di scan angolare/rotazionale
    gonio_ang_met1 = copy.deepcopy(truex)
    gonio_cradle_met1 = np.mean(xinfo[:,not_scanind])    # Faccio la media dei valori misurati

elif scantype == 'cradle':
    # Opzione di scan cradle
    gonio_cradle_met1 = copy.deepcopy(truex)
    gonio_ang_met1 = np.mean(xinfo[:,not_scanind])    # Faccio la media dei valori misurati

gonio_PH_met1 = copy.deepcopy(histosum)

# ==========================================================================================
#                      CALCOLO DEGLI ANGOLI DI INCIDENZA DEGLI ELETTRONI
# ==========================================================================================
if (scantype != 'lateral'):
    # Estraggo le sole coordinate di posizione che ci interessano: i due silici prima del cristallo
    posind = distances['relevantsili']             # Ultimo silicio prima del cristallo (1,2,3...)
    clean_pos = ch_pos[:,(2*posind-4):(2*posind)]  # x1,y1,x2,y2

    # Estraggo le distanze nel setup
    zs1s2 = distances['zs1s2'] # cm
    zcry = distances['zcry']   # cm
    zs1cry = zs1s2+zcry        # cm

    # Calcolo l'angolo di incidenza delle particelle in ingresso al cristallo
    crys_angles = np.zeros((clean_pos.shape[0],2))

    for j in range(2):
        # j indica se sto guardando x oppure y. In entrambi i casi, seleziono dai silici gli eventi di interesse
        refind = j % 2                       # Vale 0 quando lavoro su x e 1 su y
        cleanpos_a = clean_pos[:,refind]     # Accoppio 0 e 2 (x dei primi 2 silici) o 1 e 3 (y)
        cleanpos_b = clean_pos[:,refind+2]

        # Ricostruisco l'angolo di incidenza delle traiettorie
        crys_angles[:,j] = np.arctan((cleanpos_b - cleanpos_a)/zs1s2)   # arctan (cm/cm) = RADIANTI

    # Passo l'angolo di incidenza in urad
    crys_angles = crys_angles*1e6

    # Ripulisco gli angoli che non sono stati calcolati correttamente
    conf = 5
    condcut = (np.isinf(crys_angles) == False) & (np.isnan(crys_angles) == False) \
                & (crys_angles >= np.nanmean(crys_angles) - conf*np.nanstd(crys_angles)) \
                & (crys_angles <= np.nanmean(crys_angles) + conf*np.nanstd(crys_angles))

    # Calcolo la somma di angolo di incidenza e angolo del goniometro.
    incid_angles_x = crys_angles[:,0] + xinfo[:,gonio_content['rot']]
    incid_angles_y = crys_angles[:,1] + xinfo[:,gonio_content['cradle']]

# ==========================================================================================
#                       TERZO PLOT: HISTO 2D (angolo di incidenza vs PH)
# ==========================================================================================
# Eseguo l'istogramma 2D lungo la coordinata di interesse
if (scantype == 'rot') | (scantype == 'lateral'):
    histovec = incid_angles_x
    histocond = condcut[:,0]
else:
    histovec = incid_angles_y
    histocond = condcut[:,1]

histo2D, binsX, binsY = truehisto2D(histovec[histocond],refPHs[histocond],nbinsX = thetastep, nbinsY = PHstep,
                                    ifstepX = True, ifstepY = True)

# Produco anche la proiezione del plot sull'asse orizzontale
truex, histosum, _ = histoproj(histo2D, binsX, binsY, axis = 'x', projtype = pr)

# Plot dell'istogramma e della proiezione
fig, ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binsX,binsY,histo2D,colmap,slope,r'(Incidence angle + ' + scantype + ' angle) ' + r"$[\mu rad]$",
               f' PH ch. {PHrefind}',textfont,iflog)
ax.plot(truex,histosum,color = 'black', marker = '.', linestyle = '--', markersize = 3, linewidth = 2, 
        label = 'Average PH (profile plot)')
ax.set_xlim(xlims)
ax.legend(loc = 'upper right',fontsize = textfont)
fig.suptitle(scantype + f' scan', fontsize = 3*textfont)
fig.set_tight_layout('tight')
fig.savefig(os.path.join(imgloc,f'run{run}_crystal_{crystalid}_gonioscan3' + filetype),dpi = filedpi)
plt.show()
plt.close(fig)

# Salvo in variabili separate le coordinate di questo scan angolare. Servono sia la coordinata 
# di variazione primaria sia quella che rimane ferma durante lo scan, per poter ricostruire poi
# lo stereogramma del cristallo.
if (scantype == 'rot') | (scantype == 'lateral'):
    # Opzione di scan angolare/rotazionale o laterale
    gonio_ang_met2 = copy.deepcopy(truex)
    gonio_cradle_met2 = np.mean(xinfo[:,not_scanind])    # Faccio la media dei valori misurati

elif scantype == 'cradle':
    # Opzione di scan cradle
    gonio_cradle_met2 = copy.deepcopy(truex)
    gonio_ang_met2 = np.mean(xinfo[:,not_scanind])       # Faccio la media dei valori misurati

gonio_PH_met2 = copy.deepcopy(histosum)

# ==========================================================================================
#                                  MERGE DEI PLOT
# ==========================================================================================
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + f'run{run}_crystal_{crystalid}_crystalscan' + filetype
if os.path.exists(mergedfile):
    os.remove(mergedfile)

allfiles = glob.glob(imgloc + f'run{run}_crystal_{crystalid}_gonioscan*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()

print(f"")
print(f"All the goniometer scan histograms have been created, merged and saved.")
print(f"")

# ==========================================================================================
#                                  SALVATAGGIO DEI DATI
# ==========================================================================================
# Per creare lo stereogramma mi serve salvare il profilo di punti creato in questo script
if (scantype != 'lateral'):
    filename = temploc + f'crystalstereo_run{run}_crystal_{crystalid}.npz'
    np.savez_compressed(filename, gonio_ang_met1 = gonio_ang_met1,   # Metodo 1: angoli coperti in rot
                        gonio_cradle_met1 = gonio_cradle_met1,       # Metodo 1: angoli coperti in cradle
                        gonio_PH_met1 = gonio_PH_met1,               # Metodo 1: PH media in ogni punto
                        gonio_ang_met2 = gonio_ang_met2,             # Metodo 2: angoli coperti in rot
                        gonio_cradle_met2 = gonio_cradle_met2,       # Metodo 2: angoli coperti in cradle
                        gonio_PH_met2 = gonio_PH_met2,               # Metodo 2: PH media in ogni punto
                        scantype = scantype)                         # Tipo di scan: rot o cradle

    print(f"All the values of the scan have been saved and prepared for the stereogram.")
    print(f"")