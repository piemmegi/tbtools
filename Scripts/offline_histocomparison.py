# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è comparare gli istogrammi acquisiti in varie run di presa dati
# e analizzati mediante "tbeam_singledigiwfc.py"
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 offline_histocomparison.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *
from beamfunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]

# ==========================================================================================
#                                  DEFINIZIONE DEGLI INPUT
# ==========================================================================================
# Definisco il canale che voglio confrontare
digich = 1

# Definisco le run da guardare 
runs = [680555,680560,680557,680564,680556]
outfilenames = [os.path.join(temploc,f'run{run}_PHcut_ch_{digich}.npz') for run in runs] 

# Definisco i colori e nomi per i plot
angles = [0,4,8,16,50]
datanames = [f"{angle} mrad" for angle in angles]
datacolors = ['red','darkorange','darkgreen','darkturquoise','mediumblue']

# ==========================================================================================
#                                  CARICAMENTO DEI DATI
# ==========================================================================================
# Per ogni file estraggo i dati corrispondenti
all_PHs = []
all_means = np.zeros((len(runs),))
all_stds = np.zeros_like(all_means)

for i,el in enumerate(outfilenames):
    with np.load(el) as file:
        thisPH = file['cutPH']
        all_PHs.append(thisPH)
        all_means[i] = np.mean(thisPH)
        all_stds[i] = np.std(thisPH)/np.sqrt(np.shape(thisPH))

# ==========================================================================================
#                                         ISTOGRAMMI
# ==========================================================================================
# Mostro gli istogrammi in un'unica figura
nbins_PH = 50
fig,ax = plt.subplots(figsize = dimfig)

for i,el in enumerate(outfilenames):
    # Istogramma i-esimo
    histo_PH, bins_PH = truehisto1D(all_PHs[i],nbins_PH,ifstep=True,edgeL=0)
    histoplotter1D(ax,bins_PH,histo_PH,'PH','Counts',datanames[i],'best',textfont,hcolor=datacolors[i])

ax.set_yscale('log')
fig.set_tight_layout('tight')
fig.savefig(imgloc + 'crystalcomparison' + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'crystalcomparison' + filetype,dpi = filedpi_www)
fig.savefig(imgloc_www + 'crystalcomparison' + filetype_www,dpi = filedpi_www)
plt.show()
plt.close(fig)

# ==========================================================================================
#                                      PLOT DELLE MEDIE
# ==========================================================================================
# Mostro gli istogrammi in un'unica figura
fig,ax = plt.subplots(figsize = dimfig)

for i,el in enumerate(outfilenames):
    # Istogramma i-esimo
    ax.errorbar(np.array(angles),all_means,all_stds,label='Data',color='mediumblue',
            linestyle = '--', linewidth = 1, marker = '.', markersize = 5, capsize = 5)
    
ax.set_xlabel('Angle from axis [mrad]',fontsize = textfont)
ax.set_ylabel('Average LG PH',fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(imgloc + 'crystalmeancomparison' + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'crystalmeancomparison' + filetype,dpi = filedpi_www)
fig.savefig(imgloc_www + 'crystalmeancomparison' + filetype_www,dpi = filedpi_www)
plt.show()
plt.close(fig)

# Chiudo lo script
print(f"")
print(f"Job done!\n")