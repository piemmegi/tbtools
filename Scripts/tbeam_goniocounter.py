# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è verificare la posizione iniziale e finale del goniometro
# durante una run.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_goniocounter.py nrun
#
# Argomenti:
# 1 [nrun]:         int, numero della run da analizzare
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster)
veclist = [data_namesdict['xinfo'],data_namesdict['info_plus']]
xinfo, info_plus = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)
goniostep = info_plus[:,1]

# ==========================================================================================
#                                  POSIZIONE
# ==========================================================================================
# Stampo la prima e ultima posizione del goniometro
rot_first, rot_last       = xinfo[[0,-1],gonio_content["rot"]]
cradle_first, cradle_last = xinfo[[0,-1],gonio_content["cradle"]]
lat_first, lat_last       = xinfo[[0,-1],gonio_content["lateral"]]
latbig_first, latbig_last = xinfo[[0,-1],gonio_content["lateral_big"]]
vert_first, vert_last     = xinfo[[0,-1],gonio_content["vertical"]]

print(f"Initial goniometer configuration:")
print(f"\t Rot: {rot_first} urad")
print(f"\t Cradle: {cradle_first} urad")
print(f"\t Lat: {lat_first} mm")
print(f"\t Lat big: {latbig_first} mm")
print(f"\t Vert: {vert_first} mm\n")

print(f"Current goniometer configuration:")
print(f"\t Rot: {rot_last} urad")
print(f"\t Cradle: {cradle_last} urad")
print(f"\t Lat: {lat_last} mm")
print(f"\t Lat big: {latbig_last} mm")
print(f"\t Vert: {vert_last} mm\n")

# ==========================================================================================
#                                  PASSO
# ==========================================================================================
# Stampo il numero di step del goniometro
step_first = goniostep[0]+1
step_last = goniostep[-1]+1

print(f"Initial number of step of the goniometer (counting from 1): {step_first}")
print(f"Current number of step of the goniometer (counting from 1): {step_last}\n")

# ==========================================================================================
#                                  WRITEOUT
# ==========================================================================================
# Definisco dove printare l'output
outfilename = os.path.join(imgloc_www + 'goniocounter_out.txt')

# Scrivo l'output
print(f"Writing the output file... \n")
with open(outfilename,'w', encoding="utf-8") as file:
    # Timestamp
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    file.write(f"Current time: {datetime.date.today()}, {time_readable} \n")

    # Numero di run
    file.write(f"Run number: {run} \n\n")

    # Status del goniometro
    file.write("Initial goniometer configuration:\n")
    file.write(f"\t Rot: {rot_first} urad\n")
    file.write(f"\t Cradle: {cradle_first} urad\n")
    file.write(f"\t Lat: {lat_first} mm\n")
    file.write(f"\t Lat big: {latbig_first} mm\n")
    file.write(f"\t Vert: {vert_first} mm\n\n")

    file.write(f"Current goniometer configuration:\n")
    file.write(f"\t Rot: {rot_last} urad\n")
    file.write(f"\t Cradle: {cradle_last} urad\n")
    file.write(f"\t Lat: {lat_last} mm\n")
    file.write(f"\t Lat big: {latbig_last} mm\n")
    file.write(f"\t Vert: {vert_last} mm\n\n")

    # Status dello scan
    print(f"Initial number of step of the goniometer: {step_first}\n")
    print(f"Current number of step of the goniometer: {step_last}\n")

print(f"File written:")
print(outfilename)