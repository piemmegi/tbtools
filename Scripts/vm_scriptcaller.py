# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è richiamare alcuni script di base che possono essere usati in
# analisi dati online in una qualsiasi run di test beam 
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
#
# $     conda activate pypmg
# $     python3 vm_scriptcaller.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import glob                         # Per manipolazione dei path
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import subprocess                   # Per chiamare python come script esterno
import PyPDF2                       # Per unire i PDF alla fine
import json                         # Per leggere i file JSON
import time

# ==========================================================================================
#                                   INPUT DEFINITION
# ==========================================================================================
# Definisco che script chiamare per ogni giro. N.B. "#!" means "marked for removal after 
# the running beamtest"
if_acounter = True             # Conteggio del numero di eventi acquisiti e buoni
if_digiplots = True            # Plot dei canali del digitizer
if_digiplots_timediff = True   # Plot delle differenze di tempo di picco dei canali del digitizer
if_divergence = True           # Plot della divergenza del fascio
if_efficiency = True           # Plot di una mappa di efficienza
if_goniocounter = True         # Status del goniometro
if_siliplots = True            # Plot del profilo del fascio
if_singledigiwfc = False       # Plot di un istogramma con tagli e fit

# ==========================================================================================
#                                   CALL PRELIMINARI
# ==========================================================================================
# Ricavo la cartella di lavoro
thisfolder = os.path.dirname(os.path.abspath(__file__))

# Chiamo lo status creator per sicurezza
print(f"")
print(f"Job starting... \n")
print(f"Calling the status file creator... \n")
statuspy = 'statuscreator.py'
subprocess.call(['python3', statuspy])

# Chiamo il cut configurator per sicurezza
print(f"Calling the cut configurator... \n")
configuratorpy = 'cutconfigurator.py'
subprocess.call(['python3',configuratorpy])
print(f"Done!")

# ==========================================================================================
#                                   CALL DI OGNI SCRIPT
# ==========================================================================================
# Ricavo dallo status creator il path dei file da analizzare
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
prename = statusdata['prename']

# Definisco una stringa per i printout tra uno script e l'altro
ProcessEndString = '\nJob done! \n\n' + 50*'-' + '\n'

# Richiamo gli script di interesse. Continuo PERPETUAMENTE a farlo, 
# aggiornando ogni volta l'ultima run disponibile.
sleeptime_minutes = 1
minutes_to_seconds = 60
while True:
    try:
        # Ricavo l'ultimo file disponibile nella cartella dei dati
        lastfile = glob.glob(prename + '*')[-1]                  # Nome completo dell'ultimo file script
        numlastrun = lastfile.split('run')[-1].split('.')[0]     # Estraggo solo il numero dopo "run" nel nome completo
        for _ in range(3): print('*'*75 + '\n')
        print(f"Analyzing run n. {numlastrun}")
        
        # ==========================================================
        #                       TBEAM SCRIPTS
        # ==========================================================
        if if_acounter:
            # python3 tbeam_acounter.py nrun
            print(f"Calling tbeam_acounter.py ...")
            subprocess.call(['python3','tbeam_acounter.py',numlastrun])
            print(ProcessEndString)

        if if_digiplots:
            # python3 tbeam_digiplots.py nrun PHstep ifPHlog timestep iftimelog ieventstep ifprofile
            print(f"Calling tbeam_digiplots.py ...")
            subprocess.call(['python3','tbeam_digiplots.py',numlastrun,'100','True','75','False','200','None','False'])
            print(ProcessEndString)

        if if_digiplots_timediff:
            # python3 tbeam_digiplots_timediff.py nrun timerefch timenbins timelims iftimelog PHnbins
            print(f"Calling tbeam_digiplots_timediff.py ...")
            subprocess.call(['python3','tbeam_digiplots_timediff.py',numlastrun,'0','300','[-150,-80]','False','250'])
            print(ProcessEndString)

        if if_divergence:
            # python3 tbeam_divergence.py nrun binstep ifstep
            print(f"Calling tbeam_divergence.py ...")
            subprocess.call(['python3','tbeam_divergence.py',numlastrun,'0.00001','True'])
            print(ProcessEndString)

        if if_efficiency:
            # python3 tbeam_efficiency.py nrun cut_type cut_inds cut_thr cut_coupling nbins_pos slope weight axlims
            print(f"Calling tbeam_efficiency.py ...")
            subprocess.call(['python3','tbeam_efficiency.py',numlastrun,'PH','[4,5]','[100,100]','or','0.1','0.5'])
            print(ProcessEndString)

        if if_goniocounter:
            # python3 tbeam_goniocounter.py nrun
            print(f"Calling tbeam_goniocounter.py ...")
            subprocess.call(['python3','tbeam_goniocounter.py',numlastrun])
            print(ProcessEndString)

        if if_siliplots:
            # python3 tbeam_siliplots.py nrun binstep edgeL edgeU
            print(f"Calling tbeam_siliplots.py ...")
            subprocess.call(['python3','tbeam_siliplots.py',numlastrun,'0.1'])
            print(ProcessEndString)

        if if_singledigiwfc:
            # python3 tbeam_singledigiwfc.py nrun cutloc digich PHstep ifPHlog fittype fitext Ebeam
            print(f"Calling singledigiwfc.py ...")
            subprocess.call(['python3','tbeam_singledigiwfc.py',numlastrun, 'www', '1', '100', 'True', 'None', 'None', 'None'])
            print(ProcessEndString)

        # ==========================================================
        #                      OFFLINE SCRIPTS
        # ==========================================================
        # Chiamo eventuali script di analisi offline: ...

        # ==========================================================
        #                         WAIT
        # ==========================================================
        # Aspetto qualche tempo
        print(f"Going to sleep for {sleeptime_minutes} minutes...")
        time.sleep(sleeptime_minutes*minutes_to_seconds)
        print(f"Waking up again!")
        
    except KeyboardInterrupt:
        print(f"Ctrl+C command invoked: aborting the script...")
        break

    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=}")
        print(f"Restarting the cycle...")

print(f"Job aborted!")
