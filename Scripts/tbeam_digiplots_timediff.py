# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre gli spettri di differenza di tempo di picco tra
# tutti i canali del digitizer e un canale di riferimento, oltre che le correlazioni
# tra la PH e la differenza di tempo di picco.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_digiplots_timediff.py nrun timerefch timenbins timelims iftimelog PHnbins
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [timerefch]     int, indice del canale di digitizer da considerare come riferimento
#                     nel calcolo delle differenze (default: 0)
# 3 [timenbins]     int, numero di bin per gli spettri in tempo di picco (default: 50)
# 4 [timelims]      list of int, estremi di plot degli spettri in tempo, oppure "None",
#                     se non si vogliono usare limiti custom (default: "None")
# 5 [iftimelog]     str, se fare gli spettri di tempo di picco in scala log ("True", "False")
#                     (default: "False")
# 6 [PHnbins]       int, numero di bin step per gli spettri in PH (default: 100)
#
# 
# Esempi:
# $     python3 tbeam_digiplots_timediff.py 680556 0 10 [-500,0] True 25
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiTime'], data_namesdict['digiPH'], data_namesdict['ievent'] ]
digitime, digiPH, eventNumber = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    digitime = digitime[cond,:]
    eventNumber = eventNumber[cond]

# ==========================================================================================
#                                  PLOT DI CONTROLLO: TEMPO
# ==========================================================================================
# Definisco alcune variabili preliminari
nplot_perside = 3
nplots = nplot_perside**2
ncols = np.shape(digiPH)[1]                 # Numero di canali
nfigs = int(np.ceil(ncols/nplots))          # Numero di figure da creare
deflims = [-1000, 1000]                     # Taglio le differenze oltre questi valori

try:
    timerefch = int(sys.argv[2])            # Indice di riferimento del digitizer
except:
    timerefch = 0

try:
    nbins_time = int(sys.argv[3])           # Binning per spettri in tempo
except:
    nbins_time = 50                         # Default value

try:
    if (sys.argv[4] == "None"):
        timelims = None
    else:
        timelims = str2list(sys.argv[4],int)
except:
    timelims = None

try:
    iflogtime = (sys.argv[5] == 'True')     # Se usare la scala log
except:
    iflogtime = False
    
# Per ogni blocco di figure procedo a creare gli istogrammi di frequenze di tempo e li raggruppo in figure 3x3
for i in range(nfigs):
    # Creo la figura
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplots):
        # Se ho finito le variabili, esco dal ciclo
        plotind = j+i*nplots    # Questa variabile punta a ogni iterazione su una colonna diversa di digiPH
        if plotind >= ncols:
            break

        # Se il canale da guardare è vuoto, proseguo oltre
        if (digi_content[str(plotind)] is None) | (plotind == timerefch):
            continue
        
        # Creo l'istogramma di frequenze e lo rappresento
        timediff = digitime[:,plotind] - digitime[:,timerefch]
        cond = (timediff >= deflims[0]) & (timediff <= deflims[1])
        histo_time, bins_time = truehisto1D(timediff,nbins_time,ifstep=False)
        
        ax = fig.add_subplot(nplot_perside,nplot_perside,j+1)
        histoplotter1D(ax,bins_time,histo_time,f'Peak time difference (ch. {plotind} - {timerefch}) [ticks]','Counts',
                        f"{digi_content[str(plotind)]} - {digi_content[str(timerefch)]}",'upper left',textfont*1.25)
        ax.legend(loc='upper right',fontsize=0.85*textfont,framealpha=1)
        if timelims is not None:
            ax.set_xlim(timelims)
        
        if iflogtime:
            ax.set_yscale('log')

    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"Peak Time diff monitor n. " + str(i+1) + f' ({datetime.date.today()}, {time_readable})', 
                 fontsize = 2.5*textfont)

    # Salvo la figura in due location
    savefigname = '_digi_histo1D_timediff_' + str(i+1)
    fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + 'run' + str(run) + '_digi_histo1D_timesdiff' + filetype
allfiles = glob.glob(imgloc + 'run' + str(run) + '_digi_histo1D_timediff_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()

print(f"")
print(f"Peak Time Difference spectra have been created, merged and saved.")

# ==========================================================================================
#                            PLOT DI CONTROLLO: PH/TEMPO DI PICCO
# ==========================================================================================  
# Definisco una variabile preliminare
try:
    nbins_PH = int(sys.argv[6])           # Binning per spettri in PH
except:
    nbins_PH = 100                        # Default value

# Per ogni blocco di figure procedo a creare gli istogrammi 2D di tempo e PH
colmap = 'jet'   # Parametri per gli istogrammi 2D
slope = 1

for i in range(nfigs):
    # Creo la figura
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplots):
        # Se ho finito le variabili, esco dal ciclo
        plotind = j+i*nplots    # Questa variabile punta a ogni iterazione su una colonna diversa di digiPH
        if (plotind >= ncols):
            break
        
        # Se il canale da guardare è vuoto, proseguo oltre
        if (digi_content[str(plotind)] is None) | (plotind == timerefch):
            continue

        # Creo l'istogramma di frequenze e lo rappresento
        timediff = digitime[:,plotind] - digitime[:,timerefch]
        cond = (timediff >= deflims[0]) & (timediff <= deflims[1])
        histo2D_PHtime, binsX_PH, binsY_time = truehisto2D(digiPH[cond,plotind],timediff[cond],nbins_PH,nbins_time,
                                                           ifstepX = False, ifstepY = False,edgeLX=0)
        timestring = f"Ch. {plotind} - Ch. {timerefch}"

        ax = fig.add_subplot(nplot_perside,nplot_perside,j+1)
        histoplotter2D(ax,binsX_PH,binsY_time,histo2D_PHtime,colmap,slope,'Pulse Height [a.u.]',"Time difference [ticks]",
                       1.25*textfont,iflog = True,ifcbarfont=True)
        
        if timelims is not None:
            ax.set_ylim(timelims)

        ax.set_title(label = timestring, fontsize = 2*textfont)
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(r"PH/$\Delta$ PT" + f" monitor n. " + str(i+1) + f' ({datetime.date.today()}, {time_readable})',
                 fontsize = 2.5*textfont)

    # Salvo la figura in due location
    savefigname = '_digi_histo2D_PHtimediff_' + str(i+1)
    fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + 'run' + str(run) + '_digi_histo2D_PHtimesdiff' + filetype
allfiles = glob.glob(imgloc + 'run' + str(run) + '_digi_histo2D_PHtimediff_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()

print(f"PH vs Peak Time Difference 2D-spectra have been created, merged and saved.\n")