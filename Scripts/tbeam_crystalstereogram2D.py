# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre lo stereogramma di un cristallo, dopo molteplici
# run di scan angolare. 
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_crystalstereogram2D.py stereotype axlims cbarlims crystalid
#
# Argomenti:
# 1 [stereotype]    str, tipo di angolo da usare per costruire lo stereogramma.
#                     Usando "1" si usano i profili di angolo goniometro vs PH.
#                     Usando "2" si usano di profili di (angolo goniometro + incidenza) vs PH
# 2 [axlims]        list of float, lista dei limiti degli assi del tipo [xmin, xmax, ymin, ymax],
#                     oppure "None" per nessun limite custom (default: "None")
# 3 [cbarlims]      list of float, lista dei limiti della colorbar nel formato [vmin, vmax],
#                     oppure "None" per nessun limite custom"
# 4 [crystalid]     str, un identificativo che riconosce il cristallo sotto test e serve a
#                     distinguerlo, in caso si facciano scan su più cristalli (default: "0")
#
# Esempi:
# $     python3 tbeam_crystalstereogram2D.py 1 None None
# $     python3 tbeam_crystalstereogram2D.py 1 [3138000,3142000,65000,75000] [9000,13000]
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc as gc                     # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                          ESTRAZIONE DELLE VARIABILI DI INPUT
# ==========================================================================================
# Estraggo gli input dell'utente. Inizio dal tipo di stereogramma da eseguire
try:
    if sys.argv[1] == '1':
        stereotype = '1'
    else:
        stereotype = '2'
except:
    steretoype = '1'

# Definisco i limiti degli assi per il plot
try:
    if sys.argv[2] == 'None':
        axlims = None
    else:
        axlims = str2list(sys.argv[2],float)
except:
    axlims = None

# Definisco i limiti per la colorbar
try:
    if sys.argv[3] == 'None':
        cbarlims = None
    else:
        cbarlims = str2list(sys.argv[3],float)
except:
    cbarlims = None

# Definisco il cristallo da usare nei plot
try:
    if sys.argv[4] == 'None':
        crystalid = '0'
    else:
        crystalid = sys.argv[4]
except:
    crystalid = '0'

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico l'elenco dei file da caricare per il plot
allfiles = glob.glob(os.path.join(temploc,f'crystalstereo_run*_crystal_{crystalid}.npz'))

# ==========================================================================================
#                                  PLOT DEI DATI
# ==========================================================================================
# Prima di plottare effettivamente i dati, ho bisogno di avere la minima e massima PH del
# rivelatore di riferimento. Posso usare la user-defined o calcolarla a mano.
# In entrambi i casi conviene comunque aprire tutti i file e calcolare la massima e minima
# PH del piano, così posso definire la coordinata di massimo deposito energetico
nfiles = len(allfiles)
minPHs = np.zeros((nfiles,))
maxPHs = np.zeros((nfiles,))
for i,el in enumerate(allfiles):
    # Carico il file dati i-esimo e ne estraggo i dati
    with np.load(el) as datafile:
        # Uso i dati ottenuti dalle proiezioni degli istogrammi 2D "angolo goniometro vs PH"
        # se "stereotype" vale 1; "angolo incidenza vs PH" altrimenti.
        gonio_PH = datafile[f'gonio_PH_met{stereotype}']

    # Estraggo min e max della PH
    minPHs[i] = np.min(gonio_PH)
    maxPHs[i] = np.max(gonio_PH)

# Calcolo i limiti di PH da plottare
trueminPH = np.min(minPHs)
truemaxPH = np.max(maxPHs)
print(f"Min PH: {trueminPH:.3f}")
print(f"Max PH: {truemaxPH:.3f}")

# Decido se usare le soglie di PH (quindi per l'asse colore dello stereogramma) di tipo
# used-defined o quelle di default del sistema
if cbarlims is None:
    # Default: massimi e minimi assoluti
    absminPH = trueminPH
    absmaxPH = truemaxPH
else:
    # User-defined
    absminPH,absmaxPH = cbarlims

# Per ogni file da guardare, carico i dati e li aggiungo al plot
fig, ax = plt.subplots(figsize = dimfig)
colmap = 'jet_r'

for i,el in enumerate(allfiles):
    # Carico il file dati i-esimo e ne estraggo i dati
    with np.load(el) as datafile:
        # Uso i dati ottenuti dalle proiezioni degli istogrammi 2D "angolo goniometro vs PH"
        # se "stereotype" vale 1; "angolo incidenza vs PH" altrimenti.
        gonio_ang = datafile[f'gonio_ang_met{stereotype}']
        gonio_cradle = datafile[f'gonio_cradle_met{stereotype}']
        gonio_PH = datafile[f'gonio_PH_met{stereotype}']
        scantype = datafile['scantype']

    # Capisco se lo scan era angolare o cradle 
    if scantype == 'rot':
        true_gonio_ang = gonio_ang
        true_gonio_cradle = np.ones_like(gonio_ang)*gonio_cradle
    else:
        true_gonio_ang = np.ones_like(gonio_cradle)*gonio_ang
        true_gonio_cradle = gonio_cradle

    # Eseguo il plot opportuno
    cond = (gonio_PH > 0)
    gonioimg = ax.scatter(true_gonio_ang[cond],true_gonio_cradle[cond],c=gonio_PH[cond], 
                                                vmin=absminPH, vmax=absmaxPH, cmap=colmap)
    

# A fine ciclo, sistemo quanto rimane della figura. Se serve, sistemo anche i limiti degli assi
if axlims is not None:
    # Sistemo gli assi. Estraggo prima di tutto i loro valori
    ax.set_xlim([axlims[0],axlims[1]])
    ax.set_ylim([axlims[2],axlims[3]])
    
fig.colorbar(gonioimg)
ax.set_xlabel(r"Rot angle $[\mu rad]$",fontsize = textfont)
ax.set_ylabel(r"Cradle angle $[\mu rad]$", fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
ax.set_title(f'Crystal: {crystalid}',fontsize = 1.5*textfont)
fig.set_tight_layout('tight')
fig.savefig(imgloc + f'crystalstereogram2D_{crystalid}' + filetype,dpi = filedpi)
plt.show()

print(f"")
print(f"The 2D stereogram has been created and saved")
print(f"")

# ==========================================================================================
#                                  PRINTOUT OF AXIS LOC
# ==========================================================================================
# Stampo le coordinate di massima PH. Per farlo, ciclo finché non trovo il file
# dove c'era lo scan che contiene il massimo
for i,el in enumerate(allfiles):
    # Carico il file dati i-esimo e ne estraggo i dati
    with np.load(el) as datafile:
        # Uso i dati ottenuti dalle proiezioni degli istogrammi 2D "angolo goniometro vs PH"
        # se "stereotype" vale 1; "angolo incidenza vs PH" altrimenti.
        gonio_ang = datafile[f'gonio_ang_met{stereotype}']
        gonio_cradle = datafile[f'gonio_cradle_met{stereotype}']
        gonio_PH = datafile[f'gonio_PH_met{stereotype}']
        scantype = datafile['scantype']

    # Controllo se è la run giusta. Se sì, mi fermo.
    if np.max(gonio_PH) == truemaxPH:
        if scantype == 'rot':
            print(f"Axis coordinates (guess):")
            print(f"\tAngular = {gonio_ang[np.argmax(gonio_PH)]:.2f} urad")
            print(f"\tCradle = {gonio_cradle:.2f} urad")
        elif scantype == 'cradle':
            print(f"Axis coordinates (guess):")
            print(f"\tAngular = {gonio_ang:.2f} urad")
            print(f"\tCradle = {gonio_cradle[np.argmax(gonio_PH)]:.2f} urad")
        
        break

print(f"")