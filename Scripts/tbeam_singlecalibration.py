# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è calibrare un rivelatore a scelta, usando i dati di tante run
# precedenti di calibrazione.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_singlecalibration.py detcol fittype startpts ifzero
#
# Argomenti:
# 1 [detcol]        int, indice della colonna del digitizer da calibrare
# 2 [fittype]       str, tipo di fit da applicare alla curva di calibrazione
#                     ("linear" o "quad") (default: "linear")
# 3 [startpts]      list of float, starting point per il fit quadratico,
#                     oppure "None", se non richiesto (default: "None")
# 4 [ifzero]        str, se aggiungere un punto (0,0) nella calibrazione oppure no
#                     ("True" o "False") (default: "False")
# Esempi:
# $     python3 tbeam_singlecalibration.py 19
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]  

# ==========================================================================================
#                                     UPLOAD SPECIFICI 
# ==========================================================================================
# Carico il rivelatore da analizzare
chind = int(sys.argv[1])

# Definisco tutte le run disponibili e le corrispondenti energie
templatename = temploc + 'ch_' + str(chind) + '_run*GeV.npz'     # I nomi sono formattati così
allfiles = glob.glob(templatename)
allenergies = []
for i,el in enumerate(allfiles):
    # Usando degli split ricostruisco l'energia del file i-esimo e la appendo
    tempstr1 = el.split('GeV')[0]                  # e.g. da "ch_19_run500720_120GeV.npz' passo a "ch_19_run500720_120"
    tempstr2 = tempstr1.split('_')[-1]             # e.g. passo a "120"
    allenergies.append(int(tempstr2))
                       
energies = np.array(allenergies)

# Itero su ogni canale ed estraggo i punti per calibrazione e risoluzione
npoints = len(allfiles)
medie = np.zeros((npoints,))
medie_err = np.zeros_like(medie)
widths = np.zeros_like(medie)
widths_err = np.zeros_like(medie)

for i,el in enumerate(allfiles):
    datafile = np.load(el,allow_pickle=True)
    medie[i] = datafile['media']
    medie_err[i] = datafile['media_err']
    widths[i] = datafile['width']
    widths_err[i] = datafile['width_err']
    
# ==========================================================================================
#                                       CALIBRAZIONE 
# ==========================================================================================
# Definisco come calibrare il calorimetro
''
try:
    if (sys.argv[2] != 'quad') & (sys.argv[2]  != 'linear'):
        print(f"Fit function unrecognized. Setting a linear function.")
        fitway = 'linear'
    else:
        fitway = sys.argv[2]                     # Questa stringa vale per default "linear" o "quad"
except:
    fitway = 'linear'

# Per fit quadratici, definisco gli starting point del fit
try:
    startfit = str2list(sys.argv[3],float)   # Se esiste, converto la input string in lista
except:
    startfit = [0,0,0]    
    
# Se aggiungo uno zero
try:
    ifzero = (sys.argv[4] == 'True')
except:
    ifzero = False

if ifzero:
    energies = np.hstack((np.array([0]),energies))
    medie = np.hstack((np.array([0]),medie))
    medie_err = np.hstack((np.array([0.0001]),medie_err))
    widths = np.hstack((np.array([0.0001]),widths))
    widths_err = np.hstack((np.array([0.0001]),widths_err))

# Produco la retta o parabola di calibrazione
xth = np.linspace(0,np.max(energies),nptsfit)

if fitway == 'linear':
    # Retta
    print(energies)
    result,_,_ = fitter_linear(energies,medie,medie_err)
    m,q = result.params['m'],result.params['q']

    print(f"")
    print(f"Straight line fit to the calibration points:")
    try:
        str1 = f"m: ({m.value:.3f} +/- {m.stderr:.3f}) ADC/GeV"
        str2 = f"q: ({q.value:.3f} +/- {q.stderr:.3f}) ADC" 
    except:
        str1 = f"m: {m.value:.3f} ADC/GeV"
        str2 = f"q: {q.value:.3f} ADC"
    
    print(str1)
    print(str2)

    # Curva teorica della calibrazione
    ytrue = m*energies+q
    yth = m*xth+q
    npars = 2
    
else:
    # Parabola
    result,_,_ = fitter_quad(energies,medie,medie_err,start_pars = startfit)
    a,b,c = result.params['a'],result.params['b'],result.params['c']

    print(f"")
    print(f"Quadratic curve fit to the calibration points:")
    try:
        str1 = f"a: ({a.value:.3f} +/- {a.stderr:.3f}) ADC/GeV^2"
        str2 = f"b: ({b.value:.3f} +/- {b.stderr:.3f}) ADC/GeV"
        str3 = f"c: ({c.value:.3f} +/- {c.stderr:.3f}) ADC" 
    except:
        str1 = f"a: {a.value:.3f} ADC/GeV^2"
        str2 = f"b: {b.value:.3f} ADC/GeV"
        str3 = f"c: {c.value:.3f} ADC" 
    
    print(str1)
    print(str2)
    print(str3)
        
    # Curva teorica della calibrazione
    ytrue = quadfun(energies,a,b,c)
    yth = quadfun(xth,a,b,c)
    npars = 3

# Calcolo e mostro il chi quadro ridotto
chi2 = np.sum( (ytrue-medie)**2/ytrue )/(np.shape(energies)[0]-npars)
print(f"Reduced chi square of the fit: {chi2:.3f}")
print(f"")

# Calcolo i residui percentuali
yres = 100*2*(ytrue-medie)/(ytrue+medie) # Residui %

# Mostro la retta di calibrazione
fig = plt.figure(figsize = dimfigbig)
ax = fig.add_subplot(2,1,1)
ax.errorbar(energies,medie,medie_err,color = 'mediumblue', marker = '.', linestyle = '',
            label = 'Data points ch. ' + str(chind), markersize = 10, capsize = 5)
ax.plot(xth,yth,color = 'red', linestyle = '-', linewidth = 1, label = str1 + '\n' + str2)
ax.legend(loc = 'upper left',fontsize = textfont*1.5)
ax.set_xlabel('Energy [GeV]',fontsize = textfont*1.5)
ax.set_ylabel('Energy [a.u.]',fontsize = textfont*1.5)
ax.set_xlim([-0.5,1.1*np.max(energies)])
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont*1.25)

# Plot dei residui
ax = fig.add_subplot(2,1,2)
ax.plot(energies,yres,color = 'mediumblue',label = 'Data points ch. ' + str(chind), marker = '.', linestyle = '', markersize = 10)
ax.plot([0,np.max(energies)+5],[0,0],color='black',linestyle = '--', linewidth = 2)
ax.legend(loc = 'best',fontsize = textfont*1.5)
ax.set_xlim([0,1.1*np.max(energies)])
ax.set_ylim([-5,5])
ax.set_xlabel('Energy [GeV]',fontsize = textfont*1.5)
ax.set_ylabel('Residuals [%]',fontsize = textfont*1.5)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont*1.25)
fig.set_tight_layout('tight')

savefile1 = imgloc + 'calibration_ch_' + str(chind) + filetype
fig.savefig(savefile1, dpi = filedpi)
plt.show()
plt.close(fig)

# ==========================================================================================
#                                       RISOLUZIONE 
# ==========================================================================================
# Calcolo le risoluzioni
res = widths/medie
res_err = res*np.sqrt( (widths_err/widths)**2 + (medie_err/medie)**2 )

# Fitto la risoluzione se ho almeno tre punti
ifresfit = (np.shape(energies)[0] >= 3)
if ifresfit:
    result,xth,yth = fitter_resolution(energies,res,res_err,ifzero=False)
    a,b,c = result.params['a'],result.params['b'],result.params['c']

    print(f"Fit on the resolution points:")
    try:
        str1 = f"a: ({100*a.value:.3f} +/- {100*a.stderr:.3f}) % GeV^(1/2)"
        str2 = f"b: ({100*b.value:.3f} +/- {100*b.stderr:.3f}) % GeV"
        str3 = f"c: ({100*c.value:.3f} +/- {100*c.stderr:.3f}) % "
    except:
        str1 = f"a: {100*a.value:.3f} % GeV^(1/2)"
        str2 = f"b: {100*b.value:.3f} % GeV"
        str3 = f"c: {100*c.value:.3f} % "

    print(str1)
    print(str2)
    print(str3)

# Plot della risoluzione e del fit
fig, ax = plt.subplots(figsize = dimfigbig)
ax.errorbar(energies,100*res,100*res_err,color = 'mediumblue',label = 'Data points ch. ' + str(chind), 
            marker = '.', linestyle = '--',capsize = 5, markersize = 10)
if ifresfit:
    ax.plot(xth,100*yth,color = 'red', linestyle = '-', linewidth = 1, label = str1 + '\n' + str2 + '\n' + str3)
ax.legend(loc = 'upper right',fontsize = textfont)*1.5
ax.set_xlabel('Energy [GeV]',fontsize = textfont*1.5)
ax.set_ylabel('Resolution [%]', fontsize = textfont*1.5)
ax.set_xlim([0,1.1*np.max(energies)])
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont*1.25)
fig.set_tight_layout('tight')

savefile2 = imgloc + 'resolution_ch_' + str(chind) + filetype
fig.savefig(savefile2, dpi = filedpi)
plt.show()
plt.close(fig)

# ==========================================================================================
#                                       MERGE
# ==========================================================================================
mergedfile = imgloc + 'singlecalibration_ch_' + str(chind) + filetype
if os.path.exists(mergedfile):
    os.remove(mergedfile)
    
allfiles = [savefile1,savefile2]
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()

# Chiudo lo script
print(f"The calibration and resolution data points have been plotted, fitted, merged and saved.")
print(f"")