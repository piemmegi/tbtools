# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre lo spettro di PH di un certo canale del digitizer,
# applicando un certo numero di tagli (nessun taglio è obbligatorio):
#   - PH di uno o più canali di riferimento
#   - PT di uno o più canali di riferimento
#   - PTD tra i canali e uno o più canali di riferimento
#   - Posizione di hit estrapolata dai silici a una certa distanza
#   - Angolo di incidenza (divergenza)
#   - Numero di evento
#
# I tagli vengono definiti in un file di riferimento, che può essere localizzato o nella 
# cartella degli script oppure nella cartella dove l'HTML dell'online monitor è localizzato.
# Dopo il taglio, si può anche eseguire un fit sullo spettro di PH prodotto.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_singledigiwfc.py nrun cutloc digich PHstep ifPHlog fittype fitext Ebeam
#
# Argomenti:
# 1  [nrun]         int, numero della run da analizzare
# 2  [cutloc]       str, tipo di tagli da applicare ("tbtools" per il file in locale,
#                     "www" per il file prodotto dal monitor online, "None" per nessun 
#                     taglio) (default: "None")
# 3  [digich]       int, indice del digitizer da analizzare (default: 0)
# 4  [PHstep]       int, numero di bin per gli spettri in PH (default: 100)
# 5  [ifPHlog]      str, se plottare lo spettro in scala log ("True" oppure "False")
#                     (default: "False")
# 6  [fittype]      str, funzione da usare per il fit ("Gaussian", "Alternate", "Landau",
#                     oppure "None" per nessun fit) (default: "None")
# 7  [fitext]       list of int, lista degli estremi per la regione di fit, altrimenti
#                     "None" per non applicare regioni custom (default: "None")
# 8  [Ebeam]        int, energia del fascio primario di particelle in GeV, da settare solo se 
#                     l'analisi serve per la calibrazione di un rivelatore, oppure "None"
#                     se non si vuole salvare i dati per la calibrazione (default: "None")
#
#
# Esempio per un istogramma senza fit di alcun tipo ma con tagli da web:
# $     python3 tbeam_singledigiwcf.py 720210 www 1 100 True None None None
#
# Esempio per un istogramma senza tagli ma con fit per calibrazione:
# $     python3 tbeam_singledigiwfc.py 720210 None 1 100 True Gaussian [50,10000] 1
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'], data_namesdict['digiTime'], data_namesdict['ievent'],
            data_namesdict['xpos'] ]
digiPH, digitime, eventNumber, ch_pos = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                                   TAGLIO IN CLUSTER
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
     # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    digitime = digitime[cond,:]
    eventNumber = eventNumber[cond]
    ch_pos = ch_pos[cond,:]

# ==========================================================================================
#                          ESTRAZIONE DEGLI INPUT
# ==========================================================================================
# Definisco la provenienza dello status file che contiene i tagli da applicare
try:
    if (sys.argv[2] == "tbtools") | (sys.argv[2] == "www"):
        # Caso 1: keyword riconosciuta
        cutloc = sys.argv[2]
    else:
        # Caso 2: keyword non riconosciuta, ricadi nel default
        cutloc = None
except:
    # Caso 3: keyword non definita, ricadi nel default
    cutloc = None

# Indice del digitizer da usare per l'analisi
try:
    digich = int(sys.argv[3])
except:
    digich = 0

# Binning step per l'istogramma di PH
try:
    nbins_PH = int(sys.argv[4])
except:
    nbins_PH = 100

# Se usare una scala log nell'istogramma
try:
    if (sys.argv[5] == 'True'):
        ifPHlog = True
    else:
        ifPHlog = False
except:
    ifPHlog = False

# Tipo di fit da eseguire
try:
    if (sys.argv[6] == 'Gaussian') | (sys.argv[6] == 'Alternate') | (sys.argv[6] == 'Landau'):
        fittype = sys.argv[6]
    else:
        fittype = None
except:
    fittype = None
    
# Definisco la regione di fit
try:
    if sys.argv[7] == 'None':
        fitregion = None
    else:
        fitregion = str2list(sys.argv[7],int)
except:
    fitregion = None

# Definisco l'energia del fascio
try:
    if sys.argv[8] == 'None':
        Ebeam = None
    else:
        Ebeam = int(sys.argv[8])
except:
    Ebeam = None

# ==========================================================================================
#                          DEFINIZIONE E APPLICAZIONE DEI TAGLI
# ==========================================================================================
# Applico i tagli, se è richiesto
print(f"")
if cutloc is not None:
    # Ricavo dallo statusfile il path del file con i tagli
    print(f"Applying the cuts from: {cutloc}\n")
    whichcuts_path = cutfilepaths["cutstatus_whichones_" + cutloc]
    whichvalues_path = cutfilepaths["cutstatus_whichvalues_" + cutloc]

    # Uso la funzione custom per costruire la condizione di taglio globale
    globalcut, cutoutputdict = cutcalculator(digich,digiPH,digitime,ch_pos,distances['zs1s2'],whichcuts_path,whichvalues_path)
else:
    print(f"Not applying cuts of any type.\n")
    globalcut = np.ones((np.shape(digiPH)[0],),dtype=bool)

# Applico il taglio
cutPH = digiPH[globalcut,digich]

# ==========================================================================================
#                               ISTOGRAMMA DI PH E FIT
# ==========================================================================================
# Creo l'istogramma di frequenze
histo_PH, bins_PH = truehisto1D(cutPH,nbins_PH,ifstep=False,edgeL=0)

# Calcolo e stampo il valore medio dello spettro di PH
meanPH, errPH = wmean(bins_PH,histo_PH)
print(f"Channel {digich}, average PH after all the cuts: {meanPH:.3f} +/- {errPH:.3f}")

# Eseguo il fit, se richiesto
if fittype is not None:
    # Estraggo la regione da fittare
    if fitregion is None:
        bins_fit = bins_PH
        histo_fit = histo_PH
    else:
        fitcond = (bins_PH >= fitregion[0]) & (bins_PH <= fitregion[1])
        bins_fit = bins_PH[fitcond]
        histo_fit = histo_PH[fitcond]

    # Procedo con la funzione custom del tipo di fit da applicare
    if fittype == 'Gaussian':
        # Fit gaussiano classico
        result,xth,yth = fitter_gauss1fun(bins_fit,histo_fit,'P')
        b,c = result.params['b'],result.params['c']

        # Salvo gli output
        media = b.value
        media_err = b.stderr
        width = c.value
        width_err = c.stderr

    elif fittype == 'Alternate':
        # Fit Crystal-Ball like
        result,xth,yth = fitter_expgaussexp(bins_fit,histo_fit,'P')
        mu,sigma = result.params['mu'],result.params['sigma']
        FWHM,err_FWHM,_ = fwhm_expgaussexp(result,xth,yth)
            
        # Salvo gli output
        media = mu.value
        media_err = mu.stderr
        width = FWHM/2.355
        width_err = err_FWHM/2.355
            
    elif fittype == 'Landau':
        # Fit Landau
        result,xth,yth = fitter_landau(bins_fit,histo_fit,'P')
        Empv,fwhm = result.params['Empv'],result.params['fwhm']

        # Salvo gli output
        media = Empv.value
        media_err = Empv.stderr
        width = fwhm.value/2.355
        width_err = fwhm.stderr/2.355

    # Calcolo la risoluzione dai parametri di fit
    res = width/media
    try:
        res_err = res*np.sqrt( (media_err/media)**2 + (width_err/width)**2 )
    except:
        res_err = np.nan

    # Stampo media e risoluzione
    try:
        print(f"Media: {media:.2f} +/- {media_err:.2f}" + '\n' + f"Sigma: {width:.2f} +/- {width_err:.2f}")
        print(f"Risoluzione: ({100*res:.2f} +/- {100*res_err:.2f}) %)")
    except:
        print(f"Media: {media:.2f}" + '\n' + f"Sigma: {width:.2f}")
        print(f"Risoluzione: {100*res:.2f} %")

# ==========================================================================================
#                                        PLOT
# ==========================================================================================
# Mostro il plot dell'istogramma ed eventualmente del fit
fig,ax = plt.subplots(figsize = dimfig)
histoplotter1D(ax,bins_PH,histo_PH,'PH','Counts',f"Ch. {digich} ({digi_content[str(digich)]})",'best',textfont)
if ifPHlog:
    ax.set_yscale('log')
else:
    ax.set_ylim([0,1.1*np.max(histo_fit)])

if fittype is not None:
    ax.plot(xth,yth,color = 'red', linestyle = '-', linewidth = 1, label = fittype + ' fit')
    ax.plot([fitregion[0],fitregion[0]],[0,np.max(histo_PH)],color = 'darkgreen', linestyle = '--', label = 'Fit region', linewidth = 2)
    ax.plot([fitregion[1],fitregion[1]],[0,np.max(histo_PH)],color = 'darkgreen', linestyle = '--', linewidth = 2)
    ax.set_xlim([0.75*fitregion[0],1.25*fitregion[1]])
    ax.legend(loc = 'best', fontsize = textfont)


# Rifinisco la grafica e salvo la figura
fig.set_tight_layout('tight')
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)
fig.suptitle(f'PH spectrum with {cutloc} cuts ({datetime.date.today()}, {time_readable})', fontsize = 2*textfont)
fig.savefig(imgloc + f'run{run}_singledigiPHwithcuts_ch{digich}' + filetype,dpi = filedpi)
fig.savefig(imgloc_www + f'lastrun_singledigiPHwfc_ch{digich}' + filetype,dpi = filedpi_www)
fig.savefig(imgloc_www + f'lastrun_singledigiPHwfc_ch{digich}' + filetype_www,dpi = filedpi_www)
plt.show()
plt.close(fig)

print(f"")
print(f"PH spectrum with cuts has been created and saved.\n")

# ==========================================================================================
#                                  TEMPOUT
# ==========================================================================================
# Salvo le PH tagliate in un file npz a parte, per possibili future analisi
outfilename = os.path.join(temploc,f'run{run}_PHcut_ch_{digich}.npz')
np.savez_compressed(outfilename, cutPH = cutPH)
print(f"Cutted PH saved at:")
print(outfilename)
print(f"")

# Salvo MPV e FWHM in un file npz a parte, per le analisi di calibrazione
if fittype is not None:
    outfilename = temploc + f'ch_{digich}_run{run}_{Ebeam}GeV.npz'
    np.savez_compressed(outfilename, 
                        media = media, media_err = media_err, 
                        width = width, width_err = width_err)

    print(f"Fit parameter data (MPV and width) saved at:")
    print(outfilename)
    print(f"")

print(f"Job done!\n")