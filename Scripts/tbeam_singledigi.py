# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i plot di analisi di un canale del digitizer, ovvero:
#   - Spettro di PH (1D)
#   - Spettro di tempo di picco (1D)
#   - Spettro di PH vs tempo di picco (2D)
#   - Spettro di PH vs numero di evento (2D)
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_singledigi.py nrun digi_inds PHstep ifPHlog timestep iftimelog ieventstep ifprofile
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [digi_inds]     list of int, indici del digitizer da analizzare (default: [0])
# 3 [PHstep]        int, binning step per gli spettri in PH (default: 25)
# 4 [ifPHlog]       str, se fare gli spettri di PH in scala log ("True", "False") 
#                     (default: "False")
# 5 [timestep]      int, binning step per gli spettri in tempo di picco (default: 10)
# 6 [iftimelog]     str, se fare gli spettri di tempo di picco in scala log ("True", "False")
#                     (default: "False")
# 7 [ieventstep]    int, binning step per gli spettri in numero di eventi (default: 200)
# 8 [ifprofile]     str, se fare il profile plot in PH vs numero di evento ("True", "False") 
#                     (default: "False")
#
# Esempi:
# $     python3 tbeam_singledigi.py 680556 [1,2] 25 False 10 False 250 False
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'], data_namesdict['digiTime'], 
            data_namesdict['ievent'], data_namesdict['info_plus'] ]
digiPH, digitime, eventNumber, info_plus = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    digitime = digitime[cond,:]
    eventNumber = eventNumber[cond]
    info_plus = info_plus[cond,:]

# ==========================================================================================
#                                  PLOT DI CONTROLLO: PH
# ==========================================================================================
# Definisco alcune variabili preliminari
try:      # Canali da guardare
    listchs = str2list(sys.argv[2],int)      # Se esiste, converto la input string in lista
except:
    listchs = [0]                            # Default value

nchs = len(listchs)

try:      # Binning step degli istogrammi
    nbins_PH = int(sys.argv[3])              # Binning per spettri in PH, in ADC
except:
    nbins_PH = 25                            # Default value

try:      # Scala log?
    iflogPH = (sys.argv[4] == 'True')        # Se usare la scala log
except:
    iflogPH = False
    
# Per ogni variabile procedo a creare gli istogrammi di frequenze di PH e li mostro in figure diverse
print(f"")
for i,el in enumerate(listchs):
    # Creo la figura per l'i-esimo canale
    fig,ax = plt.subplots(figsize = dimfig)

    # Creo l'istogramma di frequenze e lo rappresento
    histo_PH, bins_PH = truehisto1D(digiPH[:,listchs[i]],nbins_PH,ifstep=True,edgeL=0)
    histoplotter1D(ax,bins_PH,histo_PH,'PH','Counts',f"Ch. {listchs[i]}",'upper right',textfont)
    if iflogPH:
        ax.set_yscale('log')
        
    # Calcolo e stampo il valore medio dello spettro di PH
    meanPH, errPH = wmean(bins_PH,histo_PH)
    print(f"Channel {el}, average PH: {meanPH:.3f} +/- {errPH:.3f}")
     
    # Rifinisco la grafica e salvo la figura
    ax.set_title(label = f"PH ch n. " + str(listchs[i]), fontsize = 2.5*textfont)
    fig.set_tight_layout('tight')
    fig.savefig(imgloc + 'run' + str(run) + '_singledigiPH_ch' + str(listchs[i]) + filetype,dpi = filedpi)
    plt.show()
    
# Unisco tutte le figure in un file unico e le rimuovo
print(f"")
print(f"PH spectra have been created and saved.")
    
# ==========================================================================================
#                                  PLOT DI CONTROLLO: TEMPO
# ==========================================================================================
# Definisco alcune variabili preliminari
try:
    nbins_time = int(sys.argv[5])             # Binning per gli spettri di tempo di picco
except:
    nbins_time = 10                           # Default value

try:
    iflogtime = (sys.argv[6] == 'True')       # Se usare la scala log
except:
    iflogtime = False
    
# Per ogni variabile procedo a creare gli istogrammi di frequenze di PH e li mostro in figure diverse
for i,el in enumerate(listchs):
    # Creo la figura per l'i-esimo canale
    fig,ax = plt.subplots(figsize = dimfig)

    # Creo l'istogramma di frequenze e lo rappresento
    histo_time, bins_time = truehisto1D(digitime[:,listchs[i]],nbins_time,ifstep=True,edgeL=0)
    histoplotter1D(ax,bins_time,histo_time,f"Peak time [ticks]",'Counts',f"Ch. {listchs[i]}",'upper right',textfont)
    if iflogtime:
        ax.set_yscale('log')
    
    # Rifinisco la grafica e salvo la figura
    ax.set_title(label = f"Peak Time ch n. " + str(listchs[i]), fontsize = 2.5*textfont)
    fig.set_tight_layout('tight')
    fig.savefig(imgloc + 'run' + str(run) + '_singledigiTime_ch' + str(listchs[i]) + filetype,dpi = filedpi)
    plt.show()
    
print(f"Peak Time spectra have been created and saved.")

# ==========================================================================================
#                            PLOT DI CONTROLLO: PH/TEMPO DI PICCO
# ==========================================================================================  
# Per ogni blocco di figure procedo a creare gli istogrammi 2D di tempo e PH
colmap = 'jet'   # Parametri per gli istogrammi 2D
slope = 1

for i,el in enumerate(listchs):
    # Creo la figura per l'i-esimo canale
    fig,ax = plt.subplots(figsize = dimfig)

    # Creo l'istogramma di frequenze e lo rappresento
    histo2D_PHtime, binsX_PH, binsY_time = truehisto2D(digiPH[:,listchs[i]],digitime[:,listchs[i]],
                                                        nbins_PH,nbins_time,edgeLX=0,edgeLY=0,
                                                        ifstepX = True, ifstepY = True)

    histoplotter2D(ax,binsX_PH,binsY_time,histo2D_PHtime,colmap,slope,'Pulse Height',f"Peak time [ticks]",textfont,iflog = True)
    ax.set_title(label = f"PH/Peak Time ch n. " + str(listchs[i]), fontsize = 2.5*textfont)
    fig.set_tight_layout('tight')
    fig.savefig(imgloc + 'run' + str(run) + '_singledigiPHTime_ch' + str(listchs[i]) + filetype,dpi = filedpi)
    plt.show()
    
print(f"PH vs Peak Time 2D-spectra have been created and saved.")
    
# ==========================================================================================
#                                PLOT DI CONTROLLO: PH/EVENTO
# ==========================================================================================
# Definisco il binning per l'asse del numero di evento e se fare anche i profile plot
try:
    nbins_event = int(sys.argv[7])
except:
    nbins_event = 250
    
try:
    if sys.argv[8] == 'True':
        ifproj = True
        pr = 'mean'
    else:
        ifproj = False
except:
    ifproj = False
    
# Per ogni blocco di figure procedo a creare gli istogrammi 2D di tempo e PH
for i,el in enumerate(listchs):
    # Creo la figura per l'i-esimo canale
    fig,ax = plt.subplots(figsize = dimfig)

    # Creo l'istogramma di frequenze e lo rappresento
    histo2D_PHnevent, binsX_nevent, binsY_PH = truehisto2D(eventNumber,digiPH[:,listchs[i]],
                                                       nbins_event,nbins_PH,edgeLX = 0, edgeLY = 0,
                                                       ifstepX = True, ifstepY = True)

    histoplotter2D(ax,binsX_nevent,binsY_PH,histo2D_PHnevent,colmap,slope,'Event number','Pulse Height',textfont,iflog = True)
    if ifproj:
            # If required, add the projection (profile plot) of the 2D histogram
            truex, histosum, _ = histoproj(histo2D_PHnevent, binsX_nevent, binsY_PH, axis = 'x', projtype = pr)
            ax.plot(truex,histosum,color = 'red', marker = '.', markersize = 3, linestyle = '--', linewidth = 1, 
                    label = 'Profile plot')
    ax.set_ylim([0,np.nanmax(digiPH[:,listchs[i]])/10])
    ax.set_title(label = f"PH/Event monitor ch. " + str(listchs[i]), fontsize = 2.5*textfont)
    fig.set_tight_layout('tight')
    fig.savefig(imgloc + 'run' + str(run) + '_singledigiPHEvent_ch' + str(listchs[i]) + filetype,dpi = filedpi)
    plt.show()
    
print(f"PH vs Event 2D-spectra have been created and saved.")

# ==========================================================================================
#                                MERGE DEI PLOT PRODOTTI
# ==========================================================================================  
for i in range(nchs):
    mergedfile = imgloc + 'run' + str(run) + '_singledigi_ch' + str(listchs[i]) + filetype
    if os.path.exists(mergedfile):
        # Rimuovo eventuali file precedentemente creati con lo stesso nome
        os.remove(mergedfile)
    
    allfiles = glob.glob(imgloc + 'run' + str(run) + '_singledigi*_ch' + str(listchs[i]) + filetype)
    merger = PyPDF2.PdfMerger()
    for pdf in allfiles:
        merger.append(pdf)
        os.remove(pdf)

    merger.write(mergedfile)
    merger.close()
        
print(f"")                 
print(f"All plots have been separately merged")
print(f"")