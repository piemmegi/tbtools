# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre una sezione unidimensionale dello stereogramma di un 
# cristallo, dopo molteplici run di scan angolare. 
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_crystalstereogram1D.py stereotype scantype otheraxisvalue tolerance 
#                                            ylims iffit fitext crystalid
#
# Argomenti:
# 1 [stereotype]        str, tipo di angolo da usare per costruire lo stereogramma.
#                         Usando "1" si usano i profili di angolo goniometro vs PH.
#                         Usando "2" si usano di profili di (angolo goniometro + incidenza) vs PH
# 2 [scantype]          str, indica quale coordinata angolare viene fatta variare in questo scan
#                         ("rot" o "cradle") (default: "rot")
# 3 [otheraxisvalue]    float, indica la quota fissata per l'altro asse di variazione angolare
#                         (e.g., se "scantype" è "rot", allora caricherò tutti i dati di scan
#                         angolare di tipo "rot" eseguiti a una coordinata "cradle" fissata
#                         al valore "otheraxisvalue") (default: 0)
# 4 [tolerance]         float, indica la tolleranza in urad fissata sul valore di "otheraxisvalue"
#                         per definire se caricare una run o no. In altre parole, vengono caricati
#                         tutti i file dove la coordinata opposta a "scantype" ha un valore in
#                         [otheraxisvalue - tolerance, otheraxisvalue + tolerance] 
#                         (default: 100)
# 5 [ylims]             list of float, lista dei limiti degli assi del tipo [ymin, ymax],
#                         oppure "None" per nessun limite custom (default: "None")
# 6 [ifquadfit]         str, se eseguire un fit parabolico oppure no
# 7 [fitext]            list of float, estremi della regione di fit, oppure "None" per usare
#                         i dati disponibili (default: "None")
# 8 [crystalid]         str, un identificativo che riconosce il cristallo sotto test e serve a
#                         distinguerlo, in caso si facciano scan su più cristalli (default: "0")
#
# Esempio per mostrare uno scan senza fit nè limiti custom:
# $     python3 tbeam_crystalstereogram1D.py 1 rot -3000 500 None None None OREO
#
# Esempio per mostrare uno scan con fit e limiti custom
# $     python3 tbeam_crystalstereogram1D.py 1 rot -3000 500 None True [24000,040000] OREO
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc as gc                     # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                          ESTRAZIONE DELLE VARIABILI DI INPUT
# ==========================================================================================
# Estraggo gli input dell'utente. Inizio dal tipo di stereogramma da eseguire
try:
    if sys.argv[1] == '1':
        stereotype = '1'
    else:
        stereotype = '2'
except:
    steretoype = '1'

# Definisco il tipo di scan da eseguire
try:
    if (sys.argv[2] == "rot"):
        scantype = sys.argv[2]
        otheraxisname = "cradle"
    elif (sys.argv[2] == "cradle"):
        scantype = sys.argv[2]
        otheraxisname = "rot"
    else:
        scantype = "rot"
        otheraxisname = "cradle"
except:
    scantype = "rot"

# Definisco il valore di quota dell'altro asse
try:
    if sys.argv[3] == "None":
        otheraxisvalue = 0
    else:
        otheraxisvalue = float(sys.argv[3])
except:
    otheraxisvalue = 0

# Definisco la tolleranza su otheraxisvalue
try:
    if sys.argv[4] == "None":
        tolerance = 100
    else:
        tolerance = float(sys.argv[4])
except:
    tolerance = 100

# Definisco i limiti in y del plot
try:
    if sys.argv[5] == "None":
        ylims = None
    else:
        ylims = str2list(sys.argv[5],float)
except:
    ylims = None

# Definisco se eseguire un fit parabolico sui dati
try:
    ifquadfit = (sys.argv[6] == 'True')
except:
    ifquadfit = False

# Definisco gli estremi della regione di fit
try:
    if sys.argv[7] == "None":
        fitext = None
    else:
        fitext = str2list(sys.argv[7],float)
except:
    fitext = None

# Definisco il cristallo da usare nei plot
try:
    if sys.argv[8] == 'None':
        crystalid = '0'
    else:
        crystalid = sys.argv[8]
except:
    crystalid = '0'

# ==========================================================================================
#                                   ESTRAZIONE DEI DATI
# ==========================================================================================
# Carico l'elenco dei file da caricare per il plot
allfiles = glob.glob(os.path.join(temploc,f'crystalstereo_run*_crystal_{crystalid}.npz'))

# Itero su ciascun file disponibile e controllo se va mostrato o no (cioè se ha lo scan del
# tipo richiesto e con l'altra coordinata fissata entro le tolleranze). Se sì, aggiungo i 
# dati alla lista di quelli da mostrare.
all_angles = np.zeros((0,))
all_PHs = np.zeros_like(all_angles)

for i,el in enumerate(allfiles):
    # Carico il file i-esimo e ne estraggo il tipo di scan e i dati
    with np.load(el) as datafile:
        # Uso i dati ottenuti dalle proiezioni degli istogrammi 2D "angolo goniometro vs PH"
        # se "stereotype" vale 1; "angolo incidenza vs PH" altrimenti.
        gonio_ang = datafile[f'gonio_ang_met{stereotype}']
        gonio_cradle = datafile[f'gonio_cradle_met{stereotype}']
        gonio_PH = datafile[f'gonio_PH_met{stereotype}']
        scantype = datafile[f'scantype']

        # Controllo se lo scan è del tipo corretto e in posizione corretta
        cond_ang = (scantype == 'rot') & (np.mean(gonio_cradle) >= otheraxisvalue - tolerance) & \
                                         (np.mean(gonio_cradle) <= otheraxisvalue + tolerance)

        cond_cradle = (scantype == 'cradle') & (np.mean(gonio_ang) >= otheraxisvalue - tolerance) & \
                                               (np.mean(gonio_ang) <= otheraxisvalue + tolerance)
        
        if cond_ang:
            # Condizione corretta per lo scan in rot: salvo i dati
            all_angles = np.hstack((all_angles,gonio_ang))
            all_PHs = np.hstack((all_PHs,gonio_PH))
        elif cond_cradle:
            # Condizione corretta per lo scan in cradle: salvo i dati
            all_angles = np.hstack((all_angles,gonio_cradle))
            all_PHs = np.hstack((all_PHs,gonio_PH))
        else:
            # Condizione scorretta: salto il file
            continue

# Rimuovo i punti a PH nulla, che non vanno analizzati
nullcond = (all_PHs > 0)
all_angles = all_angles[nullcond]
all_PHs = all_PHs[nullcond]

# ==========================================================================================
#                               SORTING DEI DATI
# ==========================================================================================
# Riordino i dati da mostrare successivamente
sortcond = np.argsort(all_angles)
plot_angles = all_angles[sortcond]
plot_PHs = all_PHs[sortcond]

# Stampo i punti di minimo e massimo
minloc = np.argmin(plot_PHs)
maxloc = np.argmax(plot_PHs)
minangle = plot_angles[minloc]
maxangle = plot_angles[maxloc]
minPH = np.min(plot_PHs)
maxPH = np.max(plot_PHs)
print(f"Min PH of {minPH:.3f} obtained at {minangle:.3f}")
print(f"Max PH of {maxPH:.3f} obtained at {maxangle:.3f}\n")

# ==========================================================================================
#                                  FIT DEI DATI
# ==========================================================================================
# Eseguo il fit parabolico, se richiesto
if ifquadfit:
    # Estraggo i dati da fittare
    if fitext is None:
        fitcond = np.ones((np.shape(plot_angles),),dtype=bool)
    else:
        fitcond = (plot_angles >= fitext[0]) & (plot_angles <= fitext[-1])
    
    xfit = plot_angles[fitcond]
    yfit = plot_PHs[fitcond]

    # Eseguo il fit
    result, xth, yth = fitter_quad(xfit,yfit,0.01*yfit)
    minpoint = -result.params['b'].value / (2*result.params['a'].value)
    
    # Printout di controllo
    print(f"Quadratic fit performed successfully!")
    print(f"Point of minimum/maximum in the fitted curve (axis guess): {minpoint:.0f} urad\n")

# ==========================================================================================
#                                  PLOT DEI DATI
# ==========================================================================================
# Definisco i limiti per l'asse verticale
if ylims is None:
    ymin = np.min(plot_PHs)*0.98
    ymax = np.max(plot_PHs)*1.02
else:
    ymin = ylims[0]
    ymax = ylims[-1]

# Mostro i dati
fig, ax = plt.subplots(figsize = dimfig)
ax.plot(plot_angles,plot_PHs,color='mediumblue',linestyle='-',linewidth=1,
            marker = '.', markersize = 10, label = f'Crystal: {crystalid}')
ax.plot([minangle,minangle],[ymin,ymax], color = 'cyan', linestyle = '--', linewidth = 2.5,
                                         label = f'Data point of minimum ({minangle:.0f} urad)')
ax.plot([maxangle,maxangle],[ymin,ymax], color = 'darkorange', linestyle = '--', linewidth = 2.5,
                                          label = f'Data point of maximum ({maxangle:.0f} urad)')
if ifquadfit:
    ax.plot(xth,yth,color='red',linestyle='-',linewidth=1.5,marker='',
                                        label=f'Quadratic fit (minimum in {minpoint:.0f} urad)')

# Rifinisco la grafica
titlestring = f"{scantype} scan with {otheraxisname} fixed at {otheraxisvalue:.0f} urad (" + \
              r"$\pm$" + f" {tolerance:.0f})"
ax.set_title(titlestring, fontsize = 1.5*textfont)
ax.legend(loc = 'best', fontsize = 0.8*textfont, framealpha = 1)
ax.set_xlabel(f"{scantype} angle " + r"$[\mu rad]$",fontsize = textfont)
ax.set_ylabel(f"PH [a.u.]", fontsize = textfont)
ax.set_ylim([ymin,ymax])
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
fig.set_tight_layout('tight')
fig.savefig(imgloc + f'crystalstereogram1D_{crystalid}' + filetype,dpi = filedpi)
plt.show()

print(f"The 1D stereogram has been created and saved!")
print(f"")