# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre un file di stato TXT che contiene la codifica dei 
# tagli da applicare sui dati acquisiti durante un beamtest.
#
# Modalità d'uso: lo script va chiamato via python3 da linea di comando, senza argomenti. 
# 
# $ python3 cutconfigurator.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import os

# ==========================================================================================
#                                  QUALI TAGLI APPLICARE
# ==========================================================================================
# Definisco quali tagli applicare
ifPH = True
ifPT = True
ifPTD = True
iftheta_in = True
ifhitpos = True
ifevnum = False

# Produco il file dove lo scrivo
filepath = 'cutstatus_whichones_tbtools.txt'
with open(filepath,'w') as file:
    file.write(f"ifPH = {ifPH}\n")
    file.write(f"ifPT = {ifPT}\n")
    file.write(f"ifPTD = {ifPTD}\n")
    file.write(f"iftheta_in = {iftheta_in}\n")
    file.write(f"ifhitpos = {ifhitpos}\n")
    file.write(f"ifevnum = {ifevnum}\n")

print(f"Cut configuration file n. 1 (cut types) written at:")
print(filepath)
print(f"")

# ==========================================================================================
#                                  QUALI SOGLIE APPLICARE
# ==========================================================================================
# Definisco quali soglie applicare sui tagli
refPHchs = [5]                          # list of int, or "None"
PHcut_values = [200,10000]              # list of int, or "None"
refPTchs = [5]                          # list of int, or "None"
PTcut_values = [150,250]                # list of int, or "None"
refPTDchs = [2]                         # list of int, or "None"
PTDcut_values = [-100,100]              # list of int, or "None"
theta_in_values = [-3,1,-3,3]           # list of float, or "None"
hitpos_dist = 50                        # float, or "None"
hitpos_values = [4,6,2,5]               # list of float, or "None"
evnum_values = None                     # list of int, or "None"

# Produco il file dove lo scrivo
filepath = 'cutstatus_whichvalues_tbtools.txt'
with open(filepath,'w') as file:
    file.write(f"refPHchs = {refPTchs}\n")
    file.write(f"PHcut_values = {PHcut_values}\n")
    file.write(f"refPTchs = {refPTchs}\n")
    file.write(f"PTcut_values = {PTcut_values}\n")
    file.write(f"refPTDchs = {refPTDchs}\n")
    file.write(f"PTDcut_values = {PTDcut_values}\n")
    file.write(f"theta_in_values = {theta_in_values}\n")
    file.write(f"hitpos_dist = {hitpos_dist}\n")
    file.write(f"hitpos_values = {hitpos_values}\n")
    file.write(f"evnum_values = {evnum_values}\n")

print(f"Cut configuration file n. 2 (cut thresholds) written at:")
print(filepath)
print(f"")
print(f"Job done!")
print(f"")