# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre i plot di analisi di tutti i canali del digitizer, ovvero:
#   - Spettri di PH (1D)
#   - Spettri di tempo di picco (1D)
#   - Spettri di PH vs tempo di picco (2D)
#   - Spettri di PH vs numero di evento (2D)
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_digiplots.py nrun PHstep ifPHlog timestep iftimelog ieventstep ifprofile
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [PHstep]        int, numero di bin per gli spettri in PH (default: 100)
# 3 [ifPHlog]       str, se fare gli spettri di PH in scala log ("True", "False") 
#                     (default: "False")
# 4 [timestep]      int, numero di bin per gli spettri in tempo di picco (default: 50)
# 5 [iftimelog]     str, se fare gli spettri di tempo di picco in scala log ("True", "False")
#                     (default: "False")
# 6 [ieventstep]    int, binning step per gli spettri in numero di eventi (default: 250)
# 7 [ifprofile]     str, se fare il profile plot in PH vs numero di evento ("True", "False") 
#                     (default: "False")
#
# Esempi:
# $     python3 tbeam_digiplots.py 530090 20 True 10 False 100 True
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster nei silici, digi PH e tempi di picco)
veclist = [ data_namesdict['digiPH'], data_namesdict['digiTime'], data_namesdict['ievent'] ]
digiPH, digitime, eventNumber = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    digitime = digitime[cond,:]
    eventNumber = eventNumber[cond]

# ==========================================================================================
#                                  PLOT DI CONTROLLO: PH
# ==========================================================================================
# Definisco alcune variabili preliminari: numero di figure da creare, binning step per gli spettri, 
# se usare una scala log...
nplot_perside = 3
nplots = nplot_perside**2
ncols = np.shape(digiPH)[1]                # Numero di canali
nfigs = int(np.ceil(ncols/nplots))         # Numero di figure da creare

try:
    nbins_PH = int(sys.argv[2])            # Binning per gli spettri di PH
except:
    nbins_PH = 100                         # Default value

try:
    iflogPH = (sys.argv[3] == 'True')      # Se usare la scala log
except:
    iflogPH = False
    
# Per ogni blocco di figure procedo a creare gli istogrammi di frequenze di PH e li raggruppo in figure 3x3
xlimsup = [15000, 15000, 1000, 200, 10000, 50, 12000, 1000]
for i in range(nfigs):
    # Creo la figura
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplots):
        # Se ho finito le variabili, esco dal ciclo
        plotind = j+i*nplots    # Questa variabile punta a ogni iterazione su una colonna diversa di digiPH
        if plotind >= ncols:
            break

        # Se il canale da guardare è vuoto, proseguo oltre
        if digi_content[str(plotind)] is None:
            continue
        
        # Creo l'istogramma di frequenze e lo rappresento
        histo_PH, bins_PH = truehisto1D(digiPH[:,plotind],nbins_PH,ifstep=False,edgeL=0)
        
        ax = fig.add_subplot(nplot_perside,nplot_perside,j+1)
        histoplotter1D(ax,bins_PH,histo_PH,'PH [a.u.]','Counts',f"Ch. {plotind}, {digi_content[str(plotind)]}",
                      'best',textfont*1.25)
        ax.legend(loc='best',framealpha=1,fontsize=textfont)
        ax.set_xlim([0,xlimsup[plotind]])
        if iflogPH:
            ax.set_yscale('log')
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"PH monitor n. " + str(i+1) + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

    # Salvataggio in due location
    savefigname = '_digi_histo1D_PH_' + str(i+1)
    fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + 'run' + str(run) + '_digi_histo1D_PHs' + filetype
allfiles = glob.glob(imgloc + 'run' + str(run) + '_digi_histo1D_PH_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)
    
merger.write(mergedfile)
merger.close()

print(f"")
print(f"PH spectra have been created, merged and saved.")
    
# ==========================================================================================
#                                  PLOT DI CONTROLLO: TEMPO
# ==========================================================================================
# Definisco alcune variabili preliminari
try:
    nbins_time = int(sys.argv[4])           # Binning per gli spettri di tempo di picco
except:
    nbins_time = 50                         # Default value

try:
    iflogtime = (sys.argv[5] == 'True')     # Se usare la scala log
except:
    iflogtime = False
    
# Per ogni blocco di figure procedo a creare gli istogrammi di frequenze di tempo e li raggruppo in figure 3x3
for i in range(nfigs):
    # Creo la figura
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplots):
        # Se ho finito le variabili, esco dal ciclo
        plotind = j+i*nplots    # Questa variabile punta a ogni iterazione su una colonna diversa di digiPH
        if plotind >= ncols:
            break

        # Se il canale da guardare è vuoto, proseguo oltre
        if digi_content[str(plotind)] is None:
            continue
        
        # Creo l'istogramma di frequenze e lo rappresento
        histo_time, bins_time = truehisto1D(digitime[:,plotind],nbins_time,ifstep=False,edgeL=0)
        
        ax = fig.add_subplot(nplot_perside,nplot_perside,j+1)
        histoplotter1D(ax,bins_time,histo_time,'Peak time [ticks]','Counts',f"Ch. {plotind}, {digi_content[str(plotind)]}",
                            'best',textfont*1.25)
        ax.legend(loc='best',framealpha=1,fontsize=textfont)
        if iflogtime:
            ax.set_yscale('log')

    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"Peak Time monitor n. " + str(i+1) + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

    # Salvo la figura in due location
    savefigname = '_digi_histo1D_time_' + str(i+1)
    fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + 'run' + str(run) + '_digi_histo1D_times' + filetype
allfiles = glob.glob(imgloc + 'run' + str(run) + '_digi_histo1D_time_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()

print(f"Peak Time spectra have been created, merged and saved.")

# ==========================================================================================
#                            PLOT DI CONTROLLO: PH/TEMPO DI PICCO
# ==========================================================================================  
# Per ogni blocco di figure procedo a creare gli istogrammi 2D di tempo e PH
colmap = 'jet'   # Parametri per gli istogrammi 2D
slope = 1

for i in range(nfigs):
    # Creo la figura
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplots):
        # Se ho finito le variabili, esco dal ciclo
        plotind = j+i*nplots    # Questa variabile punta a ogni iterazione su una colonna diversa di digiPH
        if plotind >= ncols:
            break
        
        # Se il canale da guardare è vuoto, proseguo oltre
        if digi_content[str(plotind)] is None:
            continue

        # Creo l'istogramma di frequenze e lo rappresento
        histo2D_PHtime, binsX_PH, binsY_time = truehisto2D(digiPH[:,plotind],digitime[:,plotind],nbins_PH,nbins_time,
                                                           ifstepX = False, ifstepY = False,edgeLX=0,edgeLY=0)
        
        ax = fig.add_subplot(nplot_perside,nplot_perside,j+1)
        histoplotter2D(ax,binsX_PH,binsY_time,histo2D_PHtime,colmap,slope,'Pulse Height [a.u.]','Peak Time [ticks]',
                        1.25*textfont,iflog = True,ifcbarfont=True)
        ax.set_title(label = f"Ch. {plotind}, {digi_content[str(plotind)]}", fontsize = 2*textfont)
    
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"PH/PT monitor n. " + str(i+1) + f'({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

    # Salvo la figura in due location
    savefigname = '_digi_histo2D_PHtime_' + str(i+1)
    fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + 'run' + str(run) + '_digi_histo2D_PHtimes' + filetype
allfiles = glob.glob(imgloc + 'run' + str(run) + '_digi_histo2D_PHtime_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()

print(f"PH vs Peak Time 2D-spectra have been created, merged and saved.")
    
# ==========================================================================================
#                                PLOT DI CONTROLLO: PH/EVENTO
# ==========================================================================================  
# Definisco il binning per l'asse del numero di evento e i limiti dei plot
try:
    nbins_event = int(sys.argv[6])
except:
    nbins_event = 250

try:
    if sys.argv[7] == 'True':
        ifproj = True
        pr = 'mean'
    else:
        ifproj = False
except:
    ifproj = False

# Per ogni blocco di figure procedo a creare gli istogrammi 2D di tempo e PH
xlims_low = [5000,5000,0,0,0,0,0,0]
xlims_up = [15000,15000,1000,100,1000,30,1000,600]

for i in range(nfigs):
    # Creo la figura
    fig = plt.figure(figsize = dimfigbig)

    # Itero su ogni canale del blocco i-esimo
    for j in range(nplots):
        # Se ho finito le variabili, esco dal ciclo
        plotind = j+i*nplots    # Questa variabile punta a ogni iterazione su una colonna diversa di digiPH
        if plotind >= ncols:
            break

        # Se il canale da guardare è vuoto, proseguo oltre
        if digi_content[str(plotind)] is None:
            continue
        
        # Creo l'istogramma di frequenze e lo rappresento
        histo2D_PHnevent, binsX_nevent, binsY_PH = truehisto2D(eventNumber,digiPH[:,plotind],nbins_event,2*nbins_PH,
                                                           ifstepX = False, ifstepY = False,edgeLX=0,edgeLY=0)
        
        ax = fig.add_subplot(nplot_perside,nplot_perside,j+1)
        histoplotter2D(ax,binsX_nevent,binsY_PH,histo2D_PHnevent,colmap,slope,'Event number','Pulse Height',
                        1.25*textfont,iflog = True,ifcbarfont=True)
        
        if ifproj:
            # If required, add the projection (profile plot) of the 2D histogram
            truex, histosum, _ = histoproj(histo2D_PHnevent, binsX_nevent, binsY_PH, axis = 'x', projtype = pr)
            ax.plot(truex,histosum,color = 'red', marker = '.', markersize = 3, linestyle = '--', linewidth = 1, label = 'Profile plot')
        #ax.set_ylim([0,np.nanmax(digiPH[:,plotind])/15])
        ax.set_ylim([xlims_low[plotind],xlims_up[plotind]])
        ax.set_title(label = f"Ch. {plotind}, {digi_content[str(plotind)]}", fontsize = 2*textfont)
        
        
    # Rifinisco la grafica
    fig.tight_layout(rect=[0, 0.03, 1, 0.925])
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    fig.suptitle(f"PH/Event monitor n. " + str(i+1) + f' ({datetime.date.today()}, {time_readable})', fontsize = 2.5*textfont)

    # Salvo la figura in due location
    savefigname = '_digi_histo2D_PHevent_' + str(i+1)
    fig.savefig(imgloc + 'run' + str(run) + savefigname + filetype,dpi = filedpi)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype_www,dpi = filedpi_www)
    fig.savefig(imgloc_www + 'lastrun' + savefigname + filetype,dpi = filedpi)
    plt.show()
    plt.close(fig)
    
# Unisco tutte le figure in un file unico e le rimuovo
mergedfile = imgloc + 'run' + str(run) + '_digi_histo2D_PHevents' + filetype
if os.path.exists(mergedfile):
    os.remove(mergedfile)
    
allfiles = glob.glob(imgloc + 'run' + str(run) + '_digi_histo2D_PHevent_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    os.remove(pdf)

merger.write(mergedfile)
merger.close()
    
print(f"PH vs Event 2D-spectra have been created, merged and saved.\n")