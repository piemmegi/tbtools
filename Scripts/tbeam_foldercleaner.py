# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è svuotare la cartella dove sono contenuti tutti i file immagine
# e i file temporanei prodotti dal monitor di analisi dati.#
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_imagecleaner.py ifcleanimg ifcleantemp
#
# Argomenti:
# 1 [ifcleanimg]    str, se pulire la cartella delle immagini ("True", "False") 
#                     (default: "True")
# 2 [ifcleantemp]   str, se pulire la cartella dei file temporanei ("True", "False") 
#                     (default: "False")
#
# Esempi:
# $     python3 tbeam_foldercleaner.py True False
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                    PULIZIA DELLE CARTELLE
# ==========================================================================================
# Definisco se pulire la cartella delle immagini
try:
    if (sys.argv[1] == 'True'):
        ifcleanimg = True
    else:
        ifcleanimg = False
except:
    ifcleanimg = True

# Definisco se pulire la cartella temp
try:
    if (sys.argv[2] == 'True'):
        ifcleantemp = True
    else:
        ifcleantemp = False
except:
    ifcleantemp = False

# Procedo a rimuovere immagini e file temporanei, se richiesto.
print(f"")

if ifcleanimg:
    filetypes = ['pdf','png','jpg','jpeg']
    for el in filetypes:
        allfiles = glob.glob(os.path.join(imgloc,'*.' + el))
        for pdf in allfiles:
            os.remove(pdf)

    print(f"All the images previously saved in the following folder have been deleted:")
    print(imgloc)
else:
    print(f"No image has been deleted.")

print(f"")

if ifcleantemp:
    filetypes = ['txt','npz','npy']
    for el in filetypes:
        allfiles = glob.glob(os.path.join(temploc,'*.' + el))
        for pdf in allfiles:
            os.remove(pdf)

    print(f"All the temporary files previously saved in the following folder have been deleted:")
    print(temploc)
else:
    print(f"No temporary file has been deleted.")

print(f"")
print(f"Job done! \n")