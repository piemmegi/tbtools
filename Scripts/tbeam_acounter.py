# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è contare online il numero di eventi presenti in una run dati.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_acounter.py nrun
#
# Argomenti:
# 1 [nrun]:         int, numero della run da analizzare
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster)
veclist = [data_namesdict['ievent']]
ievent = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# ==========================================================================================
#                                  TAGLIO E PRINTOUT
# ==========================================================================================
# Stampo il numero di eventi disponibili
dimtot = np.shape(ievent)[0]
print(f"")
print(f"Number of events available in the run: {dimtot}")

# Se necessario, stampo il numero di single cluster event
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (ievent >= 0) & (np.isnan(ievent) == False)

    # La applico
    ievent_cut = ievent[cond]

    # Conto gli eventi single cluster
    dimsingle = np.shape(ievent_cut)[0]
    fracsingle = dimsingle/dimtot
    print(f"Single cluster events: {dimsingle} ({fracsingle:2.2f} %)")

# Calcolo il timestamp di generazione del file
time_generation = time.localtime(os.path.getatime(run_name))
strtime1 = f"{time_generation[0]:02d}-{time_generation[1]:02d}-{time_generation[2]:02d}, "
strtime2 = f"{time_generation[3]:02d}:{time_generation[4]:02d}:{time_generation[5]:02d}"

# Calcolo il rate di produzione degli eventi
rate = dimtot / (os.path.getctime(run_name) - os.path.getatime(run_name))   # eventi/s
rate_min = rate * 60  # eventi/min

# ==========================================================================================
#                                  WRITEOUT
# ==========================================================================================
# Definisco dove printare l'output
outfilename = os.path.join(imgloc_www + 'acounter_out.txt')

# Scrivo l'output
print(f"Writing the output file... \n")
with open(outfilename,'w', encoding="utf-8") as file:
    # Timestamp
    time_now = time.localtime()
    time_readable = time.strftime("%H:%M:%S", time_now)
    file.write(f"Current time: {datetime.date.today()}, {time_readable} \n")

    # Numero di run
    file.write(f"Run number: {run} \n")

    # Tempo di creazione della run
    file.write(f"Time of creation of the run: {strtime1+strtime2} \n\n")

    # Eventi disponibili
    file.write(f"Number of events available in the run: {dimtot} \n")
    if ifcuthit:
        print(f"Number of single cluster events avalable in the run: {dimsingle} ({fracsingle:2.2f} %) \n")

    file.write(f"Average event acquisition rate: {rate_min:.0f} events/min \n\n")

print(f"File written:")
print(outfilename)