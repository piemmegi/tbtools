# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre la mappa di efficienza di un qualsiasi oggetto o 
# rivelatore, ricostruita usando due tracciatori e applicando un taglio in una variabile
# di riferimento.
#
# Modalità d'uso (chiamata da linea di comando, assumendo di essere in ./tbtools/Scripts):
# $     python3 tbeam_efficiency.py nrun cut_type cut_inds cut_thr cut_coupling nbins_pos slope weight axlims
#
# Argomenti:
# 1 [nrun]          int, numero della run da analizzare
# 2 [cut_type]      str, tipo di taglio da applicare ("PH" oppure "cluster") (default: "PH")
# 3 [cut_inds]      list of int, indici delle variabili da usare per i tagli in efficienza.
#                     Se cut_type è "PH", gli indici sono di digiPH. Se cut_type è "cluster",
#                     gli indici sono di nclu. Non si possono fare tagli sia in cluster sia in PH
#                     I tagli possono essere in and o in or. (default: [0])
# 4 [cut_thr]       list of int, threshold per i tagli in efficienza. (default: 100 se
#                     cut_type è "PH", altrimenti 4)
# 5 [cut_coupling]  str, tipo di accoppiamento dei tagli se si indicano variabili multiple
#                     ("and", "or") (default: "and")
# 6 [nbins_pos]     float, binning step in cm per gli istogrammi 2D e la mappa di efficienza
#                     (default: 0.01)
# 7 [slope]         float, punto di half-way della scala colore, altrimenti "log",
#                     se si vuole una scala log, altirmenti "None" per nessuna scala custom
#                     (default: "None")
# 8 [weight]        int, indice del canale da usare se si vogliono costruire mappe di hit
#                     pesate rispetto a una variabile del digitizer, oppure "None", se non
#                     si vogliono applicare pesi (default: "None")
# 9 [axlims]        list of float, lista dei limiti degli assi del tipo [xmin, xmax,
#                     ymin, ymax], oppure "None" per nessun limite custom (default: "None")
#
# Esempi:
# $     python3 tbeam_efficiency.py 680490 PH [1] [100] or 0.1 0.5 1 None None
# $     python3 tbeam_efficiency.py 520092 PH [3,4] [500,500] or 0.025 0.6 None None
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import datetime                     # Per timestamp

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
thisfolder = os.path.dirname(os.path.abspath(__file__))
parentfolder = os.path.dirname(thisfolder)
sys.path.append(parentfolder + '/Functions/')
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
                                             # Queste mi servono, in realtà, solo nei casi di calcoli di efficienza

# Definizione delle variabili per i plot
textfont = 15         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filetype_www = '.png' # Formato di salvataggio delle immagini per il monitor online
filedpi_www = 250     # Risoluzione in caso di salvataggio di png/jpeg
filedpi = 300         # Risoluzione in caso di salvataggio di immagini per il monitor online
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                              VARIABILI DELLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = thisfolder + '/statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
datafiletype     = statusdata["datafiletype"]
globaltree       = statusdata["globaltree"]
prename          = statusdata["prename"]
distances        = statusdata["distances"]
ndet             = statusdata["ndet"]
data             = statusdata["data"]
data_namesdict   = statusdata["data_namesdict"]
digi_content     = statusdata["digi_content"]
gonio_content    = statusdata["gonio_content"]
ifmatrix         = statusdata["ifmatrix"]
ifcuthit         = statusdata["ifcuthit"]
nsili_cut        = statusdata["nsili_cut"]
imgloc           = statusdata["imgloc"]
imgloc_www       = statusdata["imgloc_www"]
temploc          = statusdata["temploc"]
cutfilepaths     = statusdata["cutfilepaths"]

# ==========================================================================================
#                                     UPLOAD DEI DATI
# ==========================================================================================
# Carico la run da esaminare
run = sys.argv[1]
run_name = pathnamedef(prename,run,datafiletype)

# Estraggo i vettori necessari per l'analisi dal file dati (numero di cluster e posizioni 
# di hit nei silici, digi PH)
veclist = [ data_namesdict['digiPH'], data_namesdict['xpos'],
            data_namesdict['ievent'], data_namesdict['info_plus'] ]

digiPH, ch_pos, eventNumber,info_plus = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

# Definisco una variabile tipo "zs2s3" ma in modo generale (2, 3 sono qui i silici prima e
# dopo il cristallo) e calcolo le distanze del setup
name_intersili = "zs" + str(distances["relevantsili"]-1) + "s" + str(distances["relevantsili"]) 
z_intersili = distances[name_intersili]
z_silicrys = z_intersili + distances["zcry"]

# Indico l'indice che indica la colonna di ch_numhit corrispondente alla x del primo da silicio 
# da usare nel tracciamento. Esempio: se traccio con S2 e S3, la x è (numerano da zero) la n. 2,
#  da accoppiare a n. 4 (x di S3), mentre le corrispondenti y sono n. 3 (S2) e n. 5 (S3). 
ind = 2*(distances["relevantsili"]-1)-2     # Se S2-S3, qui vale 2

# ==========================================================================================
#                              PREPARAZIONE DELLE VARIABILI
# ==========================================================================================
# Se richiesto, taglio gli eventi in numero di cluster
if ifcuthit:
    # Carico il numero di cluster nei silici
    veclist = [data_namesdict['nclu']]
    ch_numhit = dataupload(data,run_name,veclist,datafiletype,globaltree,ifmatrix)

    # Creo la condizione di taglio
    condmap = (ch_numhit[:,:nsili_cut] == 1)
    condcomp = condmap.prod(axis = 1)
    cond = (condcomp == 1) & (eventNumber >= 0) & (np.isnan(eventNumber) == False)

    # La applico
    digiPH = digiPH[cond,:]
    ch_pos = ch_pos[cond,:]
    eventNumber = eventNumber[cond]
    info_plus = info_plus[cond,:]

# ==========================================================================================
#                                ESTRAZIONE DEGLI ARGOMENTI
# ==========================================================================================
# Definisco il tipo di taglio per la mappa di efficienza (cluster o PH)
try:
    if (sys.argv[2] != 'PH') & (sys.argv[2] != 'cluster'):
        # Variabile non riconosciuta
        print(f"Type of variable to be used in the efficiency map unrecognized. PH was chosen.")
        cutmethod = 'PH'
    else:
        # Variabile riconosciuta
        cutmethod = sys.argv[2]
except:
    cutmethod = 'PH'

# Colonne da usare per il taglio
try:
    listvars = str2list(sys.argv[3],int)      # Se esiste, converto la input string in lista
except:
    listvars = [0]                            # Di default, se non riesco a definire le colonne da usare, uso la prima

ncutvars = len(listvars)                      # Numero di variabili su cui applicare il taglio
    
# Soglie di taglio
try:
    listthrs = str2list(sys.argv[4],int)      # Se esiste, converto la input string in lista
except:
    # Se non ho definito le soglie, uso valori diversi per PH e cluster.
    if cutmethod == 'PH':
        mythr = 100                           # Soglia arbitraria: tanti ADC
    else:                                     # Qui ricadono i casi "cluster" e "qualsiasi altra cosa"
        mythr = 4                             # Soglia arbitraria: tanti cluster
    
    listthrs = [mythr for _ in listvars]

# Tipo di accoppiamento per tagli multipli (and/or)
try:
    if (sys.argv[5] != 'and') & (sys.argv[5] != 'or'):
        # Variabile non riconosciuta
        print(f"Type of coupling cut to be used in the efficiency map unrecognized. 'and'' was chosen.")
        couplingcut = 'and'
    else:
        # Variabile riconosciuta
        couplingcut = sys.argv[5]
except:
    couplingcut = 'and'

# Binning step per la mappa di efficienza
try:
    nbins_pos = float(sys.argv[6])
except:
    nbins_pos = 0.01                        # Default value in cm
    
# Definisco la midway della slope e se andare in scala log lungo z:
try:
    if sys.argv[7] == 'log':
        iflog = True
        slope = 1
    else:
        iflog = False
        slope = float(sys.argv[7])
except:
    iflog = False
    slope = 0.5

# Definisco se pesare l'istogramma o no
try:
    if sys.argv[8] == 'None':
        ifnoweight = True
        all_weights = None
        good_weights = None
        weightstr = 'unweighted'
    else:
        # Costruisco i pesi
        wind = int(sys.argv[8])
        weights = digiPH[:,wind]
        
        # Costruisco altre variabili id flag
        ifnoweight = False
        weightstr = 'weighted'
except:
    ifnoweight = True
    all_weights = None
    good_weights = None
    weightstr = 'unweighted'

# Definisco i limiti per i plot
try:
    if sys.argv[9] == 'None':
        # Caso 1: non implemento limiti di asse
        listcut = None
    else:
        # Caso 2: implemento limiti specifici (in cm)
        listcut = str2list(sys.argv[9],float)
except:
    listcut = None

# ==========================================================================================
#                         DEFINIZIONE DELLA CONDIZIONE DI SELEZIONE
# ==========================================================================================
# Definisco gli eventi sopra e sotto soglia per la mappa di efficienza. In base al tipo di 
# metodo di taglio applicato, devo procedere in un modo diverso
allconds = np.zeros((np.shape(digiPH)[0],ncutvars))
if cutmethod == 'PH':
    # Definisco i vettori booleani di selezione degli eventi. Per farlo, itero sulle variabili da usare come reference.
    for i,el in enumerate(listvars):
        cond = (digiPH[:,el] >= listthrs[i])
        allconds[:,i] = cond
else: # Qui ricadono i casi "cluster" e "qualsiasi altra cosa"
    # Definisco i vettori booleani di selezione degli eventi. Per farlo, itero sulle variabili da usare come reference.
    for i,el in enumerate(listvars):
        cond = (ch_numhit[:,el] >= listthrs[i])
        allconds[:,i] = cond
        
# Se ho più di un vettore condizionale, devo fare un merge (and/or).
if (ncutvars > 1):
    # Switch sul tipo di merging condition (and/or)
    if couplingcut == 'or':
        finalcond = allconds.sum(axis = 1)
        finalcond = (finalcond >= 1)
    else: # Qui ricade il caso 'and' e tutto il resto
        finalcond = allconds.prod(axis = 1)
        finalcond = (finalcond == 1)
else:
    finalcond = (allconds[:,0] == 1)

# ==========================================================================================
#                                 RICOSTRUZIONE DELLE TRACCE
# ==========================================================================================
# Ricostruzione delle tracce e dei punti di impatto
crys_pos = []

for j in range(2):
    # j indica se sto guardando x oppure y. In entrambi i casi, seleziono dai silici gli eventi di interesse
    refind = j % 2                         # Vale 0 quando lavoro su x e 1 su y
    cleanpos_a = ch_pos[:,ind+refind]      # Accoppio 2 e 4 (x dei primi 2 silici) o 3 e 5 (y)
    cleanpos_b = ch_pos[:,ind+refind+2]

    # Ricostruisco m e q delle traccie lineari delle traiettorie
    m = (cleanpos_b - cleanpos_a)/z_intersili
    q = cleanpos_b - m*z_intersili
    
    # Calcolo il punto di impatto sul piano del cristallo
    hitpos = m*z_silicrys+q
    crys_pos.append(hitpos)
    
# Ricavo la condizione che definisce quali eventi corrispondono a hit fuori dall'area attiva
# di rivelazione (di solito, da 0 a 10 con le camere, da 0 a 2 con i silici)
posthr = [0,10]
poscond = (crys_pos[0] >= posthr[0]) & (crys_pos[1] >= posthr[0]) & (crys_pos[0] <= posthr[1]) & (crys_pos[1] <= posthr[1])

# Applico le condizioni di taglio
all_pos = []
all_pos.append(crys_pos[0][poscond])
all_pos.append(crys_pos[1][poscond])

good_pos = []
good_pos.append(crys_pos[0][finalcond & poscond])
good_pos.append(crys_pos[1][finalcond & poscond])

# Taglio i pesi come i vettori da usare negli istogrammi, se richiesto
if not ifnoweight:
    all_weights = weights[poscond]
    good_weights = weights[finalcond & poscond]

# ==========================================================================================
#                                  MAPPA DI EFFICIENZA
# ==========================================================================================
# Creo gli istogrammi di frequenze degli eventi con e senza taglio in efficienza
histo2D_nocut, binsX, binsY = truehisto2D(all_pos[0],all_pos[1],nbinsX = nbins_pos, nbinsY = nbins_pos,
                                         ifstepX = True, ifstepY = True, weights = all_weights)
histo2D_cut, _, _ = truehisto2D(good_pos[0],good_pos[1],binsX = binsX,binsY = binsY,weights = good_weights)

# Creo la mappa di efficienza
histofrac = histo2D_cut/histo2D_nocut
histofrac[np.isnan(histofrac)] = 0

# ==========================================================================================
#                                  PLOT
# ==========================================================================================
# Calcolo il timestamp di processamento
time_now = time.localtime()
time_readable = time.strftime("%H:%M:%S", time_now)

# Definisco altre variabili utili per i plot
colmap = 'jet'      # Mappa di colore
partslope1 = np.max(histo2D_nocut)/2
partslope2 = np.max(histo2D_cut)/2

# Prima figura: mappa degli hit senza condizioni di taglio, con o senza pesi
namestr1 = '_efficiency_allhits'

fig, ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binsX,binsY,histo2D_nocut,colmap,partslope1,'Hit position along x [cm]','Hit position along y [cm]',textfont)
ax.set_title(f'Hit map (all events, {weightstr}) \n({datetime.date.today()}, {time_readable})',fontsize = 1.5*textfont)
if listcut is not None:
    ax.set_xlim(listcut[0:2])
    ax.set_ylim(listcut[2:])
fig.set_tight_layout('tight')
fig.savefig(imgloc + 'run' + str(run) + namestr1 + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + namestr1 + filetype,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + namestr1 + filetype_www,dpi = filedpi_www)
plt.show()
plt.close(fig)

# Seconda figura: mappa degli hit con condizioni di taglio, con o senza pesi
namestr2 = '_efficiency_cuthits'

fig, ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binsX,binsY,histo2D_cut,colmap,partslope2,'Hit position along x [cm]','Hit position along y [cm]',textfont)
ax.set_title(f'Hit map (selected events, {weightstr}) \n({datetime.date.today()}, {time_readable})',fontsize = 1.5*textfont)
if listcut is not None:
    ax.set_xlim(listcut[0:2])
    ax.set_ylim(listcut[2:])
fig.set_tight_layout('tight')
fig.savefig(imgloc + 'run' + str(run) + namestr2 + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + namestr2 + filetype,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + namestr2 + filetype_www,dpi = filedpi_www)
plt.show()
plt.close(fig)

# Terza figura: mappa di efficienza
namestr3 = '_efficiency_map'

fig,ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,binsX,binsY,histofrac,colmap,slope,'Hit position along x [cm]','Hit position along y [cm]',textfont,iflog)
if listcut is not None:
    ax.set_xlim(listcut[0:2])
    ax.set_ylim(listcut[2:])
ax.set_title(f'Efficiency map ({weightstr}) \n({datetime.date.today()}, {time_readable})', fontsize = 1.5*textfont)
fig.set_tight_layout('tight')
fig.savefig(imgloc + 'run' + str(run) + namestr3 + filetype,dpi = filedpi)
fig.savefig(imgloc_www + 'lastrun' + namestr3 + filetype,dpi = filedpi_www)
fig.savefig(imgloc_www + 'lastrun' + namestr3 + filetype_www,dpi = filedpi_www)
plt.show()

# Unisco le figure senza rimuovere le parziali
mergedfile = imgloc + 'run' + str(run) + '_mergedefficiency' + filetype
allfiles = glob.glob(imgloc + 'run' + str(run) + '_efficiency_*')
merger = PyPDF2.PdfMerger()
for pdf in allfiles:
    merger.append(pdf)
    #os.remove(pdf)

merger.write(mergedfile)
merger.close()
print(f"")
print(f"The efficiency map has been created and saved.")
print(f"")