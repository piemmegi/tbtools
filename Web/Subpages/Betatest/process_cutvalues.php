<?php
// ====================================================================
// This script aims at processing the data collected with one of the
// HTML forms available in the Live Online Monitor. Specifically,
// this one processes the thresholds for the cuts to be applied
// to the data collected in the beamtest (but not which cut to apply).
// ====================================================================

// First of all, define all the reference channels for the PH, PT and PTD
// cuts, and the threshold for these cuts and also the incidence angle,
// divergence and event interval cuts. Each of these values should be
// initialized to "None", which means that the scripts will ignore them.
$refPHchs = $refPTchs = $refPTDchs = "None";
$PHcut_values = $PTcut_values = $PTDcut_values = "None";
$theta_in_values = $hitpos_dist = $hitpos_values = $evnum_values = "None";

// Now check every entry for the PH, PT and PTD cuts. If one
// is set, change the corresponding variable.
if(isset($_POST['PH_channels'])) {
    $refPHchs = $_POST['PH_channels'];
}

if(isset($_POST['PH_values'])) {
    $PHcut_values = $_POST['PH_values'];
}

if(isset($_POST['PT_channels'])) {
    $refPTchs = $_POST['PT_channels'];
}

if(isset($_POST['PT_values'])) {
    $PTcut_values = $_POST['PT_values'];
}

if(isset($_POST['PTD_channels'])) {
    $refPTDchs = $_POST['PTD_channels'];
}

if(isset($_POST['PTD_values'])) {
    $PTDcut_values = $_POST['PTD_values'];
}

// Now do the same for the hist
if(isset($_POST['theta_in_values'])) {
    $theta_in_values = $_POST['theta_in_values'];
}

if(isset($_POST['hitpos_dist'])) {
    $hitpos_dist = $_POST['hitpos_dist'];
}

if(isset($_POST['hitpos_values'])) {
    $hitpos_values = $_POST['hitpos_values'];
}

if(isset($_POST['evnum_values'])) {
    $evnum_values = $_POST['evnum_values'];
}

// Now prepare the output to be written to the ifcut status file
$outdata = "refPHchs = " . $refPHchs . "\r\n"
            . "PHcut_values = " . $PHcut_values . "\r\n"
            . "refPTchs = " . $refPTchs . "\r\n"
            . "PTcut_values = " . $PTcut_values . "\r\n"
            . "refPTDchs = " . $refPTDchs . "\r\n"
            . "PTDcut_values = " . $PTDcut_values . "\r\n"
            . "theta_in_values = " . $theta_in_values . "\r\n"
            . "hitpos_dist = " . $hitpos_dist . "\r\n"
            . "hitpos_values = " . $hitpos_values . "\r\n"
            . "evnum_values = " . $evnum_values . "\r\n";

// Now write them
$outfilename = 'cutstatus_whichvalues_www.txt';
$ret = file_put_contents($outfilename, $outdata, LOCK_EX);
if($ret === false) {
    die('There was an error writing the output data!');
}
else {
    echo nl2br ("Status: $ret bytes written to the output file, named: << $outfilename >> \r\n \r\n");
    echo nl2br ("The data written to the output is: \r\n \r\n");
    echo nl2br ("$outdata \r\n \r\n");
}