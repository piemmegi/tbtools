<?php
// ====================================================================
// This script aims at processing the data collected with one of the
// HTML forms available in the Live Online Monitor. Specifically,
// this one processes which cuts should be applied to the data collected
// in the beamtest (but not the values for the cuts).
// ====================================================================

// First of all, define all the possible cuts and set the corresponding
// "ifapply" variables to False. This means that these cuts will not
// be applied.
$ifPH = $ifPT = $ifPTD = $iftheta_in = $ifhitpos = $ifevnum = "False";

// Now check every entry from the FORM. If one is found to be set to 
// "True", then change the value of the corresponding "ifapply" variable.
if(isset($_POST['ifPH'])) {
    $ifPH = "True";
}

if(isset($_POST['ifPT'])) {
    $ifPT = "True";
}

if(isset($_POST['ifPTD'])) {
    $ifPTD = "True";
}

if(isset($_POST['iftheta_in'])) {
    $iftheta_in = "True";
}

if(isset($_POST['ifhitpos'])) {
    $ifhitpos = "True";
}

if(isset($_POST['ifeventnum'])) {
    $ifevnum = "True";
}

// Now prepare the output to be written to the ifcut status file
$outdata = "ifPH = " . $ifPH . "\r\n"
            . "ifPT = " . $ifPT . "\r\n"
            . "ifPTD = " . $ifPTD . "\r\n"
            . "iftheta_in = " . $iftheta_in . "\r\n"
            . "ifhitpos = " . $ifhitpos . "\r\n"
            . "ifevnum = " . $ifevnum . "\r\n";

// Now write them
$outfilename = 'cutstatus_whichones_www.txt';
$ret = file_put_contents($outfilename, $outdata, LOCK_EX);
if($ret === false) {
    die('There was an error writing the output data!');
}
else {
    echo nl2br ("Status: $ret bytes written to the output file, named: << $outfilename >> \r\n \r\n");
    echo nl2br ("The data written to the output is: \r\n \r\n");
    echo nl2br ("$outdata \r\n \r\n");
}