# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è preparare la cartella di lavoro per l'analisi online dei 
# file prodotti durante un test beam del gruppo Insulab, svolto all'SPS e al PS del CERN
# La sua modalità d'uso è semplice: lo script va chiamato come eseguibile in python3 da
# linea di comando, con un solo argomento, che definisce se eventuali pacchetti mancanti
# saranno installati via pip3 o via conda. 
#
# Esempio con pip3:
# $ python3 ./setup.py pip3
#
# Esempio con conda (N.B. bisogna già essere nell'environment giusto)
# $ python3 ./setup.py conda
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import sys                          # Per gestione del sistema operativo
import os                           # Per gestione dei path di sistema
import glob                         # Per manipolazione dei path
import subprocess                   # Per eseguire script
import importlib.util               # Per importare i pacchetti non caricati
print(f"PMG is setting up your environment. Nobody moves and no one gets hurt. \n")

# ==========================================================================================
#                                         SCRIPT
# ==========================================================================================
# Assumiamo che la cartella di analisi sia stata integralmente copiata da git e quindi siano presenti MyFunctions.py, beamfunctions.py, statustcreator.py e tutti gli script di tipo tbeam_*.py.

# A questo punto, creiamo le cartelle di default per l'inserimento di immagini e file temporanei, se serve
thisfolder = os.path.dirname(__file__)
imgfolder = thisfolder + '/Images/'
filefolder = thisfolder + '/Tempfiles/'

if not os.path.exists(imgfolder):
    os.mkdir(imgfolder)
    print(f"The folder for image storage has been created:")
    print(imgfolder)
    print(f"")
    
if not os.path.exists(filefolder):
    os.mkdir(filefolder)
    print(f"The folder for temporary files storage has been created:")
    print(filefolder)
    print(f"")

# A questo punto devo verificare che tutti i moduli siano correttamente stati installati. Prima di tutto, elenco quelli che mi servono.
mymodules = ['h5py',
             'IPython',
             'lmfit',
             'matplotlib',
             'numpy',
             'pandas',
             'PyPDF2',
             'scipy',
             'scikit-learn',
             'seaborn',
             'uproot']

# Definisco una funzione che permette di caricare i moduli non disponibili (installazione tramite pip, ovviamente)
def install(package,installer):
    if (installer == 'pip') | (installer == 'pip3'):
        subprocess.check_call([sys.executable, "-m", "pip", "install", package])
    elif:
        subprocess.check_call([sys.executable, "-m", "conda", "install", package, "-c", "conda-forge"])
    else:
        print(f"Installer is not correctly defined...")

# Definisco l'installer usato in questo caso
try:
    installer = sys.argv[1]
except:
    # Default to pip, if nothing is defined
    installer = 'pip3'

# Itero su ogni modulo e verifico la sua installazione
for mod in mymodules:
    spec = importlib.util.find_spec(mod)
    if spec is None:
        print(mod +" package was not installed. Installing... \n")
        install(mod,installer)

# Concludo
print("PMG has finished. Have a nice beamtest! \n")