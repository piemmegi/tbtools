"""
This file contains a copy of several functions, which at some point were constructed
and used to perform some analysis, but then were essentially never used again.

Thus, this file acts as a historical archive, which may be used spot-on to make
some old code work again. 

Author: Pietro Monti-Guarnieri, Università di Ferrara (mntptr@unife.it)
Last update: 09th October, 2023
"""
# Module imports
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import h5py                         # Per caricare i file .h5
import copy                         # Per copiare gli oggetti in modo corretto
import subprocess                   # Per chiamare script come da bash
import matplotlib.cm as pltmaps     # Per le mappe
import scipy.special as sp

# Single function imports
from IPython.display import display, Javascript   # Per il warning visivo
from IPython.display import Audio   # Per un warning audio
from lmfit import Model             # Per fare fit
from functools import reduce        # Per usare intersect1d su più vettori assieme
from scipy.optimize import curve_fit as ft        # Per fare fit

# ===========================================================================================================
#                                       FROM OLD FILE: MyFunctions.py
# ===========================================================================================================
'''
Linear Regression function.
This function uses the analytical formulas given by the Numerical Recipes in C for the exact (non iterative) linear regression, with evaluation also of theh covariance between the fitting parametra m,q. This function has proven to be more stable than numpy.polyfit in most cases and it has also shown to extract parametra with smaller error.

Inputs (should be ndarrays, but not necessarily):
    - x,y: data to be fitted (with fit equation y = mx+q)
    - yerr: errors associated to y
    
Outputs:
    - m,q: fit parametra
    - errm,errq,cov: errors (std) associated to fit parametra m,q and their covariance
'''

def lfit(x,y,yerr):
    # Verify if the inputs are ndarrays
    x,y,yerr = np.array(x), np.array(y), np.array(yerr)
    
    # Evaluate weights and parametra needed for the regression
    w = yerr**(-2)
    w[w == np.inf] = 0
    S = w.sum()
    Sx = (x*w).sum()
    Sxx = ((x**2)*w).sum()
    Sy = (y*w).sum()
    Syy = ((y**2)*w).sum()
    Sxy = (x*y*w).sum()
    D = S*Sxx-Sx**2
    
    # Evaluate regression parametra
    M = (S*Sxy-Sx*Sy)/D
    Q = (Sxx*Sy-Sx*Sxy)/D
    errM = np.sqrt(S/D)
    errQ = np.sqrt(Sxx/D)
    cov = -Sx/D
    return([M,Q],[errM,errQ,cov])


'''
Bar Binomial Error function.
This function evaluates the uncertainities associated to a set of points by means of the binomial statistics. This is the standard way to associate errors to the values represented in a histogram. 

Inputs (should be ndarrays, but not necessarily):
    - H: values to which errors will be associated.
    
Outputs:
    - err: errors associated to H, determined as $sqrt(N*P*(1-P))$, whereas $N$ is the sum of $H$ and $P=H/N$.
    - w: weights corresponding to the inverse square of err. Null values of err give w = 1e+20
'''
def bberr(H):
    # Verify if the input is an ndarray
    H = np.array(H)
    
    # Apply binomial statistics and evaluate errors and weights
    N = np.sum(H)
    err = np.sqrt(H*(N-H)/N)
    w = np.sqrt(H*(N-H)/N)
    err[err == 0] = 1e-10
    w[w == 0] = 1e+10
    w = w**(-2)
    return(err,w)

'''
Arbitrary Fit function.
This function relies on the scipy.optimize module to perform a fit (on a set of data) with an arbitrary model function.

Inputs:
    - func: Python function used as model for the fit. Example with a log function:
    def logfit(x,a,b,c,d):
        return(a*np.log(b*(x+c))+d)
        
    - x,y: array-like, data to be fitted, with generic function y = func(x)
    - yerr: array-like, errors associated to y points (standard deviations, not weights)
    - start: list containing the ordered set of starting points for the fit function
    - lims: tuple containing the minimum and maximum values accepted for all the fit parametra.
    If every parameter has a different minimum and maximum value accepted, they should be given explicitly in the tuple. Example with a log function: ((0,0,-np.inf,-100),(1000,1,np.inf,+100))
    
Outputs:
    - par: ndarray containing the ordered set of all the optimal parametra
    - parerr: ndarray containing the errors associated to optimal parametra
    - chi2red: reduced chi-squared of the fit (chi2 is the ratio of the residuals to the points' variances)
    - RMSE: root of the mean squared error between the fit points and the data
'''
def fitfun(func,x,y,yerr,start,lims):
    # Verify if the inputs are all ndarrays
    x,y,yerr = np.array(x), np.array(y), np.array(yerr)
    
    # Perform fit and evaluate parametra errors
    par,cov = ft(func,x,y,sigma=yerr,absolute_sigma=True,p0=start,bounds=lims)
    parerr=np.sqrt(np.diag(cov))

    # Evaluate goodness of fitting parametra
    yfit = func(x,*par)
    chi2 = np.sum((y-yfit)**2/yerr**2)
    chi2red = chi2/(x.size-par.size)
    RMSE = np.sqrt(np.sum((y-yfit)**2)/yfit.size)
    return(par,parerr,chi2red,RMSE)

'''
Bi-Exponential Fit function.
This function is a callable that evaluates the input data $x$ and returns a double exponential function $y = a1*exp(-(x-b)/c1) + a2*exp(-(x-b)/c2) + d$.

Inputs:
    - x: input datas, array-like
    - a,b,c,d,e,f: function parametra
        
Outputs:
    - y: output datas
'''

def biexpfun(x,a1,a2,b,c1,c2,d):
    y = a1*np.exp(-(x-b)/c1) + a2*np.exp(-(x-b)/c2) + d
    return(y)


'''
Complementary Error Fit function.
This function is a callable that evaluates the input data $x$ and returns $y = a*erfc(b*(x-c))+d$. Since this is a complementary error function, its derivative is a negative gaussian function (i.e. $y(-\infty) > y(+\infty)$).

Inputs:
    - x: input datas, array-like
    - a,b,c,d: function parametra
        
Outputs:
    - y: output datas
'''

def erffun(x,a,b,c,d):
    y = a*sp.erfc(b*(x-c))+d
    return(y)

'''
Erfc Starting Point function.
This function evaluates the starting points for a complementary error function, to be fitted on the input datas. The starting points are defined in a statistical fashion: if the fit function has the form specified in "erffun", that is, $y = a*erfc(b*(x-c))+d$, then we may set:
    - "a" as half of the difference between y(-\infty) and y(+\infty).
    - "b" as an arbitrary non-zero low value
    - "c" as the the midpoint of x
    - "d" as $y(+\infty)$

Inputs:
    - x: input datas, array-like
    - y: input datas y=f(x), array-like
        
Outputs:
    - a,b,c,d: starting value of the function parametra.
'''

def erfstart(x,y):
    a = (np.max(y)-np.min(y))/2
    b = 0.00001
    c = (np.max(x)+np.min(x))/2
    d = y[-1]
    return[a,b,c,d]

'''
Double Gaussian Fit function.
This function is a callable that evaluates the input data $x$ and returns $y =  a1*exp(-(x-b1)^2/2c1^2) + a2*exp(-(x-b2)^2/2c2^2)$, that is, the sum of two non normalized gaussian functions.

Inputs:
    - x: input datas, array-like
    - a1,b1,c1,a2,b2,c2: function parametra
        
Outputs:
    - y: output datas
'''

def gauss2fun(x,a1,b1,c1,a2,b2,c2):
    y = a1*np.exp(-(x-b1)**2/(2*c1**2)) + a2*np.exp(-(x-b2)**2/(2*c2**2))
    return(y)

'''
Double Gaussian Starting Point function.
This function evaluates the starting points for a the function defined as the sum of two non-normalized gaussian functions, to be fitted on the input datas. The parametra are evaluated in a statistical fashion and within the hypothesis that one of the curves has an amplitude much greater than the other (the higher gaussian is located at the lower mean value):
    - First, the parametra of the first gaussian are evaluated. The mean value $b1$ is defined as the middle point of the set of points that lie above half of the maximum value of $y$. The corresponding point $y(b1)$ is considered the amplitude of the gaussian ($a1$) and from the FHWM the sigma is derived ($c1$). For convenience, input data are smoothed a bit with a moving average filter.
    - Second, a "fake" gaussian curve is generated, with the parametra found in the previous point. This fake gaussian is subtracted from the input data and, for safety, all the points less distant (on $x$) than N*c1 from $b1$ are set to zero.
    - Finally, the parametra of the second gaussian ($a2,b2,c2$) are derived from the "cleaned" $y$ sample. The amplitude $a2$ is simply the maximum value of $y(clean)$ and the corresponding $x$ value is $b2$. The sigma $c2$ is derived by evaluating the right-side half-width at half-maximum.

Inputs:
    - x: input datas, array-like
    - y: input datas y=f(x), array-like
        
Outputs:
    - a1,b1,c1,a2,b2,c2: starting value of the function parametra.
'''

def gauss2start(x,y,conf=2.55):
    # Smooth the signal
    y2 = ndmg.uniform_filter(y,3)

    # Find the parametra of the first gaussian
    ind = np.where(y2 >= np.max(y2)/2)
    b1 = (x[ind[0][0]] + x[ind[0][-1]])/2
    a1 = y[np.where(x >= b1)[0][0]]       
    FWHM = np.abs(x[ind[0][0]] - x[ind[0][-1]])
    c1 = FWHM/2.355
    
    # Produce an artificial gaussian and subtract it from the data. Clean the sample.
    yth = a1*np.exp(-(x-b1)**2/(2*c1**2))
    ysub = y2 - yth
    ysub[ysub<0] = 0
    ysub[x<=b1+conf*c1] = 0
    
    # Find the parametra that may fit the second gaussian
    a2 = np.max(ysub)
    b2 = x[np.argmax(ysub)]
    ind = np.where(ysub >= a2/2)[0][-1]
    HWHM = np.abs(b2-x[ind])
    c2 = HWHM*2/2.355
    return[a1,b1,c1,a2,b2,c2]

'''
Californium-252 Preprocessing function.
This function takes as input a data file containing events corresponding to the detection of a neutral particle by a Silicon Photomultiplier. The preprocessing consists in the rejection of all the signals that don't pass all the required checks. All the non-rejected signals are given as output, after baseline subtraction.

Inputs:
    - "mat", 2d-array-like containing all the waveforms (each row is a signal)
    - "pretrig", the number of points from the beginning of the signals on which the baseline is calculated
    - "trigin", the beginning of the energy window
    - "trigout", the end of the energy window
    - "thrbaseline", the maximum std of the baseline allowed for the signals to pass the check
    - "risepoint", the expected position of the inflection point of the rise of the signal
    - "risewindow", the range of acceptability of the inflection points of the rise of the signals to pass the check
    
Outputs:
    - "cleanmat": 2d-array-like, containing all the non-rejected baseline-subtracted waveforms
'''

def preprocessCf(mat,pretrig,trigin,trigout,thrbaseline,risepoint,risewindow):
    # For every event, create a "check" variable and set it to True. Change to False if a check is not passed.
    sizemat = mat.shape[0]
    check = np.ones((sizemat,),dtype=bool)
    
    for i in range(sizemat):
        # First check: standard deviation of the baseline (and baseline subtraction)
        signalstd = np.std(mat[i,trigin-pretrig:trigin])
        if signalstd >= thrbaseline:
            check[i] = False
            continue
        baseline = np.mean(mat[i,:pretrig])
        mat[i,:] = mat[i,:] - baseline
        
        # Second check: integral of the signal must be positive (and non-divergent)
        energy = np.sum(mat[i,trigin:trigout])
        if energy <= 0:
            check[i] = False
            continue
        
        # Third check: inflection point of the rise of the signal
        diffsig = np.diff(mat[i,:])
        inflect = np.argmax(diffsig)
        if inflect < risepoint-risewindow or inflect > risepoint+risewindow:
            check[i] = False
            
    cleanmat = mat[check,:]
    return(cleanmat)

'''
FOM calculation function.
This function evaluates the Figure Of Merit by its definition, that is: take a set of PSD observables, project them performing an energy cut, fit the two resulting gaussians and finally calculate FOM. If the gaussians have generic form $y =  a1*exp(-(x-b1)^2/2c1^2) + a2*exp(-(x-b2)^2/2c2^2)$, then the FOM is:
    $ FOM = (b2 - b1)/2.355 (c1+c2) $

Inputs:
    - "PSD": 1d-array-like, the PSD observables previously determined
    - "Sint": 1d-array-like, the energies corresponding to the PSDs
    - "estart","estop": the extremes (lower, superior) for the energy cut
    - "nbins": the number of bins to be used in the projection

Outputs:
    - "FOM": the value of the Figure Of Merit.
    - "bintrue", "isto": the frequency histogram of the recorded PSD values, with the indicated energy cut performed
    - "par": a list containing the fit parametra of the two-gaussian-fit
'''

def fom(PSD,Sint,estart,estop,nbins=250,iffit=True):
    # Filter the PSDs
    PSDfilt = PSD[(Sint > estart) & (Sint <= estop)]
    
    # Projection and parametra evaluation
    bins = np.linspace(np.min(PSDfilt),np.max(PSDfilt),nbins)
    isto,_ = np.histogram(PSDfilt,bins)
    bintrue = truebins(bins)
    par = gauss2start(bintrue,isto)
    
    # If you want a more precise estimate, then use a gaussian fit. Otherwise, just evaluate the FOM.
    flag = False
    if iffit == True:
        x = bintrue
        y = isto
        
        # Define fit model and set limits on parametra
        gmodel = Model(gauss2fun)
        gmodel.set_param_hint('a1',min=0,max=par[0]+100)
        gmodel.set_param_hint('a2',min=0,max=par[0]+100)
        gmodel.set_param_hint('b1',min=0,max=par[1]+4)
        gmodel.set_param_hint('b2',min=0,max=par[4]+4)
        gmodel.set_param_hint('c1',min=0,max=4)
        gmodel.set_param_hint('c2',min=0,max=4)
        gmodel.nan_policy = 'propagate'
        
        # Perform fit and extract parametra
        result = gmodel.fit(y, x=x, a1 = par[0], b1 = par[1], c1 = par[2], a2 = par[3], b2 = par[4], c2 = par[5])

        a1 = result.params['a1'].value
        b1 = result.params['b1'].value
        c1 = result.params['c1'].value
        a2 = result.params['a2'].value
        b2 = result.params['b2'].value
        c2 = result.params['c2'].value
        par = [a1,b1,c1,a2,b2,c2]
        
        # If one of the parametra is a NaN, then go back to the first estimate and raise a flag!
        if any(np.isnan(par)):
            par = gauss2start(bintrue,isto)
            flag = True
        
    # Evaluate FOM            
    FOM = np.abs(par[1]-par[4])/(2.355*(par[2]+par[5]))
    return(FOM,bintrue,isto,par,flag)

''' An alternative to the Crystall Ball fit function: gaussexp
Alternative fitting function used for the fitting of an energy distribution resulting from a lossy interaction process
It is essentially a gaussian function with a single exponential tail.
cfr. "A simple alternaative to the Crystal Ball function", from S. Das, 2016

Input:
    - x, 1D array of data to be fitted with the function
    - mu,sigma,k,N: parametra for the function
    Suitable starting points for these values are as follows:
        - mu, sigma: mean value and std of the gaussian side of the function
        - N: amplitude of the peak of the data
        - k: idk, maybe something about 0.5? Non negative, to be sure
Output:
    - y, 1D array x-like, with the value of the function in each point


'''

def gaussexp(x,mu,sigma,k,N):
    # For the points with (x-mu)/\sigma > -k, use a gaussian function
    cond = (x >= -k*sigma + mu)
    x1 = x[cond]
    y1 = N * np.exp(- (x1-mu)**2/(2*sigma**2) )

    # For the remaining points, use a gaussian*exponential function
    anticond = (cond == False)
    x2 = x[anticond]
    y2 = N*np.exp((k**2)/2 + k*((x2-mu)/sigma))
    
    # Cat the arrays
    y = np.zeros_like(x)
    y[cond] = y1
    y[anticond] = y2
    return(y)

''' SiPM Peak Locator
The following function is built to analyze a SiPM waveform and extract its main peak. The analyzing procedure is as follows:
- The waveform is inverted, if necessary
- The baseline is subtracted
- The original signal is smoothed with a moving average filter
- A derivative signal is derived from the original signal (zero pole cancellation)
- The peak of the derivative is used as starting point for a local maximum search, within a certain range

Parametra of the function (moving average interval, range for the maximum search) are to be adjusted to one's necessity.

Inputs:
- x, 1D-array containing the input signal
- nbase, int, number of points from the beginning of the signal for the calculation of the baseline
- zerostep, int, number of points for the signal translation in the zero pole cancellation
- meanrange, int, extension of the filtering region
- peakrange, int, extension of the maximum search
- ifneg, boolean, set to True if the polarity of the signal is negative and False if positive
- ifbase, boolean, set to True if the baseline of the signals must be subtracted

Outputs:
- xclean, 1D-array containing the "cleaned" signal
- peakind, index of the position of the peak
- peak, peak value
- xdiff, 1D-array "derivative" of the input signal
'''

def peaksipm(x,nbase = 100,zerostep = 5,meanrange = 5,peakrange = 30, ifneg = True, ifbase = True):
    # Prima di tutto inverto il segnale (se segnale) e sottraggo la baseline
    x = np.float32(x)
    if ifbase:
        xclean = x - np.mean(x[:nbase])
    else:
        xclean = copy.copy(x)
    
    if ifneg == True:
        xclean = -xclean
    
    # Pulisco il segnale
    xclean = ndmg.uniform_filter1d(xclean,meanrange)
    
    # Applico la cancellazione di polo zero e pulisco la derivata
    xdiff = xclean[zerostep:] - xclean[:-zerostep]
    xdiff = ndmg.uniform_filter1d(xdiff,meanrange)
    
    # Cerco il max del primo segnale in corrispondenza di un intervallo intorno al picco di xdiff
    # (se l'intervallo di ricerca è vuoto, prendo "banalmente" il max del segnale)
    ind1 = np.argmax(xdiff)-peakrange + zerostep
    ind2 = np.argmax(xdiff)+peakrange + zerostep
    if np.size(xclean[ind1:ind2]) > 0:
        peakind = np.argmax(xclean[ind1:ind2]) + ind1
    else:
        peakind = np.argmax(xclean)
    peak = xclean[peakind]
        
    return(xclean,peakind,peak,xdiff)

''' Projection of a 2D histogram function by integral of an exponential fit

Functions works as above. Only difference: for each column to be projected, instead of just calculating the sum or the mean or whatever, perform an exponential fit and evaluate the theoretical sum (which is A/b, is the function is A*exp(-bx)). The errors follow by error propagation. Also: it works only for projections along the y axis...

'''

def histoproj_fitterexp(histo2D,binsx,binsy,obs='mean',obsthr=10**(-7),lowcut=None,upcut=None,start_a=0.001,start_b=0.5,selferr = 0.01):    
    # First of all, check if the size of the binning vectors are greater than the ones of the histogram.
    # If not, use the truebins function to adjust them.
    if np.shape(histo2D)[0] != np.shape(binsx):
        truex = truebins(binsx)
    else:
        truex = binsx
    
    if np.shape(histo2D)[1] != np.shape(binsy):
        truey = truebins(binsy)
    else:
        truey = binsy
        
    # Now disable a pathetic warning from numpy that will inevitably activate in case of x/0 divisions
    np.seterr(divide='ignore', invalid='ignore')
    
    # Now define an empty vector for the histogram and fill it iteratively with the integrals.
    histosum = np.zeros((np.shape(truex)[0],))
    histosum_err = np.zeros_like(histosum)

    # Check the boundaries for the fit. If not set, use all the data available
    if lowcut == None:
        lowcut = np.min(truey)

    if upcut == None:
        upcut = np.max(truey)

    # Define the fit data
    fitcutcond = (truey >= lowcut) & (truey <= upcut)
    xfit = truey[fitcutcond]

    # Define an object which will contain all the fit results
    allresults = []

    for i in range(histo2D.shape[0]):
        # Define the fit data
        yfit = histo2D[i,fitcutcond]

        # If all the data are null, go on without fitting
        if np.shape(yfit[yfit > 0])[0] == 0:
            allresults.append(None)
            continue
            
        # Otherwise, perform an exponential fit
        gmodel = Model(expfun_notail)
        gmodel.set_param_hint('a',min=0)
        gmodel.set_param_hint('b',min=0)
        result = gmodel.fit(yfit, x=xfit, a=start_a, b=start_b)
        a,b = result.params['a'].value,result.params['b'].value
        err_a,err_b = result.params['a'].stderr,result.params['b'].stderr

        if (err_a == None):
            # If something goes wrong in the fit and None/nans are recovered, use another value
            err_a = a*selferr

        if (err_b == None):
            # If something goes wrong in the fit and None/nans are recovered, use another value
            err_b = b*selferr

        if np.isnan(err_a):
            # If something goes wrong in the fit and None/nans are recovered, use another value
            err_a = a*selferr

        if np.isnan(err_b):
            # If something goes wrong in the fit and None/nans are recovered, use another value
            err_b = b*selferr

        # Compute the requested observable and its error
        if obs == 'mean':
            histosum[i] = 1/b
            histosum_err[i] = err_b/b**2
        elif obs == 'integral':
            histosum[i] = a/b
            histosum_err[i] = histosum[i]*np.sqrt((err_a/a)**2+(err_b/b)**2)
        elif obs == 'intersection':
            histosum[i] = ( np.log(a/obsthr) )/b
            histosum_err[i] = np.sqrt( (err_a/(a*b))**2 + (histosum[i]*err_b/b)**2 )
                                       
        allresults.append(result)
    return(truex, histosum, histosum_err, allresults)

''' Rising edge t0 finder
This function takes as input a matrix of waveforms produced by a detector (e.g., SiPM, MCP, etc.) and computes the timestamp associated to each event, defined as the intersection between the horizontal axis and the linear extrapolation of the waveform's rising edge. 

Inputs:
    - wvfs, 2D array containing one event for each row and one signal value for each column
    - pheight, 1D array containing the Pulse Height of each event (if not given, the PHs are considered simply the maxima of each event)
    - thr1, first threshold for linear interpolation of the rising edge (if not given, set to 10%)
    - thr2, second threshold for linear interpolation of the rising edge (if not given, set to 80%)
    - scalefac, multiplicative factor which will be applied to all the output and should convert from time ticks to ns/ps (if not given, no conversion is performed)
    
Output:
    - t0, the zero crossings of each event
    - t10, t90, the points used for the interpolation
'''

def zerotime(wvfs,pheight = None,thr1=0.1,thr2=0.8,scalefac=1):
    # Se necessario, calcolo le pulse heights
    if pheight is None:
        pheight = np.max(wvfs,axis = 1)
    
    # Creo due matrici che hanno la stessa dimensione della matrice di waveforms e sono riempite da 
    # booleani, posti a True ogni volta che il segnale del punto (i,j) esimo supera il le due threshold
    # della PH della riga i-esima
    condmat1 = (wvfs >= np.tile(thr1*pheight, (np.shape(wvfs)[1],1)).transpose())
    condmat2 = (wvfs >= np.tile(thr2*pheight, (np.shape(wvfs)[1],1)).transpose())

    # A questo punto devo prendere, per ogni riga, il primo elemento True disponibile nelle due matrici.
    # Sfrutto la definizione pythoniana della funzione argmax per farlo: l'argmax per righe di una matrice 
    # di booleani mi ritorna un vettore che ha per elementi gli indici dove appaiono per le prime volte i
    # vari elementi True
    t10 = np.argmax(condmat1, axis = 1)
    t90 = np.argmax(condmat2, axis = 1)

    # Calcolo m e q dell'estrapolazione lineare degli edges a partire dai valori così trovati
    inds10 = t10.astype('int')    # Indici corrispondenti ai punti t10, t90 
    inds90 = t90.astype('int')
    t0 = np.zeros((np.shape(t10)[0],))
    for j,el in enumerate(inds10):
        m = ( wvfs[j,inds90[j]] - wvfs[j,inds10[j]] ) / ( t90[j] - t10[j] )
        q = wvfs[j,inds90[j]] - m*t90[j]

        # Estrapolo a zero e riscalo con il fattore di timing
        t0[j] = scalefac*(-q/m)
        
    # Riscalo gli output
    t10 = t10*scalefac
    t90 = t90*scalefac
    return(t0, t10, t90)


''' Function for MCP-SiPM timing
This function takes as input a few data files where the timing resolutions of SiPM-MCP couples are evaluated. The code extracts, from each file, the resolution and stores it in a DataFrame, which is then produced as output.

Inputs:
    - t0types: a string (such as '_t10t80') which will be used to find the files corresponding to a certain interpolation method.
    - runs: a list of numbers, each containing one of the runs to be analyzed.
    - ifmeanerr: if True, the errors associated to the mean value will be computed. The simple standard deviations will be printed if False.
    - numsipms: amount of SiPMs to be analyzed.
    - prename, midname: names that define the filed to be analyzed
    
Outputs:
    - timeres, matrix containing all the extracted values and their average and std
    - timeresframe, the corresponding DataFrame
'''

def timing_SiPMvsMCP(t0types,runs,ifmeanerr,numsipms,prename,thrPHL_pbf2,thrPHU_pbf2,selferr=0.25):
    # Per ogni metodo di interpolazione, estraggo i dati di ogni run e calcolo le corrispondenti risoluzioni
    # (N.B., MCP vs SiPM)
    timeres = np.zeros((len(runs)+2,numsipms*len(t0types))) # Ogni riga è un evento (+ ultime due: medie e std)
                                                            # Ogni colonna è una coppia tipo-rivelatore
    timereserr = np.zeros_like(timeres)                     # Come sopra
    
    for i,t0 in enumerate(t0types):
        # Definisco i file di riferimento
        savefiles = [prename + str(el) + '_MCPt0' + t0 + '_thrs_' + str(thrPHL_pbf2) + '_' + str(thrPHU_pbf2) + '.npz' for el in runs]

        # Itero sui file e carico le risoluzioni temporali
        for k in range(numsipms):
            for j,el in enumerate(savefiles):
                timeres[j,k*len(t0types)+i] = np.load(el)['timeres'][k]
                timereserr[j,k*len(t0types)+i] = np.load(el)['timereserr'][k]
                
                # Se un errore è stato trovato NaN, lo sovrastimo con un parametro arbitrario (eg 25%)
                if np.isnan(timereserr[j,k*len(t0types)+i]):
                    timereserr[j,k*len(t0types)+i] = selferr*timeres[j,k*len(t0types)+i]

    # Calcolo le risoluzioni medie, iterando sulle colonne di timeres (cioè, per ogni rivelatore e tipo di interpolazione)
    for i,t0 in enumerate(t0types):
        for k in range(numsipms):
            datavec = timeres[:-2,k*len(t0types)+i]
            dataerr = timereserr[:-2,k*len(t0types)+i]
            if ifmeanerr:
                # Media pesata ed errore del dataset
                meanres, stdres = wmean(datavec,dataerr)
            else:
                # Media aritmetica e std del dataset
                meanres = np.mean(datavec)
                stdres = np.std(datavec)
                
            timeres[-2,k*len(t0types)+i] = meanres
            timeres[-1,k*len(t0types)+i] = stdres

    # A solo titolo illustrativo, converto i dati in un DataFrame e li stampo
    if ifmeanerr:
        rownames = [prename[-6:] + str(el) for el in runs] + ['Mean value'] + ['Std of the mean']
    else:
        rownames = [prename[-6:] + str(el) for el in runs] + ['Mean value'] + ['Std']

    colnames = []
    for i in range(numsipms):
        colnames += [f'SiPM {int(i+1)} (' + el[1:] + ')' for el in t0types]

    timeresframe = pd.DataFrame(timeres, index = rownames, columns = colnames)
    return(timeres,timeresframe)


''' Function for SiPM-SiPM timing
This function takes as input a few data files where the timing resolutions of SiPM-MCP couples are evaluated. The code extracts, from each file, the resolution and stores it in a DataFrame, which is then produced as output.

Inputs:
    - t0types, runs, ifemanerr, numsipms, prename: as above
    - couplings: a list of strings containing names defining the types of SiPMs coupled in the readout
    - saveinds: a list of lists, each containing an ordered couple: SiPMs to be saved and index
    - savenames: names for the coulmns of the DataFrame

Outputs:
    - timeres_pairs, matrix containing all the extracted values and their average and std
    - timeresframe, the corresponding DataFrame

'''

def timing_SiPMvsSiPM(t0types,runs,ifmeanerr,numsipms,prename,couplings,saveinds,savenames,thrPHL_pbf2,thrPHU_pbf2,selferr=0.25):
    # Per ogni metodo di interpolazione, estraggo i dati di ogni run e calcolo le corrispondenti risoluzioni
    # (N.B., SiPM vs SiPM)
    timeres_pairs = np.zeros((len(runs)+2,len(saveinds)*len(t0types))) 
                # Ogni riga è un evento (+ ultime due: medie e std), ogni colonna è [coppia SiPM]-[tipo interpolazione]
    timeres_pairserr = np.zeros_like(timeres_pairs)

    # Itero
    colnames = ['' for i in range(len(saveinds)*len(t0types))]    # Nomi delle colonne (li sovrascrivo iterativamente)
    for i,t0 in enumerate(t0types):
        # Itero su ogni combinazione di indici da salvare e ogni run disponibile
        for k,inds in enumerate(saveinds):
            # Definisco il nome della colonna che sto scrivendo
            cname = 'SiPM ' + savenames[k] + ' (' + t0[1:] + ')'
            colnames[k*len(t0types)+i] = cname

            for j,el in enumerate(runs):
                # Definisco il file da guardare
                savefile = prename + str(el) + '_' + inds[0] + t0 + '_thrs_' + str(thrPHL_pbf2) + '_' + str(thrPHU_pbf2) + '.npz'

                # Carico la corrispondente risoluzione
                timeres_pairs[j,k*len(t0types)+i] = np.load(savefile)['timeres'][inds[1]]
                timeres_pairserr[j,k*len(t0types)+i] = np.load(savefile)['timereserr'][inds[1]]
                
                if np.isnan(timeres_pairserr[j,k*len(t0types)+i]):
                    timeres_pairserr[j,k*len(t0types)+i] = selferr*timeres_pairs[j,k*len(t0types)+i]

    # Calcolo le risoluzioni medie, iterando sulle colonne di timeres (cioè, per ogni rivelatore e tipo di interpolazione)
    for i,t0 in enumerate(t0types):
        for k,inds in enumerate(saveinds):
            datavec = timeres_pairs[:-2,k*len(t0types)+i]
            dataerr = timeres_pairserr[:-2,k*len(t0types)+i]
            
            if ifmeanerr:
                # Media pesata ed errore del dataset
                meanres, stdres = wmean(datavec,dataerr)
            else:
                # Media aritmetica e std del dataset
                meanres = np.mean(datavec)
                stdres = np.std(datavec)
                
            timeres_pairs[-2,k*len(t0types)+i] = meanres
            timeres_pairs[-1,k*len(t0types)+i] = stdres

    # A solo titolo illustrativo, converto i dati in un DataFrame e li stampo
    if ifmeanerr:
        rownames = [prename[-6:] + str(el) for el in runs] + ['Mean value'] + ['Std of the mean']
    else:
        rownames = [prename[-6:] + str(el) for el in runs] + ['Mean value'] + ['Std']

    timeresframe = pd.DataFrame(timeres_pairs, index = rownames, columns = colnames)
    return(timeres_pairs,timeresframe)


# ===========================================================================================================
#                                       FROM OLD FILE: beamfunctions.py
# ===========================================================================================================
"""
Pedestal Run Function
"""

def cmode(tree,varname,stdthr,cut0=4,cut1=2,numASIC=3):
    # Estraggo dal tree la variabile relativa alla singola strip e ripulisco dalle righe inutili  
    strip = np.stack(np.array(tree[varname]))
    strip = strip[:,cut0:-cut1]
    eventind = np.arange(strip.shape[0])+1 
    stripind = np.arange(strip.shape[1])+1
    numstrip = int(strip.shape[1]/numASIC)    # Numero di strip presenti in un ASIC

    # Calcolo il piedestallo e la sua deviazione standard e individuo le strip outliar
    pede = np.mean(strip,0)
    pedestd = np.std(strip,0)
    badstrips = np.where(pedestd >= stdthr)[0]
    
    # Costruisco gli indici per il calcolo del common mode. Questi sono definiti come 2*(N ASIC)-1 
    # valori, dati dagli estremi che definiscono la regione di ogni ASIC (inf. incluso, sup. escluso)
    # L'ultimo indice sarà poi rimosso, per come lavora la funzione add.reduceat
    # Se in altre parole abbiamo 3 ASIC da 128 strip, gli indici saranno [0,128,128,256]
    inds = []
    numstrips = np.zeros((numASIC))
    for i in range(numASIC):
        # Costruisco gli indici
        inds.append(i*numstrip)
        inds.append((i+1)*numstrip)
        
        # Già che ci sono, conto le strip "buone" ho nel singolo ASIC
        numstrips[i] = numstrip - badstrips[(badstrips >= i*numstrip) & (badstrips < (i+1)*numstrip)].shape[0]
    inds = inds[:-1]

    # Sottraggo il piedestallo e calcolo il common mode secondo definizione
    stripclean = strip - pede
    stripclean[:,badstrips] = 0
    cm = np.add.reduceat(stripclean, inds, axis = 1)[:,::2]/numstrips

    # Sottraggo il common mode e ricalcolo valori medi e deviazioni standard delle strip
    cmmat = np.tile(cm[:,0],(numstrip,1)).transpose()
    for i in range(numASIC-1):
        a = np.tile(cm[:,i+1],(numstrip,1)).transpose()
        cmmat = np.hstack((cmmat,a))
    stripcleanCM = stripclean - cmmat
    pedestdCM = np.std(stripcleanCM,0)
    
    # Rimuovo gli outliar
    pede[badstrips] = np.nan
    pedestd[badstrips] = np.nan
    pedestdCM[badstrips] = np.nan
    return(pede,pedestd,pedestdCM,stripind,badstrips)

"""
Silicon Strip Analysis Function
"Stripping" function, in the sense that takes the raw strip data and returns the coordinates of the hits
"""

def anachrys(tree,varname,pede,badinds,pedestdCM,cut0=4,cut1=2,numASIC=3,thrbad=4095,pull_max=np.nan,pull_lat=np.nan):
    # Estraggo dal tree la variabile relativa alla singola strip e ripulisco dalle righe inutili  
    strip = np.stack(np.array(tree[varname]))
    strip = strip[:,cut0:-cut1].astype('float')
    numstrip = int(strip.shape[1]/numASIC)       # Numero di strip presenti in un ASIC
    
    # ==========================================================================================
    # FASE 1: NOISE SUBTRACTION (pede + common mode)
    # ==========================================================================================
    
    # Creo una maschera di stato con flag True per ogni strip "cattiva" e per ogni (strip, evento)
    # che supera la soglia prefissata. Setto a NaN ogni elemento con flag True.
    stripmask = (strip >= thrbad)
    stripmask[:,badinds] = True
    strip[stripmask] = np.NaN

    # Come nella funzione cmode, costruisco gli indici per il funzionamento di "reduceat"
    inds = []
    numstrips = np.zeros((numASIC))
    for i in range(numASIC):
        # Costruisco gli indici necessari al funzionamento di "reduceat"
        inds.append(i*numstrip)
        inds.append((i+1)*numstrip)
        
        # Conto quante strip "buone" ho nel singolo ASIC
        numstrips[i] = numstrip - len(badinds)
    inds = inds[:-1]
    
    # Sottraggo il common mode.
    stripclean = strip - pede
    stripclean[:,badinds] = 0
    cm = np.add.reduceat(stripclean, inds, axis = 1)[:,::2]/numstrips
    cmmat = np.tile(cm[:,0],(numstrip,1)).transpose()
    for i in range(numASIC-1):
        vec = np.tile(cm[:,i+1],(numstrip,1)).transpose()
        cmmat = np.hstack((cmmat,vec))
    stripcleanCM = stripclean - cmmat
    
    # ==========================================================================================
    # FASE 2: ANALISI DI PH
    # ==========================================================================================
    
    # Su base evento per evento, determino la massima PH e il pull centrale. Definisco la soglia
    # "pull_max" (distinzione segnale/rumore) come un valore di preinput 20, oppure come
    # il primo minimo dell'istogramma di frequenze calcolato a 50 bin.
    maxstrip = stripcleanCM.max(1)
    argmaxstrip = stripcleanCM.argmax(1)
    pull = maxstrip/pedestdCM[argmaxstrip]
    pull[np.isnan(pull)] = 0
    
    nbins = 50
    bins = np.linspace(np.min(pull),np.max(pull),nbins)
    histo,_ = np.histogram(pull,bins)
    if np.isnan(pull_max) == True:
        pull_max = bins[np.argmin(histo[bins<50])]
    
    # Su base evento per evento, determino le PH laterali al massimo e i corrispondenti pull
    # Costruisco le soglie "pull_lat" prendendo il valore medio delle rispettive distribuzioni
    argmaxstripL = stripcleanCM.argmax(1) - 1
    argmaxstripR = stripcleanCM.argmax(1) + 1
    argmaxstripL[argmaxstripL == -1] = 0
    argmaxstripR[argmaxstripR == stripcleanCM.shape[1]] = stripcleanCM.shape[1]-1
    
    vec = range(len(argmaxstripL))
    maxstripL = stripcleanCM[vec,argmaxstripL]
    maxstripR = stripcleanCM[vec,argmaxstripR]
    pullL = maxstripL/pedestdCM[argmaxstripL]
    pullR = maxstripR/pedestdCM[argmaxstripR]
    pullL[np.isnan(pullL)] = 0 # Questo check serve perché potrei avere una strip "bad" a lato
    pullR[np.isnan(pullR)] = 0 # di quella massimale. Pertanto potrei dividere per 0, da cui un nan
    if np.isnan(pull_lat) == True:
        pull_lat = np.max([np.mean(pullL),np.mean(pullR)])
    
    # ==========================================================================================
    # FASE 3: ANALISI DI CLUSTER I (cluster dei massimi, distribuzione eta)
    # ==========================================================================================
    
    # Costruisco il pull relativo ad ogni evento
    pedestdCMmat = np.tile(pedestdCM,(stripcleanCM.shape[0],1))
    pullclean = stripcleanCM/pedestdCMmat
    pullclean[np.isnan(pullclean)] = 0

    # Eseguo una "cluster analysis" grezza. Per ogni evento cerco la strip con il segnale
    # massimo e poi costruisco il suo "cluster" usando come soglia "pull_lat" e guardando i primi
    # tre punti attorno al massimo (destra e sinistra). Per ogni cluster devo salvare il numero
    # di strip presenti, la PH totale (somma) e il SNR totale.
    nstripcluster = np.zeros((len(argmaxstrip),))
    phcluster = np.zeros_like(nstripcluster)
    snrcluster = np.zeros_like(nstripcluster)
    for k in range(len(argmaxstrip)):
        # Nell'evento k-esimo, guardo tre strip a dx e sx di quella centrale e conto quante sono
        # sopra soglia.
        ind1 = np.max([argmaxstrip[k]-3,0]) # Evito problemi con i cluster ai bordi
        ind2 = np.min([argmaxstrip[k]+4,stripcleanCM.shape[1]+1])
        vec_1 = pullclean[k,ind1:ind2]
        nstripinds = list(np.where(vec_1 >= pull_lat)[0])
        nstripcluster[k] = len(nstripinds)

        # Calcolo la pulse height totale del cluster (somma delle singole PH)
        vec_2 = stripcleanCM[k,ind1:ind2]
        phcluster[k] = np.sum(vec_2[nstripinds])

        # Calcolo il SNR del cluster come rapporto della PH totale e della sigma totale.
        # La sigma è la somma in quadratura delle singole RMSE CM-subtracted.
        vec_3 = (pedestdCM[ind1:ind2])**2
        sigmacluster = np.sqrt(np.sum(vec_3[nstripinds]))
        if sigmacluster != 0: # Se la sigma è nulla, il SNR resta a zero
            snrcluster[k] = phcluster[k]/sigmacluster
            
    # Calcolo la eta corrispondente ad ogni evento
    etaR = (-maxstrip+maxstripR)/(maxstrip+maxstripR)
    etaL = (maxstrip-maxstripL)/(maxstrip+maxstripL)
    etas = np.hstack((etaR[etaL>=-etaR],etaL[etaL<-etaR]))

    # ==========================================================================================
    # FASE 4: ANALISI DI CLUSTER II (cluster generali e centroidi degli eventi)
    # ==========================================================================================
    # Individuo tutte le coppie (strip, evento) che hanno segnale superiore alla soglia "pull_max"
    # Per ogni evento, riduco gli eventi di segnale in clusters. Di regola, due strip sono inserite
    # nello stesso cluster se distano meno di 5 indici. Nell'iterazione, conto anche il numero
    # di clusters totali e mi segno quali eventi sono single-cluster-like. Per questi ultimi,
    # costruisco anche il baricentro del cluster, preso come media pesata delle posizioni delle
    # strip attive, essendo le PH i loro pesi.
    overinds = (pullclean > pull_lat).astype('int') # (strip,evento) di segnale
    
    # Per indicare i cluster, uso la lista "clusters": un elemento per ogni evento, contenente
    # tante liste quanti cluster.
    clusters = []
    nclusters = np.zeros((pedestdCMmat.shape[0],))
    sclusters = []
    cidclusters = []
    for k in range(pedestdCMmat.shape[0]):
        # Individuo le strip dell'evento k-esimo che sono considerate "segnale"
        overstrips = np.where(overinds[k,:] == 1)[0]
        
        # Se non ci sono strip, appendo una lista vuota. Se ne ho esattamente una, appendo
        # una lista contenente la strip relativa. Se ne ho più di una, algoritmo di clustering
        maxstep = 5 # Massima distanza tra strip in un singolo cluster
        if overstrips.shape[0] == 0:
            clusters.append([])
        elif overstrips.shape[0] == 1:
            clusters.append([overstrips[0]])
        else:
            # Appendo una prima lista in cui inserisco la prima strip individuata. Itero poi
            # su tutte le strip rimaste e valuto se distano meno di 5 indici dall'ultima strip
            # che ho inserito nell'ultimo cluster. Se sì, appendo la strip nello stesso cluster,
            # altrimenti creo un nuovo cluster (quindi una nuova lista)
            clusters.append([])
            clusters[-1].append([overstrips[0]])
            for l in range(len(overstrips)-1):
                delta = overstrips[l+1]-overstrips[l]
                if delta <= maxstep:
                    clusters[-1][-1].append(overstrips[l+1])
                else:
                    clusters[-1].append([overstrips[l+1]])

        # Calcolo il numero di cluster presenti nell'evento k-esimo. Per i soli single-cluster
        # events, trascrivo l'indice e calcolo il baricentro
        nclusters[k] = len(clusters[-1])
        if nclusters[k] == 1:
            sclusters.append(k) # Salvo l'indice dell'evento single-cluster
            pos = clusters[-1][0]
            pheights = stripcleanCM[k,pos]
            meanpos,_ = wmean(pos,pheights,True) # Media pesata delle PH del cluster
            cidclusters.append(meanpos)
            
    # Formatto gli output come dizionario
    outs = dict()
    outs['pull'] = pull
    outs['pullL'] = pullL
    outs['pullR'] = pullR
    outs['pull_max'] = pull_max
    outs['pull_lat'] = pull_lat
    outs['argmaxstrip'] = argmaxstrip
    outs['nstripcluster'] = nstripcluster
    outs['phcluster'] = phcluster
    outs['snrcluster'] = snrcluster
    outs['etas'] = etas
    outs['nclusters'] = nclusters
    outs['cidclusters'] = cidclusters
    outs['sclusters'] = sclusters
    return(outs)


""" 
Spatial resolution calculation.
indevents = indici degli eventi buoni (single clusters) per il calcolo della res
posevents = corrispondenti posizioni dei centri dei baricentri
"""
def spaceres(posevents,indevents,refx1,refx2,refx3,z1,z2,z3,scale,conf=2.55,iffit=False):
    # In primo luogo seleziono tutti e soli gli eventi che sono stati visti sia dai reference
    # detector sia dal rivelatore sotto test.
    x1inds = np.array(indevents[refx1]) # Indici eventi buoni secondo rivelatore di rif. 1
    x2inds = np.array(indevents[refx2]) # Indici eventi buoni secondo rivelatore di test
    x3inds = np.array(indevents[refx3]) # Indici eventi buoni secondo rivelatore di rif. 2
    xinds = reduce(np.intersect1d,(x1inds,x2inds,x3inds)) # Indici comuni

    _,temp1,_ = np.intersect1d(x1inds,xinds,return_indices = True)
    xpos1 = np.array(posevents[refx1])
    xpos1 = xpos1[temp1]

    _,temp2,_ = np.intersect1d(x2inds,xinds,return_indices = True)
    xpos2 = np.array(posevents[refx2])
    xpos2 = xpos2[temp2]

    _,temp3,_ = np.intersect1d(x3inds,xinds,return_indices = True)
    xpos3 = np.array(posevents[refx3])
    xpos3 = xpos3[temp3]
    
    # Calcolo l'offset tra le misure del primo e secondo rivelatore di riferimento, per ricalibrare
    # queste ultime (e allinearle). Posso farlo per estimazione semplice o via fit.
    delta = xpos3-xpos1
    if iffit == False:
        offx = np.mean(delta)
    else:
        nbins = 50
        liminf = np.mean(delta)-conf*np.std(delta)
        limsup = np.mean(delta)+conf*np.std(delta)
        bins = np.linspace(liminf,limsup,nbins)
        histo,_ = np.histogram(delta,bins)
        bins_true = truebins(bins)
        gmodel = Model(gauss1fun)
        gmodel.nan_policy = 'propagate'
        result = gmodel.fit(histo, x=bins_true, a = np.max(histo), b = np.mean(delta), 
                            c = np.std(delta), d = histo[-1])
        offx = result.params['b']
    
    # Allineamento offline
    xalign1 = xpos1+offx
    xalign3 = xpos3
    
    # Calcolo le traiettorie delle particelle attraverso i reference detector e ne ricostruisco
    # le proiezioni nel piano del rivelatore intermedio
    m = (xalign3-xalign1)/(z3-z1)
    q = xalign1 - m*z1
    x2proj = m*z2+q
    
    # Calcolo gli scarti tra le posizioni ricostruite e quelle viste dal rivelatore sotto esame.
    # La std degli scarti (o la std del fit gaussiano della loro distribuzione di frequenza)
    # è la risoluzione spaziale.
    diffs = xpos2-x2proj
    liminf = np.mean(diffs)-conf*np.std(diffs)
    limsup = np.mean(diffs)+conf*np.std(diffs)
    if iffit == False:
        cleandiffs = diffs[(diffs>liminf) & (diffs<limsup)]
        resolution = np.std(cleandiffs)*scale
        others = []
    else:
        # Eseguo un rapido fit gaussiano sull'istogramma di frequenze degli scarti per estrarne
        # la sigma
        nbins = 75
        bins = np.linspace(liminf,limsup,nbins)
        histo,_ = np.histogram(diffs,bins)
        bins_true = truebins(bins)
        gmodel = Model(gauss1fun)
        gmodel.nan_policy = 'propagate'
        gmodel.set_param_hint('a',min=np.max(histo)/2,max=np.max(histo)*2)
        gmodel.set_param_hint('b',min=np.mean(diffs)-5*np.std(diffs),max=np.mean(diffs)+5*np.std(diffs))
        gmodel.set_param_hint('c',min=0,max=np.inf)
        gmodel.set_param_hint('d',min=0,max=50)
        result = gmodel.fit(histo, x=bins_true, a = np.max(histo), b = np.mean(diffs), 
                            c = np.std(diffs), d = histo[-1])
        resolution = (result.params['c'].value)*scale
        
        # Restituisco come altri parametri utili della funzione l'istogramma di frequenze
        # degli scarti e i suoi parametri di fit
        others = dict()
        others['bins_true'] = bins_true
        others['histo'] = histo
        others['results'] = result
    return(resolution,diffs,others)

'''
Exponential Fit function.
This function is a callable that evaluated the input data $x$ and returns $y = a*exp(b*(x-c))+d$.

Inputs:
    - x: input datas, array-like
    - a,b,c,d: function parametra
        
Outputs:
    - y: output datas
'''

def expfun(x,a,b,c,d):
    y = a*np.exp(-(x-b)/c)+d
    return(y)

''' Crystall Ball fit function
Fitting function used for the fitting of an energy distribution resulting from a lossy interaction process. More info at https://www.jlab.org/primex/weekly_meetings/slides_2009_07_17/dmitry/crystalball.html
https://en.wikipedia.org/wiki/Crystal_Ball_function

Input:
    - x, 1D array of data to be fitted with the function
    - alpha, n, mu, sigma, single-valued parametra for the function
    Suitable starting points for these values are as follows:
        - alpha, mu: location of the peak of the data
        - n: anywhere from 3 to 5
        - sigma: HWHM of the data (gaussian side)
        - N: amplitude of the peak of the data
Output:
    - y, 1D array x-like, with the value of the function in each point
'''


def crystalball(x,alpha,n,mu,sigma,N):
    # Define the auxiliar parametra A, B and N (for the latter, we need also C, D)
    A = (n/(np.abs(alpha)))**n * np.exp(-np.abs(alpha)**2/2)
    B = n/np.abs(alpha) - np.abs(alpha)

    # For the points with (x-mu)^2/\sigma > -alpha, use a right-side gaussian function
    cond = (x > -alpha*sigma + mu)
    x1 = x[cond]
    y1 = N * np.exp(- (x1-mu)**2/(2*sigma**2) )

    # For the remaining points, use a power-law left-side function
    anticond = (x <= -alpha*sigma + mu)
    x2 = x[anticond]
    y2 = N*A*(B-((x2-mu)/sigma))**(-n)
    
    # Cat the arrays
    y = np.zeros_like(x)
    y[cond] = y1
    y[anticond] = y2
    return(y)