"""
This file contains multiple functions, developed during the analysis of the Fermi-LAT data.
Author: Pietro Monti-Guarnieri, University of Trieste (pietro.monti-guarnieri@phd.units.it)
Last update: 14th February 2024
"""

# ==========================================================================================
#                                  PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd                    # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import copy                            # To copy objects in memory efficiently

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import healpy as hp                        # To build Healpix grids
import gt_apps                             # To use the fermitools in Python
import astropy.units as u                  # Celestial units of measurement
import pyLikelihood as pyLike              # Likelihood analysis
from astropy.io import fits                # I/O of .fits files
from astropy.stats import bayesian_blocks  # Bayesian Block segmentation algorithm
from LATSourceModel import SourceList      # To create the XML files required by the Fermitools
from GtApp import GtApp                    # fermitools in Python
from gammapy.data import EventList         # To read event lists from fits files
from gammapy.maps import Map,WcsNDMap      # To work with sky maps
from astropy.coordinates import Angle, SkyCoord, Distance  # To work with sky frames
from UnbinnedAnalysis import UnbinnedObs, UnbinnedAnalysis # To use gtlike in python

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')                    # To avoid 0/0 errors
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# ===========================================================================================================
#                                         BASIC UTILITIES
# ===========================================================================================================
# Here is an ensemble of basic utilities for several purposes

"""
Inverse gaussian function

This function is the inverse of a standard normalized gaussian function (with null mean and unit variance),
defined only in the positive x domain.

Inputs:
    - "y"               float or 1D-array, the y values of the gaussian function for which
                          the x values should be found.
    
Outputs:
    - "x"               float of 1D-array, the corresponding x values.
"""

def invgaussfun(y):
    return(np.sqrt(2*np.log(1 / (np.sqrt(2*np.pi)*y) ) ) )


''' 
Points-over-threshold calculator function

This function takes as input a pair of vectors, x-y, and a threshold. It then returns a list
of all the intervals in x and y where y is larger than the threshold (e.g., time and signal). 
Sufficiently close intervals are merged before the return (and the user controls the notion of closeness)

Inputs:
    - "x"                       1D-array, the x values (e.g., time)
    - "y"                       1D-array, the y values (e.g., signal)
    - "thr"                     float, the threshold for the comparison of the y vector
    - "mergedist"               int, the maximum distance between adjacent clusters of points-over-thr
                                  which will lead to merging in the processing (default value: 2)

The outputs are all list of lists, each one corresponding to a cluster of points-over-thr.
Outputs: 
    - "merged_clusters_inds"    the indexes of the points in the cluster
    - "merged_clusters_x"       the x values of the points in the cluster
    - "merged_clusters_y"       the y values of the points in the cluster

'''

def tot_intervals(x,y,thr,mergedist=None):
    # ===============================================
    #                   CROSSCHECK
    # ===============================================
    # I use a try/except because if np.shape(x,y) were to be
    # equal to (), the comparison would throw an error and thus
    # still lead to the following if being entered
    try:
        cond = (np.shape(x)[0] <= 1) | (np.shape(y)[0] <= 1)
    except:
        cond = True
    
    if cond:
        merged_clusters_inds = []
        merged_clusters_x = []
        merged_clusters_y = []
        return(merged_clusters_inds,merged_clusters_x,merged_clusters_y)

    # ===============================================
    #                   CREATE
    # ===============================================
    # Iterate over every value of the y set and check if 
    # it is greater than the threshold. If so, append it.
    all_clusters_inds = [ [] ]
    all_clusters_x = [ [] ]
    all_clusters_y = [ [] ]
    for i,(elx,ely) in enumerate(zip(x,y)):
        if (ely >= thr):
            # Point over threshold: append to the
            # last open cluster.
            all_clusters_inds[-1].append(i)
            all_clusters_x[-1].append(elx)
            all_clusters_y[-1].append(ely)
        else:
            # Point under threshold: append a new cluster to the set
            all_clusters_inds.append([])
            all_clusters_x.append([])
            all_clusters_y.append([])
    
    # After the creation of all the clusters, remove
    # the empty ones. There could be one at index 0
    # if the first point of the y vector was under threshold.
    nclusters = len(all_clusters_inds)
    new_clusters_inds = []
    new_clusters_x = []
    new_clusters_y = []
    for i in range(nclusters):
        ind = all_clusters_inds[i]
        x = all_clusters_x[i]
        y = all_clusters_y[i]
        
        if (ind != []) & (x != []) & (y != []):
            new_clusters_inds.append(ind)
            new_clusters_x.append(x)
            new_clusters_y.append(y)
    
    # ===============================================
    #                   CROSS-CHECK
    # ===============================================
    # If there are no clusters remaining, this means
    # that no point was over threshold. Thus, skip
    # the merge and return
    nclusters = len(new_clusters_inds)
    if nclusters < 1:
        print(f"No point over threshold was detected!")
        return([],[],[])


    # ===============================================
    #                   MERGE
    # ===============================================
    # Merge the clusters whose edges are distant less
    # than the user-given parameter
    if mergedist is not None:
        merged_clusters_inds = [ new_clusters_inds[0] ]
        merged_clusters_x = [ new_clusters_x[0] ]
        merged_clusters_y = [ new_clusters_y[0] ]

        for i in range(nclusters-1):
            # Compute the distance between the last element
            # of the previous cluster and the first element
            # of the new cluster. If this is less than the 
            # user-input, the clusters should be merged
            delta = new_clusters_inds[i+1][0] - new_clusters_inds[i][-1]

            if (delta <= mergedist):
                merged_clusters_inds[-1] += new_clusters_inds[i+1]
                merged_clusters_x[-1] += new_clusters_x[i+1]
                merged_clusters_y[-1] += new_clusters_y[i+1]
            else:
                merged_clusters_inds.append(new_clusters_inds[i+1])
                merged_clusters_x.append(new_clusters_x[i+1])
                merged_clusters_y.append(new_clusters_y[i+1])
    else:
        merged_clusters_inds = new_clusters_inds
        merged_clusters_x = new_clusters_x
        merged_clusters_y = new_clusters_y
   
    return(merged_clusters_inds,merged_clusters_x,merged_clusters_y)


"""
Circular ROI integrator

This function takes as input a 2D-array and returns the sum of all its values, contained in a
circular region with given radius and center equal to the center of the array itself.

Inputs:
    - "data"            1D-array, the data to be summed
    - "radius"          float, the radius of the circular area to be summed, expressed
                          in pixel units
    
Outputs:
    - "datasum"         float, the sum of the requested ROI
"""

def SumCircleFromArray(data,radius):
    # Funzione per sommare tutti gli elementi in "data"
    # che stanno a distanza pari o inferiore a "radius"
    # dal centro.
    nrows, ncols = np.shape(data) # must be 2D
    i0 = round((nrows-1)/2)
    j0 = round((ncols-1)/2)
    datasum = 0
    for i in range(nrows):
        for j in range(ncols):
            dist_ij = np.sqrt((i-i0)**2+(j-j0)**2)
            if (dist_ij <= radius*np.sqrt(2)):
                datasum += data[i,j]
    return(datasum)

"""
gttsmap getter function

This function takes as input the output of the gttsmap command and the informations about
the space structure (how many pixels were analyzed in the map and with which dimension)
and returns the map in a ready-to-use format (where ready-to-use means that it is compatible
with histoplotter2D)

Inputs:
    - "datafile"        str, path of the gttsmap output file
    - "ROI_ra"          float, x center of the gttsmap (as given in the gttsmap "xref" argument)
    - "ROI_dec"         float, y center of the gttsmap (as given in the gttsmap "yref" argument)
    - "nxpix"           float, number of pixel along x (as given in the gttsmap "nxpix" argument)
    - "nypix"           float, number of pixel along y (as given in the gttsmap "nypix" argument)
    - "binsz"           float, pixel width (as given in the gttsmap "binsz" argument)
    - "ifzero"          bool, whether to floor at zero any negative value (default: True)
    
Outputs:
    - "tsmap"           2D-array, the map of the TS values
    - "binsx"           1D-array, the x bins of the map
    - "binsy"           1D-array, the y bins of the map
"""

def getgttsmap(datafile,ROI_ra,ROI_dec,nxpix,nypix,binsz,ifzero=True):
    # Apro il file ed estraggo la tsmap
    with fits.open(datafile) as file:
        rawtsmap = file[0].data

    # la converto in array
    tsmap = np.array(rawtsmap.data).transpose()
    
    # Creo il binning
    binsx = np.arange(ROI_ra   - (nxpix/2)*binsz, ROI_ra  + (nxpix/2 + 1)*binsz, binsz)
    binsy = np.arange(ROI_dec  - (nypix/2)*binsz, ROI_dec + (nypix/2 + 1)*binsz, binsz)
    binsx = binsx[::-1]

    # Se necessario, azzero i pixel negativi
    if ifzero:
        tsmap[tsmap < 0] = 0
    return(tsmap,binsx,binsy)


"""
Function for the estimation of a source emission rate

This function takes as main input a list of timestamps, corresponding to the detection
of photons with the Fermi LAT (within a certain energy band). Then, the list is analyzed
to determine a sort of "source emission rate" as a function of the time. This rate should
reflect the variation of emission intensity over time, i.e., the presence of short bursts
with a rate much higher than the mission-long background average. 

There are multiple ways to compute the emission rates. The method here implemented 
is based on the Finite Difference between the emission times vector. In other words,
the rate is estimated by taking the difference between the emission times vector and its
translation in time by some steps (then the difference is inverted and properly normalized).

Inputs:
    - "data_times"      1D-array, timestamps of the photon in the chosen energy band,
                          expressed in MET and extracted from the Fermi photon files
   - "FDnum"           int, the translation factor in the FD algorithm, meaning that
                          the computed difference is such as vec[FDnum:] - vec[:-FDnum]
                          (default value: 3)

The outputs are organized in a dictionary: "outputdict". The keys are:
    - "out_times"           1D-array, the times associated to each FD-estimated rate.
    - "out_rates"           1D-array, the corresponding source emission rates.
    - "av_rate"             1D-array, the average of the out_rates vector
    - "significance_geom"   1D-array, the significance of each emission rate w.r.t. the
                              mission-long average. This is a geometric significance, i.e.,
                              the ratio of the emission rate and the average rate.
"""

def nu_estimator(data_times,FDnum=3):
    # Compute the times and differential rates
    out_times = data_times[FDnum:]
    out_rates = (FDnum-1) / (data_times[FDnum:] - data_times[:-FDnum])

    # Compute the average rate and derive the significance
    av_rate = np.mean(out_rates)
    significance_geom = out_rates / av_rate

    # Return everything
    outputdict = {"out_times":         out_times,
                  "out_rates":         out_rates,
                  "av_rate":           av_rate,
                  "significance_geom": significance_geom}
    return(outputdict)

'''
Functions for pacman clustering of a TS map

This is a pair of functions, one of which calls the other. The aim of this pair
is to perform a pacman-style segmentation of a binary 2D-array, i.e., a matrix
composed only of 1s and 0s. The idea is to analyze the array and determine the
clusters composing the image, where a cluster is simply defined as a set of
pixel with a value of 1 and without loss of continuity (i.e., each pixel is 
adjacent to at least one of the others). 

The segmentation is performed with a recursive function, which iterates over
every non-zero pixel of the matrix and starts a search of contiguous triggered
pixel from there. Specifically, the "master" function called by the user is
tsmapclustering, which works in this way:
- It generates a "to do list": list of points to be classified in clusters
- For each non null point in the matrix, it checks if the point is on the list.
    If so, it starts a search of the contiguous points in the cluster. This is
    done calling the "slave" functon called clusterfinder. This function returns
    a list of all the points contiguous to the under analysis and belonging to
    the same cluster. Such points are listed as done in the continuously-updated
    to-do list. Note that the to-do list is NOT the list on which this function
    is iterating!
- At the end, the function computes the centroid of each cluster and its radius,
    where the former is the average point in x and y, and the latter is the maximum
    euclidean distance between the centroid and any pixel in the cluster
- Depending on the user, the output can be given as index of the initial matrix
    or in the physical units of the matrix itself. In the latter case, the user
    should provide the original binning vectors for the map, as used in 
    histoplotter2D() or as given by getgttsmap().

The clusterfinder function works in this way:
- It starts a search from a given pixel
- It looks to the closest neighbors of the pixel and checks if they are in the
    to-do list. If so, the points are removed from the latter and added to the 
    cluster list
- The function iterates over every remaining point in the cluster list, and for
    each one it calls itself recursively. In this way, the function is exited
    if and only if the cluster is finished.

------------------------------------------------------------------------------------
Inputs of tsmapclustering():
    - "boolmat"             2D-array, the matrix to be clustered, containing only 1s
                              and 0s (the clusters are the 1s).
    - "mode"                str, an argument passed to "clusterfinder" specifying what
                              is actually a cluster. Accepted values: "1NN" to indicate
                              that only continuous streaks of contiguous points are
                              accepted (default value: "1NN")
    - "ifrescaleoutput"     bool, whether to provide the output clusters as sets of
                              indices (False) or as sets of coordinates (True). Note that
                              this also holds for the radiuses and so on. Note that binsx
                              and binsy must not be None, otherwise the scaling is not
                              triggered (default value: False)
    - "binsx"               1D-array or None, the binning vector for the x axis of the
                              matrix, used in the rescaling of the indices in coordinates.
                              (default: None, meaning that no scaling is done)
    - "binsy"               1D-array or None, the binning vector for the x axis of the
                              matrix, used in the rescaling of the indices in coordinates.
                              (default: None, meaning that no scaling is done)

IMPORTANT NOTE: binsx and binsy, if given, should be cast in a particular form. First of all,
binsx, binsy and boolmat must be the same that are passed to MyFunctions.histoplotter2D()
in order to show the map in 2D. This means that the same vector must be given as input to both
functions, otherwise there may be mismatches between the graphical representations. Secondly,
the vector binsx must be decreasing as indexes go from 0 to -1, while binsy must increase
(i.e., be normally ordered). This is weird for the standards of histogram creation, but it
is what works best with the Fermitools. Thus, if one may want to use this function without the
Fermitools but just with a matrix created through MyFunctions.truehisto2D(), then one should:
    - Create the histogram through truehisto2D()
    - Invert the binsx vector: binsx = binsx[::-1]
    - Call tsmapclustering() passing binsx, binsy and boolmat as arguments
    - Call histoplotter2D() passing binsx, binsy and boolmat as arguments


Outputs of tsmapclustering():
    - "clusterCenters"      2D-array, with each row being the center of a different cluster.
                              The columns are simply the x and y of each center.
    - "clusterData"         dict, containing some more details about each cluster. Currently
                              the available keys are: 
                              - "clusterRadius", 1D-array containing an estimate of the extension
                              of each cluster. This is computed simply as the average euclidean
                              distance between the pixels in the cluster and the center.
                              If "ifrescaleoutput" is True and "binsx" and "binsy" are 1D-arrays,
                              then "clusterRadius" will be rescaled too, from indices to coordinates

                              - "clusterPoints", list containing in each element a list of all the 
                              points belonging to a certain cluster. Each point is formatted as a 
                              tuple of (x,y) coordinates. If "ifrescaleoutput" is True and "binsx" 
                              and "binsy" are 1D-arrays, then the points will be rescaled too, from 
                              indices to coordinates.

------------------------------------------------------------------------------------
Inputs of clusterfinder():
    - "thisPoint"           tuple, x and y of the point under examination.
    - "pointsThisCluster"   list of lists, each containing the points belonging to a certain
                              cluster. The points are formatted as (x,y) tuples
    - "pointsToDo"          list of tuples, each refering to a point which must still be
                              analyzed (since it is over threshold AND non associated to any
                              cluster)
    - "mode"                str, the definition of a cluster. Accepted values: "1NN" means
                              that the points belonging to a cluster are all contiguous, i.e.,
                              1st neighbors (default value: "1NN")

Outputs of clusterfinder():
    - "pointsThisCluster"   as the input, since this function is recursive
    - "pointsToDo"          as the input, since this function is recursive
'''
def tsmapclustering(boolmat,mode = '1NN',ifrescaleoutput=False,binsx=None,binsy=None):
    # Get x,y of all the good (non zero) pixels and cast them as list of tuples
    xpix, ypix = np.nonzero(boolmat)
    points = [(x, y) for x,y in zip(xpix,ypix)]

    # Initialize the to-do list and the list of the cluster points
    pointsToDo = copy.deepcopy(points)           # The "to-do list"
    clusterPoints = [ ]                          # The list of the points of each clusters
    
    # Iterate over every point to do and perform the clustering
    for i,thisPoint in enumerate(points):
        # If the point is NOT in the to do-list, it should be skipped!
        if thisPoint in pointsToDo:
            # The point must be done: remove it from the to-do list and then
            # find all of its contiguous neighbors
            pointsToDo.remove(thisPoint)
            pointsThisCluster = [thisPoint] # Initialize new cluster as non empty list
            pointsThisCluster,pointsToDo = clusterfinder(thisPoint,pointsThisCluster,
                                                         pointsToDo,mode)
            clusterPoints.append(pointsThisCluster)

    # At this point, all the clusters have been found. Thus, we iterate on them
    # to compute their centroids and extensions (radius)
    numClusters = len(clusterPoints)
    clusterCenters = np.zeros((numClusters,2))
    clusterRadius = np.zeros((numClusters,))
    
    for i,clu in enumerate(clusterPoints):
        # Extract the cluster as ndarray and compute the centroid as average 
        # (x,y) coordinate pair
        cluarray = np.array(clu)
        clusterCenters[i,0], clusterCenters[i,1] = np.mean(clu,axis=0)

        # Compute the radius of the cluster as the average distance between the
        # cluster points and the centroid
        nullclu = cluarray - np.mean(clu,axis=0) # Centering the cluster
        distances = np.sqrt(nullclu[:,0]**2 + nullclu[:,1]**2) # Dist from points to center
        clusterRadius[i] = np.mean(distances)

    # If requested, rescale the outputs in physical units
    if ifrescaleoutput & (binsx is not None) & (binsy is not None):
        '''
        Brief comment on the conversion from pixel to units.
            y: start from the lower edge of binsy and add a number of binning steps 
                equal to the y index which is being scaled + half step to correct 
                the fact that the pixel indices are "centered" in the pixels themselves
            x: as above but instead of starting from the lower edge, start from the 
                upper edge and go down from there. This is done due to how binsx is 
                constructed: the vector should be inverted (i.e., monotonically 
                decreasing), since it is shaped to work with plt.imshow(), which inverts 
                the axis!
        '''
        # Compute the binning step along the x and y axis. Assume bins with
        # equal dimension for simplicity.
        binstepx = np.abs( (binsx[1] - binsx[0]) )
        binstepy = np.abs( (binsy[1] - binsy[0]) )

        # Rescale the cluster points in a new list
        newclusterPoints = []
        for i,cluster in enumerate(clusterPoints):
            # Append an empty list to the new list, to allocate space for a cluster
            newclusterPoints.append([])
            
            for el in cluster:
                # Perform the conversion
                xscaled = binsx[0] - el[0]*binstepx - binstepx/2
                yscaled = el[1]*binstepy + binsy[0] + binstepy/2
                
                # Append the coordinate to a new list
                pointscaled = (xscaled,yscaled)
                newclusterPoints[-1].append(pointscaled)

        # Replace the old list with a new one
        clusterPoints = newclusterPoints
        # --------------------------------------------------------------------

        # Rescale the cluster centers and radiuses identically
        numclusters = len(clusterCenters)
        newclusterCenters = np.zeros_like(clusterCenters)
        newclusterRadius = np.zeros_like(clusterRadius)

        for i in range(numclusters):
            # Perform the conversion. For now, the radius is scaled with the
            # AVERAGE binning step along x and y. To be more accurate, one
            # should repeat the radius calculation weighting the pixel distances
            # by the correct binning steps. However, binstepx and binstepy
            # will almost always be equal, so this is not a huge issue.
            newclusterCenters[i,0] = binsx[0] - clusterCenters[i,0]*binstepx - binstepx/2
            newclusterCenters[i,1] = clusterCenters[i,1]*binstepy + binsy[0] + binstepy/2
            newclusterRadius[i] = clusterRadius[i]*(binstepx+binstepy)/2
            
        # Replace the old vectors with the new ones
        clusterCenters = newclusterCenters
        clusterRadius = newclusterRadius

    # Build the output dictionary
    clusterData = {"clusterCenters": clusterCenters,
                   "clusterRadius": clusterRadius}        
        
    return(clusterPoints,clusterData)

# -----------------------------------------------------------------------

def clusterfinder(thisPoint,pointsThisCluster,pointsToDo,mode='1NN'):
    # Find the points neighboring to "thisPoint" which should belong to
    # the same cluster. They could be the closest neighbors, or anything else
    if mode == '1NN':
        # Consider only the first 4 closest neighbors. Enumerate them in
        # clock-wise order starting from north (i.e. N, E, S, W)
        x0, y0 = thisPoint
        pointsClosest = [ (x0,y0+1), (x0+1,y0), (x0,y0-1), (x0-1,y0) ]

    # Iterate over the closest neighbors
    for point in pointsClosest:
        # Check only the neighbors which are eligible (i.e., in the to-do
        # list) and skip the others
        if point in pointsToDo:
            # If the point is in the to-do list, remove it from the latter
            # and append it to the cluster list
            pointsThisCluster.append(point)
            pointsToDo.remove(point)

            # Call recursively the clusterfinder function, to check also the
            # first neighbors of the neighbors, until no points are left
            pointsThisCluster,pointsToDo = clusterfinder(point,pointsThisCluster,
                                                         pointsToDo,mode)

    # At this point no points are left to check: return to all the former
    # layer in the recursion chain!
    return(pointsThisCluster,pointsToDo)


"""
J2000 formatter function

This function takes as input a pair of coordinates (in decimal RA/DEC degrees) and uses it
to format a J2000 name.

Inputs:
    ...
    
Outputs:
    ...
"""

def J2000formatter(RA,DEC):
    # Create a SkyCoord object with the given coordinates
    SCobj = SkyCoord(ra=RA*u.degree, dec=DEC*u.degree)

    # Use it to obtain the RA hours and minutes
    hours = SCobj.to_string('hmsdms').split('h')[0]
    minutes = SCobj.to_string('hmsdms').split('h')[1].split('m')[0]

    # Return the obtained hours and minutes and add the sign of the
    # DEC and the DEC value
    return(f"J{hours}{minutes}{'+' if (DEC > 0) else '-'}{np.abs(DEC):04.1f}")

'''
Sky grid builder function

This function is used mainly by the HEPA monitor but it is of general use. Its main aim is to build
a grid of points in the galactic latitude - longitude plane, trying to have equal spacing between
each point. Then, a radius should be assigned to each point: the union of the circular regions
produced by each point should give a complete tassellation of the sky. Theoretically, this problem
is unsolvable in an exact way, but there are many workarounds which work reasonably well. 
In this function we implement the "golden spiral" method explained here:

https://extremelearning.com.au/how-to-evenly-distribute-points-on-a-sphere-more-effectively-than-the-canonical-fibonacci-lattice/

The idea is to build a grid of points in a theta-phi (long-lat) space by using the golden ratio
to determine the point following any given one. In this way, we just need to provide as input 
to the algorithm two parameters: the numeber of points to create and the value of the epsilon
parameter (which is essentially a regularization term). Then, we look at the average distance
from each point to its closest neighbors and determine what radius could allow a good sky
coverage. Experimentally, we have found that with n = 750 and epsilon = -0.5 (which is negative
in order to fill better the region around the poles) all the points are within approximately 8.20°
from the first 6 neighbors. The distance is smaller at the poles. This means that with a radius of 
5° we can assure a full coverage of the sky, without holes.

------------------------------------------------------------------------------------
Inputs:
    - n                     int, how many points to generate (default value: 750)
    - epsilon               float, the regularization term. Positive values pull all the points
                              towards the galactic equator, while negative values shrink the points
                              towards the galactic poles (default value: -0.5)
    - radius_deg            float, the radius of all the regions, in deg (default value: 5°)
    
Outputs:
    - grid_ra_deg           1D-array, the RA of the points in the grid, expressed in deg
    - grid_dec_deg          1D-array, the DEC of the points in the grid, expressed in deg
    - grid_blong_deg        1D-array, the galactic longitude of the points in the grid, expressed in deg
    - grid_blat_deg         1D-array, the galactic latitude of the points in the grid, expressed in deg
    - grid_radius_deg       1D-array, the radius associated to the circular region built around each
                              point in the grid, expressed in deg
'''

def skygridbuilder(n=750,epsilon=-0.5,radius_deg=5):
    # Build the grid of galactic latitudes and longitudes using the "golden spiral" method
    goldenRatio = (1 + 5**0.5)/2
    i = np.arange(0, n) 
    grid_blong_rad = 2 *np.pi * i / goldenRatio
    arglat = 1 - 2*(i+epsilon)/(n-1+2*epsilon)
    arglat[arglat <= -1] = -1  # Note that |arglat| is around 1.00001 for i = 0, n-1: we need to floor
    arglat[arglat >= 1] = 1    # any exceeding value, to avoid np.nan outputs from np.arccos
    grid_blat_rad = np.arccos(arglat) - np.pi/2  # Note the domain shifting from [0,pi] to [-pi/2,pi/2]
    
    # Note that blong_rad, as it is defined, is not bounded: it scales linearly with i (which ranges
    # from 0 to n). Thus, we want to remove the periodicity and reduce the domain to [0,2pi] before 
    # converting in degrees.
    grid_blong_rad = grid_blong_rad % (2*np.pi)
    
    # Now we can scale the latitude and longitude to degrees.
    grid_blong_deg = np.degrees(grid_blong_rad)
    grid_blat_deg = np.degrees(grid_blat_rad)
    
    # Convert the latitudes and longitudes from galactic coordinates to RA/DEC (in deg)
    cord = SkyCoord(grid_blong_deg,grid_blat_deg, frame='galactic', unit='deg')
    grid_ra_deg, grid_dec_deg = cord.icrs.ra.deg, cord.icrs.dec.deg

    # Compute the ROI radiuses
    grid_radius_deg = grid_blat_deg*0  + radius_deg
    
    # Return the outputs
    return(grid_ra_deg, grid_dec_deg, grid_blong_deg, grid_blat_deg,grid_radius_deg)





























# ===========================================================================================================
#                                              FERMITOOLS
# ===========================================================================================================
# Here is an ensemble of wrappers built around the fermitools and fermipy to facilitate their use in Python 

"""
gtselect function

This function performs a call to the Fermitool "gtselect" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtselect.txt
    
This function has no outputs.
"""
def gtselect(infile,outfile,ROI_ra=0,ROI_dec=0,ROI_rad=2.5,Emin=100,Emax=300000,zmax=90,
             tstart="INDEF",tstop="INDEF",evclass=128,evtype=3,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtselect'])

    # Set the parameters for the gtselect app
    gt_apps.filter['infile']  = infile    # Input file
    gt_apps.filter['outfile'] = outfile   # Output file
    gt_apps.filter['ra']      = ROI_ra    # [deg] center of the ROI in RA coordinates
    gt_apps.filter['dec']     = ROI_dec   # [deg] center of the ROI in DEC coordinates
    gt_apps.filter['rad']     = ROI_rad   # [deg] radius of the ROI
    gt_apps.filter['zmax']    = zmax      # [deg] maximum zenith angle
    gt_apps.filter['emin']    = Emin      # [MeV] minimum photon energy
    gt_apps.filter['emax']    = Emax      # [MeV] minimum photon energy
    gt_apps.filter['tmin']    = tstart    # [MET] beginning of the time interval
    gt_apps.filter['tmax']    = tstop     # [MET] end of the time interval
    gt_apps.filter['evclass'] = evclass   # Class of the event
    gt_apps.filter['evtype']  = evtype    # Type of the event
    gt_apps.filter['chatter'] = chatter   # Verbosity
    gt_apps.filter['mode']    = gtmode    # ['ql','h'] Mode of execution
    
    # Call the application
    print(f"Calling gtselect... \n")
    gt_apps.filter.run()
    print(f"\nDone!\n")
    return

"""
gtmktime function

This function performs a call to the Fermitool "gtmktime" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtmktime.txt
    
This function has no outputs.
"""

def gtmktime(infile,outfile,SCfile,ROI_ra=None,ROI_dec=None,roicut='no',SUNsep=None,
             filtercond='(DATA_QUAL>0)&&(LAT_CONFIG==1)',chatter=4,gtmode="h"):
    # Add the sun separation angle (if it is defined) to the filter condition
    if (SUNsep is not None) & (ROI_ra is not None) & (ROI_dec is not None):
        filtercond += f"&&(angsep({ROI_ra},{ROI_dec},RA_SUN,DEC_SUN)>{SUNsep})"    

    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtmktime'])

    # Set the parameters for the gtmktime app
    gt_apps.maketime['evfile']  = infile        # Input file (from gtselect)
    gt_apps.maketime['outfile'] = outfile       # Output file
    gt_apps.maketime['scfile']  = SCfile        # Spacecraft file
    gt_apps.maketime['filter']  = filtercond    # [str] Filter condition
    gt_apps.maketime['roicut']  = roicut        # Apply ROI correction?
    gt_apps.maketime['chatter'] = chatter       # Verbosity
    gt_apps.maketime['mode']    = gtmode        # ['ql','h'] Mode of execution
    
    # Call the application
    print(f"Calling gtmktime... \n")
    gt_apps.maketime.run()
    print(f"\nDone!\n")
    return

"""
gtbindef function

This function performs a call to the Fermitool "gtbindef" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbindef.txt
    
This function has no outputs.
"""

def gtbindef(binfile,outfile,bintype='T',chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtbindef'])

    # Set the parameters for the gtbindef app
    bindef = GtApp('gtbindef','Likelihood')
    bindef['binfile'] = binfile     # Binning file
    bindef['outfile'] = outfile     # Output file
    bindef['bintype'] = bintype     # Type of binning
    bindef['chatter'] = chatter     # Verbosity
    bindef['mode']    = gtmode      # ['ql','h'] Mode of execution
    
    # Call the application
    print(f"Calling gtbindef... \n")
    bindef.run()
    print(f"\nDone!\n")
    return

"""
gtbin function

This function performs a call to the Fermitool "gtbin" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbin.txt
    
This function has no outputs.
"""

def gtbin(infile,outfile,SCfile,tbinfile=None,tstart=0,tstop=100,algorithm='LC',tbinalg='FILE',
          chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtbin'])

    # Set the parameters for the gtbin app. Note that tstart and tstop are ignored if tbinfile
    # is specified.
    gt_apps.evtbin['evfile']    = infile       # Input file (from gtmktime)
    gt_apps.evtbin['outfile']   = outfile      # Output file
    gt_apps.evtbin['scfile']    = SCfile       # Spacecraft file
    gt_apps.evtbin['tstart']    = tstart       # Start of the timing vector
    gt_apps.evtbin['tstop']     = tstop        # Stop of the timing vector
    gt_apps.evtbin['algorithm'] = algorithm    # Type of output (e.g., Light Curves, MAPs, etc.)
    gt_apps.evtbin['tbinalg']   = tbinalg      # How the time bins will be specified
    gt_apps.evtbin['chatter']   = chatter      # Verbosity
    gt_apps.evtbin['mode']      = gtmode       # ['ql','h'] Mode of execution

    if tbinfile is not None:
        gt_apps.evtbin['tbinfile']  = tbinfile     # Binning file
    
    # Call the application
    print(f"Calling gtbin... \n")
    gt_apps.evtbin.run()
    print(f"\nDone!\n")
    return

"""
gtbin (in CMAP mode) function

This function performs a call to the Fermitool "gtbin" utility, passing to it several 
arguments, setting the CMAP mode of operation.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbin.txt
    
This function has no outputs.
"""

def gtbin_CMAP(infile,outfile,SCfile,algorithm='CMAP',nxpix=10,nypix=10,
               binsz=0.5,coordsys='GAL',xref=0,yref=0,axisrot=0,
               proj='AIT',chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtbin'])

    # Set the parameters for the gtbin app.
    gt_apps.evtbin['evfile']    = infile       # Input file (from gtmktime)
    gt_apps.evtbin['outfile']   = outfile      # Output file
    gt_apps.evtbin['scfile']    = SCfile       # Spacecraft file
    gt_apps.evtbin['algorithm'] = algorithm    # Force to "CMAP" to have counting map
    gt_apps.evtbin['nxpix']     = nxpix        # Number of pixels from yref in total
    gt_apps.evtbin['nypix']     = nypix        # Number of pixels from yref in total
    gt_apps.evtbin['binsz']     = binsz        # Binning step in x, y
    gt_apps.evtbin['coordsys']  = coordsys     # Coordinate system for xref, yref
    gt_apps.evtbin['xref']      = xref         # x center of the frame
    gt_apps.evtbin['yref']      = yref         # y center of the frame
    gt_apps.evtbin['axisrot']   = axisrot      # Rotation angle in the x-y frame
    gt_apps.evtbin['proj']      = proj         # ['AIT','CART', ...] Projection style
    gt_apps.evtbin['chatter']   = chatter      # Verbosity
    gt_apps.evtbin['mode']      = gtmode       # ['ql','h'] Mode of execution

    # Call the application
    print(f"Calling gtbin in CMAP mode... \n")
    gt_apps.evtbin.run()
    print(f"\nDone!\n")
    return

"""
gtexposure function

This function performs a call to the Fermitool "gtexposure" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexposure.txt
    
This function has no outputs.
"""

def gtexposure(infile,SCfile,irfs='P8R3_SOURCE_V3',scrmdl="none",specin=-2.1,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtexposure'])

    # Set the parameters for the gtexposure app
    exposure = GtApp('gtexposure','Likelihood')
    exposure['infile']  = infile         # Input file
    exposure['scfile']  = SCfile         # Spacecraft file
    exposure['irfs']    = irfs           # [str] IRFs
    exposure['srcmdl']  = scrmdl         # Source model
    exposure['specin']  = specin         # Photon spectral index to use for weighting the exposure
    exposure['chatter'] = chatter        # Verbosity
    exposure['mode']    = gtmode         # ['ql','h'] Mode of execution

    # Call the application
    print(f"Calling gtexposure... \n")
    exposure.run()
    print(f"\nDone!\n")
    return  


"""
gtltcube function

This function performs a call to the Fermitool "gtltcube" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtltcube.txt
    
This function has no outputs.
"""

def gtltcube(infile,outfile,SCfile,zmax=90,dcostheta=0.025,binsz=0.25,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtltcube'])

    # Set the parameters for the gtltcube app
    gt_apps.expCube['evfile']    = infile
    gt_apps.expCube['outfile']   = outfile
    gt_apps.expCube['scfile']    = SCfile    
    gt_apps.expCube['zmax']      = zmax
    gt_apps.expCube['dcostheta'] = dcostheta
    gt_apps.expCube['binsz']     = binsz
    gt_apps.expCube['chatter']   = chatter
    gt_apps.expCube['mode']      = gtmode

    # Call the application
    print(f"Calling gtltcube... \n")
    gt_apps.expCube.run()
    print(f"\nDone!\n")
    return

"""
gtexpcube2 function

This function performs a call to the Fermitool "gtexpcube2" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexpcube2.txt
    
This function has no outputs.
"""

def gtexpcube2(infile,outfile,cmap='none',irfs='P8R3_SOURCE_V3',evtype='INDEF',nxpix=1,
               nypix=1,binsz=0.25,coordsys='GAL',xref=0,yref=0,axisrot=0,proj='AIT',
               ebinalg='LOG',Emin=1e3,Emax=2e6,enumbins=20,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtexpcube2'])

    # Set the parameters for the gtexpcube2 app
    gtexpcube2 = GtApp('gtexpcube2','Likelihood')
    gtexpcube2['infile']    = infile
    gtexpcube2['outfile']   = outfile
    gtexpCube2['cmap']      = cmap
    gtexpCube2['irfs']      = irfs
    gtexpCube2['evtype']    = evtype
    gtexpCube2['nxpix']     = nxpix
    gtexpCube2['nypix']     = nypix
    gtexpCube2['binsz']     = binsz
    gtexpCube2['coordsys']  = coordsys
    gtexpCube2['xref']      = xref
    gtexpCube2['yref']      = yref
    gtexpCube2['axisrot']   = axisrot
    gtexpCube2['proj']      = proj
    gtexpCube2['ebinalg']   = ebinalg
    gtexpCube2['emin']      = Emin
    gtexpCube2['emax']      = Emax
    gtexpCube2['enumbins']  = enumbins
    gtexpCube2['chatter']   = chatter

    # Call the application
    print(f"Calling gtexpcube2... \n")
    gtexpCube2.run()
    print(f"\nDone!\n")
    return

"""
gtexpmap function

This function performs a call to the Fermitool "gtexpmap" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexpmap.txt
    
This function has no outputs.
"""

def gtexpmap(infile,outfile,SCfile,expcubefile,evtype="INDEF",irfs="CALDB",srcrad=10,nlong=10,
             nlat=10,nenergies=20,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtexpmap'])

    # Set the parameters for the gtexpmap app
    gt_apps.expMap['evfile']    = infile
    gt_apps.expMap['outfile']   = outfile
    gt_apps.expMap['scfile']    = SCfile
    gt_apps.expMap['expcube']   = expcubefile
    gt_apps.expMap['evtype']    = evtype
    gt_apps.expMap['irfs']      = irfs
    gt_apps.expMap['srcrad']    = srcrad
    gt_apps.expMap['nlong']     = nlong
    gt_apps.expMap['nlat']      = nlat
    gt_apps.expMap['nenergies'] = nenergies
    gt_apps.expMap['chatter']   = chatter
    gt_apps.expMap['mode']      = gtmode

    # Call the application
    print(f"Calling gtexpmap... \n")
    gt_apps.expMap.run()
    print(f"\nDone!\n")
    return

"""
gtdiffrsp function

This function performs a call to the Fermitool "gtdiffrsp" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtdiffrsp.txt
    
This function has no outputs.
"""

def gtdiffrsp(infile,SCfile,srcmdl,irfs='CALDB',clobber='yes',chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtdiffrsp'])

    # Set the parameters for the gtdiffrsp app
    gt_apps.diffResps['evfile']  = infile
    gt_apps.diffResps['scfile']  = SCfile
    gt_apps.diffResps['srcmdl']  = srcmdl
    gt_apps.diffResps['irfs']    = irfs
    gt_apps.diffResps['clobber'] = clobber
    gt_apps.diffResps['chatter'] = chatter
    gt_apps.diffResps['mode']    = gtmode

    # Call the application
    print(f"Calling gtdiffrsp... \n")
    gt_apps.diffResps.run()
    print(f"\nDone!\n")
    return

"""
gttsmap function

This function performs a call to the Fermitool "gttsmap" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gttsmap.txt
    
This function has no outputs.
"""

def gttsmap(infile,outfile,SCfile,expcubefile,expmapfile,srcmdl,statistics="UNBINNED",
            irfs="CALDB",optimizer="NEWMINUIT",nxpix=10,nypix=10,binsz=0.025,coordsys="CEL",
            xref=0,yref=0,proj="AIT",chatter=2,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gttsmap'])

    # Set the parameters for the gttsmap app
    gt_apps.TsMap['evfile'] = infile
    gt_apps.TsMap['outfile'] = outfile
    gt_apps.TsMap['scfile'] = SCfile
    gt_apps.TsMap['expcube'] = expcubefile
    gt_apps.TsMap['expmap'] = expmapfile
    gt_apps.TsMap['srcmdl'] = srcmdl
    gt_apps.TsMap['statistic'] = statistics
    gt_apps.TsMap['irfs'] = irfs
    gt_apps.TsMap['optimizer'] = optimizer
    gt_apps.TsMap['nxpix'] = nxpix
    gt_apps.TsMap['nypix'] = nypix
    gt_apps.TsMap['binsz'] = binsz
    gt_apps.TsMap['coordsys'] = coordsys
    gt_apps.TsMap['xref'] = xref
    gt_apps.TsMap['yref'] = yref
    gt_apps.TsMap['proj'] = proj
    if chatter <= 2:
        gt_apps.TsMap['chatter'] = chatter
    else:
        # I'm adding an artificial cap to the chatter here, because a value of 4 would
        # absolutely render the terminal screen unreadable. This can be changed, if the user
        # really wants a complete output...
        gt_apps.TsMap['chatter'] = 2
    gt_apps.TsMap['mode'] = gtmode

    # Call the application
    print(f"Calling gttsmap... \n")
    gt_apps.TsMap.run()
    print(f"\nDone!\n")
    return

"""
gtlike function

This function performs a call to the Fermitool "gtlike" utility, passing to it several 
arguments. In this case, we do not simply wrap code around the official utility,
but rather use the Fermipy implementation of the function.

Inputs:
    - infile            [str] the path of the photon input file, as given by gtmktime
    - outfile           [str] the path of the XML output file, which will include all
                          the sources already present in the input XML file + the sources
                          here fitted
    - SCfile            [str] the path of the spacecraft input file
    - expcubefile       [str] the path of the LAT livetime file, as given by gtltcube
    - expmapfile        [str] the path of the LAT livetime file, as given by gtexposure
    - srcmdl            [str] the path of the XML input file, containing a list of all
                          the sources known in the area (+ eventual flares)
    - irfs              [str] the IRFs to be used in the fitting (default value: "CALDB",
                          meaning something like Pass 8 release 3)
    - optimizer         [str] the optimizer to be used in the likelihood fits (defualt value:
                          "NewMinuit")
    - verbosity         [int] the verbosity for the execution (default value: 1, relatively
                          low verbosity for the call)

This function has no outputs.
"""

def gtlike(infile,outfile,SCfile,expcubefile,expmapfile,srcmdl,irfs='CALDB',
           optimizer='NewMinuit',verbosity=1):
    # Set the parameters for the gtlike app wrapped in Python by pyLikelihood
    print(f"Initializing gtlike...")
    obs = UnbinnedObs(infile,SCfile,expCube=expcubefile,expMap=expmapfile,irfs=irfs)
    like = UnbinnedAnalysis(obs,srcmdl,optimizer=optimizer)
    if optimizer == 'NewMinuit':
        likeobj = pyLike.NewMinuit(like.logLike)
    print(f"Initialization completed!\n")

    # Call the application
    print(f"Running gtlike...")
    like.fit(verbosity=verbosity,covar=True,optObject=likeobj)

    # Save the output in the corresponding file and return the exit code
    like.logLike.writeXml(outfile)
    returncode = likeobj.getRetCode()
    print(f"gtlike exit code: {returncode} \n")
    return(like)