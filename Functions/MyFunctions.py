"""
This file contains multiple functions, often used during statistical and physical analysis. 
Author: Pietro Monti-Guarnieri, Università di Ferrara (mntptr@unife.it)
Last update: 06th May, 2024
"""

# ===========================================================================================================
#                                       GLOBAL IMPORTS
# ===========================================================================================================
# Module imports
import matplotlib.pyplot as plt     # Per ambienti di plot
import matplotlib.colors as colors  # Per i colori dei plot
import scipy.io as io               # Per particolari funzioni di I/O
import scipy.signal as sig          # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg        # Per analisi dei segnali e delle immagini
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import numpy as np                  # Per analisi numeriche
import pandas as pd                 # Per analisi numeriche con i dataframe
import time                         # Per il timing degli script
import gc                           # Per la pulizia della memoria
import uproot                       # Per l'upload dei file root
import lmfit                        # Per fit avazati
import json                         # Per leggere i file JSON
import PyPDF2                       # Per unire i PDF alla fine
import glob                         # Per manipolazione dei path
import h5py                         # Per caricare i file .h5
import copy                         # Per copiare gli oggetti in modo corretto
import subprocess                   # Per chiamare script come da bash
import matplotlib.cm as pltmaps     # Per le mappe
import scipy.special as sp

# Sub-module imports
from sklearn import ensemble        # Machine Learning: Random Forests
from sklearn import neural_network  # Machine Learning: Neural Networks

# Single function imports
from IPython.display import display, Javascript   # Per il warning visivo
from IPython.display import Audio   # Per un warning audio
from lmfit import Model             # Per fare fit
from functools import reduce        # Per usare intersect1d su più vettori assieme
from scipy.optimize import curve_fit as ft        # Per fare fit


# ===========================================================================================================
#                                         BASIC UTILITIES
# ===========================================================================================================
# Here is an ensemble of basic utilities for several purposes

"""
String to list converter

This function is used to convert a string describing a list of values into a list containing said values.
This is useful essentially when a Python script is called by command-line or with the subprocess module,
since in those cases some arguments may be passed to the script as strings and should be converted to lists.

Inputs:
    - "inputstr"        string, the one to be converted. It should begin and end with a square parenthesis
                          and it should contain comma-parsed values. Example: "[1,2,404]"
    - "dataformat"      type, the data type in which all the values contained in the script will be casted.
                          Default value: int
    
Outputs:
    - "liststr"         list, the desired one
"""

def str2list(inputstr,dataformat=int):
    # Remove empty spaces from the string (which may impact on the subsequent conversion)
    nospacestr = inputstr.replace(' ','')

    # Remove the square parentheses, assuming they are the first and last character of the string
    cuttedstr = inputstr[1:-1]
    
    # Split the list in correspondence of the commas and cast each element in the desired type
    liststr = cuttedstr.split(',')
    for i,el in enumerate(liststr):
        # Note that here "dataformat" is the function names as the input type. Example: int(something)
        liststr[i] = dataformat(el)
        
    # Return the output
    return(liststr)

''' 
Rectangle plotter

This function simply plots a box or rectangle of given edges, in a given figure 

Inputs:
    - "ax"          Axes-type object, the set of axis on which the plot will be drawn
    - "edges"       2D-array of dimension (2,2), the edges of the box to be shown. The format is:
                      np.array([[xmin,xmax],[ymin,ymax]])
    - "data_label"  string, the label for the plot. Default value: "Box"
    - "data_color"  string, the color for the plot. Default value: "red"
    - "data_ls"     string, the linestyle for the plot. Default value: "--"
    - "data_lw"     float, the linestyle for the plot. Default value: 1

Outputs: None
There are no outputs (the function just renders the requested plot)
'''

def rectplotter(ax,edges,data_label='Box',data_color='red',data_ls='--',data_lw=1):
    # Simply plot all the edges. There is no return.
    ax.plot([edges[0,0], edges[0,1]],[edges[1,0],edges[1,0]], color = data_color, linestyle = data_ls, 
            linewidth = data_lw, label = data_label)
    ax.plot([edges[0,0], edges[0,1]],[edges[1,1],edges[1,1]], color = data_color, linestyle = data_ls, 
            linewidth = data_lw)
    ax.plot([edges[0,0], edges[0,0]],[edges[1,0],edges[1,1]], color = data_color, linestyle = data_ls, 
            linewidth = data_lw)
    ax.plot([edges[0,1], edges[0,1]],[edges[1,0],edges[1,1]], color = data_color, linestyle = data_ls, 
            linewidth = data_lw)


''' 
Memory cleaner

This function calles iteratively the garbage collector, in order to clear the memory of the system

There are no inputs and no outputs (the function just performs the memory clean-up)
'''

def gc_cleaner():
    for ind in range(3):
        gc.collect(ind)
    return()

    
''' 
Warning function

When called, this function simply produces a visible warning (in Jupyter Notebook, not in command-line
python) signaling the end of execution of the script.

There are no inputs and no outputs (the function just performs the print-out)
'''

def warncall():
    display(Javascript("""
    require(
        ["base/js/dialog"], 
        function(dialog) {
            dialog.modal({
                title: 'Warning',
                body: 'Code execution has ended!',
                buttons: {
                    'Return to the script': {}
                }
            });
        })
    """))
    
''' 
Audio cue function

When called, this function simply produces an audible warning (in Jupyter Notebook, not in command-line
python) signaling the end of execution of the script.

Inputs:
    - "path"            string or path-like object, the path where the cue file (.mp3) is stored

There are no outputs (the function just performs the call)
'''

def audiocall(path = 'C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi Dati\\Functions\\cavcharge.mp3'):
    display(Audio(path, autoplay=True))






























# ===========================================================================================================
#                                 HISTOGRAM PRODUCTION, PLOT AND ANALYSIS
# ===========================================================================================================
# Here we list every function useful in the production of a 1D or a 2D histogram, but also in their
# graphical representation and their manual analysis. Note: no fit function is used here.

'''
Bin Centering

This function allows to pass from a set of binning edges to a set of binning centers. In the operation, 
every point given is shifted to the right by half of the binning step (assumed to be constant) and the 
last point is removed.

Inputs:
    - "bins"            1D-array of shape (N,), the binning edges
    
Outputs:
    - "tbins"           1D-array of shape (N-1,), the binning centers
'''

def truebins(bins):
    # Shift the binning edges and remove the last point
    step = np.abs(bins[1]-bins[0])/2
    tbins = bins[:-1]+step
    return(tbins)

'''
1D-Histogram production

This function returns the 1-Dimensional histogram of a given dataset, with user-defined binning.

Inputs:
    - "data"            1D-array, the data to be histogrammed
    - "nbins"           float, the number of bins to produce or the binning step depending on "ifstep"
    - "edgeL"           float, the lower edge of the binning vector. Default value: None, meaning that the 
                          lower edge will be the minimum of the data
    - "edgeU"           float, the upper edge of the binning vector. Default value: None, meaning that the 
                          lower edge will be the minimum of the data
    - "ifstep"          bool, if "nbins" is a binning step (True) or a number of bins (False). Default value: False
    - "bins"            1D-array, the extremes of the bins to be used in the histogram production.
                          Defualt value: None, meaning that the binning vector is manually constructed
                          instead of user-defined

Outputs:
    - "histo"           1D-array, the histogram of the given data (= number of entries per bin)
    - "bins_true"       1D-array, the center of the bins used to produce the histogram

'''

def truehisto1D(data,nbins = 100,edgeL = None, edgeU = None,ifstep = False,bins = None):            
    # Check whether the binning vector is empty. If not, pass and use it. If yes, define it.
    if np.shape(bins) == ():
        # Determine the binning edges: if they are NaN, define them. Otherwise, use the already available.
        if edgeL is None:
            edgeL = np.nanmin(data)
        if edgeU is None:
            edgeU = np.nanmax(data)
        
        # Check how to eventually build the binning vector: by linear spacing or ranging in a fixed interval.
        if ifstep:
            # Use a fixed step
            bins = np.arange(edgeL-nbins/2,edgeU+nbins,nbins)            
        else:
            # Use a fixed number of bins
            bins = np.linspace(edgeL,edgeU,nbins)
        
    # Build the histogram and center the bins
    histo,_ = np.histogram(data,bins)
    bins_true = truebins(bins)
    return(histo,bins_true)

'''
2D-Histogram production

This function returns the 2-Dimensional histogram of a given dataset, with user-defined binning.

Inputs:
    - "dataX"           1D-array, the data to be histogrammed on the first dimension.
    - "dataY"           1D-array, the data to be histogrammed on the second dimension.

    - "nbinsX"          float, either the number of bins to produce (for the first axis) or the binning step, 
                          depending on the value of "ifstepX"
    - "nbinsY"          float, either the number of bins to produce (for the second axis) or the binning step, 
                          depending on the value of "ifstepY"
    - "edgeLX"          float, the lower edge of the binning vector (for the first axis). 
                          Default value: None, meaning that the lower edge will be the minimum of the data
    - "edgeUX"          float, the upper edge of the binning vector (for the first axis).
                          Default value: None, meaning that the lower edge will be the minimum of the data
    - "edgeLY"          float, the lower edge of the binning vector (for the second axis). 
                          Default value: None, meaning that the lower edge will be the minimum of the data
    - "edgeUY"          float, the upper edge of the binning vector (for the second axis).
                          Default value: None, meaning that the lower edge will be the minimum of the data
    - "weights"         1D-array, the weights to be applied to each entry added to the histogram. 
                          Default value: None, meaning that no weight is considered (= all entries count equally)

Outputs:
    - "histo"           2D-array, the histogram of the given data. Note that by default the histogram is 
                          transposed with respect to an x-y reference frame. This means that, in order to 
                          show it, a np.transpose() command should be applied first.
    - "bins_trueX"      1D-array, the extremes of the bins (along the first axis)
    - "bins_trueY"      1D-array, the extremes of the bins (along the second axis)
'''

def truehisto2D(dataX,dataY,nbinsX = 100,nbinsY = 100,edgeLX = None,edgeUX = None,edgeLY = None,edgeUY = None,
                                    ifstepX = False, ifstepY = False,binsX = None,binsY = None,weights = None):
    # Check whether the binning vectors are defined or not. If yes, go on with the histogram. 
    # If not, check whether the extremes are defined or not. If yes, pass. If not, define them manually.
    if binsX is None:
        if edgeLX is None:
            edgeLX = np.nanmin(dataX)
        if edgeUX is None:
            edgeUX = np.nanmax(dataX)
            
        # Check how to eventually build the binning vector: by linear spacing or ranging in a fixed interval.
        if ifstepX:
            # Use a fixed step
            binsX = np.arange(edgeLX-nbinsX/2,edgeUX+nbinsX*1.5,nbinsX)
        else:
            # Use a fixed number of bins
            binsX = np.linspace(edgeLX,edgeUX,nbinsX)
    
    # Repeat for y axis    
    if binsY is None:
        if edgeLY is None:
            edgeLY = np.nanmin(dataY)
        if edgeUY is None:
            edgeUY = np.nanmax(dataY)
            
        # Check how to eventually build the binning vector: by linear spacing or ranging in a fixed interval.
        if ifstepY:
            # Use a fixed step
            binsY = np.arange(edgeLY-nbinsY/2,edgeUY+nbinsY*1.5,nbinsY)            
        else:
            # Use a fixed number of bins
            binsY = np.linspace(edgeLY,edgeUY,nbinsY)

    # Build the histogram
    histo2D,_,_=np.histogram2d(dataX,dataY,bins=[binsX,binsY],weights=weights)

    return(histo2D,binsX,binsY)

''' 
1D Histogram plotting

This functions applied a standardized scheme for plotting a 1D histogram: step-type plotting in blue, 
labels and large ticks on both axes, legend turned on, grid turned on, linear or log scale.

Inputs: 
    - "ax"          Axes-type object, the set of axis on which the histogram will be shown
    - "bins"        1D-array of shape (N,), the binning vector of the histogram (bin centers)
    - "histo"       1D-array of shape (N,), the histogram values (entries)
    - "xlabel"      string, label for the X axis. Default value: "X axis"
    - "ylabel"      string, label for the Y axis. Default value: "Y axis"
    - "leglabel"    string, label for the histogram in the axis legend, or None, if no legend should be shown
                      and no label should be inserted for the plot. Default value: "Data"
    - "legloc"      string, location of the legend. Default value: "best"
    - "textfont"    float, scaling for the axes, the ticks and the legend. Default value: 15
    - "hcolor"      string, color for the plot. Default value: "mediumblue"
    - "hstyle"      string, the linestyle of the plot (assuming drawingstyle "steps-mid").
                      Default value: "-"

There are no outputs (the function just renders the requested plot)
    
'''
def histoplotter1D(ax,bins,histo,xlabel = 'X axis',ylabel = 'Y axis',leglabel = 'Data',legloc = 'best',
                                                   textfont = 15, hcolor = 'mediumblue', hstyle = '-'):
    # Plot the histogram and build the legend.
    if legloc is None:
        ax.step(bins, histo, color = hcolor, where = 'mid', linestyle = hstyle)
    else:
        ax.step(bins, histo, color = hcolor, where = 'mid', linestyle = hstyle, label = leglabel)
        ax.legend(loc = legloc, fontsize = textfont, framealpha = 1)        

    # Set labels and other properties. 
    ax.set_xlabel(xlabel,fontsize = textfont)
    ax.set_ylabel(ylabel,fontsize = textfont)
    ax.grid(linewidth=0.5)
    ax.tick_params(labelsize = textfont)
    
    # If there exists only one bin, set a particular limit for the x-axis. Otherwise, use the bins edges.
    # The y-axis remains user-undefined.
    if np.min(bins) == np.max(bins):
        ax.set_xlim([np.min(bins)-5,np.max(bins)+5])
    else:
        ax.set_xlim([np.min(bins),np.max(bins)])
    return()

''' 
2D Histogram plotting

This functions applied a standardized scheme for plotting a 2D histogram: arbitrary colormap, label and large 
ticks on both axes, no legend, grid turned on, linear or log scale on z.

Inputs:
    - "ax"          Axes-type object, the set of axis on which the histogram will be shown
    - "binsX"       1D-array of shape (N,), the binning vector of the histogram (bin centers) for the first dimension
    - "binsX"       1D-array of shape (N,), the binning vector of the histogram (bin centers) for the second dimension
    - "histo2D"     2D-array, containing the histogram values (entries)
    - "colmap"      string, the name of the colormap to be used. Default: "jet".
    - "slope"       float, the point of change for the TwoSlopeNorm option. Default value: None (i.e., it isn't used)
    - "xlabel"      string, label for the X axis. Default value: "X axis"
    - "ylabel"      string, label for the Y axis. Default value: "Y axis"
    - "textfont"    float, scaling for the axes, the ticks and the legend. Default value: 15
    - "iflog"       bool, whether to use a logarithmic scale for the z (color) axis. Default value: False.
    - "vmin"        float, the minimum value for the z (color) axis. Default value: None, meaning that the
                      minimum of the histogram is used.
    - "vmax"        float, the maximum value for the z (color) axis. Default value: None, meaning that the
                      maximum of the histogram is used.
    - "ifcbarfont"  bool, whether to scale with "textfont" also the colorbar axis. Default value: False.
    - "cbarlabel"   str, the label for the colorbar. Default value: None.
    
There are no outputs (the function just renders the requested plot)

'''

def histoplotter2D(ax,binsX,binsY,histo2D,colmap = 'jet',slope = None,xlabel = 'X axis',ylabel = 'Y axis',
                                  textfont = 15,iflog = False,vmin = None,vmax = None,ifcbarfont = False,
                                  cbarlabel = None):
    # Deduce what is the figure from which "ax" is drawn
    f = ax.get_figure()
    
    # Define the colormap and set the bad pixels to the same color of the minimum of the scale.
    my_cmap = eval('copy.copy(pltmaps.' + colmap + ')')
    my_cmap.set_bad(my_cmap(0))

    # Plot the 2D histogram transposed. Note that this is due to how plt.imshow() works, and it assumes
    # that the histogram has been produced with np.histogram2d() or equally with truehisto2D().
    # Choose the normalization as defined by the user.
    if iflog:
        # Assume a log scale and normalize as a consequence.
        thisnorm = colors.LogNorm(vmin = vmin, vmax = vmax)
    else:
        # Assume a linear scale, which can be either of the TwoSlope type or not
        if slope is None:
            thisnorm = colors.Normalize(vmin = vmin, vmax = vmax)
        else:
            thisnorm = colors.TwoSlopeNorm(slope, vmin = vmin, vmax = vmax)

    imhisto = ax.imshow(histo2D.transpose(),cmap = my_cmap,norm=thisnorm,aspect = 'auto',
                                        extent = [binsX[0],binsX[-1],binsY[-1],binsY[0]])

    # Set labels, grid, colorbar and axis propeties.
    ax.set_xlabel(xlabel,fontsize = textfont)
    ax.set_ylabel(ylabel,fontsize = textfont)
    ax.grid(linewidth=0.5)
    cb = f.colorbar(imhisto,ax=ax)
    if ifcbarfont:
        # Set the fontsize for the colorbar
        for el in cb.ax.get_yticklabels():
            el.set_fontsize(textfont)

    if cbarlabel is not None:
        cb.set_label(cbarlabel,fontsize=textfont,rotation=270,labelpad=50)
    ax.tick_params(labelsize = textfont)

    # If there exists only one bin in either direction, set a particular limit for the axis. 
    # Otherwise, use the bins edges.
    if np.min(binsX) == np.max(binsX):
        ax.set_xlim([np.min(binsX)-5,np.max(binsX)+5])
    else:
        ax.set_xlim([np.min(binsX),np.max(binsX)])
        
    if np.min(binsY) == np.max(binsY):
        ax.set_ylim([np.min(binsY)-5,np.max(binsY)+5])
    else:
        ax.set_ylim([np.min(binsY),np.max(binsY)])
    return()

'''
Weighted Mean

This function returns the weighted mean (and its error) of the input data, with either the inverse 
square of the uncertainities provided or the values provided. 

Inputs:
    - "x"           1D-array, the data used for the weighting
    - "w"           1D-array, the weights. 
    - "ifweights"   bool, if w contains weights or sigmas (and the weights are then 1/w^2)
                      Default value: True, meaning that "w" contains weights.  
    - "errtype"     string, the way to compute the uncertainity on the mean. 
                      Accepted values: "bootstrap", "Taylor" and anything else.
                      Default value: "bootstrap"  
    
Outputs:
    - "xmean"       float, the weighted mean of the input data
    - "xerr"        float, the error associated to "xmean"
'''

def wmean(x,w,ifweights=True,errtype='bootstrap'):
    # Verify if the inputs are all ndarrays
    x,w = np.array(x), np.array(w)
    
    # If needed, evaluate the weights associated to the input data
    if ifweights == False:
        err = w
        err[err == 0] = 1e-10
        w = err**(-2)
        
    # Evaluate the weighted mean of array x, with weights given in w
    xmean = (x*w).sum() / w.sum()
    
    # Evaluate the uncertainity associated to x: if the weights used are errors, follow Taylor's formula.
    # Otherwise, you could associate to each w a Poisson error and propagate over those.
    if ifweights:
        if errtype == 'bootstrap':
            # Use the formula taken from "The standard error of a weighted mean concentration - Bootstrapping vs
            # other methods" (D. F. Gatz and L. Smith, 1977)
            n = np.shape(x)[0]
            wmean = np.mean(w)
            wsum = np.sum(w)

            xerr_1 = n/((n-1)*wsum**2)
            xerr_2 = np.sum((x*w-wmean*xmean)**2) - 2*xmean*np.sum((w-wmean)*(w*x-wmean*xmean)) + xmean**2*np.sum((w-wmean)**2)
            xerr = np.sqrt(xerr_1*xerr_2)
        elif errtype == 'Taylor':
            # Use the formula from Taylor book on statistics
            xerr = 1/np.sqrt(np.sum(w))
        else:
            # Use the formula taken from http://seismo.berkeley.edu/~kirchner/Toolkits/Toolkit_12.pdf [case 1]
            xerr_1 = ( np.sum(w*x**2) ) / (np.sum(w)) - xmean**2
            xerr_2 = ( np.sum(w**2) )/( (np.sum(w))**2 - (np.sum(w**2)) )
            xerr = np.sqrt(xerr_1*xerr_2)
    else:
        # Use again the formula from Taylor
        xerr = (w.sum())**(-1/2)    
    return(xmean, xerr)

''' 
FWHM calculator

This function takes as input an histogram and computed its numerical FWHM (by definition).

Inputs:
    - "bins"        1D-array, the binning vector of the histogram
    - "histo"       1D-array, the histogram values (entries)

Outputs:
    - "FWHM"        float, the Full-Width at Half-Maximum of the histogram
    - "FWHMerr"     float, the uncertainity on the FWHM, computed with a simple approximation
    
'''
def fwhm_exp(bins,histo):
    # Calcolo i punti sopra threshold
    overthr = bins[(histo >= np.max(histo)/2)]
    x1 = overthr[0]
    x2 = overthr[-1]
    FWHM = x2-x1
    FWHMerr = (overthr[1]-overthr[0])/np.sqrt(2)
    return(FWHM,FWHMerr)  

''' 
Projection of a 2D histogram

This function takes as input the 2D histogram built with the "truehisto2D" function (or in any other way, 
as long as the binning vectors are explicitly given and are long 1 element more than each side of the 
histogram 2d-array AND the histogram is "flipped" with respect to how it would appear, in a plt.imshow figure). 
Then, it builds a "projection" of the histogram on either axis: as projection, it is taken the weighted 
mean value of the binning axis, with the weights being the histogram heights and the mean taken over rows or 
columns.

Legend for the methods (supposing the projection is by columns):
- mean: evaluate the weighted mean of each single column. The weights are the histogram heights, while 
the values used for the average are "dimensional" (the binning).
- sum: as above, but the weighted means are NOT normalized. Thus, they are simply weighted sums.
- max: for each column, the bin where the maximum number of events were found, is selected.
- argmax: the maximum bin with a non null deposit is selected for each column


Inputs:
    - "histo2D"         2D-array, the histogram values (entries)
    - "binsx"           1D-array, the binning vector along the first dimension
    - "binsy"           1D-array, the binning vector along the first dimension
    - "axis"            string, the axis of projection. "x" means that for each fixed value of x, 
                          the weighted mean of y will be taken. "y" is the reverse.
    - "selferr"         float, the uncertainity to be associated with the binning vector of projection
                          in "sum" mode
    - "errtype"         string, the way to compute the error of the weighted mean. For details on the
                          possible ways, check the "wmean" function. Default value: "Taylor".
    
Output:
    - "truex/truey"     1D-array, the binning vector of the projection
    - "histosum"        1D-array, the projection values
    - "histosum_err"    1D-array, the errors associated to the projection values
'''

def histoproj(histo2D,binsx,binsy,axis='x',projtype='mean',selferr = 0.01,errtype='Taylor'):    
    # First of all, check if the size of the binning vectors are greater than the ones of the histogram.
    # If not, use the truebins function to adjust them.
    if np.shape(histo2D)[0] != np.shape(binsx):
        truex = truebins(binsx)
    else:
        truex = binsx
    
    if np.shape(histo2D)[1] != np.shape(binsy):
        truey = truebins(binsy)
    else:
        truey = binsy
        
    # Now disable a pathetic warning from numpy that will inevitably activate in case of /0 divisions
    np.seterr(divide='ignore', invalid='ignore')
    
    # Now define an empty vector for the histogram and fill it iteratively with the weighted means.
    # Notice the different path taken depending on the direction chosen for the projection.
    if axis == 'x':
        histosum = np.zeros((np.shape(truex)[0],))
        histosum_err = np.zeros_like(histosum)
        if projtype == 'mean':
            # I evaluate the weighted mean of the histogram. The weights are the histo values.
            for i in range(histo2D.shape[0]):
                histosum[i], histosum_err[i] = wmean(truey, histo2D[i,:], ifweights = True, errtype=errtype)
            histosum[np.isnan(histosum)] = 0
            histosum_err[np.isnan(histosum_err)] = 0
        elif projtype == 'sum':
            # I sum the histogram. The weights are the histo values.
            for i in range(histo2D.shape[0]):
                histosum[i] = np.sum(truey * histo2D[i,:])
                histosum_err[i] = np.sqrt(np.sum(histo2D[i,:]**2))
        elif projtype == 'argmax':
            # I take the location where the maximum number of entries are found.
            for i in range(histo2D.shape[0]):
                maxind = np.argmax(histo2D[i,:])
                histosum[i] = truey[maxind]
                histosum_err[i] = truey[1]-truey[0]
        elif projtype == 'max':
            # I take the maximum registered value along the axis
            for i in range(histo2D.shape[0]):
                histoinds = (histo2D[i,:] > 0)
                if np.shape(truey[histoinds])[0] == 0:
                    histosum[i] = np.nan
                    histosum_err[i] = np.nan
                else:
                    histosum[i] = np.max(truey[histoinds])
                    histosum_err[i] = truey[1]-truey[0]
            
        return(truex, histosum, histosum_err)
            
    elif axis == 'y':
        histosum = np.zeros((np.shape(truey)[0],))
        histosum_err = np.zeros_like(histosum)
        if projtype == 'mean':
            # I evaluate the weighted mean of the histogram. The weights are the histo values.
            for i in range(histo2D.shape[1]):
                histosum[i], histosum_err[i] = wmean(truex, histo2D[:,i], ifweights = True, errtype=errtype)
            histosum[np.isnan(histosum)] = 0
            histosum_err[np.isnan(histosum_err)] = 0
        elif projtype == 'sum':
            # I sum the histogram. The weights are the histo values.
            for i in range(histo2D.shape[1]):
                histosum[i] = np.sum(truex * histo2D[:,i])
                histosum_err[i] = np.sqrt(np.sum(histo2D[:,i]**2))
        elif projtype == 'argmax':
            # I take the location where the maximum number of entries are found.
            for i in range(histo2D.shape[1]):
                maxind = np.argmax(histo2D[:,i])
                histosum[i] = truex[maxind]
                histosum_err[i] = truex[1]-truex[0]
        elif projtype == 'max':
            # I take the maximum registered value along the axis
            for i in range(histo2D.shape[1]):
                if np.shape(truex[histoinds])[0] == 0:
                    histosum[i] = np.nan
                    histosum_err[i] = np.nan
                else:
                    histosum[i] = np.max(truex[histoinds])
                    histosum_err[i] = truex[1]-truex[0]
        return(truey, histosum, histosum_err)

''' 
Projection of a 2D histogram (from the data)

This function takes as input two variables (X,Y) and a binning vector.
It performs the calculation of the average value of one of the variables ("projected variable"), when the
corresponding values of the other variable fall within a given interval ("fixed variable"). The considered
intervals are the ones given in the binning vector. 

In other words, this function works similarly to "histoproj", but by diretly analyzing the data instead of 
passing through the histogram. This does not change anything in terms of average values, but it allows to 
compute a more precise error on the projection.

Inputs:
    - "dataX"           1D-array, the data represented on the first dimension
    - "dataY"           1D-array, the data represented on the second dimension
    - "bins"            1D-array, the binning vector used for the projection. It can also be an int number
                          (= number of bins uniformly distributed between the min and max of the data) or 
                          "None" (= 100 bins uniformly distributed). Default value: "None"
    - "binaxis"         string, the axis to consider for the binning, either 'x' or 'y' (e.g., with "x" the 
                          binning is considered for the dataX vector and so the average of dataY is computed)
                          Default value: "x"
    - "iferrmean"       bool, if the returned average is the std of the mean of the data or simply the std
                          Default value: "True"

Output:
    - "truebinvector"   1D-array, the vector of the center of the bins of the data fixed in the projection
    - "histosum"        1D-array, the projection of the other variable (average value)
    - "histoerr"        1D-array, the error on the other variable (std of the mean)
'''

def histoproj_fromdata(dataX,dataY,bins=None,binaxis='x',iferrmean=True):
    # Input check: projection axis
    if (binaxis != 'x') & (binaxis != 'y'):
        print(f"binaxis parameter set to an unknown value (only accepted: 'x', 'y')")
        print(f"Reverting to a projection along the x axis...")
        binaxis = 'x'

    # Input check: binning vector (suppose that it is "None", an integer or otherwise a
    # numpy array
    if bins is None:
        # Undefined vector: build one from scratch
        NumOfBins = 100
        if binaxis == 'x':
            bins = np.linspace(np.min(dataX),np.max(dataX),NumOfBins)
        else:
            bins = np.linspace(np.min(dataY),np.max(dataY),NumOfBins)

    elif isinstance(bins,int):
        # The number of bins is defined.
        if binaxis == 'x':
            bins = np.linspace(np.min(dataX),np.max(dataX),bins)
        else:
            bins = np.linspace(np.min(dataY),np.max(dataY),bins)
    
    # Prepare the projection
    if binaxis == 'x':
        data_fixed = dataX
        data_averaged = dataY
    else:
        data_fixed = dataY
        data_averaged = dataX

    # Set the default value for averages and errors of the projected variable,
    # if there are no corresponding values for the fixed variable
    nodata_meanvalue = 0          # otherwise ok: NaN, None, ...
    nodata_errvalue = 0           # otherwise ok: NaN, None, ...
    
    # Perform the projection
    truebinvector = truebins(bins)
    histosum = np.zeros_like(truebinvector)
    histoerr = np.zeros_like(truebinvector)
    
    for i,el in enumerate(truebinvector):
        # Examining the i-th bin of the fixed data
        cond = (data_fixed >= bins[i]) & (data_fixed < bins[i+1])
        data_averaged_bin = data_averaged[cond]
        numdata_averaged_bin = np.shape(data_averaged_bin)[0]
        
        # Compute average and std of the data
        if numdata_averaged_bin == 0:
            histosum[i] = nodata_meanvalue
            histoerr[i] = nodata_errvalue
        else:
            histosum[i] = np.mean(data_averaged_bin)
            histoerr[i] = np.std(data_averaged_bin)
            if iferrmean:
                histoerr[i] /= np.sqrt(numdata_averaged_bin)
            
    # Return
    return(truebinvector,histosum,histoerr)






























# ===========================================================================================================
#                                      FIT FUNCTIONS / MODELS
# ===========================================================================================================
# All of these functions are callables that evaluate an input 1D-array vector (usually x) and return
# as output (y) the result of the application of a certain function. These functions are useful for 
# fitting purposes.

# Linear function (y = mx + q)
def linfun(x,m,q):
    return(m*x+q)

# Quadratic function (y = ax^2 + bx + c)
def quadfun(x,a,b,c):
    return(a*x**2+b*x+c)

# Exponential function with no tail (y = a*exp(-bx))
def expfun_notail(x,a,b):
    return(a*np.exp(-b*x))

# Exponential function with a tail (y = a*exp(-bx) + c)
def expfun_dec(x,a,b,c):
    return(a*np.exp(-b*x)+c)

# Gaussian function with a tail (y = a*exp(-(x-b)^2 / 2c^2) +d)
def gauss1fun(x,a,b,c,d):
    return(a*np.exp(-(x-b)**2/(2*c**2))+d)

# Energy resolution of a calorimeter (y = a/sqrt(x) \oplus b/x \oplus c)
def resfun(E,a,b,c):
    return( np.sqrt( (a/np.sqrt(E))**2 + (b/E)**2 + c**2 ) )

# Energy resolution of a calorimeter with only stocastic and constant term (y = a/sqrt(x) \oplus c)
def resfun_nonoise(E,a,c):
    return( np.sqrt( (a/np.sqrt(E))**2 + c**2) )

# Log function with a constant term (good for TOT measurement) (y = a + b*log(x+c))
def totfun(x,a,b,c):
    return(a + b*np.log(x+c))

# Landau function (parametrized)
def landaufun(x,ampli,fwhm,Empv):
    xi = fwhm/4.02
    lambdapar = (x-Empv)/xi
    F = ampli*np.exp(-0.5*((lambdapar)+np.exp(-lambdapar)))
    return(F)

# e.m. shower longitudinal profile function (parametrized)
def showerfun(t,a,b,E0):
    return(E0 * b * ( (b*t)**(a-1)*np.exp(-b*t) ) / (sp.gamma(a)) )

''' 
An alternative to the Crystall Ball fit function: expgaussexp

This function reproduces the energy deposited in a medium in a lossy process and is essentially
composed by a gaussian core with two exponential tails. More details in https://arxiv.org/abs/1603.08591

Parameters:
    - mu, sigma         MPV and std of the gaussian core
    - N, kL, kH         scaling factors for the exponential tails

Suitable starting points in a fit of a histogram:
    - mu, sigma         MPV and FWHM/2 of the histogram peak (or mean and std of the data)
    - N                 np.max() of the histogram
    - kL, kH            0.5
'''

def expgaussexp(x,mu,sigma,kL,kH,N):
    # For the points with (x-mu)/\sigma <= -kL, use a gaussian*exponential function
    cond1 = (x <= -kL*sigma + mu)
    x1 = x[cond1]
    y1 = N*np.exp((kL**2)/2 + kL*((x1-mu)/sigma))

    # For the points with (x-mu)/\sigma > kH, use a gaussian*exponential function (different from the first)
    cond2 = (x > kH*sigma + mu)
    x2 = x[cond2]
    y2 = N*np.exp((kH**2)/2 - kH*((x2-mu)/sigma))
    
    # For the remaining points, use a gaussian function
    anticond = (cond1 == False) & (cond2 == False)
    x3 = x[anticond]
    y3 = N * np.exp(- (x3-mu)**2/(2*sigma**2) )
    
    # Merge the arrays
    y = np.zeros_like(x)
    y[cond1] = y1
    y[cond2] = y2
    y[anticond] = y3
    return(y)






























# ===========================================================================================================
#                                  AUTOMATED ROUTINES FOR FITTING
# ===========================================================================================================
# All of these functions are used to perform fits in an automated fashion.

''' 
Linear function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a linear fit,
with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    
Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve
'''

def fitter_linear(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit.
    start_m = (yfit[-1]-yfit[0])/(xfit[-1]-xfit[0])
    start_q = yfit[-1]-start_m*xfit[-1]
    
    # Perform fit and extract parameters. Use weights if necessary
    gmodel = Model(linfun)
    if yerr is not None:
        # Define the weights               
        pesi = 1/yerr[cond]
        weightcond = (yfit > 0)
        xfit = xfit[weightcond]
        yfit = yfit[weightcond]
        pesi = pesi[weightcond]
        result = gmodel.fit(yfit, x=xfit, m=start_m, q=start_q, weights = pesi, scale_covar = False)
    else:
        result = gmodel.fit(yfit, x=xfit, m=start_m, q=start_q)
    
    m,q = result.params['m'],result.params['q']

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = linfun(xth,m,q)
    
    # Return fit results
    return(result,xth,yth)

'''
Quadratic function parameters calculator

This function takes as input a set of three 2-D points (x,y) and returns the parameters
of the y=ax^2+bx+c curve which passes through them.

Inputs:
    - "x1"              float, x of the first point (good choice: first point of the set)
    - "y1"              float, y of the first point (good choice: first point of the set)
    - "x2"              float, x of the second point (good choice: midway point of the set)
    - "y2"              float, y of the second point (good choice: midway point of the set)
    - "x3"              float, x of the third point (good choice: last point of the set)
    - "y3"              float, y of the third point (good choice: last point of the set)

Outputs:
    - "[a,b,c]"         list of float, the parameters of the function
'''

def quadfun_startpts(x1,y1,x2,y2,x3,y3):
    a = ( y3 - y1 ) / ( x3**2 - x1**2 - (x2**2-x1**2)*(x3-x1)/(x2-x1) )
    b = ( y2 - y1 - a*(x2**2 - x1**2) ) / ( x2 - x1 )
    c = y1 - a*x1**2 - b*x1
    return([a,b,c])

'''
Quadratic function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a quadratic fit,
with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    - "start_pars"      list, the starting points for the fit parameters. By convention, higher power
                          goes first (i.e., the list is [a,b,c] where y = ax^2 + bx + c).
                          Default value: "None", meaning that the optimal starting points are computed
                          using a default procedure (interpolation of the first, middle and last
                          point of the data)

Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve
'''

def fitter_quad(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,start_pars=None):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit.
    if start_pars is None:
        # Automatically computed
        mid = int(np.shape(xfit)[0]/2)
        start_a, start_b, start_c = quadfun_startpts(xfit[0],yfit[0],xfit[mid],yfit[mid],xfit[-1],yfit[-1])
    else:
        # User-defined
        start_a, start_b, start_c = start_pars
    
    # Perform fit and extract parameters. Use weights if necessary
    gmodel = Model(quadfun)
    if yerr is not None:
        # Define the weights               
        pesi = 1/yerr[cond]
        weightcond = (yfit > 0)
        xfit = xfit[weightcond]
        yfit = yfit[weightcond]
        pesi = pesi[weightcond]
        result = gmodel.fit(yfit, x=xfit, a = start_a, b = start_b, c = start_c, 
                                            scale_covar = False, weights = pesi)
        
    else:
        result = gmodel.fit(yfit, x=xfit, a = start_a, b = start_b, c = start_c)
    
    a,b,c = result.params['a'],result.params['b'],result.params['c']

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = quadfun(xth,a,b,c)
    
    # Return fit results
    return(result,xth,yth)


'''
Exponential function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs an exponential fit,
with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    - "start_pts"       list, the starting points for the fit parameters. By convention, higher power
                          goes first (i.e., the list is [a,b,c] where y = ax^2 + bx + c)
    - "iftail"          bool, whether the exponential has an offset (i.e., the function is of the form
                        y = a*e^(-bx)+c) or not (i.e., y = a*e^(-bx)). Default value: False
    - "ifset_lowlims"   bool, whether to set lower limits for the parameters or not. Default value: True

Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve    
'''

def fitter_exp(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,start_pts = None,iftail = False,
               ifset_lowlims = True):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit. 
    if start_pts is None:
        start_a = np.max(yfit)
        start_b = 0.001          # Arbitrary!
        start_c = yfit[-1]
    else:
        start_a, start_b, start_c = start_pts

    # Decide which model to use for the fit
    if iftail:
        # Model: y = a*e^{-bx}+c
        gmodel = Model(expfun_dec)
    else:
        # Model: y = a*e^{-bx}
        gmodel = Model(expfun_notail)
    
    # Set fit boundaries
    if ifset_lowlims:
        gmodel.set_param_hint('a', min = 0)
        gmodel.set_param_hint('b', min = 0)
    
    # Perform fit and extract parameters. Use weights if necessary
    if yerr is not None:
        if type(yerr) == str:
            if yerr == 'P':
                yerr = np.sqrt(yvec)
            
        pesi = 1/yerr[cond]
        pesi[np.isinf(pesi)] = np.min(pesi)
        if iftail:
            result = gmodel.fit(yfit, x=xfit, a = start_a, b = start_b, c = start_c, 
                                                scale_covar = False, weights = pesi)
        else:
            result = gmodel.fit(yfit, x=xfit, a = start_a, b = start_b,
                                                scale_covar = False, weights = pesi)
        
    else:
        if iftail:
            result = gmodel.fit(yfit, x=xfit, a = start_a, b = start_b, c = start_c)
        else:
            result = gmodel.fit(yfit, x=xfit, a = start_a, b = start_b)

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    if iftail:
        yth = expfun_dec(xth,result.params['a'],result.params['b'],result.params['c'])
    else:
        yth = expfun_notail(xth,result.params['a'],result.params['b'])
    
    # Return fit results
    return(result,xth,yth)

''' 
Gaussian function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a 1D-gaussian fit,
with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Note that if "yerr" is set
                          to "P", a Poissonian error is used (i.e., yerr = sqrt(y). Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    - "low_amp_coeff"   float, the minimum value for the amplitude parameter, expressed as a fractione
                          of the maximum of the y vector. Default value: 0.95
    - "up_amp_coeff"    float, the maximum value for the amplitude parameter, expressed as a fraction
                          of the maximum of the y vector. Default value: 1.05
    - "low_offset"      float, the minimum value for the offset parameter. Default value: 0
    - "up_offset"       float, the maximum value for the offset parameter. Default value: "None", meaning
                          that no boundary is actually applied.
    
Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve
'''


def fitter_gauss1fun(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,low_amp_coeff = 0.95,
                                                    up_amp_coeff = 1.05,low_offset=0,up_offset=None):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit. Since this is a Gaussian fit, we need at least a 
    # starting point for the amplitude (a), the mean (b), the standard deviation (c) and a constant
    # offset (d).
    start_ampli = np.max(yfit)              # Amplitude: maximum value of the data
    start_mean = xfit[np.argmax(yfit)]      # MPV: point of maximum value of the data
    overthr = xfit[yfit >= np.max(yfit)/2]
    FWHM = overthr[-1]-overthr[0]
    start_std = FWHM/2.355                  # Gaussian core sigma: approx. the FWHM of the data
    start_offset = (yfit[0]+yfit[-1])/2     # Offset: mean value of the yfit edges
    
    # Define the boundaries for the fit parameters.
    conf = 5    # Confidence level cut for the definition of the boundaries on MPV
    low_ampli = start_ampli*low_amp_coeff   # Amplitude: around the maximum
    up_ampli = start_ampli*up_amp_coeff
    low_mean = start_mean - conf*start_std  # Mean: around the experimental MPV
    up_mean = start_mean + conf*start_std
    low_std = 0                             # Gaussian core sigma: non negative
    
    # Check that no lower boundary is equal to the upper
    if low_mean == up_mean:
        low_mean = low_mean*low_amp_coeff
        up_mean = up_mean*up_amp_coeff
        print(f"Lower and upper bounds for b were found to be equal.")
    
    # Set fit boundaries
    gmodel = Model(gauss1fun)
    gmodel.set_param_hint('a', min = low_ampli, max = up_ampli)
    gmodel.set_param_hint('b', min = low_mean, max = up_mean)
    gmodel.set_param_hint('c', min = low_std)
    if up_offset != None:
        gmodel.set_param_hint('d', min = low_offset, max = up_offset)
    
    # Perform fit and extract parameters. Use weights if necessary
    if yerr is not None:
        if yerr == 'P':
            yerr = np.sqrt(yvec)
            
        pesi = 1/yerr[cond]
        weightcond = (yfit > 0)
        xfit = xfit[weightcond]
        yfit = yfit[weightcond]
        pesi = pesi[weightcond]
        result = gmodel.fit(yfit, x=xfit, a=start_ampli, b=start_mean, c=start_std, d=start_offset,
                                                                scale_covar = False, weights = pesi)
        
    else:
        result = gmodel.fit(yfit, x=xfit, a=start_ampli, b=start_mean, c=start_std, d=start_offset)
    
    a,b,c,d = result.params['a'],result.params['b'],result.params['c'],result.params['d']

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = gauss1fun(xth,a,b,c,d)
    
    # Return fit results
    return(result,xth,yth)

'''
Resolution function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a resolution-like fit,
such as y = a/sqrt(x) \oplus b/x \oplus c, with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used
    - "ifzero"          bool, whether the linear term of the fit is set to zero (b=0) or not.
                          Default value: True
    - "p0"              list, starting point for the parameters (a,b,c) in the fit. Defualt value:
                          [0.1,0.1,0.015], which is a reasonable combination when fitting a resolution.
    - "ifnorm"          bool, whether the data are normalized (i.e. y<1) or not. Default value: True
    - "bounds"          list of lists, where the global list has a length of 2 and contains first the
                          list of all the lower boundaries for the parameters, and then the upper
                          boundaries. Default value: [[-1,-1,-1],[1,1,1]]

Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve 
'''


def fitter_resolution(xvec,yvec,yerr=None,fitcut_low=None,fitcut_up=None,ifzero=True,
                                    p0=[0.1,0.1,0.015],ifnorm=True,bounds=[[-1,-1,-1],[1,1,1]]):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
       
    # Create fit models and compute the starting points for the fit
    start_a = p0[0]
    start_c = p0[-1]
    if ifzero:
        # y = a/sqrt(x) \oplus b/x \oplus c 
        gmodel = Model(resfun_nonoise)
    else:
        # y = a/sqrt(x) \oplus b/x \oplus c
        gmodel = Model(resfun)
        start_b = p0[1]
            
    # Set fit boundaries
    gmodel.set_param_hint('a', min = bounds[0][0], max = bounds[1][0])
    gmodel.set_param_hint('c', min = bounds[0][-1], max = bounds[1][-1])
    if not ifzero:
        gmodel.set_param_hint('b', min = bounds[0][1], max = bounds[1][1])
    
    # Compute weights
    if yerr is not None:          
        pesi = 1/yerr[cond]
        pesi[np.isinf(pesi)] = np.min(pesi)

    # Perform fit and extract parameters. Use weights if necessary
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    if ifzero:
        if yerr is not None:
            result = gmodel.fit(yfit, E=xfit, a=start_a, c=start_c, scale_covar = False, weights = pesi)
        else:
            result = gmodel.fit(yfit, E=xfit, a=start_a, c=start_c)

        a,c = result.params['a'],result.params['c']
        
        # Build thoretical x and y vector
        yth = resfun_nonoise(xth,a,c)
    else:
        if yerr is not None:          
            result = gmodel.fit(yfit, E=xfit, a=start_a, b=start_b, c=start_c, scale_covar = False)
        else:
            result = gmodel.fit(yfit, E=xfit, a=start_a, b=start_b, c=start_c)

        a,b,c = result.params['a'],result.params['b'],result.params['c']
        
        # Build thoretical x and y vector
        yth = resfun(xth,a,b,c)
        
    # Return fit results
    return(result,xth,yth)

'''
TOT-log function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a fit with a 
TOT-like function (i.e., a log function with offset), with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used
    - "start_values"    list, starting points for the parameters. Default value: [1,1,1]

Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve   
'''

def fitter_totfun(xvec,yvec,yerr = None,fitcut_low=None,fitcut_up=None,start_values = [1,1,1]):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]

    # Define the starting points for the fit.
    start_a, start_b, start_c = start_values
    
    # Set fit boundaries
    gmodel = Model(totfun)
    
    # Perform fit and extract parameters. Use weights if necessary
    if yerr is not None:           
        pesi = 1/yerr[cond]
        pesi[np.isinf(pesi)] = np.min(pesi)
        result = gmodel.fit(yfit, x=xfit, a=start_a, b=start_b, c=start_c, 
                            scale_covar = False, weights = pesi, max_nfev=5000)
        
    else:
        result = gmodel.fit(yfit, x=xfit, a=start_a, b=start_b, c=start_c)

    a,b,c= result.params['a'],result.params['b'],result.params['c']

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = totfun(xth,a,b,c)
    
    # Return fit results
    return(result,xth,yth)

'''
Landau function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a Landau fit,
with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Note that if "yerr" is set
                          to "P", a Poissonian error is used (i.e., yerr = sqrt(y). Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    - "low_lims"        float, the minimum value for the amplitude and FWHM parameters, expressed as a 
                          fraction of the starting point of the latter. Default value: 0.5
    - "up_lims"         float, the maximum value for the amplitude and FWHM parameters, expressed as a 
                          fraction of the starting point of the latter. Default value: 2
    
Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve  
'''

def fitter_landau(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,low_lims=0.5,up_lims=2):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit. Since this is a Landau fit, we need values for the
    # amplitude (maximum of the histogram), most probable x value (argmax of the histogram) and
    # FWHM
    start_ampli = np.max(yfit)              # Amplitude: maximum value of the data
    start_mpv = xfit[np.argmax(yfit)]       # MPV: point of maximum value of the data
    overthr = xfit[yfit >= np.max(yfit)/2]
    start_FWHM = overthr[-1]-overthr[0]
    
    # Define the boundaries for the fit parameters.
    conf = 5                                # Confidence level cut for the definition of the boundaries on MPV
    low_ampli = 0                           # Amplitude: non null
    up_ampli = start_ampli*up_lims
    low_mean = start_mpv - conf*start_FWHM  # Mean: around the experimental MPV
    up_mean = start_mpv + conf*start_FWHM
    low_fwhm = start_FWHM*low_lims          # Amplitude: around the maximum
    up_fwhm = start_FWHM*up_lims
    
    # Check that no lower boundary is equal to the upper
    if low_mean == up_mean:
        low_mean = low_mean*0.9
        up_mean = up_mean*1.1
        print(f"Lower and upper bounds for Empv were found to be equal.")
    
    # Perform fit and extract parameters. Use weights if necessary. 
    gmodel = Model(landaufun)
    gmodel.set_param_hint('ampli', min = low_ampli)
    gmodel.set_param_hint('Empv', min = low_mean, max = up_mean)
    gmodel.set_param_hint('fwhm', min = low_fwhm, max = up_fwhm)

    if yerr is not None:
        if yerr == 'P':
            yerr = np.sqrt(yvec)
            
        pesi = 1/yerr[cond]
        pesi[np.isinf(pesi)] = np.min(pesi)
        result = gmodel.fit(yfit, x=xfit, ampli = start_ampli, Empv = start_mpv, fwhm = start_FWHM,
                                                                scale_covar = False, weights = pesi)
    else:
        result = gmodel.fit(yfit, x=xfit, ampli = start_ampli, Empv = start_mpv, fwhm = start_FWHM)
    
    ampli, Empv, fwhm = result.params['ampli'], result.params['Empv'], result.params['fwhm']

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = landaufun(xth,ampli,fwhm,Empv)
    
    # Return fit results
    return(result,xth,yth)

''' 
e.m. shower function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs a 1D fit with
a model function that parametrizes the longitudinal development of the e.m. shower, with optimized starting 
points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    - "up_E0"           float, upper limit for the E0 parameter. Default value: None (meaning that
                          10*sum(y) will be used as limit).

Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve
'''


def fitter_shower(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,up_E0 = None):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit. In this parametrization of the e.m. shower
    # development, we follow the recipe provided by Rev. Mod. Phys. 75.4 (2003): 1243 (2003)
    # In other words, we estimate b as 1/2 and we use it (and the experimental depth of
    # maximum deposit) to compute the starting point for a. Finally, E0 is simply given
    # by the sum of the entire curve (i.e., the experimental total energy deposit)
    start_b = 0.5
    start_a = start_b * xfit[np.argmax(yfit)] + 1
    start_E0 = np.sum(yfit)*np.abs(xvec[1]-xvec[0])

    # Define the boundaries for the fit parameters. These are pretty much arbitrary
    # and seem to provide acceptable results, but they may be improven.
    low_a = 0           # Absolute units
    up_a = 10
    low_b = 0           # Absolute units
    up_b = 1
    low_E0 = 0.1*start_E0
    if up_E0 is None:
        up_E0 = 10*start_E0

    if low_E0 >= up_E0:
        # If the bounds are not correcly ordered, this means that E0 == 0.
        # Then, we put arbitrary bounds...
        low_E0 = start_E0 - 10
        up_E0 = start_E0 + 10
    
    # Set fit boundaries
    gmodel = Model(showerfun)
    gmodel.set_param_hint('a', min = low_a, max = up_a)
    gmodel.set_param_hint('b', min = low_b, max = up_b)
    gmodel.set_param_hint('E0', min = low_E0, max = up_E0)
    
    # Perform fit and extract parameters. Use weights if necessary
    #print(
    if yerr is not None:    
        pesi = 1/yerr[cond]
        weightcond = (np.isnan(pesi) == False) & (np.isinf(pesi) == False)
        xfit = xfit[weightcond]
        yfit = yfit[weightcond]
        pesi = pesi[weightcond]
        result = gmodel.fit(yfit, t=xfit, a=start_a, b=start_b, E0=start_E0, scale_covar = False,
                            weights = pesi,nan_policy='omit')        
    else:
        result = gmodel.fit(yfit, t=xfit, a=start_a, b=start_b, E0=start_E0,nan_policy='omit')
    
    a,b,E0 = result.params['a'],result.params['b'],result.params['E0']

    # Build thoretical x and y vector
    nptsfit = 10000
    xth = np.linspace(np.nanmin(xfit),np.nanmax(xfit),nptsfit)
    yth = showerfun(xth,a,b,E0)
    
    # Return fit results
    return(result,xth,yth)

'''
expgaussexp function automated fitter

This function takes as input a set of unidimensional data (x,y) and automatically performs an expgaussexp fit,
with optimized starting points and parameters boundaries.

Inputs:
    - "xvec"            1D-array, the x of the data to be fitted
    - "yvec"            1D-array, the y of the data to be fitted
    - "yerr"            1D-array, the uncertainities on the y of the data to be fitted. In this case,
                          the fit is performed using as weights the values 1/yerr. Otherwise, if "yerr"
                          is set to "None", an unweighted fit is performed. Note that if "yerr" is set
                          to "P", a Poissonian error is used (i.e., yerr = sqrt(y). Default value: "None"
    - "fitcut_low"      float, the minimum x for the fit. Default value: None (meaning that the min
                          of the data is used)
    - "fitcut_up"       float, the maximum x for the fit. Default value: None (meaning that the max
                          of the data is used)
    - "low_amp_coeff"   float, the minimum value for the amplitude parameter, expressed as a fractione
                          of the maximum of the y vector. Default value: 0.95
    - "up_amp_coeff"    float, the maximum value for the amplitude parameter, expressed as a fraction
                          of the maximum of the y vector. Default value: 1.05
    
Outputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve, filled with a high density
    - "yth"             1D-array, the y of the fitted curve 
'''

def fitter_expgaussexp(xvec,yvec,yerr = None,fitcut_low = None,fitcut_up = None,low_amp_coeff = 0.95,
                                                                                up_amp_coeff = 1.05):
    # Extract from the data the region to be fitted. If one of the two fit region boundaries
    # isn't defined, then ignore it.
    if (fitcut_low is not None) & (fitcut_up is not None):
        cond = (xvec >= fitcut_low) & (xvec <= fitcut_up)
    elif (fitcut_low is not None) & (fitcut_up is None):
        cond = (xvec >= fitcut_low)
    elif (fitcut_low is None) & (fitcut_up is not None):
        cond = (xvec <= fitcut_up)
    else:
        cond = (xvec == xvec)
    
    xfit = xvec[cond]
    yfit = yvec[cond]
    
    # Define the starting points for the fit. Since this is a Crystal Ball-like fit,
    # we need at least a starting point for the amplitude (N), the most probable value (MPV),
    # the standard deviation of the gaussian core (sigma) and the exponential tails.
    start_ampli = np.max(yfit)              # Amplitude: maximum value of the data
    start_mean = xfit[np.argmax(yfit)]      # MPV: point of maximum value of the data
    overthr = xfit[yfit >= np.max(yfit)/2]
    FWHM = overthr[-1]-overthr[0]
    start_std = FWHM/2.355                  # Gaussian core sigma: approx. the FWHM of the data
    start_ks = 1                            # Exponential tails: around 1, hopefully
    
    # Define the boundaries for the fit parameters.
    conf = 5    # Confidence level cut for the definition of the boundaries on MPV
    low_ampli = start_ampli*low_amp_coeff   # Amplitude: around the maximum
    up_ampli = start_ampli*up_amp_coeff
    low_mean = start_mean - conf*start_std  # Mean: around the experimental MPV
    up_mean = start_mean + conf*start_std
    low_std = 0                             # Gaussian core sigma: non negative
    low_ks = 0                              # Exponential tails factors: non negative
    
    # Check that no lower boundary is equal to the upper
    if low_mean == up_mean:
        low_mean = low_mean*0.9
        up_mean = up_mean*1.1
        print(f"Lower and upper bounds for mu were found to be equal.")
        
    # Set fit boundaries
    gmodel = Model(expgaussexp)
    gmodel.set_param_hint('N', min = low_ampli, max = up_ampli)
    gmodel.set_param_hint('mu', min = low_mean, max = up_mean)
    gmodel.set_param_hint('sigma', min = low_std)
    gmodel.set_param_hint('kL', min = low_ks)
    gmodel.set_param_hint('kH', min = low_ks)
    
    # Perform fit and extract parameters. Use weights if necessary
    if yerr is not None:
        if type(yerr) == str:
            if yerr == 'P':
                yerr = np.sqrt(yvec)
            
        pesi = 1/yerr[cond]
        pesi[np.isinf(pesi)] = np.min(pesi)
        result = gmodel.fit(yfit, x=xfit, mu=start_mean, sigma=start_std, N=start_ampli, kL=start_ks, kH=start_ks,
                                                                            scale_covar = False,weights = pesi)
        
    else:
        result = gmodel.fit(yfit, x=xfit, mu=start_mean, sigma=start_std, N=start_ampli, kL=start_ks, kH=start_ks)
    
    mu,sigma,N,kL,kH = result.params['mu'],result.params['sigma'],result.params['N'],result.params['kL'],result.params['kH']

    # Build thoretical x and y vector
    nptsfit = 100000
    xth = np.linspace(np.nanmin(xvec),np.nanmax(xvec),nptsfit)
    yth = expgaussexp(xth,mu,sigma,kL,kH,N)
    
    # Return fit results
    return(result,xth,yth)

''' 
FHWM calculator (with error propagation)

This function takes as input the fit object obtained by fitting an expgaussexp (Crystal Ball-like function)
on a dataset and returns the corresponding FWHM, as computed by the definition and using the obtained 
parameters values. The function is defined in such a way that also the error on the FWHM is obtained 
(it is computed by standard error propagation).

Inputs:
    - "result"          result-type object (lmfit), the fit report
    - "xth"             1D-array, the x of the fitted curve
    - "yth"             1D-array, the y of the fitted curve 

Outputs:
    - "trueFWHM"        float, the FWHM, as calculated by the theoretical formula
    - "errFWHM"         float, the error of the FWHM, as propagated by the errors available
    - "approxFWHM"      float, the FWHM, as calculated on the finite-vector xth
'''

def fwhm_expgaussexp(result,xth,yth):
    # First of all, extract from the fitresult object all of its parameters.
    mu = result.params['mu'].value
    sigma, sigma_err = result.params['sigma'].value, result.params['sigma'].stderr
    kL, kL_err = result.params['kL'].value, result.params['kL'].stderr
    kH, kH_err = result.params['kH'].value, result.params['kH'].stderr
    
    # Check if any uncertainity is null. If that is the case, in a totally non statistical sense but
    # definetly in a practical fashion, just take the root of the corresponding value as error.
    if sigma_err is None:
        sigma_err = np.sqrt(sigma)
        print('Exception: sigma parameter found to be without error!')
    
    if kL_err is None:
        kL_err = np.sqrt(kL)
        print('Exception: kL parameter found to be without error!')
        
    if kH_err is None:
        kH_err = np.sqrt(kH)
        print('Exception: kH parameter found to be without error!')
        
    # The half-height points may belong either in the gaussian part or in the exponential tails.
    # To determine which is the case, it is necessary to compute their value using xth and yth
    overthr = xth[yth >= np.max(yth)/2]
    x1true = overthr[0]
    x2true = overthr[-1]
    approxFWHM = x2true-x1true
    
    # Now check if the xtrues fall in the gaussian core (thus x-mu/sigma is between -kL and kH) or
    # in the exponential tails
    flag1 = ((x1true-mu)/sigma <= -kL)   # True if x1 is in the exponential tail
    flag2 = ((x2true-mu)/sigma > kH)     # True if x2 is in the exponential tail
    if flag1:
        # x1 is in the exponential tail
        x1 = mu - (sigma/kL)*(np.log(2)+kL**2/2)
    else:
        # x1 is in the gaussian core
        x1 = mu - sigma*np.sqrt(2*np.log(2))
        
    if flag2:
        # x2 is in the exponential tail
        x2 = mu + (sigma/kH)*(np.log(2)+kH**2/2)
    else:
        # x2 is in the gaussian core
        x2 = mu + sigma*np.sqrt(2*np.log(2))
        
    # Now compute the true FWHM
    trueFWHM = x2 - x1
    
    # Now apply error propagation, depending on the case. In any case, the possible contributions
    # to the uncertainity are three: one given by sigma, one by kL and one by kH
    sumterm_sigma = ((trueFWHM/sigma)*sigma_err)**2
    sumterm_kL = (sigma*(1/2 - np.log(2)/(kL**2) )*kL_err)**2
    sumterm_kH = (sigma*(1/2 - np.log(2)/(kH**2) )*kH_err)**2
    
    if flag1 & flag2:
        # Both edges are in the exponential tails: all contributions matter
        errFWHM = np.sqrt(sumterm_sigma + sumterm_kL + sumterm_kH)
    elif flag1 & (flag2 == False):
        # Only the first edge is in the exponential tail
        errFWHM = np.sqrt(sumterm_sigma + sumterm_kL)
    elif (flag1 == False) & flag2:
        # Only the second edge is in the exponential tail
        errFWHM = np.sqrt(sumterm_sigma + sumterm_kH)
    else:
        # Both edges are in the gaussian tail
        errFWHM = np.sqrt(sumterm_sigma)
        
    # Finally, return the obtained values
    return(trueFWHM,errFWHM,approxFWHM)






























# ===========================================================================================================
#                                      FUNCTIONS FOR WAVEFORM ANALYSIS
# ===========================================================================================================
# These functions are specifically used for the analysis and processing of the waveforms produced by an
# analog particle detector, such as a PMT, a SiPM or an MCP-PMT. They can compute the waveforms integrals,
# baselines, TOT, etc. They can also show the waveforms in a certain fashion.

'''
2D Waveform plotting

This functions applies a standardized scheme for showing several waveforms at once, in a single plot.
The plot is a sort of "intensity map": the x axis shows the time, the y shows the signal and the color (z axis)
shows the number of times that the given value was recorded.

Inputs: 
    - "ax"          Axes-type object, the set of axis on which the histogram will be shown
    - "wavemat"     2D-array of shape (N,M), the waveforms to be plotted. By convention, each row of the 
                      array is considered as a different event (thus there are N events) and each column
                      is a point in the waveform
    - "basevec"     1D-array of shape (N,), the baselines of the signals. It can also be an integer number,
                      representing how many points at the beginning of each waveform are used to compute
                      the baselines. Default value: 100
    - "nev"         int, number of events to display simultaneously. Default value: 1000
    - "timestep"    float, the binning step for showing the time in the intensity map. Default value: 1
    - "sigstep"     float, the binning step for showing the signal in the intensity map. Default value: 10
    - "ifbase"      bool, whether to subtract the baselines from the signals or not. Default value: True
    - "ifpol"       bool, whether to invert the polarity of the signals or not. Default value: True
    - "colmap"      string, the name of the colormap to be used. Default: "jet".
    - "slope"       float, the point of change for the TwoSlopeNorm option. Default value: None (i.e., it isn't used)
    - "iflog"       bool, whether to use a logarithmic scale for the z (color) axis. Default value: False.
   
There are no outputs (the function just renders the requested plot)    
'''

def wvfplotter2D(ax,wavemat,baselines=100,nev = 1000,timestep = 1,sigstep = 10, ifbase = True, ifpol = True, 
                 colmap = 'jet', slope = 1, textfont = 10, iflog = True):
    # Compute the number of points in each waveform and create the vector representing the "time",
    # namely the x for the x-y plot of a single waveform
    wvfdim = wavemat.shape[1]
    timevec = np.arange(wvfdim)

    # Compute the baselines, if they are not given by the user
    if np.shape(basevec) == ():
        # Case 1: "baselines" contain a numerical value. In this case, compute the baselines
        # manually
        basevec = np.mean(wavemat[:,:baselines],axis = 1)
    else:
        # Case 2: "baselines" is a vector. Use it for the baselines
        basevec = baselines
    
    # Cast the waveforms in float32 and subtract the baselines, if required
    floatwvf = np.float32(wavemat[:nev,:])
    if ifbase:
        floatbase = np.float32(basevec[:nev])
        sipmwfs = floatwvf-np.transpose(np.tile(floatbase,(floatwvf.shape[1],1)))
    else:
        sipmwfs = floatwvf

    # Invert the polarity, if required
    if ifpol:
        sipmwfs = -sipmwfs
        
    # Create binning vectors for the temporal axis and the signal axis
    timebin = np.arange(-0.5*timestep,wvfdim+0.5*timestep,timestep)
    sigbin = np.arange(np.min(sipmwfs)-0.5*sigstep,np.max(sipmwfs)+1.5*sigstep,sigstep)
    
    # Create the 2D-array which will be filled with all the signal values coming from all
    # the waveforms (i.e., a waveform intensity map)
    histosum = np.zeros((wvfdim,np.shape(truebins(sigbin))[0]))
    
    for k in range(nev):
        # Create the 2D histogram of the signal values vs time values, for the k-th event
        histo2D, _, _ = truehisto2D(timevec,sipmwfs[k,:],binsX = timebin, binsY = sigbin)

        # Add the histogram to the intensity map
        histosum += histo2D
        
    # Show the intensity map
    histoplotter2D(ax,timevec,sigbin,histosum,colmap,slope,'Time','Signal',textfont, iflog = iflog)
    return()


''' 
TOT calculator

This function can be used to compute the Time-Over-Threshold of a set of signal waveforms.

Inputs:
    - "wave"        2D-array of shape (N,M), the waveforms to be analyzed. By convention, each row of the 
                      array is considered as a different event (thus there are N events) and each column
                      is a point in the waveform
    - "baselines"   1D-array of shape (N,), the baselines of the signals. It can also be an integer number,
                      representing how many points at the beginning of each waveform are used to compute
                      the baselines. It can also be "None", if the baselines should not be subtracted. 
                      Default value: "None"
    - "threshold"   float, the value used for computing the Time-Over-Threshold. Default value: 100
    - "ifneg"       bool, whether to invert the polarity of the signals or not. Default value: True
    - "typecast"    string, the format in which the waveforms will be casted during the processing.
                      Note that, if the baselines are subtracted, this format should allow for floating-point
                      precision (either at 16, 32 or 64 bits) and avoid roll-over effects. Default value: "float32"

Outputs:
    - "subwave"     2D-array of shape (N,M), the analyzed waveforms, with baseline eventually subtracted
                      and polarity eventually reversed
    - "oversum"     1D-array of shape (N,), the TOT values
    - "max_inds"    1D-array of shape (N,), the instants in time where the interval Over the Threshold begins
                      in each event
    - "min_inds"    1D-array of shape (N,), the instants in time where the interval Over the Threshold end
                      in each event
'''

def tot_calculator(wave,baselines=None,threshold=100,ifneg=True,typecast='float32'):
    # Compute the number of rows and columns of the input matrix
    nrows = np.shape(wave)[0]
    ncols = np.shape(wave)[1]
    
    # Cast the waveforms and subtract the baselines, if required
    cleanwave = wave.astype(typecast)
        
    if baselines is not None:
        # Case 1: subtract the baselines. Consider the different possibilities for the calculation
        # of the latter
        if np.shape(baselines) == ():
            # Case 1.A: compute the baselines as averages of the signals, in a given interval
            basevec = np.mean(cleanwave[:,:baselines],axis = 1)
        else:
            # Case 1.B: user-given baselines
            basevec = baselines

        # Subtraction
        subwave = np.zeros_like(cleanwave)
        for i in range(nrows):
            # Note that here we subtract the baseline on an event by event basis and not with a vectorialized
            # technique because a proper subtraction would require the creation of a "baseline matrix" of shape
            # nrows x ncols, which is definetly not memory-friendly. 
            subwave[i,:] = cleanwave[i,:]-basevec[i]
    else:
        # Case 2: no subtraction is needed
        subwave = cleanwave
        
    # Invert the polarity, if required.
    if ifneg:
        subwave = -subwave
    
    # Compute the TOT as the number of points over a given threshold. Note that we consider ALL the points
    # over the threshold, even if they are not in a single interval. The threshold should be set accordingly.
    # (non troppo basso, altrimenti triggero sul rumore)
    overthr = (subwave>threshold)
    oversum = overthr.sum(axis = 1)
    
    # Compute the indexes corresponding to the beginning and ending of the Over Threshold intervals.
    # To do so, cast the overthr array in integer: it will be 1 anywhere the signal is above threshold
    # and 0 anywhere else. Ideally each row of this matrix should be a square wave, and thus its derivative
    # should be 0 everywhere except at the beginning of the square part (where it is equal to +1) and at 
    # the ending (where it is equal to -1). Thus, it is reasonable to consider as beginning and ending of
    # the over-threshold interval, respectively, the argmax and argmin of the derivative of overthr.
    overthr_int = overthr.astype('int')            # Boolean matrix cast in 0/1
    overthr_diff = np.diff(overthr_int,axis = 1)   # Derivatives of the signals
        
    max_inds = np.argmax(overthr_diff,axis = 1)+1  # TOT interval beginnings (+1 due to the diff operation)
    min_inds = np.argmin(overthr_diff,axis = 1)+1  # TOT interval endings (+1 for the same reason as above)
    
    # Return the outputs
    return(subwave,oversum,max_inds,min_inds)


'''
Integral calculator

This function can be used to compute the integral of a set of signal waveforms.

Inputs:
    - "wave"        2D-array of shape (N,M), the waveforms to be analyzed. By convention, each row of the 
                      array is considered as a different event (thus there are N events) and each column
                      is a point in the waveform
    - "baselines"   1D-array of shape (N,), the baselines of the signals. It can also be an integer number,
                      representing how many points at the beginning of each waveform are used to compute
                      the baselines. It can also be "None", if the baselines should not be subtracted. 
                      Default value: "None"
    - "ifneg"       bool, whether to invert the polarity of the signals or not. Default value: True
    - "ifonlypos"   bool, whether to consider only the positive points in the calculation of the integrals,
                      or all the points. Default value: True
    - "typecast"    string, the format in which the waveforms will be casted during the processing.
                      Note that, if the baselines are subtracted, this format should allow for floating-point
                      precision (either at 16, 32 or 64 bits). Default value: "float32"

Outputs:
    - "subwave"     2D-array of shape (N,M), the analyzed waveforms, with baseline eventually subtracted
                      and polarity eventually reversed
    - "maskwave"    2D-array of shape (N,M). the analyzed waveforms, with the negative values set to zero
    - "integral"    1D-array of shape (N,), the integrals of the waveforms
'''
def integral_calculator(wave,baselines=None,ifneg=True,ifonlypos=True,typecast='float32'):
    # Compute the number of rows and columns of the waveforms matrix.
    nrows = np.shape(wave)[0]
    ncols = np.shape(wave)[1]
    
    # Cast the waveforms and subtract the baselines, if required
    cleanwave = wave.astype(typecast)
        
    if baselines is not None:
        # Case 1: subtract the baselines. Consider the different possibilities for the calculation
        # of the latter
        if np.shape(baselines) == ():
            # Case 1.A: compute the baselines as averages of the signals, in a given interval
            basevec = np.mean(cleanwave[:,:baselines],axis = 1)
        else:
            # Case 1.B: user-given baselines
            basevec = baselines

        # Subtraction
        subwave = np.zeros_like(cleanwave)
        for i in range(nrows):
            # Note that here we subtract the baseline on an event by event basis and not with a vectorialized
            # technique because a proper subtraction would require the creation of a "baseline matrix" of shape
            # nrows x ncols, which is definetly not memory-friendly. 
            subwave[i,:] = cleanwave[i,:]-basevec[i]
    else:
        # Case 2: no subtraction is needed
        subwave = cleanwave

    # Invert the polarity, if required.
    if ifneg:
        subwave = -subwave
    
    # Generate a second matrix, similar to "cleanwave" but with the negative values set to zero.
    maskwave = copy.deepcopy(subwave)
    maskwave[maskwave < 0] = 0

    # Compute the integrals
    if ifonlypos:
        integral = np.sum(maskwave,axis=1)
    else:
        integral = np.sum(subwave,axis=1)
    
    # Return the required outputs
    return(subwave,maskwave,integral)






























# ===========================================================================================================
#                                      FUNCTIONS FOR BEAMTESTS
# ===========================================================================================================
# These functions are specifically used in the online and offline analysis of beamtest data.
# As such, they are largely employed by the scripts in the repository at https://gitlab.com/piemmegi/tbtools/

''' 
Data file name definition

This function simply takes as input the number of a data run of interest and return the corresponding 
correctly-formatted name. In other words, this function defines the header and trailer of the data runs.

Inputs:
    - "run"             int, the run of interest
    - "datafiletype"    str, the extension of the file with no dot (e.g., "npz", not ".npz")
    
Outputs:
    - "filepath"        str, the path of the run of interest
'''
def pathnamedef(prename,run,datafiletype='h5py'):
    # Format the string as requested.
    filepath = prename + str(run) + '.' + datafiletype

    # Return the output
    return(filepath)

'''
Data file extraction

This function takes as input the path of a file containing numeric data, formatted in a known way.
It can be either .npz, .h5 or .root (ASCII will be supported in a later release of this code).
Depending on the structure of the file, single columns may be extracted in two ways:
    1. If the file is dictionary-like, the columns that will be extracted must be given as their names (str)
    2. If the file is matrix-like, the columns that will be extracted must be given as their indexes (int)
If the file has a global argument-content (e.g., a ROOT file with tree 't' and subtrees 'xRaw' etc), 
it should be passed explicitly.

Inputs:
    "data"          dict, the correspondence between str-type keys and column indexes
    "filepath"      str, the location of the file of interest
    "keys"          list, the keys or columns of interest
    "datafiletype"  str, the data file type ('npz','h5','root','ascii')
    "globaltree"    str, the global key to be used in the extraction of the files. Default value:
                      None, meaning that no key is present
    "ifmatrix"      bool, if the data are formatted as a huge and unique matrix or are unpacked as
                      submatrixes with key-value naming system. Default value: False.
    "inds"          list, first and last events to be loaded. Default value: [0,None], meaning that
                      all the events are loaded
    
Output:
    "outputdata"    list, the data column requested
'''

def dataupload(data,filepath,keys,datafiletype,globaltree=None,ifmatrix=False,inds=[0,None]):
    # Load the requested file with the correct method. If necessary, apply a global-keyword 
    # extraction before the true extraction. In the extraction, remember that data are loaded 
    # as dictionaries and then extrated with key arguments, but keys may be formatted as strings 
    # (keys of dictionaries) or integers (IDs of columns)
    outputdata = []
    
    if datafiletype == 'npz':          # NUMPY
        # Load file
        if globaltree is not None:
            datafile = np.load(filepath)[globaltree]
        else:
            datafile = np.load(filepath)
        
        # From the input file, extract the requested columns
        for i,ith_key in enumerate(keys):
            if not ifmatrix:
                # Keys are strings: the loaded file is treated like a dictionary containing all the data vectors
                outputdata.append(datafile[ith_key][inds[0]:inds[-1]])  
            else:
                # Keys are integers: the loaded file is treated as a big matrix. The integers are deduced from "data"
                outputdata.append(data[inds[0]:inds[-1],ith_key])
        
        # Close the file
        datafile.close()
        
    elif datafiletype == 'h5':         # H5
        # Load file
        if globaltree is not None:
            datafile = h5py.File(filepath,'r',libver='latest',swmr=True)[globaltree]
        else:
            datafile = h5py.File(filepath,'r',libver='latest',swmr=True)
            
        # From the input file, extract the requested columns
        for i,ith_key in enumerate(keys):        
            # Keys may be formatted as strings (keys of dictionaries) or integers (IDs of columns)
            if not ifmatrix:
                # Keys are strings: the loaded file is treated like a dictionary containing all 
                # the data vectors
                outputdata.append(datafile[ith_key][inds[0]:inds[-1]])  
            else:
                # Keys are integers: the loaded file is treated as a big matrix. The integers are 
                # deduced from "data"
                outputdata.append(data[inds[0]:inds[-1],ith_key])
            
        # Close the file
        datafile.close()
    
    elif datafiletype == 'root':       # ROOT
        # Load file 
        if globaltree is not None:
            datafile = uproot.open(filepath)[globaltree]
        else:
            datafile = uproot.open(filepath)
            
        # From the input file, extract the requested columns
        for i,ith_key in enumerate(keys):
            if not ifmatrix:
                # Keys are strings: the loaded file is treated like a dictionary containing 
                # all the data vectors
                outputdata.append(np.array(datafile[ith_key].array(entry_start = inds[0], entry_stop = inds[-1])))
            else:
                # Keys are integers: the loaded file is treated as a big matrix. The integers 
                # are deduced from "data"
                outputdata.append(np.array(data[inds[0]:inds[-1],ith_key]))
            
        # Close the file
        datafile.close()
        
    elif datafiletype == 'ascii':      # ASCII (you would think)
        print('The ASCII format has not been implemented yet.')
    else:
        print('File format not supported.')
        
    # Return the outputs
    if len(outputdata) > 1:
        return(outputdata)
    else:
        return(outputdata[0])


''' 
Position extrapolator

This function takes as input two sets of (x,y) positions measured by two silicon detectors, 
namely (x1,y1) and (x2,y2), and extrapolates them to a given position in space (z).

Inputs:
    - "ch_pos"      2D-array of shape (N,4), containing x1, y1, x2 and y2
    - "zs1s2"       float, distance in z between the detectors [in cm]
    - "zs2obj"      float, distance in z between the second detector and the target [in cm]
                      (default value: 100)
    - "ifreturndiv" bool, whether to return from the function the particles incidence angles
                      (default value: "False")
    - "ifreturndiv" bool, whether to return from the function the particles hit positions
                      (default value: "True")
    - "divunit"     str, the unit of measurement of the divergence, if returned ("mrad", "urad")
                      (default value: "mrad")

Outputs:
    - "out_pos"     2D-array, the extrapolated positions in (x,y)
    - "out_angle"   2D-array (if required), the incidence angles in (x,y)
'''

def siliextrapolator(ch_pos,zs1s2,zs2obj=100,ifreturndiv=False,ifreturnpos=True,divunit='mrad'):
    # Define the variables to be filled in the iteration
    out_pos = np.zeros((np.shape(ch_pos)[0],2))
    out_angle = np.zeros_like(out_pos)

    for j in range(2):
        # j points to 0 for the x axis and to 1 for the y axis. In both cases, extract the 
        # corresponding hit positions
        refind = j % 2
        cleanpos_a = ch_pos[:,refind]
        cleanpos_b = ch_pos[:,refind+2]
        
        # Reconstruct the particles trajectories (as straight lines: x,y = m*z+q)
        m = (cleanpos_b - cleanpos_a)/zs1s2
        q = cleanpos_b - m*zs1s2
        
        # Compute the incidence angle and the hit position at the desired value of z
        out_pos[:,j] = m*(zs1s2+zs2obj)+q
        out_angle[:,j] = np.arctan(m)
    
    # Rescale the output angles
    if (divunit == 'mrad'):
        scalefac = 1e3
    elif (divunit == 'urad'):
        scalefac = 1e6
    else:
        scalefac = 1

    out_angle = out_angle * scalefac

    # Return the outputs
    if ifreturnpos & ifreturndiv:
        return(out_pos,out_angle)
    elif ifreturnpos & (ifreturndiv == False):
        return(out_pos)
    else:
        return(out_angle)

'''
Cut configuration retriever (cut type)

This function is required for the operation of the beamtest online monitor.
In particular, it is used to determine which type of cuts should be applied in the
processing of the beamtest data.

Inputs:
    - "filepath"        str, the path where the file is located

Output: a single dict, with the following key-value pairs
    - "ifPH"            bool, whether to apply a cut on the PH of some reference channel
    - "ifPT"            bool, whether to apply a cut on the PT of some reference channel
    - "ifPTD"           bool, whether to apply a cut on the PTD w.r.t some reference channel
    - "iftheta_in"      bool, whether to apply a cut on the incidence angles
    - "ifhitpos"        bool, whether to apply a cut on the hit positions at a given distance in z
    - "ifevnum"         bool, whether to apply a cut on the event number
'''

def cutretriever_whichones(filepath):
    # Open the input file
    with open(filepath,'r') as file:
        # Read every line of the file. They are formatted always as 
        # "ifCUT = $bool". Deduce from this formatting what is the bool
        # value to assign to the variable
        allbools = []
        for i,el in enumerate(file.readlines()):
            thisbool = (el.split('=')[-1].strip() == 'True')
            allbools.append(thisbool)

        # Extract the boolean values following the format of the file
        # (i.e., the order of insertion of the "if" variables)
        ifPH, ifPT, ifPTD, iftheta_in, ifhitpos, ifevnum = allbools

    # Build the output dictionary
    outputdict = {"ifPH":       ifPH,
                  "ifPT":       ifPT,
                  "ifPTD":      ifPTD,
                  "iftheta_in": iftheta_in,
                  "ifhitpos":   ifhitpos,
                  "ifevnum":    ifevnum}

    # Return the output
    return(outputdict)

'''
Cut configuration retriever (cut values)

This function is required for the operation of the beamtest online monitor.
In particular, it is used to determine the thresholds and reference channels
for the application of the cuts defined in "cutretriever_whichones".

Inputs:
    - "filepath"            str, the path where the file is located

Output: a single dict, with the following key-value pairs
    - "refPHchs"            list of int, the channels used for the PH cut
    - "PHcut_values"        list of int, the thresholds for the PH cut, sorted as
                              [min_ch1, max_ch1, min_ch2, max_ch2, ...]
    - "refPTchs"            list of int, the channels used for the PT cut
    - "PTcut_values"        list of int, the thresholds for the PT cut, sorted as
                              [min_ch1, max_ch1, min_ch2, max_ch2, ...]
    - "refPTDchs"           list of int, the channels used for the PTD cut
    - "PTDcut_values"       list of int, the thresholds for the PTD cut, sorted as
                              [min_ch1, max_ch1, min_ch2, max_ch2, ...]
    - "theta_in_values"     list of float, the thresholds for the incidence angles cut,
                              sorted as [min_x,max_x,min_y,max_y]
    - "hitpos_dist"         float, the threshold for the hit position cut in cm
    - "hitpos_values"       list of float, the thresholds for the hit position cut in cm,
                              sorted as [min_x,max_x,min_y,max_y]
    - "evnum_values"        list of int, the thresholds for the event number cut,
                              sorted as [min_event, max_event]
'''

def cutretriever_whichvalues(filepath):
    # Open the input file
    with open(filepath,'r') as file:
        # Read the file lines one at a time. The format of the lines is not 
        # homogeneous, so each one should be treated differently. 

        # The first 6 lines include the reference channels and thresholds
        # for the PH, PT and PTD cut. Each line is a list of integers,
        # so they can be extracted in a cycle.
        all_els = []
        numreads = 6
        for i in range(numreads):
            thisel = file.readline().split('=')[-1].strip()

            if thisel == "None":
                thisvalue = None
            else:
                thisvalue = str2list(thisel,int)

            all_els.append(thisvalue)

        refPHchs, PHcut_values, refPTchs, PTcut_values, refPTDchs, PTDcut_values = all_els

        # The values for the divergence cut are formatted as a line with a list of float.
        # The values for the hit position cut are formatted as a first line with 
        # only a float value (the distance in z) and then a line with a list of float.
        # Extracting them should not require a cycle.
        thisel = file.readline().split('=')[-1].strip()          # Incidence angle cut
        if thisel == "None":
            theta_in_values = None
        else:
            theta_in_values = str2list(thisel,float)

        thisel = file.readline().split('=')[-1].strip()          # Hit position distance
        if thisel == "None":
            hitpos_dist = None
        else:
            hitpos_dist = float(thisel)

        thisel = file.readline().split('=')[-1].strip()          # Hit position cut
        if thisel == "None":
            hitpos_values = None
        else:
            hitpos_values = str2list(thisel,float)

        # The values for the event cut is formatted as a list of int. 
        # Extracting them should not require a cycle.
        thisel = file.readline().split('=')[-1].strip()
        if thisel == "None":
            evnum_values = None
        else:
            evnum_values = str2list(thisel,float)

    # Build the output dictionary
    outputdict = {"refPHchs":          refPHchs,
                  "PHcut_values":      PHcut_values,
                  "refPTchs":          refPTchs,
                  "PTcut_values":      PTcut_values,
                  "refPTDchs":         refPTDchs,
                  "PTDcut_values":     PTDcut_values,
                  "theta_in_values":   theta_in_values,
                  "hitpos_dist":       hitpos_dist,
                  "hitpos_values":     hitpos_values,
                  "evnum_values":      evnum_values}

    # Return the output
    return(outputdict)
    

'''
Cut condition calculator

This function is used to build the boolean vector determining which events pass all
the selection cut defined with the previous functions.

Inputs:
    - "digich"              int, channel of the digitizer used for the analysis. It is the first
                              in the calculation of the PTDs.
    - "digiPH"              1D-array of shape (N,M), the PH of all the channels of the digitizers
    - "digitime"            1D-array of shape (N,M), the PH of all the channels of the digitizers
    - "ch_pos"              1D-array of shape (N,4), the hit positions in x and y of the trackers
    - "distances_zs1s2"     float, the distance between the trackers (in cm)
    - "whichcuts_path"      str, path where the the txt file containing which cuts should be applied
                              is located.
    - "whichvalues_path"    str, path where the the txt file containing the threshold for the cuts 
                              that should be applied is located.
'''

def cutcalculator(digich,digiPH,digitime,ch_pos,distances_zs1s2,whichcuts_path,whichvalues_path):
    # First of all, use the custom functions to open the two txt files and determine:
    #   - Which cuts must be applied
    #   - Within what thresholds and reference channels
    outputdict_whichcuts = cutretriever_whichones(whichcuts_path)
    outputdict_whichvalues = cutretriever_whichvalues(whichvalues_path)

    # Compute the number of events
    nevents = np.shape(digiPH)[0]

    # ==========================================================================================
    #                                       PH CUT
    # ==========================================================================================
    # Construct the PH cut boolean vector. Use the thresholds and channels provided, if everything
    # is correctly set. Otherwise, construct a dummy cut.
    ifPH = outputdict_whichcuts["ifPH"]
    refPHchs = outputdict_whichvalues["refPHchs"]
    PHcut_values = outputdict_whichvalues["PHcut_values"]

    if ifPH & (refPHchs is not None) & (PHcut_values is not None):
        # Case 1: the cut is well defined. Iterate over every reference channel and determine the
        # events passing the cut on the latter
        PH_cutcondmap = np.zeros((nevents,len(refPHchs)))

        for i,el in enumerate(refPHchs):
            PH_cutcondmap[:,i] = (digiPH[:,el] >= PHcut_values[2*i]) & (digiPH[:,el] <= PHcut_values[2*i+1])

        # Merge the cut on all the channels (in "and")
        PH_condcomp = PH_cutcondmap.prod(axis=1)
        PH_cutcond = (PH_condcomp == 1)
    else:
        # Case 2: no cut must be applied (either because that was the intention, or because
        # a cut was badly defined)
        PH_cutcond = np.ones((nevents,),dtype=bool)

    # ==========================================================================================
    #                                       PT CUT
    # ==========================================================================================
    # Construct the PT cut boolean vector. Use the thresholds and channels provided, if everything
    # is correctly set. Otherwise, construct a dummy cut.
    ifPT = outputdict_whichcuts["ifPT"]
    refPTchs = outputdict_whichvalues["refPTchs"]
    PTcut_values = outputdict_whichvalues["PTcut_values"]

    if ifPT & (refPTchs is not None) & (PTcut_values is not None):
        # Case 1: the cut is well defined. Iterate over every reference channel and determine the
        # events passing the cut on the latter
        PT_cutcondmap = np.zeros((nevents,len(refPTchs)))

        for i,el in enumerate(refPTchs):
            PT_cutcondmap[:,i] = (digitime[:,el] >= PTcut_values[2*i]) & (digitime[:,el] <= PTcut_values[2*i+1])

        # Merge the cut on all the channels (in "and")
        PT_condcomp = PT_cutcondmap.prod(axis=1)
        PT_cutcond = (PT_condcomp == 1)
    else:
        # Case 2: no cut must be applied (either because that was the intention, or because
        # a cut was badly defined)
        PT_cutcond = np.ones((nevents,),dtype=bool)

    # ==========================================================================================
    #                                       PTD CUT
    # ==========================================================================================
    # Construct the PTD cut boolean vector. Use the thresholds and channels provided, if everything
    # is correctly set. Otherwise, construct a dummy cut.
    ifPTD = outputdict_whichcuts["ifPTD"]
    refPTDchs = outputdict_whichvalues["refPTDchs"]
    PTDcut_values = outputdict_whichvalues["PTDcut_values"]

    if ifPTD & (refPTDchs is not None) & (PTDcut_values is not None):
        # Case 1: the cut is well defined. Iterate over every reference channel and determine the
        # events passing the cut on the latter
        PTD_cutcondmap = np.zeros((nevents,len(refPTDchs)))

        for i,el in enumerate(refPTDchs):
            PTDiff = digitime[:,digich] - digitime[:,el]    # Differenza dei PT
            PTD_cutcondmap[:,i] = (PTDiff >= PTDcut_values[2*i]) & (PTDiff <= PTDcut_values[2*i+1])

        # Merge the cut on all the channels (in "and")
        PTD_condcomp = PTD_cutcondmap.prod(axis=1)
        PTD_cutcond = (PTD_condcomp == 1)
    else:
        # Case 2: no cut must be applied (either because that was the intention, or because
        # a cut was badly defined)
        PTD_cutcond = np.ones((nevents,),dtype=bool)

    # ==========================================================================================
    #                                  INCIDENCE ANGLE CUT
    # ==========================================================================================
    # Construct the incidence angle cut boolean vector. Use the thresholds provided, if everything
    # is correctly set. Otherwise, construct a dummy cut.
    iftheta_in = outputdict_whichcuts["iftheta_in"]
    theta_in_values = outputdict_whichvalues["theta_in_values"]

    if iftheta_in & (theta_in_values is not None):
        # Case 1: the cut is well defined. Track the particles and determine the incidence angles.
        out_angle = siliextrapolator(ch_pos,distances_zs1s2,ifreturndiv=True,ifreturnpos=False,divunit='mrad')

        # Determine the cut condition separately for x and y
        theta_in_cutcond_x = (out_angle[:,0] >= theta_in_values[0]) & (out_angle[:,0] <= theta_in_values[1])
        theta_in_cutcond_y = (out_angle[:,1] >= theta_in_values[2]) & (out_angle[:,1] <= theta_in_values[3])

        # Merge the x and y cuts (in "and")
        theta_in_cutcond = theta_in_cutcond_x & theta_in_cutcond_y
    else:
        # Case 2: no cut must be applied (either because that was the intention, or because
        # a cut was badly defined)
        theta_in_cutcond = np.ones((nevents,),dtype=bool)

    # ==========================================================================================
    #                                    HIT POSITION CUT
    # ==========================================================================================
    # Construct the hit positions cut boolean vector. Use the thresholds provided, if everything
    # is correctly set. Otherwise, construct a dummy cut.
    ifhitpos = outputdict_whichcuts["ifhitpos"]
    hitpos_dist = outputdict_whichvalues["hitpos_dist"]
    hitpos_values = outputdict_whichvalues["hitpos_values"]

    if ifhitpos & (hitpos_dist is not None) & (hitpos_values is not None):
        # Case 1: the cut is well defined. Track the particles and determine the hit positions
        # at the given depth.
        out_pos = siliextrapolator(ch_pos,distances_zs1s2,hitpos_dist,ifreturndiv=False,ifreturnpos=True)

        # Determine the cut condition separately for x and y
        hitpos_cutcond_x = (out_pos[:,0] >= hitpos_values[0]) & (out_pos[:,0] <= hitpos_values[1])
        hitpos_cutcond_y = (out_pos[:,1] >= hitpos_values[2]) & (out_pos[:,1] <= hitpos_values[3])

        # Merge the x and y cuts (in "and")
        hitpos_cutcond = hitpos_cutcond_x & hitpos_cutcond_y
    else:
        # Case 2: no cut must be applied (either because that was the intention, or because
        # a cut was badly defined)
        hitpos_cutcond = np.ones((nevents,),dtype=bool)

    # ==========================================================================================
    #                                     EVENT NUMBER CUT
    # ==========================================================================================
    # Construct the event number cut boolean vector. Use the thresholds provided, if everything
    # is correctly set. Otherwise, construct a dummy cut.
    ifevnum = outputdict_whichcuts["ifevnum"]
    evnum_values = outputdict_whichvalues["evnum_values"]

    if ifevnum & (evnum_values is not None):
        # Case 1: the cut is well defined.
        evnum_cutcond = (eventNumber >= evnum_values[0]) & (evnum_values <= evnum_values[1])
    else:
        # Case 2: no cut must be applied (either because that was the intention, or because
        # a cut was badly defined)
        evnum_cutcond = np.ones((nevents,),dtype=bool)

    # ==========================================================================================
    #                                        GLOBAL CUT
    # ==========================================================================================
    # Merge all the cuts in a single condition vector
    globalcut = PH_cutcond & PT_cutcond & PTD_cutcond & theta_in_cutcond & hitpos_cutcond & evnum_cutcond

    # Compute the number of events passing each cut separately
    cutoutputdict = {"Number of events":                       nevents,
                     "Events passing PH cut":                  np.sum(PH_cutcond),
                     "Events passing PT cut":                  np.sum(PT_cutcond),
                     "Events passing PTD cut":                 np.sum(PTD_cutcond),
                     "Events passing incidence angle cut":     np.sum(theta_in_cutcond),
                     "Events passing hit position cut":        np.sum(hitpos_cutcond),
                     "Events passing event number cut":        np.sum(evnum_cutcond)}

    # Print the number of events passing each cut separately
    print(f"")
    print(f"*"*25)
    print(f"Data from the cutcalculator function: \n")
    for key in cutoutputdict.keys():
        print(f"{key}: {cutoutputdict[key]}")
    print(f"*"*25)
        
    # Return the outputs
    return(globalcut,cutoutputdict)






























# ===========================================================================================================
#                                       MACHINE LEARNING FUNCTIONS
# ===========================================================================================================
# Here is a set of utilities for the training and testing of Machine Learning algorithms

''' 
Construction of a generic Classifier

This function takes as input a type of classifier and its hyperparameters and returns the constructor 
of the classifier, with the hyperparameters properly set.

Inputs:
    - "Algorithm"           str, the type of algorithm which must be implemented. Currently accepted:
                            "rf" for the Random Forest, "nn" for the Neural Network, "HGBC" for Gradient
                            Boosting with the Histogram approach.
    - "HyperparamDict"      dict, contains the key-value pairs which must be used to generate the classifier.
    - "random_state"        int, the inizialization value for the random state of the algorithm
                              Default value: 0 (an arbitrary value)
    - "n_jobs"              int, the number of cores to be used in the processing. If it is set to -1,
                              Can be also "-1", if all the cores should be used. Default: -1.
    
Outputs:
    - "Classifier"          Classifier-type object, the chosen classifier
'''
def ClassifierConstructor(Algorithm='rf',HyperparamDict=None,random_state=0,n_jobs=-1):
    # Check if the algorithm chosen is available
    GoodAlgorithms = ['rf','nn','HGBC']

    if (Algorithm not in GoodAlgorithms):
        print(f"Unidentified classifier algorithm: falling back on Random Forest ('rf') ...")
        Algorithm = 'rf'

    # Depending on the algorithm, instantiate a different classifier
    if (Algorithm == 'rf'):
        # Random Forest classifier.
        Classifier = ensemble.RandomForestClassifier(random_state=random_state,
                                                     n_jobs=n_jobs,
                                                     **HyperparamDict)
    elif (Algorithm == 'nn'):
        # Neural Network (FF MLP). Note: here the n_jobs parameter does not exist.
        Classifier = neural_network.MLPClassifier(random_state=random_state,
                                                  **HyperparamDict)
    elif (Algorithm == 'HGBC'):
        # Histogram-based Gradient-Boosting tree Classifier. Note: here the n_jobs parameter does
        # not exist.
        Classifier = ensemble.HistGradientBoostingClassifier(random_state=random_state,
                                                             **HyperparamDict)


    else:
        # Escape flag.
        Classifier = None

    # Return the output
    return(Classifier)